<!--#include file="../includes/clsBanco.inc"-->
<!--#include file="../includes/funcoes.inc"-->
<%
Session.LCID = 1046 'Pt-Br'
Response.Expires= 0
Response.AddHeader "PRAGMA", "NO-CACHE"
Response.Charset = "iso-8859-1"

'Conex�o
'on error resume next
Dim gConexao
AbrirConexao

'Fun��es
function f_add_naoconformidade(pnconNumero, pprojId)
'on error goto erro_exec
	chamada = "f_add_naoconformidade(" & pnconNumero & ", "& pprojId & ")"
	retorno = ""
	if ((trim(pnconNumero)="") or (not isnumeric(trim(pnconNumero)))) then
		retorno = NovoErroXML("00000001", "N�mero da N�o-Conformidade inv�lido", chamada)
	elseif ((trim(pprojId)="") or (not isnumeric(trim(pprojId)))) then
		retorno = NovoErroXML("00000002", "Projeto n�o identificado", chamada)
	else
		lsql = lsql & "select n.nconId "
		lsql = lsql & "  from NaoConformidade n "
		lsql = lsql & " where n.nconNumero = " & pnconNumero
		lsql = lsql & "   and n.projId     = " & pprojId
		set lRS_1 = gConexao.execute(lsql)
		If(not lRS_1.BOF) and (Not lRS_1.EOF) then
			retorno = lRS_1("nconId")
		end if
		lRS_1.Close	
		
		if(retorno="")then 'N�o existe ainda a n�o-conformidade apresentada, sendo assim, a criaremos
			retorno = pCad_NovoId("NaoConformidade", gConexao)
			lsql = lsql & "insert into naoconformidade (nconId, projId, nconNumero) "
			lsql = lsql & "values (" & retorno & ", "& pprojId & ", " & pnconNumero & ") "
			gConexao.execute(lsql)
		end if
	end if
	f_add_naoconformidade = retorno
'	exit function
'erro_exec:
'	f_add_naoconformidade = NovoErroXML("[" & err.number & "] " & err.description, chamada)
end function

function f_cria_rece(preceNotaFiscal, ppedcId, pprojId, pnconNumero, pusuaIdCad, pOPCAO)
'on error goto erro_exec
    chamada = "f_cria_rece(" & preceNotaFiscal & ", " & ppedcId & ", " & pprojId & ", " & pnconNumero & ", " & pusuaIdCad & ", " & pOPCAO & ")"
	retorno = "" 
	if(trim(preceNotaFiscal)="") then
		retorno = NovoErroXML("00000004", "Nota fiscal inv�lida", chamada)
	else 
		if ((trim(pprojId)="") or (trim(pprojId)="0")) then
			retorno = NovoErroXML("00000006", "Projeto n�o identificado", chamada)
		else
			lnconId="0" 'N�o-Conformidade
			if (pOPCAO<>"PEDIDO") then lnconId=f_add_naoconformidade(pnconNumero, pprojId)

			if (not isnumeric(trim(lnconId))) then
				retorno = lnconId
			else
				'Inicializando recebimento
				if ((trim(lnconId)="") or (trim(lnconId)="0")) then lnconId = "null"
				if ((trim(ppedcId)="") or (trim(ppedcId)="0")) then ppedcId = "null"
				lreceId = ""
				
				lsql = ""
				lreceId = pCad_NovoId("Recebimento", gConexao)
				
				lsql = lsql & "insert into recebimento (receId, pedcId, projId, nconId, receNotaFiscal, receDt, usuaIdCad) "
				lsql = lsql & "values (" & lreceId & ", " & ppedcId & ", " & pprojId & ", " & lnconId & ", '" & preceNotaFiscal & "', getdate(), " & pusuaIdCad & ") "
				'response.write lsql 
				gConexao.execute(lsql)
				
				retorno = lreceId
			end if
		end if
	end if
	f_cria_rece = retorno
'	exit function
'erro_exec:
'	f_cria_rece = NovoErroXML("[" & err.number & "] " & err.description, chamada)
end function

function f_rec_item(preceId, pitepId, pitemCodJDE, punidId, pitrcQtd, ppedcId)
'on error goto erro_exec
    chamada = "f_rec_item(" & preceNotaFiscal & ", " & ppedcId & ", " & pprojId & ", " & pnconNumero & ", " & pusuaIdCad & ", " & pOPCAO & ", " & pitpeId & ", " & pitemCodJDE & ", " & punidId & ", " & pitrcQtd & ")"
	retorno = "" 
	
	if ((trim(pitemCodJDE) = "") or (not isNumeric(pitemCodJDE))) then
		retorno = NovoErroXML("00000003", "C�digo JDE inv�lido", chamada)
	else
		'Pegando dados do item
		litemId = ""
		lsql = ""
		lsql = lsql & "select i.itemId "
		lsql = lsql & "  from Item i "
		lsql = lsql & " where i.itemCodJDE = " & pitemCodJDE
		set lRS_1 = gConexao.execute(lsql)
		If(not lRS_1.BOF) and (Not lRS_1.EOF) then
			litemId = lRS_1("itemId")
		end if
		lRS_1.Close
		
		if (trim(litemId) = "") then
			retorno = NovoErroXML("00000007", "Nenhum item encontrado para este c�digo JDE", chamada)
		else
			if ((trim(pitepId)="") or (trim(pitepId)="0")) then pitepId = "null"
			if ((trim(punidId)="") or (trim(punidId)="0")) then punidId = "null"
			if ((trim(ppedcId)="") or (trim(ppedcId)="0")) then ppedcId = "null"
			
			if((ppedcId<>"null") and (pitepId="null")) then 'Vamos verificar se o item j� existe no pedido... caso exista, pegamos o seu itepId
				lsql = lsql & "select i.itepId "
				lsql = lsql & "  from itempedido i "
				lsql = lsql & " where i.pedcId     = " & ppedcId
				lsql = lsql & "   and i.itepCodJDE = " & pitemCodJDE
				set lRS_1 = gConexao.execute(lsql)
				If(not lRS_1.BOF) and (Not lRS_1.EOF) then
					pitepId = lRS_1(0)
				end if
				lRS_1.Close	
				if ((trim(pitepId)="") or (trim(pitepId)="0")) then pitepId = "null"
			end if
			
			'Inserindo o item em si
			lsql = ""
			litrcId = pCad_NovoId("ItemRecebimento", gConexao)
			lsql = lsql & "insert into ItemRecebimento (itrcId, receId, itepId, itemId, unidId, itrcQtd) "
			lsql = lsql & "values (" & litrcId & ", " & preceId & ", " & pitepId & ", " & litemId & ", " & punidId & ", " & CurrencyToBD(pitrcQtd,2) & ") "
			'response.write lsql 
			gConexao.execute(lsql)

			if (pitepId <> "null") then 'Atualizando saldo do item na lista
				lsql = ""
				lsql = lsql & "select j.itelId "
				lsql = lsql & "  from itemrevisao    j "
				lsql = lsql & "     , itemrequisicao k "
				lsql = lsql & "     , itempedido     x "
				lsql = lsql & " where x.itepId = " & pitepId
				lsql = lsql & "   and k.iteqId = x.iteqId "
				lsql = lsql & "   and j.iterId = k.iterId "
				lsql = lsql & "   and j.itemId = " & litemId
				litelId = ""
				set lRS_1 = gConexao.execute(lsql)
				If(not lRS_1.BOF) and (Not lRS_1.EOF) then
					litelId = lRS_1(0)
				end if
				lRS_1.Close	
				
				if (litelId<>"") then 'Se o item foi encontrado na lista
					lsql = ""
					lsql = lsql & "update ItemLista "
					lsql = lsql & "   set itelQtdRecebida = isnull(itelQtdRecebida,0) + " & CurrencyToBD(pitrcQtd,2)
					lsql = lsql & " where itelId = " & litelId
					gConexao.execute(lsql)
				end if
			end if
		end if
	end if
	
	f_rec_item = retorno
'	exit function
'erro_exec:
'	f_rec_item = NovoErroXML("[" & err.number & "] " & err.description, chamada)
end function

function f_pro_item(pitemCodJDE)
'on error goto erro_exec
	chamada = "f_pro_item(" & pitemCodJDE & ")"
	retorno = ""
	if ((trim(pitemCodJDE)="") or (not isnumeric(pitemCodJDE))) then
		retorno = "ERROC�digo JDE informado � inv�lido"
	else
		lsql = ""
		lsql = lsql & "select i.itemId, i.itemDescricao "
		lsql = lsql & "  from Item i "
		lsql = lsql & " where i.itemCodJDE = '" & pitemCodJDE & "'"
		set lRS_1 = gConexao.execute(lsql)
		If(not lRS_1.BOF) and (Not lRS_1.EOF) then
			retorno = lRS_1("itemDescricao")
		end if
		lRS_1.Close	
		
		if(retorno="")then
			retorno = "ERRONenhum item encontrado para o C�digo JDE"
		end if
	end if
	
	f_pro_item = retorno
'	exit function
'erro_exec:
'	f_pro_item = NovoErroXML("[" & err.number & "] " & err.description, chamada)
end function

'
'Pegando par�metros de request
lreceId     = trim(request.querystring("receId"))
lnf         = trim(ucase(request.querystring("nf")))
lpedcId     = trim(request.querystring("pedcId"))
lprojId     = trim(request.querystring("projId"))
lnr         = trim(request.querystring("nr"))
ldt         = trim(request.querystring("dt"))
lop         = trim(ucase(request.querystring("op")))
litepId     = trim(request.querystring("itepId"))
litemCodJDE = trim(request.querystring("itemCodJDE"))
lunidId     = trim(request.querystring("unidId"))
litrcQtd    = trim(request.querystring("itrcQtd"))
lacao       = trim(request.querystring("acao"))

'Zerando vari�veis de apoio
lR = ""

'Tomada de decis�o
select case lacao
	case "CR" 'Cria recebimento
		chamada = "f_cria_rece(" & lnf & ", " & lpedcId & ", " & lprojId & ", " & lnr & ", " & session("usuaId") & ", " & lop & ")"
		lR = f_cria_rece(lnf, lpedcId, lprojId, lnr, session("usuaId"), lop)
	case "SI" 'Salvar item
		chamada = "f_rec_item(" & lreceId & ", " & litepId & ", " & litemCodJDE & ", " & lunidId & ", " & litrcQtd & ", " & lpedcId & ")"
		lR = f_rec_item(lreceId, litepId, litemCodJDE, lunidId, litrcQtd, lpedcId)
	case "PI" 'Procurar item
		chamada = "f_pro_item(" & litemCodJDE & ")"
		lR = f_pro_item(litemCodJDE)
	case else
		lR  = NovoErroXML("00000010", "A��o inexistente!", "--")
end select

set gConexao = nothing

'Tratamento de erro
if err.number <> 0 then lR = NovoErroXML(err.number, err.description,"Query n�o dispon�vel. A��o: " & lacao & ". Chamada: " & chamada )

'Retorno
response.write lR
%>