<% 
	Server.ScriptTimeout=60000 
    pStr = "private, no-cache, must-revalidate" 
    Response.ExpiresAbsolute = #2000-01-01# 
    Response.AddHeader "pragma", "no-cache" 
    Response.AddHeader "cache-control", pStr 

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Controle remoto de projetos</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
	</HEAD>
	<body class="cabecalho" >
		<!--#include file="../includes/varControleAcesso.inc"-->
		<!--#include file="../includes/varBanco.inc"-->
		<% 
		Dim objUpload
		Dim strFileName
		Dim objConn
		Dim objRs
		Dim lngFileID
		AbrirConexao
		' Instantiate Upload Class
		Set objUpload = Server.CreateObject("SoftArtisans.FileUp")
		objUpload.ProgressID = CInt(Request.QueryString("progressid"))
		servidor = Request.ServerVariables("HTTP_HOST")
		if servidor = "www.prj.com.br" then
			xPath = "e:\home\prj\web\Anexos"
		elseif servidor = "www.m2software.com.br" then
			xPath = "e:\home\m2software\web\whitemartins\prj\Anexos"
		else
			xPath = Server.MapPath("..\listas")
		end if
		xPath = Server.MapPath("..\listas")
		objUpload.Path = xpath
			
		' Grab the file name
		projId = objUpload.form("projId")
		usuaId = objUpload.form("usuaId")
		reviId = objUpload.form("cmbRevisao")
		strFileName = objUpload.form("txtArquivo").ShortFileName
		if instr(1, strFileName, ".") then
			xSplit = split(strFileName, ".")
			i = ubound(xSplit)
			xExtensao = xSplit(i)
		else
			xExtensao = "txt"
		end if
		
		if strFileName = "" then
			Session("MsgErro") = "Arquivo n�o informado."
			Session("txtDesc") = anexDesc
			Response.Redirect ("prjRequAlterar0.asp?&projId=" & projId)
		end if
		' Cria a importacao pendente.
		xSQL = ""
		xSQL = xSQL & "exec PCAD_Importacao "
		xSQL = xSQL & "     @pimpoTipo     = 3"
		xSQL = xSQL & "   , @pimpoNomeArquivo = '" & strFileName & "'"
		xSQL = xSQL & "   , @pusuaId       = " & usuaId
		xSQL = xSQL & "   , @pprojId       = " & projId
		xSQL = xSQL & "   , @pimpoExtensao = '" & xExtensao & "'"
		'response.write xSQL
		Set xRs = gConexao.execute(xSQL)
		ximpoId = xRs("impoId")
		'
		' Salvar arquivo
		xNomeArquivo = ximpoId & "." & xExtensao
		objUpload.form("txtArquivo").SaveAs xNomeArquivo
		'
		' Encaminhar para importacao
		if ucase(xExtensao) = "XLS" then
			xImportador = "prjPedcImportarXls.asp"
		else
			xImportador = "prjPedcImportar.asp"
		end if

		Session("TituloMsg") = "Importar nova lista"
		Session("Msg") = "Pedido de compra carregado com sucesso, em " & xpath
		Response.redirect(xImportador & "?impoId=" & ximpoId)
		%>
	</body>
</HTML>