<% 	
	Session.LCID = 1046 'Pt-Br'
	Response.Expires= 0
    Response.AddHeader "PRAGMA", "NO-CACHE"
	Response.Charset = "iso-8859-1"
	
	projId = request.querystring("projId")
	excel  = false
	appl= ".."
	if (request.querystring("excel")="1") then
		excel  = true
		Response.AddHeader "Content-Disposition", "attachment; filename=LISTA_DE_PENDENCIAS_DE_RECEBIMENTO-" & right("00000" & projId, 5) & "-000.xls"
		Response.ContentType = "application/vnd.ms-excel"
		appl= "http://" & Request.ServerVariables("REMOTE_HOST") & Request.ServerVariables("PATH_INFO")
		appl = mid(appl,1,(len(appl)-27))
	end if
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ::</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="<%=appl%>/Styles.css">
		<link rel="stylesheet" type="text/css" href="<%=appl%>/EstiloMenu.css">
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<!--#include file="../includes/funcoes.inc"-->
<%  if(not excel) then

		lPerfisAutorizados = "157"
		VerificarAcesso()

		Dim gmenuGeral
		Dim gMenuSelecionado
		Dim gOpcaoSelecionada
		'gOpcaoSelecionada = "Servicos"
		
		projId = Request.QueryString("projId")
		cntrNomeServArray = Session("cntrNomeServArray")
		cntrIdArray = Session("cntrIdArray")
		emprNomeArray = Session("emprNomeArray")
		emprIdArray = Session("emprIdArray")
		TotalServ = Session("QtdServicos")
		servInd = 0
		
		gmenuGeral = "Projetos"
		gmenuGeral = "Projetos"
		gMenuSelecionado = "prjRelatorioMat"
		gOpcaoSelecionada = Request.QueryString("area")
	end if
 %>
  <% if(not excel) then %>
		<script language="JavaScript" type="text/javascript" src="../scripts/moving_div_on_screen.js"></script>
        <script language="JavaScript" type="text/javascript" src="../scripts/block_background_objects.js"></script>
        <script language="JavaScript" type="text/javascript" src="../scripts/functions.js"></script>
        <link rel="stylesheet" type="text/css" href="../scripts/block_background_objects.css">
		<link rel="stylesheet" type="text/css" href="../scripts/wbs_form.css">
		<script language="JavaScript" type="text/javascript" src="../scripts/AbrirJanela.js"></script>
	<% end if %>
		<script language="JavaScript" type="text/javascript">
			function _exp_excel(){
				window.open('prjRelPendRece.asp?projId=<%=projId%>&excel=1');
			}
		</script>
	</HEAD>
	<body class="cabecalho" >
	<%  if(not excel) then %>
		<div id="over"     style="display:none; z-index:500; width:99%; height:99%; position:absolute; background: #464; -moz-opacity:0.4;filter:alpha(opacity=40)"></div>
		<div id='procover' style='background: #464 url(../images/aguarde.gif) no-repeat fixed center; display:none'></div>
		<!--#include file="../includes/cab.inc"-->
	<%  end if %>
		<tr>
		<%  if(not excel) then %>
			<td style="padding:0px" valign="top" class="blocoOpcoes">
				<!--#include file="../includes/opcProjx2.inc"-->
			</td>
		<%  end if %>
			<td colspan="2" valign="top" style="padding:5px" class="blocoPagina">
				<table width="100%">
				<tr>
					<%  if(not excel) then %>
						<td class="docTit">
					<%  else %>
						<td colspan="2" class="docTit">
					<%  end if %>
						Lista de Pend�ncias de Recebimento<br>
						Projeto: <%=session("projNome")%><br>
						Data: <%=FormatDateTime(now(), 2)%>
					</td>
					<%  if(not excel) then %>
					<td align="right" style='width:100px'>
							<a href='#' onclick='_exp_excel();' title='Clique aqui para exportar para excel'><img src='../images/icxls.gif'class='botao'/> Exportar</a><br>
					</td>
					<%  end if %>
				</tr>
				<tr>
					<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
				</tr>
				<tr>
					<td colspan="2" style="height:5"></td>
				</tr>
		<tr>
			<td>
<%	
	tabela=""
	tabela=tabela&"<table width='99%' class='grid' id='tbItens' name='tbItens'>"
	tabela=tabela&"	<tr class='GridCab' style='text-align:center;'>"
	tabela=tabela&"		<td style='width:100px'>N�m. Ped. de Compra</td>"
	tabela=tabela&"		<td style='width:70px' >Entrega Prevista</td>"
	tabela=tabela&"		<td style='width:100px'>C�digo JDE</td>"
	tabela=tabela&"		<td style='width:330px'>Descri��o</td>"
	tabela=tabela&"		<td style='width:70px' >Unidade</td>"
	tabela=tabela&"		<td style='width:100px'>Qtd atual</td>"
	tabela=tabela&"		<td style='width:100px'>Qtd comprada</td>"
	tabela=tabela&"	</tr>"
	
	rod_tabela=""
	rod_tabela=rod_tabela&"</table>"
		
	Dim gConexao
	Dim xRs
	Dim xAlign
	AbrirConexao
	
	xAlign = array("right", "center", "right", "left", "center", "right", "right")
	
	xSQL = ""
	xSQL = xSQL & "select pe.pedcNum, it.itemCodJde, it.itemDescricao, convert(varchar(10), pe.pedcDtEntregaPrev, 103), un.unidSigla, 0, ip.itepQtd "
	xSQL = xSQL & "  from itempedido ip, item it, pedidocompra pe, unidade un "
	xSQL = xSQL & " where ip.pedcId  = pe.pedcId "
	xSQL = xSQL & "   and it.itemId  = ip.itemId "
	xSQL = xSQL & "   and ip.unidId *= un.unidId  "
	xSQL = xSQL & "   and pe.projId   = " & projId
	xSQL = xSQL & "   and ip.itepId not in (select ir.itepId from itemrecebimento ir, recebimento re where ir.receId = re.receId and re.projId = " & projId & ")"
	xSQL = xSQL & "   and pe.pedcDtEntregaPrev < getdate() "
	xSQL = xSQL & " order by it.itemCodJde, it.itemDescricao "
	Set xRs = gConexao.Execute(xSQL)
	if not xrs.eof then	
		response.write tabela
		estilo = "GridLinhaImpar"
		i = 1
		while not xrs.EOF %>
			<tr class="<%=estilo%>"> <%
				for i = 0 to 6 %>
					<td style='text-align:<%=xAlign(i)%>'><%=xrs(i)%></td> <%
				next %>
			</tr><%			
			if estilo = "GridLinhaPar" then 				
				estilo = "GridLinhaImpar"
			else
				estilo = "GridLinhaPar"
			end if
			i = i + 1
			xrs.movenext
		wend
		xrs.close
		response.write rod_tabela
	else
		response.write tabela & rod_tabela
	end if %>
					</td>
				</tr>
				<tr>
					<td colspan="2" height="20"></td>
				</tr>
			</table>
		</tr>
		<% if(not excel) then %>
			<!--#include file="../includes/rod.inc"-->
		<% else %>
			</table>
		<% end if %>
	</body>
</HTML>