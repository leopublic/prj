<%@ Import Namespace="System.IO"%>
<%@ Page aspcompat=true %>
<script language="VB" runat="server">
Sub Page_Load(sender As Object, e As EventArgs)
	
	Dim gConexao as object
	Dim oRs as object
	Dim sSQL as string 
	Dim anexId as string
	Dim filename As String
	Dim Tamanho as integer 
	Dim xConStr as string
	Dim servidor as string 
	xConStr = Session("connectionString")
	servidor = Request.ServerVariables("HTTP_HOST")
	if xConStr = "" then
		if servidor = "www.prj.com.br" then
			'xConStr = "Provider=SQLOLEDB.1;SERVER=sql172.prj.com.br;DATABASE=prj;UID=prj;PWD=sossego0001;"
			xConStr = "Provider=SQLOLEDB.1;SERVER=201.76.55.6;DATABASE=prj1;UID=prj1;PWD=gg0rezqn;"
		end if
		if servidor = "www.m2software.com.br" then
			'xConStr = "Provider=SQLOLEDB.1;SERVER=sql172.m2software.com.br;DATABASE=m2software;UID=m2software;PWD=sossego0001;"
			xConStr = "Provider=SQLOLEDB.1;SERVER=201.76.55.6;DATABASE=prj1;UID=prj1;PWD=gg0rezqn;"
		end if
		if servidor = "localhost" then
			xConStr = "Provider=SQLOLEDB.1;SERVER=FSWINSOFTER;DATABASE=PRJ;UID=sa;PWD=sossego;"
		end if
	end if
	try
		gConexao = server.createobject("ADODB.Connection")
		gConexao.ConnectionString = xConStr
		gConexao.Open
	catch exc as exception
		Session("erroFase") =  "Erro na abertura da conexao"
		Session("erroDescricao") = exc.Message
		Response.Redirect ("gerErro.asp")
	end try

	anexId = Request.QueryString("anexId")
	sSQL = "SELECT anexId, anexNomeOrig, anexTam FROM Anexo WHERE anexId = " & anexId
	try
		oRs = gConexao.execute(sSQL)
	catch exc as exception
		Session("erroFase") =  "Erro na leitura dos detalhes do arquivo. SQL=" & sSQL
		Session("erroDescricao") = exc.Message
		Response.Redirect ("gerErro.asp")
	end try

	Dim xExt as string  
	Dim xNomeOrig as string = oRs("anexNomeOrig").value
	xExt = right(xNomeOrig, Len(xNomeOrig) - InStrRev(xNomeOrig, "."))
	filename = right("00000000" & oRs("anexId").value , 8)& "." & xExt
	tamanho = oRs("anexTam").value
	oRs.close
	gConexao.close
	gConexao = nothing
	Dim xPathBase as string 
    if servidor = "www.prj.com.br" then
		xpathbase = "e:\home\prj\web\anexos\"
	elseif servidor = "www.m2software.com.br" then
		xPathBase = "e:\home\m2software\web\whitemartins\prj\Anexos\"
	end if

	Dim filepath as string = xpathbase & Right("00000000" & anexId,8)
	If Not filepath Is Nothing Then
		Response.Clear()
		Response.ContentType = "application/x-msdownload"
		Response.AddHeader("Content-Disposition", "attachment; filename=""" & filename & """")
		Response.AddHeader("Content-Length", tamanho)
		Response.Flush()
		Response.WriteFile(filepath)
	End If
End Sub
</script>
