<!--#include file="../includes/clsBanco.inc"-->
<!--#include file="../includes/funcoes.inc"-->

<%
Session.LCID=1046 'Pt-BR
response.charset = "iso-8859-1"

function erro_xml(petapa, plinha, pdescricao)
	ret = ""
	ret = ret & "<ERRO>"
	ret = ret & "<ETAPA>" & petapa & "</ETAPA>"
	ret = ret & "<LINHA>" & plinha & "</LINHA>"
	ret = ret & "<DESCRICAO>" & pdescricao & "</DESCRICAO>"
	ret = ret & "</ERRO>"
	erro_xml = ret 
end function

function validar(ptipo, pwbscId, piwbcIdPai, linha, pfcn_Id)
	ret = ""
	lSQL = ""
    lSQL = lSQL & "select i.iwbcId, isnull(i.iwbcId_Pai,0) as iwbcId_Pai, i.tiwbId, i.unimId, i.iwbcCodigo, replace(i.iwbcNome,'""','��') as iwbcNome, replace(i.iwbcDetalhamento,'""','��') as iwbcDetalhamento, i.iwbcPercPai, i.iwbcFatorHH, i.iwbcValUnitEst, isnull(i.iwbcFLFilho,0) as iwbcFLFilho, isnull(t.tiwbDescricao,'') as tiwbDescricao, isnull(u.unimAbrev,'') as unimAbrev, isnull(i.iwbcComplemento,'') as iwbcComplemento, isnull(i.iwbcQtdEst,0) iwbcQtdEst, isnull(i.iwbcQtdReal,i.iwbcQtdEst) as iwbcQtdReal, isnull(i.iwbcQtdAtual,0) as iwbcQtdAtual, isnull(i.iwbcValUnitReal,0) iwbcValUnitReal, (isnull(i.iwbcQtdEst,0) * isnull(i.iwbcFatorHH,1)) as iwbcQtdHH, (isnull(p.projIndHH,1) * isnull(i.iwbcQtdEst,0) * isnull(i.iwbcFatorHH,1)) as iwbcValHH, isnull(convert(varchar(10),i.iwbcDtInicioEst,103),'" & projSemIni & "') as iwbcDtInicioEst, isnull(i.iwbcDuracaoEst,0) iwbcDuracaoEst, (isnull(i.iwbcDtInicioEst,getdate())  + isnull(i.iwbcDuracaoEst,0)) as iwbcDtTerminoEst, i.iwbcDataConcForn, i.usuaIdVerif, i.iwbcDataVerif, i.stwbId, isnull(i.iwbcQtdAtual,0) as iwbcQtdAtual, i.fcn_Id, f.fcn_StatusEAP " 
    lSQL = lSQL & "  from (((((Projeto p inner join Contrato c on c.projId = p.projId) inner join WbsContrato w on w.cntrId = c.cntrId) inner join ItemWbsContrato i on i.wbscId = w.wbscId) left join TipoItemWbs t on i.tiwbId = t.tiwbId) left join UnidadeMedida u on i.unimId = u.unimId) left join fcn f on i.fcn_Id = f.fcn_Id " 
    lSQL = lSQL & " where i.wbscId = " & pwbscId
    if trim(piwbcIdPai) = "" then
        lSQL = lSQL & " and i.iwbcId_Pai is null " 
    else
        lSQL = lSQL & " and i.iwbcId_Pai = " & piwbcIdPai
    end if
	if trim(pfcn_Id) = "" then
        lSQL = lSQL & " and i.fcn_Id is null " 
    else
        lSQL = lSQL & " and i.fcn_Id = " & pfcn_Id
    end if
    lSQL = lSQL & " order by isnull(i.fcn_Id,0), i.iwbcCodigo " 
	
	Dim lRS_1
    'response.write lSQL & "<br><br>"
    'exit function
	Set lRS_1 = gConexao.execute(lSQL)
	ptipo = cdbl(ptipo)
	If (not lRS_1.BOF) and (not lRS_1.EOF) then
		While not lRS_1.EOF
			linha = linha + 1
			if (lRS_1("iwbcFLFilho")=0) then
				if ptipo >= 1 then 'Fechar PPU
					if(lRS_1("iwbcQtdEst")=0) then ret = ret & erro_xml("Monta PPU", linha, "Qtd. Estimada inv�lida!")
					if(lRS_1("iwbcValUnitReal")=0) then ret = ret & erro_xml("Monta PPU", linha, "Val. Unit. Real. inv�lido!")
				end if
				
				if ptipo >= 2 then 'Iniciar EAP - Finaliza��o de Cronograma
					if(lRS_1("iwbcDtInicioEst")=projSemIni) then ret = ret & erro_xml("Monta Cronograma", linha, "Data In�cio Estimada inv�lida!")
					if(lRS_1("iwbcDuracaoEst")=0) then ret = ret & erro_xml("Monta Cronograma", linha, "Dura��o(Dias)!")
				end if
			else
				ret = validar(ptipo, pwbscId, lRS_1("iwbcId"), linha, pfcn_Id)
			end if
			lRS_1.MoveNext
		Wend
	End If
	lRS_1.close
	
	validar = ret
end function

ltipo   = trim(request.querystring("tipo"))
lwbscId = trim(request.querystring("wbscId"))
lfcn_Id = trim(request.querystring("fcn_Id"))
lR      = ""
if ((ltipo="PPU") or (ltipo="EAP")) then
	if (ltipo = "PPU") then ltipo = "1"
	if (ltipo = "EAP") then ltipo = "2"
	if(not isnumeric(lwbscId))then
		lR = erro_xml("", "0", "wbscontrato_ppueap_fecharPPUEAP.asp -> wbscId inv�lido")
	elseif((not isnumeric(lfcn_Id)) and (trim(lfcn_Id)<>""))then
		lR = erro_xml("", "0", "wbscontrato_ppueap_fecharPPUEAP.asp -> fcn_Id inv�lido")
	else
		Dim gConexao
		AbrirConexao
		lR = validar(ltipo, lwbscId,  "", 0, lfcn_Id)
	end if
else
	lR = erro_xml("", "0", "wbscontrato_ppueap_fecharPPUEAP.asp -> Tipo desconhecido")
end if
response.write lR

%>