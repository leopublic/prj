<%@ Page Language="vb" AutoEventWireup="false" Codebehind="upload.aspx.vb" Inherits="prj.upload"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>upload</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<table width="100%">
				<tr>
					<td width="15"></td>
					<td></td>
				</tr>
				<tr>
					<td class="docTit" colSpan="2"><asp:label id="lblTituloPagina" runat="server"></asp:label></td>
				</tr>
				<tr>
					<td style="HEIGHT: 2px" background="../images/pont_cinza_h.gif" colSpan="2"><asp:textbox id="tpanId" runat="server" Visible="False"></asp:textbox><asp:textbox id="projId" runat="server" Visible="False"></asp:textbox><asp:textbox id="cntrId" runat="server" Visible="False"></asp:textbox></td>
				</tr>
				<tr>
					<td colSpan="2" height="15"></td>
				</tr>
				<tr>
					<td width="15"></td>
					<td>
						<table class="doc">
							<tr>
								<td class="docLabel" width="110">Arquivo</td>
								<td class="docLabel" width="5">:</td>
								<td class="docCmpLivre"><input class="docCmpLivre" id="txtArquivo" type="file" name="txtArquivo" runat="server"></td>
							</tr>
							<tr>
								<td class="docLabel" width="110"><asp:label id="lblNormaNumero" runat="server">Label</asp:label></td>
								<td class="docLabel" width="5">:</td>
								<td class="docCmpLivre"><input class="docCmpLivre" id="txtNumero" type="text" name="txtNumero"></td>
							</tr>
							<asp:panel id="panelTitulo" runat="server" Height="26px" Width="67px">
								<TR>
									<TD class="docLabel" width="110">T�tulo</TD>
									<TD class="docLabel" width="5">:</TD>
									<TD class="docCmpLivre">
										<INPUT class="docCmpLivre" id="txtNome" type="text" maxLength="150" name="txtNome"></TD>
								</TR>
							</asp:panel><asp:panel id="painelRevisaoLivre" runat="server">
								<TR>
									<TD class="docLabel" width="110">Revis�o</TD>
									<TD class="docLabel" width="5">:</TD>
									<TD class="docCmpLivre">
										<INPUT class="docCmpLivre" id="txtRevNum" type="text" maxLength="10" name="txtRevNum"></TD>
								</TR>
							</asp:panel><asp:panel id="painelRevisaoData" runat="server">
								<TR>
									<TD class="docLabel" width="110">Revis�o</TD>
									<TD class="docLabel" width="5">:</TD>
									<TD class="docCmpLivre"><SELECT class="docCmpLivre" id="cmbRevMes" style="WIDTH: 90px" name="cmbRevMes" maxlength="100">
											<OPTION value="1" selected>Janeiro</OPTION>
											<OPTION value="2">Fevereiro</OPTION>
											<OPTION value="3">Mar�o</OPTION>
											<OPTION value="4">Abril</OPTION>
											<OPTION value="5">Maio</OPTION>
											<OPTION value="6">Junho</OPTION>
											<OPTION value="7">Julho</OPTION>
											<OPTION value="8">Agosto</OPTION>
											<OPTION value="9">Setembro</OPTION>
											<OPTION value="10">Outubro</OPTION>
											<OPTION value="11">Novembro</OPTION>
											<OPTION value="12">Dezembro</OPTION>
										</SELECT>
										/&nbsp;
										<asp:DropDownList class="docCmpLivre" id="cmbRevAno" runat="server" Width="70px"></asp:DropDownList></TD>
								</TR>
							</asp:panel></table>
					</td>
				</tr>
				<tr>
					<td align="middle" colSpan="2" height="20">
						<asp:Label id="lblMsg" runat="server" ForeColor="Red" Font-Bold="True" Font-Names="Verdana" Font-Size="10pt"></asp:Label></td>
				</tr>
				<tr>
					<td align="middle" colSpan="2">
						<table>
							<tr>
								<td align="middle" width="80" height="28"><asp:imagebutton id="btnSalvar" runat="server" ImageUrl="../images/btnSalvar.gif"></asp:imagebutton></td>
								<td width="10"></td>
								<td align="middle" width="80"><IMG class="botao" onclick="javascript:window.close();" src="../images/btnCancelar.gif" border="0"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>