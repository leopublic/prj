<%
	Response.Expires= 0
    Response.AddHeader "PRAGMA", "NO-CACHE" 
    if Session("usuaId") = "" then
   		response.redirect("../login.asp")
    end if


   gMenuSelecionado = "Wbs"
   gOpcaoSelecionada = "Wbs"
   gmenuGeral = "Configuracao"
   PastaSelecionada = "Configura��o"
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Controle de WBS</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="JavaScript" type="text/javascript" src="tree/dtree.js"></script>
		<link rel="stylesheet" type="text/css" href="tree/dtree.css">
		<script language="JavaScript" type="text/javascript" src="../scripts/moving_div_on_screen.js"></script>
        <script language="JavaScript" type="text/javascript" src="../scripts/block_background_objects.js"></script>
        <script language="JavaScript" type="text/javascript" src="../scripts/functions.js"></script>
        <script language="JavaScript" type="text/javascript" src="../scripts/functions.js"></script>
        <link rel="stylesheet" type="text/css" href="../scripts/block_background_objects.css">
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<!--#include file="../includes/funcoes.inc"-->
		<script language="JavaScript" type="text/javascript" src="../scripts/PreparaJanela.js"></script>

		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<link rel="stylesheet" type="text/css" href="../EstiloMenu.css">
        <link rel="stylesheet" type="text/css" href="../scripts/wbs_form.css">
        <link rel="stylesheet" type="text/css" href="../scripts/wbs_ppueap.css">
        
		<style type"text/css">
			
		</style>
		<SCRIPT language="JavaScript">
			function recriar()
			{
				d = null;
				document.getElementById("divLista").innerHTML = '<span style="color:#396D60;font-family:Verdana, Tahoma, Arial;font-size:8pt;font-weight:normal;">Aguarde enquanto a estrutura da WBS � montada... <br/>(dependendo da quantidade de itens e subitens isso pode demorar alguns segundos)</span>';
				window.status = 'Montando...';
				_carregaitensNovissimo('d', 'divLista', 'CADM', 'WBS Modelo', '', '', '', '');
			}
			function ExcluirItem(itemId){
                //alert ('entrou ExcluirItem '+itemId);
                //return false
                
				if(confirm('Confirma a exclus�o deste item e todos os demais relacionados a ele?')){

					document.body.style.cursor='wait';
					var pagina = "wbslistar_excluir_item.asp?itemId="+itemId;
					//
					var xmlhttp = _criaxmlhttprequest();

					xmlhttp.open("GET",pagina,true);
					xmlhttp.onreadystatechange = function() {
						if(xmlhttp.readyState == 4) {
							if(xmlhttp.status == 200){
								if(_tratarxmlretorno(xmlhttp.responseText)){
									alert('Item removido c/ sucesso!');
								}else{
									alert('Falha ao remover o item!');
									return false
								}
							}
						}
					}

					xmlhttp.send(null);
					document.body.style.cursor='auto';
				}
			}

			function AbrirDetalheItem(_itemId, _itemId_Pai, _tiwbId, _unimId, _itemCodigoPai, _itemCodigo, _itemNome, _itemDetalhamento, _itemPercPai, _itemFatorHH, _itemValUnitEst, _itemFLFilho){
                //alert ('entrou AbrirDetalheItem '+_itemId+', '+_itemId_Pai+', '+_tiwbId+', '+_unimId+', '+_itemCodigo+', '+_itemNome+', '+_itemPercPai+', '+_itemFatorHH+', '+_itemValUnitEst+', '+_itemFLFilho+', '+_itemFLFCN);
                //return false
				_ocdiv('divItem'); //Exibindo div

				//Preenchendo campos do div
				//Chaves
				if (_itemId==0)
				{	document.getElementById("tituloPopUp").innerHTML = 'Adicionar novo item';	}
				else
				{	document.getElementById("tituloPopUp").innerHTML = 'Alterar item';	}
				document.getElementById("_itemId").value = _itemId;
				document.getElementById("_itemId_Pai").value = _itemId_Pai;
				document.getElementById("_tiwbId").value = _tiwbId;
				document.getElementById("_unimId").value = _unimId;
				//Descri��es
				document.getElementById("_itemCodigo").value = _itemCodigo;
				//document.getElementById("_itemCodigoPai").value = _itemCodigoPai+'.';
				document.getElementById("_itemCodigoPai").innerHTML = _itemCodigoPai+'.';
				document.getElementById("_itemNome").value = _itemNome;
                document.getElementById("_itemDetalhamento").value = _itemDetalhamento;
				//Valores para WBS
				document.getElementById("_itemPercPai").value = _itemPercPai;
				document.getElementById("_itemFatorHH").value = _itemFatorHH;
				document.getElementById("_itemValUnitEst").value = _itemValUnitEst;
				//Dados de apoio
				document.getElementById("_itemFLFilho").value = _itemFLFilho;
				//
                //alert(_itemFLFilho);
				//for(r__i=0;r__i<document.getElementsByName('sumir_linha_item').length;r__i++){
				//	document.getElementsByName('sumir_linha_item')[r__i].style.display='block';
				//}
				//if(_itemFLFilho=='1'){
				// for(r__i=0;r__i<document.getElementsByName('sumir_linha_item').length;r__i++){
				// 	document.getElementsByName('sumir_linha_item')[r__i].style.display='none';
				// 	}
				//}
				//
				//document.getElementById("cmbtipoitem").style.display = "block"
				setavaluecombo('cmbtipoitem',_tiwbId);
				setavaluecombo('cmbunidademedida',_unimId);
			}

			function AddNewItemWbs(_itemId_Pai, _tiwbId, _unimId, _itemCodigoPai){
                //alert ('entrou AddNewItemWbs '+_itemId_Pai);
                //return false
				AbrirDetalheItem(0, _itemId_Pai, _tiwbId, _unimId, _itemCodigoPai , '', '', '', 0, 1, 0, 0, 0);
			}

			function SalvaItem(){
                //alert ('entrou SalvaItem');
                //return false
				//Chaves
				var _itemId = document.getElementById("_itemId").value;
				var _itemId_Pai = document.getElementById("_itemId_Pai").value;
				var _tiwbId = document.getElementById("_tiwbId").value;
				var _unimId = document.getElementById("_unimId").value;
				//Descri��es
				var _itemCodigo = document.getElementById("_itemCodigo").value;
				var _itemNome = document.getElementById("_itemNome").value;
                var _itemDetalhamento = document.getElementById("_itemDetalhamento").value;
				//Valores para WBS
				var _itemPercPai = document.getElementById("_itemPercPai").value;
				var _itemFatorHH = document.getElementById("_itemFatorHH").value;
				var _itemValUnitEst = document.getElementById("_itemValUnitEst").value;
				//Dados de apoio
				var _itemFLFilho = document.getElementById("_itemFLFilho").value;
				//Validando campos
				//  Tipo Item Wbs
				if(_tiwbId==0 && _itemFLFilho=='0'){
					alert('Tipo de Item WBS inv�lido!');
					return false
				}
				//  Unidade de Medida
				if(_unimId==0 && _itemFLFilho=='0'){
					alert('Unidade de Medida inv�lida!');
					return false
				}
				//	Nome
				if(_itemNome.trim==""){
					alert('Nome n�o informado!');
					return false
				}
				//	C�digo Wbs
				if(_itemCodigo.trim==""){
					alert('C�digo WBS n�o informado!');
					return false
				}

				//	Percentual em rela��o ao pai
				if(isNumberJS(_itemPercPai)==false){
					alert('Peso percentual em rela��o a atividade n�o informada!');
					return false
				}

				//	Fator Homem x Hora
				if(isNumberJS(_itemFatorHH)==false && _itemFLFilho=='0'){
					alert('Fator Homem x Hora n�o informado!');
					return false
				}

				if(isNumberJS(_itemValUnitEst)==false){
					_itemValUnitEst=0;
				}

                _itemNome = _itemNome.replace("&", "|AMP|")
                _ocdiv('divItem'); //Fechando div
                
				//Atualizando item
				document.body.style.cursor='wait';
				var pagina = 'wbslistar_atualiza_item.asp?itemId='+_itemId+'&itemId_Pai='+_itemId_Pai+'&tiwbId='+_tiwbId+'&unimId='+_unimId+'&itemCodigo='+_itemCodigo+'&itemNome='+_itemNome+'&itemPercPai='+_itemPercPai+'&itemFatorHH='+_itemFatorHH+'&itemValUnitEst='+_itemValUnitEst+'&itemFLFilho='+_itemFLFilho+'&itemDetalhamento='+_itemDetalhamento;
                                    
                var xmlhttp = _criaxmlhttprequest();

				xmlhttp.open("GET",pagina,true);
				xmlhttp.onreadystatechange = function() {
					if(xmlhttp.readyState == 4) {
						if(xmlhttp.status == 200){
                            _ocdiv('over');
							if(_tratarxmlretorno(xmlhttp.responseText)){
                                alert('Item inserido com sucesso!');
                                if (document.getElementById("atualiza").checked)
								{	recriar();}
							}else{
								alert('Falha ao inserir o item!');
								return false
							}
						}
					}
				}
				xmlhttp.send(null);
				document.body.style.cursor='auto';
			}
            
		</SCRIPT>
	</HEAD>
<body id="sandbox" class="full">
<div id="over" style="display:none;"></div>
<!--#include file="../includes/varControleAcesso.inc"-->
<!--#include file="../includes/varBanco.inc"-->
<!--#include file="../includes/cab.inc"-->
	<tr>
		<td style="padding:0px" valign="top" class="blocoOpcoes">
			<!--#include file="../includes/opcConfig.inc"-->
		</td>
		<td colspan="2" valign="top" style="padding:5px" class="blocoPagina">
			<table style="width:100%">
				<tr>
					<td class="docTit">Modelo WBS</td>
					<td align="right">
						<a href="wbscontrato_geraxls.asp" target="_blank" style="margin-left:10px;margin-right:10px;">Vers�o Excel</a>&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="wbslistarPRT.asp" target="_blank" style="margin-left:10px;margin-right:10px;"><img src="../images/impressora.gif">&nbsp;Vers�o impress�o</a>
					</td>
				</tr>
				<tr>
					<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
				</tr>
				<tr>
					<td colspan="2" style="width:50%">
	
					</td>
				</tr>
			</table>
			<form action="" method="post" name="frmWBS" id="frmWBS">
				<div id="divItem" name="divItem" style="display:none; position:absolute; z-index:1000;" class="divwindow">
					<div class='div_titulo' id='divItem_TIT' name='divItem_TIT' onmousedown="javascript:dragStart(event, 'divItem');">PRJ - Modelo WBS</div>
					<div class='sep'></div>
					<input id='_itemId' type='hidden'>
					<input id='_itemId_Pai' type='hidden'>
					<input id='_tiwbId' type='hidden'>
					<input id='_unimId' type='hidden'>
					<input id='_itemFLFilho' type='hidden'>
					<table style="width:97%">
						<tr>				
							<td class="docTit" id="tituloPopUp"></td>
						</tr>
						<tr>
							<td background="../images/pont_cinza_h.gif" style="height:2"></td>
						</tr>					
					</table>
					<table class="entradadados">
						<tr>
							<th style="width:150px;">C�digo WBS</th>
							<th style="width:1px;">:</th>
							<!--<td><input id='_itemCodigoPai' type='text' readonly="readonly" style="text-align:right;width:auto;background-color:transparent;border-style:none"><input id='_itemCodigo' type='text' class='cpTexto' maxlength='10' style="width:100px"></td>-->
							<td><div id='_itemCodigoPai' style="display:inline;text-align:right;width:auto;background-color:transparent;border-style:none;padding:0px;margin:0px;font-weight:normal"></div><input id='_itemCodigo' type='text' class='cpTexto' maxlength='10' style="width:100px"></td>
						</tr>
						<tr>
							<th>Nome</th>
							<th>:</th>
							<td><input id='_itemNome' type='text' class='cpTexto' maxlength='250'></td>
						</tr>
                        <tr>
							<th>Detalhamento</th>
							<th>:</th>
							<td><textarea id='_itemDetalhamento' class='cpTexto' maxlength='2000' Rows=3 style="color:#396D60;"></textarea></td>
						</tr>
						<tr>
							<th>Peso Perc. Pai</th>
							<th>:</th>
							<td><input id='_itemPercPai' type='text' class='cpNum' maxlength='11'></td>
						</tr>
						<tr name='sumir_linha_item'>
							<th>Fator Homem X Hora</th>
							<th>:</th>
							<td><input id='_itemFatorHH' type='text' class='cpNum' maxlength='11'></td>
						</tr>
						<tr name='sumir_linha_item'>
							<th>Valor Unit�rio</th>
							<th>:</th>
							<td><input id='_itemValUnitEst' type='text' class='cpNum' maxlength='11'></td>
						</tr>
						<tr name='sumir_linha_item'>
							<th>Tipo de Item</th>
							<th>:</th>
							<td><select id="cmbtipoitem" style="display:block" onchange="javascript:document.getElementById('_tiwbId').value=document.getElementById('cmbtipoitem').value">
								<%
								lTX_Sql = "select 0 as tiwbId, '[n�o definido]' as tiwbDescricao union "
								lTX_Sql = lTX_Sql & "select t.tiwbId, t.tiwbDescricao from TipoItemWbs t order by 2 "
								AbrirConexao
								Set lRS_1 = gConexao.Execute(lTX_Sql)
								while not lRS_1.EOF
									If lRS_1("tiwbId")="0" then
										lTX_Select = "selected"
									else
										lTX_Select = ""
									end if
									response.write vbtab & "<option value=""" & lRS_1("tiwbId") & """ class=""campo"" " & lTX_Select & ">" & lRS_1("tiwbDescricao") & "</option>" & vbcrlf
									lRS_1.MoveNext
								wend
								lRS_1.close
								set lRS_1 = nothing
								%>
								</select>
							</td>
						</tr>
						<tr name='sumir_linha_item'>
							<th>Unidade de Medida</th>
							<th>:</th>
							<td><select id="cmbunidademedida" style="display:block" onchange="javascript:document.getElementById('_unimId').value=document.getElementById('cmbunidademedida').value">
								<%
								lTX_Sql = "select 0 as unimId, '[n�o definido]' as unimNome union "
								lTX_Sql = lTX_Sql & "select u.unimId, (u.unimAbrev + ' - ' + u.unimDescricao) as unimNome from UnidadeMedida u order by 2 "
								AbrirConexao
								Set lRS_1 = gConexao.Execute(lTX_Sql)
								while not lRS_1.EOF
									If lRS_1("unimId")="0" then
										lTX_Select = "selected"
									else
										lTX_Select = ""
									end if
									response.write vbtab & "<option value=""" & lRS_1("unimId") & """ class=""campo"" " & lTX_Select & ">" & lRS_1("unimNome") & "</option>" & vbcrlf
									lRS_1.MoveNext
								wend
								lRS_1.close
								set lRS_1 = nothing
								%>
								</select>
							</td>
                        </tr>
					</table>
					<div class='barrabotoes'>
						<input type='button' onclick="javascript:SalvaItem();" value="Salvar" title="Salva dados do item em quest�o">
						<input type='button' onclick="javascript:_ocdiv('divItem');" value="Fechar" title="Fecha o cadastro de itens">
					</div>
				</div>
			</form>
				<input type='hidden' id='counter' name='counter'>
				<div>
				<a href="javascript:AddNewItemWbs(0, 0, 0, '0');" title="Adicionar novo subitem na raiz"><img src="..\images\newdoc.gif" /> Adicionar nova disciplina</a>
		<table align="center" class="gridWBS">
			<thead>
				<tr style="height:40px">
					<th style="width:100px;text-align:left;">WBS</th>
					<th style="width:400px;text-align:left;">Tarefa</th>
					<th style="width:70px;text-align:center">Unidade</th>
					<th style="width:70px;">PPU</th>
					<th style="width:70px;">Cronograma</th>
					<th style="width:70px;">EAP</th>
					<th style="width:70px;">Boletim de medi��o</th>
				</tr>
			</thead>
<%
Session.LCID=1046 'Pt-BR
response.charset = "iso-8859-1"

lSQL = " select * from vWBS "
lSQL = lSQL & " where wbscId is null and iwbcId <> iwbcId_pai order by iwbcCodigoNovoOrdenavel"
AbrirConexao
Dim lRS_1
Set lRS_1 = gConexao.execute(lSQL)
	
If (not lRS_1.BOF) and (not lRS_1.EOF) then
	parimpar = ""
	cont = 0
	While not lRS_1.EOF
        lclass = ""
        '
        if (lRS_1("iwbcFLFilho")=1) then
            lclass = "identlevel"
			parimpar = ""
		else
		    if parimpar="par" then
		    	parimpar="impar"
		    else
		    	parimpar="par"
		    end if
        end if
		texto = lRS_1("iwbcCodigoNovoOrdenavel") & "."
		x = len(texto)/6
		x = x - 1
		desloc=""
		for i = 1 to x
			desloc = desloc & "&nbsp;&nbsp;&nbsp;"
		next
		cont = cont + 1
		response.write "<tr class=""" & lclass &  " " & parimpar & """>" & vbcrlf
        response.write "<td>" & lRS_1("iwbcCodigoNovo") & "</td>" & vbcrlf
        response.write "<td>" & desloc & lRS_1("iwbcNome") & "</td>" & vbcrlf
        response.write "<td style=""text-align:center"">" & lRS_1("unimAbrev") & "</td>" & vbcrlf
        response.write "<td style=""text-align:center"">-</td>" & vbcrlf
        response.write "<td style=""text-align:center"">-</td>" & vbcrlf
        response.write "<td style=""text-align:center"">-</td>" & vbcrlf
        response.write "<td style=""text-align:center"">-</td>" & vbcrlf
        response.write "<td style=""text-align:center"">"
        response.write "<a href=""javascript:AddNewItemWbs(" & lRS_1("iwbcId") & ", " & lRS_1("tiwbId") & ", " & lRS_1("unimId") & ", '" & lRS_1("iwbcCodigoNovo") & "');"" title=""Adicionar novo subitem de &quot;" & lRS_1("iwbcNome") & "&quot;""><img src=""..\images\newdoc.gif"" /></a>" 
        response.write "&nbsp;&nbsp;<a href=""javascript:ExcluirItem(" & lRS_1("iwbcId") & ");"" title=""Excluir item &quot;" & lRS_1("iwbcCodigoNovo") & "&quot;""><img src=""..\images\trash.jpg"" /></a>" 
		response.write "</tr>" & vbcrlf

		lRS_1.MoveNext
	Wend
End If
lRS_1.close
%>
		</table>
			</div>
		</td>
	</tr>
<!--#include file="../includes/rod.inc"-->
</body>
</HTML>