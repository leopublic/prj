<!--#include file="../includes/clsBanco.inc"-->
<!--#include file="../includes/clsControleAcesso.inc"-->
<!--#include file="../includes/varControleAcesso.inc"-->
<!--#include file="../includes/varBanco.inc"-->
<!--#include file="../includes/funcoes.inc"-->
<%
Dim debug 
Dim ximpoId
Sub debuga (msg)
	'xSQLD = "exec PCAD_MensagemImportacao @pimpoId = " & ximpoId & ", @pmsgiTexto = '" & replace(msg, "'", "''") & "'"
	'gConexao.execute(xSQLD)
	
	if debug="on" then
		response.write "<br>" & msg
	end if
end sub

Function campoConvertido(campo) 
	valor = trim(replace(campo, ",", "."))
	campoConvertido = valor
end function

Dim objConn
Dim objRs
Dim xPagina 
Dim destinoErro
AbrirConexao

xPagina = "prjListImportarLMT"
destinoErro = "prjListAlterar0.asp?impoId=" & ximpoId

'== Obt�m arquivo a importar
debug = request.querystring("debug")
ximpoId = request.querystring("impoId")
xPath = Server.MapPath("..\listas")
xArquivo = xPath & "\" & ximpoId & ".txt"
'== Limpa mensagens da �ltima importacao
xSQL = "delete from MensagemImportacao where impoId =  " & ximpoId
gConexao.execute(xSQL)
'== Obtem dados do arquivo
xSQL = "select tpliPrefixo "
xSQL = xSQL & " from TipoLista TL, Importacao I"
xSQL = xSQL & " where TL.tpliId = I.tpliId "
xSQL = xSQL & "   and I.impoId = " & ximpoId
'response.write xSQL 
set xrs = gConexao.execute(xSQL)
xprefixo = xrs("tpliPrefixo")
xrs.close
set xrs = nothing
'== Inicia importa��o do arquivo
	Const ForReading = 1, ForWriting = 2, ForAppending = 3
	Const TristateUseDefault = -2, TristateTrue = -1, TristateFalse = 0
	' Create a filesystem object
	Dim FSO
	set FSO = server.createObject("Scripting.FileSystemObject")

	if FSO.FileExists(xArquivo) Then
	    ' Get a handle to the file
	    Dim file    
	    set file = FSO.GetFile(xArquivo)

	    ' Open the file
	    Dim TextStream
	    Set TextStream = file.OpenAsTextStream(ForReading, TristateUseDefault)

	    ' Read the file line by line
		regNum = 0
		erro = 0
	    if ( not TextStream.AtEndOfStream) then
	        reg = TextStream.readline
			regNum = regNum + 1
			'==============================================================================================
			'== Procura identificador do arquivo
			'==============================================================================================
			debuga "Procurando header com identifica��o da lista..."
			posIdentificacao = instr(1, reg, xprefixo & "-", 1)
			Do While (posIdentificacao = 0) and (not TextStream.AtEndOfStream)
		        reg = TextStream.readline
				regNum = regNum + 1		
				posIdentificacao = instr(1, reg, xprefixo & "-", 1)
			loop

			if (posIdentificacao = 0) then
				retornarComErro xPagina & ":IdentificaLista", "N�o foi poss�vel importar a lista pois a identifica��o da lista n�o foi  encontrada no arquivo. Verifique se a exporta��o foi feita corretamente.", destinoErro 
				debuga "ERRO! Identifica��o da lista n�o encontrada."
				erro = 1
				response.end
			else
				nomeLista = mid(reg, posIdentificacao,15)
				debuga "Identifica��o encontrada na linha " & regNum & " -->" & nomeLista
				xSplit = split(nomeLista, "-")
				xlistSequencial = xsplit(3)
				xNumProjeto = xsplit(1)
			end if
			'==============================================================================================
			'== Procura a revis�o do arquivo
			'==============================================================================================
			debuga "Procurando n�mero da revis�o..."
			pos = instr(1, reg, "Rev: ", 1)
			Do While (pos = 0) and (not TextStream.AtEndOfStream)
		        reg = TextStream.readline
				regNum = regNum + 1		
				posIdentificacao = instr(1, reg, xprefixo & "-", 1)
			loop

			if (pos = 0) then
				retornarComErro xPagina & ":IdentificaRevisao", "N�o foi poss�vel importar a lista pois o n�mero da revis�o da lista n�o foi  encontrado no arquivo. Verifique se a exporta��o foi feita corretamente.", destinoErro 
				debuga "ERRO! N�mero da revis�o da lista n�o encontrado."
				erro = 1
			else
				reviNumero = mid(reg, pos+5,2)
				debuga "N�mero da revis�o encontrada na linha " & regNum & " -->" & reviNumero
			end if
			reviNumero = cInt(reviNumero)
			'==============================================================================================
			'== Cria a revis�o no banco
			'==============================================================================================
			on error resume next
			xSQL = "exec LIST_Criar "
			xSQL = xSQL & "   @pprojId         = " & session("projId")
			xSQL = xSQL & " , @pimpoId         = " & ximpoId
			xSQL = xSQL & " , @previNumero     = " & reviNumero
			xSQL = xSQL & " , @plistSequencial = " & xlistSequencial
			xSQL = xSQL & " , @pusuaId         = " & session("usuaId")
			xSQL = xSQL & " , @plistNumProjeto = " & xNumProjeto
			debuga xSQL
			'response.end
			set xrs = gConexao.execute(xSQL)
			If Err.Number <> 0 then
				erro = Err.Description
				debuga "SQL:" & xSQL
				debuga "ERRO: " & erro
				mensagem = "N�o foi poss�vel concluir a importa��o pois ocorreu um erro:\nSQL:" & xSQL & "\nErro:" & erro
				if not debug="on" then
					retornarComErro xPagina & ":LIST_Criar", mensagem , destinoErro 
				end if
			end if

			xreviId = xrs("reviId")
			debuga "Revis�o: " & xreviId
			'==============================================================================================
			'== Procura o cabe�alho das colunas
			'==============================================================================================
			debuga "Procurando cabe�alho das colunas..."
			pos = instr(1, reg, "Descri��o Resumida", 1)
			Do While (pos = 0) and (not TextStream.AtEndOfStream)
		        reg = TextStream.readline
				regNum = regNum + 1		
				pos = instr(1, reg, "Descri��o Resumida", 1)
			loop
			if (pos = 0) then
				retornarComErro xPagina & ":IdentificaCabecalho", "N�o foi poss�vel importar a lista pois o cabe�alho das colunas n�o confere com o cabe�alho esperado. Verifique se a exporta��o foi feita corretamente.", destinoErro 
				debuga "ERRO! Cabe�alho das linhas n�o encontrado."
				erro = 1
			else
				revisao = mid(reg, pos+5,2)
				debuga "Cabe�alho das colunas encontrado na linha " & regNum 
		        reg = TextStream.readline  ' Pula uma linha				
		        reg = TextStream.readline  ' Pula uma linha				
		        reg = TextStream.readline  ' Pula uma linha				
			end if
			'== Inicia a importa��o dos itens
			Do While (not TextStream.AtEndOfStream)
				'== Pula linhas em branco
				if not (trim(replace(reg, vbtab, "")) = "") then
					if instr(reg, vbtab) = 0 then
						'== Identifica nova linha
						xNomeLinha = replace(reg, vbtab, "")
						xNomeLinha = left(xNomeLinha, len(xNomeLinha) - 1)
						xNomeLinha = right(xNomeLinha, len(xNomeLinha) - 1)
						linha = split(xnomelinha, "-")
						xBitola = linha(0)
						xCod1 = linha(1)
						if ubound(linha) <2 then
							xCod2 = "-"
						else
							xCod2 = linha(2)
						end if
						debuga "Nova linha encontrada (" & regNum & ") : " & xNomeLinha
						'== Cria nova linha
						xSQL = "exec PCAD_Linha "
						xsql = xsql & "   @previId     =  " & xreviId
						xsql = xsql & " , @plinhNome   = '" & xNomeLinha & "'"
						xsql = xsql & " , @plinhBitola = '" & xBitola & "'"
						xsql = xsql & " , @plinhCod1   = '" & xCod1 & "'"
						xsql = xsql & " , @plinhCod2   = '" & xCod2 & "'"
						debuga xSQL 
						set xrs = gConexao.execute(xSQL)
						If Err.Number <> 0 then
							erro = Err.Description
							debuga "SQL:" & xSQL
							debuga "ERRO: " & erro
							mensagem = "N�o foi poss�vel concluir a importa��o pois ocorreu um erro:\nSQL:" & xSQL & "\nErro:" & erro
							if not debug="on" then
								retornarComErro xPagina & ":PCAD_Linha", mensagem , destinoErro 
							end if
						end if
						xlinhId = xrs("linhId")
						xrs.close
						set xrs = nothing
						debuga "Linha: " & xNomeLinha & "--> ID:" & xlinhId
					else
						'== Registra itens da linha
						'debuga reg
						'on error resume next
						coluna = Split(reg, vbtab)
						ordem = replace(coluna(0), """", "")
						codJde = coluna(1)
						if err.number <> 0 then
							debuga "Erro na linha: <" & reg & ">"
						end if
						descricao = coluna(2)
						descricao = left(descricao, len(descricao) - 1)
						descricao  = right(descricao, len(descricao) - 1)
						unidade = replace(coluna(3), """", "")
						revQtdAnt = campoConvertido(coluna(4))
						revNumAnt = coluna(5)
						revQtdAtual = campoConvertido(coluna(6))
						revNumAtual = coluna(7)
						obs = replace(coluna(8), """", "")

						debuga "<br>Item encontrado: " & ordem
						debuga "codJDE    -->" & codJde & "<--"
						debuga "descricao -->" & descricao & "<--"
						debuga "unidade   -->" & unidade & "<--"
						debuga "qtdAnt    -->" & revQtdAnt & "<--"
						debuga "revAnt    -->" & revNumAnt & "<--"
						debuga "qtdAtual  -->" & revQtdAtual & "<--"
						debuga "revAtual  -->" & revNumAtual & "<--"
						debuga "Obs       -->" & obs & "<--"
						if trim(revQtdAtual) = "" then
							revQtdAtual = revQtdAnt
						end if
						xSQL = " exec PCAD_ItemLista"
						xSQL = xSQL & "   @plinhId       = " & xlinhId
						xSQL = xSQL & "  ,@punidSigla    = '" & unidade & "'"
						xSQL = xSQL & "  ,@pitelOrdem    = " & ordem
						xSQL = xSQL & "  ,@pitemCodJde   = " & codJde
						xSQL = xSQL & "  ,@pitelTag      = null"
						xSQL = xSQL & "  ,@previId       = " & xreviId
						xSQL = xSQL & "  ,@pitelQtd      = " & revQtdAtual
						xSQL = xSQL & "  ,@pitemDescricao = '" & descricao & "'"
						debuga xSQL
						on error resume next
						set xrs = gConexao.execute(xSQL)
						If Err.Number <> 0 then
							erro = Err.Description
							debuga "SQL:" & xSQL
							debuga "ERRO: " & erro
							mensagem = "N�o foi poss�vel concluir a importa��o pois ocorreu um erro:\nSQL:" & xSQL & "\nErro:" & erro
							if not debug="on" then
								retornarComErro xPagina & ":PCAD_ItemLista", mensagem , destinoErro 
							end if
						end if
						xitelId = xrs("itelId")
						xrs.close
						set xrs = nothing
					end if
				end if
				'== L� proximo registro
				reg = TextStream.readline
				regNum = regNum + 1		
			loop
	    
	    end if
		TextStream.close
	    Set TextStream = nothing
		Session("msg") = "Lista carregada com sucesso!"
		Set FSO = nothing
		if not debug="on" then
			response.redirect("gerMensagem.asp")
		end if
	Else
		Session("msg") = "Arquivo (" & xArquivo & ") n�o encontrado!"	    
		debuga "Arquivo (" & xArquivo & ") n�o encontrado!"
		Set FSO = nothing
		if not debug="on" then
			response.redirect("gerMensagem.asp")
		end if
	End If
%>