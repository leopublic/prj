<% Response.Expires= -1%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Controle remoto de projetos</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<script language="JavaScript" type="text/javascript" src="../scripts/PreparaJanela.js"></script>
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<script language="JavaScript" type="text/javascript" src="../scripts/CheckNumeric.js"></script>
		<SCRIPT language="JavaScript">
			function submitform(){
			  document.form1.submit();
			}
			function validar(){
			  if (document.form1.txtNome.value == '')
			  {   alert('O preenchimento do nome � obrigatorio.');
			  	  document.form1.txtNome.select();
				  document.form1.txtNome.focus(); }
  			  {   if (chkNumeric(document.form1.txtOrd,0,9999999999,'','','')==false)
			      {   alert('A ordem deve ser um valor num�rico');
				      document.form1.txtOrd.select();
					  document.form1.txtOrd.focus();}
				  else
					  { submitform();}
			   }
			}
		</SCRIPT> 
	</HEAD>
	<body class="PopUpEntrada" onLoad="PreparaJanela(430,230);">
<% 
    lPerfisAutorizados = "51"
	VerificarAcesso
	Dim gConexao 
	AbrirConexao
	servId = Request.Querystring("servId")
	if servId = 0 then
	    titulo = "Cadastrar novo servi�o"
		Session("TituloMsg") = titulo
		Session("Msg") = "Servi�o inclu�do com sucesso!"
		servNome = ""
	else
		xSQL = "select servNome, servOrd from Servico where servId = " & servId
		set xrs = gConexao.execute(xSQL)
		titulo = "Alterar servi�o"
		Session("TituloMsg") = titulo
		Session("Msg") = "Servi�o alterado com sucesso!"
		servNome = xrs("servNome")
		servOrd = xrs("servOrd")
		xRs.CLose
		Set xRs = nothing
	end if
%>

	<form action="cfgServAlterar2.asp" method="post" name="form1">
		<input type="hidden" name="servId" value=" <% = servId %> " >
		<table width="100%">
			<tr>
				<td width="15"></td>
				<td></td>
			</tr>
			<tr>				
				<td colspan="2" class="docTit"><% = titulo %></td>
			</tr>
			<tr>
				<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
			</tr>
			<tr>
				<td colspan="2" height="15"></td>
			</tr>
			<tr>
				<td></td>
				<td>
					<table class="doc">
						<tr>
							<td width="100" class="docLabel">Nome</td>
							<td width="5" class="docLabel">:</td>
							<td class="docCmpLivre"><input type="text" name="txtNome" class="docCmpLivre" value="<% = servNome %>"></td>
						</tr>
						<tr>
							<td width="100" class="docLabel">Ordem</td>
							<td width="5" class="docLabel">:</td>
							<td class="docCmpLivre"><input type="text" name="txtOrd" class="docCmpLivre" value="<% = servOrd %>"></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2" height="20"></td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<table>
						<tr>
							<td height="28" width="45%" align="center"><a href="javascript:validar();"><img src="../images/btnSalvar.gif" class="botao"></a></td>
							<td width="10%"></td>
							<td width="45%" align="center"><a href="javascript:window.close();"><img src="../images/btnCancelar.gif" class="botao"></a></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
	</body>
</HTML>