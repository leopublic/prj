<% Response.Expires= -1%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Projetos</title><%%>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<script language="JavaScript" type="text/javascript" src="../scripts/PreparaJanela.js"></script>
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<!--#include file="../includes/basFormatador.inc"-->
		<!--#include file="../includes/basValidacao.inc"-->
		<script language="JavaScript" type="text/javascript" src="../scripts/NumberFormat.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/Mid.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/ValidacaoDatas.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/PreparaJanela.js"></script>

		<script language="JavaScript">
		var submitted = 0; // the form is not submitted
		function submit_once() {
		
			if (!submitted) { // if it's not submitted
		        document.form1.submit(); // submit it
		        submitted = 1 // it is now submitted
			}
		} 
		</script>
		<SCRIPT language="JavaScript">
			function CamposOk(){
			   var Cmp = document.form1.SemIni
				if(Cmp.value!='')
				{	if(isDate(Cmp)==false)
					{	Cmp.select();
						Cmp.focus();
						return false;
					}
				}
			   var Cmp = document.form1.SemFim
				if(Cmp.value!='')
				{	if(isDate(Cmp)==false)
					{	Cmp.select();
						Cmp.focus();
						return false;
					}
				}
				return true
			}

			function Validar(){
                if (CamposOk()==true)
			    { document.form.submit();}
			}
		</SCRIPT> 
	</HEAD>
	<body class="PopUpEntrada" onLoad="javascript:PreparaJanela(580,350);">
<% 
    lPerfisAutorizados = "23451"
	VerificarAcesso
	Dim gConexao 
	AbrirConexao
	projId = Request.Querystring("projId")
	xSQL = ""
	xSQL = xSQL & "select convert(varchar(10), projSemIni, 3) SemIni, convert(varchar(10), projSemFim , 3) SemFim"
	xSQL = xSQL & "  from Projeto"
	xSQL = xSQL & " where projId = " & projId
	set xrs = gConexao.execute(xSQL)
	titulo = Session("projNome") & " - Emiss�o de relat�rio de progresso"
	semIni = xRs("SemIni")
	semFim = xRs("SemFim")
	xRs.CLose
	Session("TituloMsg") = titulo
	Set xRs = nothing
%>

	<form  onSubmit="Validar();" action="prjRelpLiberar2.asp" method="post" name="form1">
		<input type="hidden" name="projId" value="<% = projId %>" >
		<table width="90%">
			<tr>
				<td width="15px"></td>
				<td></td>
			</tr>
			<tr>
				<td colspan="2" class="docTit"><% = titulo %></td>
			</tr>
			<tr>
				<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
			</tr>
			<tr>
				<td colspan="2" height="15"></td>
			</tr>
			<tr>
				<td colspan="2" class="doclabel">Voc� confirma a emiss�o do relat�rio de progresso da seguinte semana: </td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td align="left">
					<table class="doc">
						<tr>
							<td width="120px" class="docLabel">In�cio</td>
							<td width="05px" class="docLabel">:</td>
							<td width="100px" class="docSBorda"><input type="text" name="SemIni" value="<% = semIni %>" class="docCmpLivre" style="height:20;border:1;border-style:solid;border-color:#CCCCCC;text-align:center" onblur="javascript:this.value=FormatarData(this);"></td>
							<td class="docLabel">&nbsp;</td>
						</tr>
						<tr>
							<td class="docLabel">Fim</td>
							<td class="docLabel">:</td>
							<td class="docSBorda"><input type="text" name="SemFim" value="<% = semFim %>" class="docCmpLivre" style="height:20;border:1;border-style:solid;border-color:#CCCCCC;text-align:center" onblur="javascript:this.value=FormatarData(this);"></td>
							<td class="docLabel">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2" height="15"></td>
			</tr>
			<tr>
				<td colspan="2" class="docSubTit" align="center">ATEN��O: Depois de emitido o relat�rio n�o poder� ser alterado ou corrigido.</td>
			</tr>
			<tr>
				<td colspan="2" height="15"></td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<table>
						<tr>
							<td height="28" width="80" align="center"><a id="submit" href="javascript:submit_once();"><img src="../images/btnEmitir.gif" class="botao"></a></td>
							<td width="10"></td>
							<td width="80" align="center"><a href="javascript:window.close();"><img src="../images/btnCancelar.gif" class="botao"></a></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
	</body>
</HTML>