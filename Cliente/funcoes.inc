<%

Function AcertaCaracteresHTML(pString)
	Dim xVar
	xVar = pString
	'xVar = Replace(xVar, "<", "&lt;")
	'xVar = Replace(xVar, ">", "&gt;")
	'xVar = Replace(xVar, "&", "&amp;")

	xVar = Replace(xVar, "�", "&ETH;")
	xVar = Replace(xVar, "�", "&iexcl;")
	xVar = Replace(xVar, "�", "&Ntilde;")
	xVar = Replace(xVar, "�", "&cent;")
	xVar = Replace(xVar, "�", "&Ograve;")
	xVar = Replace(xVar, "�", "&pound;")
	xVar = Replace(xVar, "�", "&Oacute;")
	xVar = Replace(xVar, "�", "&curren;")
	xVar = Replace(xVar, "�", "&Ocirc;")
	xVar = Replace(xVar, "�", "&yen;")
	xVar = Replace(xVar, "�", "&Otilde;")
	xVar = Replace(xVar, "�", "&brvbar;")
	xVar = Replace(xVar, "�", "&Ouml;")
	xVar = Replace(xVar, "�", "&sect;")
	xVar = Replace(xVar, "�", "&times;")
	xVar = Replace(xVar, "�", "&uml;")
	xVar = Replace(xVar, "�", "&Oslash;")
	xVar = Replace(xVar, "�", "&copy;")
	xVar = Replace(xVar, "�", "&Ugrave;")
	xVar = Replace(xVar, "�", "&ordf;")
	xVar = Replace(xVar, "�", "&Uacute;")
	xVar = Replace(xVar, "�", "&laquo;")
	xVar = Replace(xVar, "�", "&Ucirc;")
	xVar = Replace(xVar, "�", "&not;")
	xVar = Replace(xVar, "�", "&Uuml;")
	xVar = Replace(xVar, "�", "&shy;")
	xVar = Replace(xVar, "�", "&Yacute;")
	xVar = Replace(xVar, "�", "&reg;")
	xVar = Replace(xVar, "�", "&THORN;")
	xVar = Replace(xVar, "�", "&macr;")
	xVar = Replace(xVar, "�", "&szlig;")
	xVar = Replace(xVar, "�", "&deg;")
	xVar = Replace(xVar, "�", "&agrave;")
	xVar = Replace(xVar, "�", "&plusmn;")
	xVar = Replace(xVar, "�", "&aacute;")
	xVar = Replace(xVar, "�", "&sup2;")
	xVar = Replace(xVar, "�", "&acirc;")
	xVar = Replace(xVar, "�", "&sup3;")
	xVar = Replace(xVar, "�", "&atilde;")
	xVar = Replace(xVar, "�", "&acute;")
	xVar = Replace(xVar, "�", "&auml;")
	xVar = Replace(xVar, "�", "&micro;")
	xVar = Replace(xVar, "�", "&aring;")
	xVar = Replace(xVar, "�", "&para;")
	xVar = Replace(xVar, "�", "&aelig;")
	xVar = Replace(xVar, "�", "&middot;")
	xVar = Replace(xVar, "�", "&ccedil;")
	xVar = Replace(xVar, "�", "&cedil;")
	xVar = Replace(xVar, "�", "&egrave;")
	xVar = Replace(xVar, "�", "&sup1;")
	xVar = Replace(xVar, "�", "&eacute;")
	xVar = Replace(xVar, "�", "&ordm;")
	xVar = Replace(xVar, "�", "&ecirc;")
	xVar = Replace(xVar, "�", "&raquo;")
	xVar = Replace(xVar, "�", "&euml;")
	xVar = Replace(xVar, "�", "&frac14;")
	xVar = Replace(xVar, "�", "&igrave;")
	xVar = Replace(xVar, "�", "&frac12;")
	xVar = Replace(xVar, "�", "&iacute;")
	xVar = Replace(xVar, "�", "&frac34;")
	xVar = Replace(xVar, "�", "&icirc;")
	xVar = Replace(xVar, "�", "&iquest;")
	xVar = Replace(xVar, "�", "&iuml;")
	xVar = Replace(xVar, "�", "&Agrave;")
	xVar = Replace(xVar, "�", "&eth;")
	xVar = Replace(xVar, "�", "&Aacute;")
	xVar = Replace(xVar, "�", "&ntilde;")
	xVar = Replace(xVar, "�", "&Acirc;")
	xVar = Replace(xVar, "�", "&ograve;")
	xVar = Replace(xVar, "�", "&Atilde;")
	xVar = Replace(xVar, "�", "&oacute;")
	xVar = Replace(xVar, "�", "&Auml;")
	xVar = Replace(xVar, "�", "&ocirc;")
	xVar = Replace(xVar, "�", "&Aring;")
	xVar = Replace(xVar, "�", "&otilde;")
	xVar = Replace(xVar, "�", "&AElig;")
	xVar = Replace(xVar, "�", "&ouml;")
	xVar = Replace(xVar, "�", "&Ccedil;")
	xVar = Replace(xVar, "�", "&divide;")
	xVar = Replace(xVar, "�", "&Egrave;")
	xVar = Replace(xVar, "�", "&oslash;")
	xVar = Replace(xVar, "�", "&Eacute;")
	xVar = Replace(xVar, "�", "&ugrave;")
	xVar = Replace(xVar, "�", "&Ecirc;")
	xVar = Replace(xVar, "�", "&uacute;")
	xVar = Replace(xVar, "�", "&Euml;")
	xVar = Replace(xVar, "�", "&ucirc;")
	xVar = Replace(xVar, "�", "&Igrave;")
	xVar = Replace(xVar, "�", "&uuml;")
	xVar = Replace(xVar, "�", "&Iacute;")
	xVar = Replace(xVar, "�", "&yacute;")
	xVar = Replace(xVar, "�", "&Icirc;")
	xVar = Replace(xVar, "�", "&thorn;")
	xVar = Replace(xVar, "�", "&Iuml;")
	xVar = Replace(xVar, "�", "&yuml;")


	AcertaCaracteresHTML = xVar
End Function

Function StringToJS(pString)
    StringToJS = replace(replace(pString,vbcrlf,"\n"),"'","`")
End Function

Function StringToBD(pString)
	if ucase(trim(pString)) <> "null" then
		pString = replace(pString,"'","''")
		pString = "'" & pString & "'"
	end if
	StringToBD = pString
End Function

Function DateToBD(pDate)
	Session.LCID = 1046 'Tow colocando pra pt-Br, com isso, o formato de data � dd/MM/yyyy
	if trim(pDate) = "" then
		pDate = "null"
	else
		if IsDate(pDate) then
			pDate = FormatDateTime(pDate, 0)
			pDate = "'" & split(pDate,"/")(2) & "-" & split(pDate,"/")(1) & "-" & split(pDate,"/")(0) & "'"
		else
			pDate = "ERRO"
		end if
	end if
	
	DateToBD = pDate
End Function

Function NumberToBD(pNumber,pDecimais)
	Session.LCID = 1046
	if trim(pNumber) = "" then
		pNumber = "null"
	else
		if IsNumeric(pNumber) then
			pNumber = FormatNumber(pNumber,pDecimais,,,0)
			'Agora q eu tenho meu n�mero sem separador d milhar e c/ o separador decimal como sendo a v�rgula, posso 
			'dar o replace sem erro =D
			pNumber = replace(pNumber,",",".")
		else
			pNumber = "ERRO"
		end if
	end if
		
	NumberToBD = pNumber
End Function

Function CurrencyToBD(pNumber,pDecimais)
	CurrencyToBD = NumberToBD(trim(replace(pNumber,"R$","")),pDecimais)
End Function

Function ProjectDateToBD(pDate)
    Dim Dias
    Dim i
    Dias = Array("SUN","MON","THU","WED","TUE","FRI","SAT","DOM","SEG","TER","QUA","QUI","SEX","SAB")
    nDate = replace(ucase(trim(pDate)),"NA","")
    if trim(nDate) <> "" then
        For i = 0 to ubound(Dias)
            nDate = replace(nDate,Dias(i),"")
        Next    
    end if
	ProjectDateToBD = DateToBD(nDate)
End Function

Function FormataData_DDMMYYYY(pData)
	Session.LCID = 1046
	if isdate(pData) then
		pData = right("00" & Day(CDate(pData)),2) & "/" & right("00" & Month(CDate(pData)),2) & "/" & right("0000" & Year(CDate(pData)),4)
	else
		pData = ""
	end if
	FormataData_DDMMYYYY = pData
End Function	

Function FormataNumero(pNumber, pDecimais)
	Session.LCID=1046
	if not isNumeric(pDecimais) then
		pDecimais = 0
	end if
	If isNumeric(pNumber) then
		pNumber = FormatNumber(pNumber,pDecimais,,,0)
	else
		pNumber = ""
	end if
	FormataNumero = pNumber
End Function

'----
'-- CARGA DE NODES PARA TREE DE WBS CONTRATO
'----

Public Function CarregaItemWBSContrato(pNM_Tree, pwbscId, piwbcId_Pai, pFL_Tipo, pfcn_Id)

	lSQL = ""
	lSQL = lSQL & "select i.iwbcId, isnull(i.iwbcId_Pai,0) as iwbcId_Pai, i.tiwbId, i.unimId, i.iwbcCodigo, replace(i.iwbcNome,'""','��') as iwbcNome, replace(i.iwbcDetalhamento,'""','��') as iwbcDetalhamento,i.iwbcPercPai, i.iwbcFatorHH, i.iwbcValUnitEst, isnull(i.iwbcFLFilho,0) as iwbcFLFilho, isnull(t.tiwbDescricao,'') as tiwbDescricao, isnull(u.unimAbrev,'') as unimAbrev " 
	lSQL = lSQL & "  from (itemWbsContrato i left join TipoItemWbs t on i.tiwbId = t.tiwbId) left join UnidadeMedida u on i.unimId = u.unimId " 
	lSQL = lSQL & " where isnull(i.wbscId,0) = isnull(" & pwbscId & ",0)"
    if ((trim(pfcn_Id) <> "") and isnumeric(pfcn_Id))then
        lSQL = lSQL & "   and i.fcn_id = " & pfcn_Id
    else
        lSQL = lSQL & "   and i.fcn_id is null "
    end if
	if trim(piwbcId_Pai) = "" then
		lSQL = lSQL & " and i.iwbcId_Pai is null " 
	else
		lSQL = lSQL & " and i.iwbcId_Pai = " & piwbcId_Pai
	end if
	lSQL = lSQL & " order by i.iwbcCodigo, i.iwbcId " 
    '
	Dim gConexao
	AbrirConexao()
    'response.write "connectionString -> " & Session("connectionString") & " <- "
    'a = false
    'if a then
	Set gConexao = server.createobject("ADODB.Connection")
	gConexao.ConnectionString = Session("connectionString")
	gConexao.Open
	'
	TX_Itens = ""
	'
	Dim lRS_1
	Set lRS_1 = gConexao.execute(lSQL)
	
	If (not lRS_1.BOF) and (not lRS_1.EOF) then
		lNR_I = 1
		While not lRS_1.EOF
            '(id, pid, name, url, title, target, icon, iconOpen, open)
            lFL_Icone = ""
            lTX_Descricao = lRS_1("iwbcCodigo") & " - " & lRS_1("iwbcNome")
			if (lRS_1("iwbcFLFilho")=0) then
                if(pFL_Tipo="CWBS") xor (pFL_Tipo="CADM") then
                    lFL_Icone = ","""",""tree/img/page.gif"", ""tree/img/page.gif"""
                end if
                lTX_Descricao = lTX_Descricao & " - Un: " & lRS_1("unimAbrev") & " - Tp Wbs: " & lRS_1("tiwbDescricao")
			end if
			lTX_Itens = lTX_Itens & pNM_Tree & ".add(" & lRS_1("iwbcId") & ", " & lRS_1("iwbcId_Pai") & ", """ & lTX_Descricao 
            
            if(pFL_Tipo="CWBS") then
				lTX_Itens = lTX_Itens & " <a href='#' onclick='javascript:AddItemWbs(" & lRS_1("iwbcId") & ");' title='Adicionar item' targe=_self>>></a>"",""""" & lFL_Icone & ");" & vbcrlf
			elseif(pFL_Tipo="CADM") then
				lTX_Itens = lTX_Itens & " <a href='#' title='Excluir item' targe=_self><img src='../images/trash.jpg' onclick='javascript:ExcluirItem(" & lRS_1("iwbcId") & ");'></a>"",""javascript:AbrirDetalheItem('" & lRS_1("iwbcId") & "', '" & lRS_1("iwbcId_Pai") & "', '" & lRS_1("tiwbId") & "', '" & lRS_1("unimId") & "', '" & lRS_1("iwbcCodigo") & "', '" & lRS_1("iwbcNome") & "', '" & lRS_1("iwbcDetalhamento") & "','" & lRS_1("iwbcPercPai") & "', '" & lRS_1("iwbcFatorHH") & "', '" & lRS_1("iwbcValUnitEst") & "', '" & lRS_1("iwbcFLFilho") & "');"",""Alterar item""" & lFL_Icone & ");" & vbcrlf
            elseif(pFL_Tipo="IWBC") then
                lTX_Itens = lTX_Itens & " <a href='#' title='Excluir item' targe=_self><img src='../images/trash.jpg' onclick='javascript:ExcluirItem(" & lRS_1("iwbcId") & ");'></a>"",""javascript:setadestino(" & lRS_1("iwbcId") & ");"");" & vbcrlf
			end if
            
			if(lRS_1("iwbcFLFilho")=1) then
				lTX_Itens = lTX_Itens & CarregaItemWBSContrato(pNM_Tree, pwbscId, lRS_1("iwbcId"), pFL_Tipo, pfcn_Id)
            elseif(pFL_Tipo="CADM") then
				lTX_Itens = lTX_Itens & pNM_Tree & ".add('N_" & lRS_1("iwbcId") & "', " & lRS_1("iwbcId") & ", ""<a href='#' onclick='javascript:AddNewItemWbs(" & lRS_1("iwbcId") & ");' title='Adicionar novo item' targe=_self>Adicionar novo item</a>"",""Adicionar novo item"","""",""tree/img/page_add.gif"",""tree/img/page_add.gif"");" & vbcrlf
			End If
            '
			lRS_1.MoveNext
		Wend
	End If
    if(pFL_Tipo="CADM") then
        if piwbcId_Pai = "" then piwbcId_Pai = "0"
		lTX_Itens = lTX_Itens & pNM_Tree & ".add('N_" & piwbcId_Pai & "', " & piwbcId_Pai & ", ""<a href='#' onclick='javascript:AddNewItemWbs(" & piwbcId_Pai & ");' title='Adicionar novo item' targe=_self>Adicionar novo item</a>"",""Adicionar novo item"","""",""tree/img/page_add.gif"",""tree/img/page_add.gif"");" & vbcrlf
	end if
	lRS_1.close
    'end if
    '
	CarregaItemWBSContrato = lTX_Itens
End Function

Function NovoErroXML(pNum_Erro, pDesc_Erro, pTX_SQL)
	lNE = ""
	lNE = lNE  & "<ERRO>"
	lNE = lNE  & "	<NUM_ERRO>" & pNum_Erro & "</NUM_ERRO>"
	lNE = lNE  & "	<DESC_ERRO>" & pDesc_Erro & "</DESC_ERRO>"
	lNE = lNE  & "	<QUERY>" & pTX_SQL & "</QUERY>"
	lNE = lNE  & "</ERRO>"

	NovoErroXML = lNE
End function

function pCad_NovoId(pTabela, pConexao)
	lnovoId = ""
	set lRS = pConexao.execute("select (isnull(c.ctrlId,0)+1) as novoId from controleId c where ctrlEntidade = '" & pTabela & "'")
	lnovoId = "1"
	if ((lRS.BOF) and (lRS.EOF)) then 'N�o existe
		lsql = "insert into controleId (ctrlId, ctrlEntidade) values(" & lnovoId & ", '" & pTabela & "')"
	else
		lnovoId = lRS("novoId")
		lsql = "update controleId set ctrlId = " & lnovoId & " where ctrlEntidade = '" & pTabela & "'"
	end if
	lRS.close
	set lRS = nothing
	
	pConexao.execute(lsql)
		
	pCad_NovoId = lnovoId
end function

Function trimaAspas(campo)
	campo = left(campo, len(campo) - 1)
	campo = right(campo, len(campo) - 1)
	trimaAspas = campo
end function

Sub debuga (msg)
	'xSQLD = "exec PCAD_MensagemImportacao @pimpoId = " & ximpoId & ", @pmsgiTexto = '" & replace(msg, "'", "''") & "'"
	'gConexao.execute(xSQLD)
	
	if debug="on" then
		response.write "<br>" & msg
	end if
end sub

Function campoConvertido(campo) 
	valor = trim(replace(campo, ",", "."))
	campoConvertido = valor
end function

Sub retornarComErro(rotina, msg, destino)
	session("msgErro") = "(" & rotina & ")\n" & replace(msg, "'", "\'")
	response.Redirect (destino)
	response.End()
end sub
%>