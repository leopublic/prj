<% Response.Expires= -1%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Controle de projetos pela internet</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<link rel="stylesheet" type="text/css" href="../EstiloMenu.css">
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsProjeto.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<script language="JavaScript" type="text/javascript" src="../scripts/AbrirJanela.js"></script>
	</HEAD>
	<body class="cabecalho" >
<%  lPerfisAutorizados = "123457"
	VerificarAcesso()
	arq = request.querystring("arq")
	titarq = ""
	if arq = "1" then
		titarq = "ARQUIVADOS"
	end if
   Dim gmenuGeral
   Dim gMenuSelecionado
   Dim gOpcaoSelecionada
   gMenuSelecionado = "Projetos"
   if arq="1" then
	   gOpcaoSelecionada = "Arquivados"
   else
	   gOpcaoSelecionada = "Projetos"
   end if
   gmenuGeral = "Projetos"
   PastaSelecionada = "Projetos"

%>
		<!--#include file="../includes/cab.inc"-->
			<tr>
				<td style="padding:0px" valign="top" class="blocoOpcoes">
					<!--#include file="../includes/opcProjetos.inc"-->
				</td>
				<td colspan="2" valign="top" style="padding:5px" class="blocoPagina">
				<form>
					<table width="100%">
					<tr>
						<td class="docTit">PROJETOS <%=titarq%></td>
					</tr>
					<tr>
						<td background="../images/pont_cinza_h.gif" style="height:2"></td>
					</tr>
					<tr>
						<td style="height:5"></td>
					</tr>
					<tr>
						<td>
							<table class="grid">
									<% 	xCampoOrdem = Request.QueryString("CampoOrdem") 
										if xCampoOrdem = "" then
											xCampoOrdem = "projNum"
										end if
										SortNum = ""
										SortNome = ""
										SortNomeGer = ""
										SortNomeSup = ""
										if xCampoOrdem = "projNum" then SortNum = "<img src=""../images/sort.gif"">"
										if xCampoOrdem = "projNome" then SortNome = "<img src=""../images/sort.gif"">"
										if xCampoOrdem = "projNomeGer" then SortNomeGer = "<img src=""../images/sort.gif"">"
										if xCampoOrdem = "projNomeSup" then SortNomeSup = "<img src=""../images/sort.gif"">"
									%>
									<tr>
										<td width="20" class="GridCab">&nbsp;</td>
										<td width="90" class="GridCab" valign="bottom"><a href="prjListarAtivos.asp?CampoOrdem=projNum" class="GridCab">N�mero</a><%=SortNum%></td>
										<td width="230" class="GridCab" valign="bottom"><a href="prjListarAtivos.asp?CampoOrdem=projNome" class="GridCab">Nome</a><%=SortNome%></td>
										<td width="210" class="GridCab" valign="bottom"><a href="prjListarAtivos.asp?CampoOrdem=projNomeGer" class="GridCab">Gerente do<br>projeto</a><%=SortNomeGer%></td>
										<td width="210" class="GridCab" valign="bottom"><a href="prjListarAtivos.asp?CampoOrdem=projNomeSup" class="GridCab">Supervisor de<br>constru��es</a><%=SortNomeSup%></td>
										<td class="GridCab">&nbsp;</td>
									</tr> 
	<%
	Dim gConexao 
	Dim gConexao2
	Dim projId() 
	i = -1
	xSQL = ""
	xSQL = xSQL & "select Projeto.projId"
	xSQL = xSQL & "     , projNum"
	xSQL = xSQL & "     , projNome"
	xSQL = xSQL & "     , isnull(U1.usuaNome, '(n�o definido)') projNomeGer"
	xSQL = xSQL & "     , isnull(U2.usuaNome, '(n�o definido)') projNomeSup"
	xSQL = xSQL & "     , isnull(emprNome, '(n�o definido)') emprNome"
	xSQL = xSQL & "  from (((Projeto left join Empreiteira on Empreiteira.emprId = Projeto.emprIdSupCmp )"
	xSQL = xSQL & "       left join Usuario U1 on U1.usuaId = usuaIdGer)"
	xSQL = xSQL & "       left join Usuario U2 on U2.usuaId = usuaIdSup)"
	if arq="1" then
		xSQL = xSQL & " where projAtivo = 0"
	else
		xSQL = xSQL & " where projAtivo = 1"
	end if
	if Session("perfId") = "3" then
		xSQL = xSQL & " and  projId in (select projId from Contrato where Contrato.usuaIdCont = " & Session("usuaId")  & ")"
	else
		if Session("perfId") = "4" then
			'xSQL = xSQL & " and emprIdSupCmp = " & Session("emprId")
			' --> Trocou pelo relacionamento com a usu�rio
			'xSQL = xSQL & " and ( (projId in (Select projId from ProjetoEmpreiteiraSupCmp where emprId = " & Session("emprId") & ")"
			xSQL = xSQL & " and projId in (Select projId from ProjetoUsuarioSupCmp where usuaId = " & Session("usuaId") & ") "
		end if
	end if
	xSQL = xSQL & " order by " & xCampoOrdem
	AbrirConexao
	AbrirConexao2
	Set xRs = gConexao.Execute(xSQL)
	if xRs.EOF then
	    Response.write "<tr>" & vbcrlf
	    Response.write "<td colspan=""6"">Nenhum projeto cadastrado</td>" & vbcrlf
	    Response.write "</tr>" & vbcrlf
	else
		estilo = "GridLinhaPar"
		while not xRs.EOF
			Response.write "<tr>" & vbcrlf
			response.write vbtab & "<td class=""" & estilo & """><a href=""prjCarregar.asp?projId=" & xRs("projId") & """><img src=""../images/folder.gif"" style=""border:0""></a></td>"
			response.write vbtab & "<td class=""" & estilo & """>" & xRs("projNum") & "</td>"
			response.write vbtab & "<td class=""" & estilo & """>" & xRs("projNome") & "</td>"
			response.write vbtab & "<td class=""" & estilo & """>" & xRs("projNomeGer") & "</td>"
			response.write vbtab & "<td class=""" & estilo & """>" & xRs("projNomeSup") & "</td>"
			response.write vbtab & "<td class=""" & estilo & """>&nbsp;</td>"
			Response.write "</tr>" & vbcrlf
			xRs.MoveNext
			if estilo = "GridLinhaPar" then
				estilo = "GridLinhaImpar"
			else
				estilo = "GridLinhaPar"
			end if
		wend
	end if
	gConexao2.close

	%>
								<tr>
									<td colspan="6" class="GridRodape">&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td class="opcSubMenu">(<b>dica</b>: clique no t�tulo das colunas para mudar a ordena��o da consulta.)</td>
					</tr>
					</table>
				</form>
				</td>
			</tr>			
		<!--#include file="../includes/rod.inc"-->
	</body>
</HTML>