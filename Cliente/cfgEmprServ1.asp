<% Response.Expires= -1%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Controle remoto de projetos</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<script language="JavaScript" type="text/javascript" src="../scripts/PreparaJanela.js"></script>
		<SCRIPT language="JavaScript">
			function submitform(){
			  document.form1.submit();
			}
			function validar(){
				 submitform();
			}
		</SCRIPT> 
		<style type="text/css">
		<!--
		input.docCmpLivre		{font-family:tahoma;font-size:8pt;font-weight:normal;color:#1D4F68;border:0;width:100%}
		-->
		</style>
	</HEAD>
	<body class="PopUpEntrada" onLoad="javascript:PreparaJanela(600,600);">
<% 
    lPerfisAutorizados = "23451"
	VerificarAcesso
	Dim gConexao 
	AbrirConexao
	emprId = Request.Querystring("emprId")
	xSQL = "select emprNome from Empreiteira where emprId = " & emprId
	set xRs = gConexao.execute(xSQL)
	emprNome = xRs("emprNome")
	xRs.close
	set xRs = Nothing
	titulo = emprNome & " - Servi�os homologados"
	Session("TituloMsg") = titulo
%>
	<form action="cfgEmprServ2.asp" method="post" name="form1">
		<input type="hidden" name="emprId" value=" <% = emprId %> " >
		<table width="100%">
			<tr>
				<td width="15"></td>
				<td></td>
			</tr>
			<tr>
				<td colspan="2" class="docTit"><% = titulo %></td>
			</tr>
			<tr>
				<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
			</tr>
			<tr>
				<td colspan="2" height="15"></td>
			</tr>
			<tr>
				<td colspan="2" class="docSBorda">Indique os servi�os para os quais esta empreiteira est� homologada:</td>
			</tr>
			<tr>
				<td width="15"></td>
				<td>
					<table class="doc">
						<tr>
							<td width="10%"></td><td width="40%"></td>
							<td width="10%"></td><td width="40%"></td>
						</tr>
<%
	xSQL = ""
	xSQL = xSQL & "select servNome, Servico.servId, emprId"
	xSQL = xSQL & "  from Servico left join EmpreiteiraServico on EmpreiteiraServico.servId = Servico.servId and EmpreiteiraServico.emprId = " & emprId 
	xSQL = xSQL & " order by servNome"
	
	set xrs = gConexao.execute(xSQL)
	x = 1
	Linha = ""
	while not xRs.eof 
		if xRs("emprId") <> "" then
			xSelected = "checked=""1""" 
		else
			xSelected = " "
		end if
		Campo = "<td class=""docSBorda"" align=""right"" style=""vertical-align:top""><input type=""checkbox"" name=""servId" & xRs("servId") & """ id=""servId" & xRs("servId") & """ " & xSelected & "></td><td class=""docSBorda"" style=""vertical-align:top"">" & xRs("servNome") & "</td>"
		if x = 1 then
			x = 2
			Linha = Campo
		else
			Linha = Linha & Campo
			Linha = "<tr>" & vbcrlf & Linha & vbcrlf & "</tr>" & vbcrlf
			response.write Linha
			x = 1
		end if
		xRs.MoveNext
	wend 
	if x = 2 then
		Linha = "<tr>" & vbcrlf & Linha & vbcrlf & "<td>&nbsp;</td><td>&nbsp;</td></tr>" & vbcrlf
		response.write Linha
	end if		
	xRs.CLose
	Set xRs = nothing
%>
					</table>

				</td>
			</tr>
			<tr>
				<td colspan="2" height="20"></td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<table>
						<tr>
							<td height="28" width="80" align="center"><img border="0" src="../images/btnSalvar.gif" class="botao" onclick="javascript:validar();"></td>
							<td width="10"></td>
							<td width="80" align="center"><img border="0" src="../images/btnCancelar.gif" class="botao" onclick="javascript:window.close();"></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
	</body>
</HTML>