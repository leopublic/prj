<%
	Session.LCID = 1046 'Pt-Br'
	Response.Expires= 0
    Response.AddHeader "PRAGMA", "NO-CACHE" 
    if Session("usuaId") = "" then
   		response.redirect("../login.asp")
    end if
%>
<!--#include file="../includes/wbscontrato_init.inc"-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
    <% 
	'--- Instantiate the FileUpProgress object.
	'Set oFileUpProgress = Server.CreateObject("SoftArtisans.FileUpProgress")
 	'intProgressID = oFileUpProgress.NextProgressID
	%>
	<HEAD>
		<title>::PRJ:: Controle de WBS</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="JavaScript" type="text/javascript" src="tree/dtree.js"></script>
		<link rel="stylesheet" type="text/css" href="tree/dtree.css">

		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
        <!--#include file="../includes/funcoes.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
        
        <script language="JavaScript" type="text/javascript" src="../scripts/moving_div_on_screen.js"></script>
        <script language="JavaScript" type="text/javascript" src="../scripts/block_background_objects.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/functions.js"></script>

		<script language="JavaScript" type="text/javascript" src="../scripts/AbrirJanela.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/NumberFormat.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/PreparaJanela.js"></script>

		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<link rel="stylesheet" type="text/css" href="../EstiloMenu.css">
        <link rel="stylesheet" type="text/css" href="../scripts/wbs_form.css">
        <link rel="stylesheet" type="text/css" href="../scripts/wbs_ppueap.css">
        
		<SCRIPT language="JavaScript" type="text/javascript">
            if('<%=StringToJS(session("msg"))%>'!=''){
                alert('<%=StringToJS(session("msg"))%>');
            }
			
			function _onofffcnrow(_fcn_Id){ //abre fecha a grid de itens da fcn
				if(_fcn_Id!=''){
					if(document.getElementById('tbFCN'+_fcn_Id)){
						var _tb = document.getElementById('tbFCN'+_fcn_Id);
						var _wr = new Number (_tb.parentNode.parentNode.parentNode.rows[_tb.parentNode.parentNode.rowIndex-1].offsetWidth);
						_wr = _wr - 10;
						_wr = new String(_wr+'px');
						//alert(_wr);
						if(_tb.style.display=='block'){
							_tb.style.display='none';
						}else{
							_tb.style.display='block';
							_tb.parentNode.parentNode.style.width=_wr;
							_tb.parentNode.style.width=_wr;
							_tb.style.width=_wr;
						}
					}else{
						alert('Esta FCN ainda n�o possui itens!');
					}
				}
			}
			
			function _openfcn(_fcn_Id, _fcn_Motivo, _fcn_Aprovada){ //abre uma fcn para edi��o (div over)
				if(isNumberJS(_fcn_Id,1)==false){
					alert('fcn_id indispon�vel!');
				}else{
					document.getElementById("ifcn_Id").value          = _fcn_Id;
					document.getElementById("ifcn_Motivo").value      = _fcn_Motivo;
					document.getElementById("ifcn_FLAprovacao").value = _fcn_Aprovada;
					if(_fcn_Aprovada==''){ //cria��o / edi��o de FCN
						_ocdiv('divFCN');
					}else if(_fcn_Aprovada=='0'){ //reprova��o de fcn
						if(document.getElementById('tbFCN'+_fcn_Id)){
							if(confirm('Voc� tem certeza que deseja reprovar esta FCN negando com isso a permiss�o de avan�o de seus itens em EAP?')){
								_salvar(1);
							}
						}else{
							alert('Esta FCN ainda n�o possui itens!\nN�o � poss�vel reprov�-la!');
						}
					}else if(_fcn_Aprovada=='1'){ //aprova��o de fcn
						if(document.getElementById('tbFCN'+_fcn_Id)){
							if(confirm('Voc� tem certeza que deseja aprovar esta FCN efetivando com isso a permiss�o de avan�o de seus itens em EAP?')){
								_salvar(1);
							}
						}else{
							alert('Esta FCN ainda n�o possui itens!\nN�o � poss�vel aprov�-la!');
						}
					}else{
						alert('A��o desconhecida!');
					}
				}
			}
			
			function _openitens(_fcn_Id){ //chama a inclus�o de itens
				//
				var _projId  = '<%=Request.QueryString("projId")%>';
				var _cntrId  = '<%=Request.QueryString("cntrId")%>';
				var _wbscId  = '<%=Request.QueryString("wbscId")%>';
				var _servInd = '<%=Request.QueryString("servInd")%>';
				var _tipo	 = '<%=Request.QueryString("tipo")%>';
				//
				window.location.href='wbscontrato.asp?projId='+_projId+'&cntrId='+_cntrId+'&wbscId='+_wbscId+'&servInd='+_servInd+'&tipo='+_tipo+'&fcn_id='+_fcn_Id;
			}
			
			function _validar(){
				var _erro = '';
				var _fcn_Motivo = document.getElementById('ifcn_Motivo').value;
				
				if(_fcn_Motivo.trim()==''){
					_erro += ', Motivo';
				}
				
				if(_erro!=''){
					alert('Os seguintes campos est�o preenchido incorretamente: '+_erro.substr(2));
				}else{
					_salvar();
					_ocdiv('divFCN');
				}
			}
			
			function _carregagrid(){
				//
				var _cntrId = '<%=Request.QueryString("cntrId")%>';
				var _wbscId = '<%=Request.QueryString("wbscId")%>';
				var _fcn_Id = '<%=Request.QueryString("fcn_Id")%>';
				var _tipo	= '<%=Request.QueryString("tipo")%>';
                var pagina  = '';				
				//
                pagina = 'wbscontrato_ppueap_carrega.asp?cntrId='+_cntrId+'&wbscId='+_wbscId+'&tipo='+_tipo+'&flfcn=1&fcn_Id='+_fcn_Id;
                var xmlhttp = _criaxmlhttprequest();
				xmlhttp.open("GET",pagina,false);
				xmlhttp.send(null);
				document.getElementById('divTab').innerHTML = xmlhttp.responseText;
                //document.getElementById('divBotoes').style.display = 'block';
				if(document.getElementById('tbFCN')){
					var _tf = document.getElementById('tbFCN').rows;
					for(_if=0;_if<_tf.length;_if++){
						_fcn_Id = '';
						if(_tf[_if].className=='fcn'){
							_fcn_Id = _tf[_if].cells[3].innerHTML;
							if(_fcn_Id!=''){
								if(document.getElementById('tbFCN'+_fcn_Id)){
									mesclar_tabela('tbFCN'+_fcn_Id);
									document.getElementById('tbFCN'+_fcn_Id).style.display='none';
								}
							}
						}
					}
					_tf=null;
				}else{
					alert('N�o existe nenhuma FCN, para este projeto, at� o momento!');
				}
			}
			
			function _salvar(_boot_screen){
				//
				var lwbscId          = '<%=Request.QueryString("wbscId")%>';
				var lfcn_Id          = document.getElementById("ifcn_Id").value;
				var lfcn_Motivo      = document.getElementById("ifcn_Motivo").value.replace(/\n/g,'<br>');
				var lfcn_FLAprovacao = document.getElementById("ifcn_FLAprovacao").value;
				//alert(document.getElementById("ifcn_Motivo").value);
				//
				var pagina = "wbscontrato_bd.asp?tipo=-FCN&wbscId="+lwbscId+"&iwbcId=0&iwbcComplemento=&fcn_Id="+lfcn_Id+"&fcn_Motivo="+lfcn_Motivo+"&fcn_FLAprovacao="+lfcn_FLAprovacao;
				//alert(pagina);
				var xmlhttp = _criaxmlhttprequest();
				xmlhttp.open("GET",pagina,false);
				xmlhttp.send(null);
				var xml_ret = xmlhttp.responseText;
				xmlhttp = null;
				//alert(xml_ret);
				//
				if(_tratarxmlretorno(xml_ret)){
					alert('Dados salvos com sucesso!');
					if(_boot_screen==1){
						window.location.reload();
					}else{
						_carregagrid();
					}
				}else{
					alert(msg_erro);
				}
			}
        </SCRIPT>
	</HEAD>
<body class="full" onload='javascript:_carregagrid();'>
<div id="over" class="over" style='display:none;'></div>
<!--#include file="../includes/varControleAcesso.inc"-->
<!--#include file="../includes/cab.inc"-->
	<tr>
		<td style="padding:0px" valign="top" class="blocoOpcoes">
			<!--#include file="../includes/opcWBS.inc"-->
		</td>
		<td colspan="2" valign="top" style="padding:5px" class="blocoPagina">
            <form action="" method="post" name="frmWBS" id="frmWBS">
				<div id="principal">
                    <div id="divFCN" name="divFCN" style="display:none; position:absolute; z-index:1000; width:700px" class="divwindow">
    					<div class='div_titulo' id='divFCN_TIT' name='divFCN_TIT' onmousedown="javascript:dragStart(event, 'divFCN');">Cadastro de FCN</div>
    					<div class='sep'></div>
    					<input id='ifcn_Id' name='ifcn_Id' type='hidden' value=''>
						<input id='ifcn_FLAprovacao' name='ifcn_FLAprovacao' type='hidden' value=''>
    					<table class="entradadados">
    						<tr>
    							<th style="width:700px">Motivo:</th>
    						</tr>
                            <tr>
    							<td><textarea id='ifcn_Motivo' type='text' class='cpLTexto' style='width:700px;' maxlength=2000 rows=4/></textarea></td>
    						</tr>
    					</table>
    					<div class='barrabotoes'>
    						<input type='button' onclick="javascript:_validar();" value="Salvar" title="Informe o motivo desta FCN">
    						<input type='button' onclick="javascript:_ocdiv('divFCN');" value="Fechar" title="Fecha a tela de cadastro de fcn">
    					</div>
    				</div>
                
                
					<input type="hidden" id="curr_wbscId" name="curr_wbscId" value='<%=lwbscId%>' />
					<input type="hidden" id="curr_cntrId" name="curr_cntrId" value='<%=lcntrId%>' />
                    <input type="hidden" id="curr_wbscStatusPPU" name="curr_wbscStatusPPU" value='<%=lwbscStatusPPU%>' />
					<input type="hidden" id="curr_wbscStatusEAP" name="curr_wbscStatusEAP" value='<%=lwbscStatusEAP%>' />
					<input type='hidden' id='curr_tipo' value='<%=request.querystring("tipo")%>'/>
					<input type="hidden" id="curr_projNome" name="curr_cntrId" value='<%=projNome%>' />
					<input type="hidden" id="curr_cntrDesc" name="curr_cntrId" value='<%=cntrDesc%>' />
					<input type="hidden" id="curr_wbscStatusEAP" name="curr_wbscStatusEAP" value='<%=lwbscStatusEAP%>' />
					<input type="hidden" id="curr_wbscFinalizada" name="curr_wbscFinalizada" value='<%=lwbscFinalizada%>' />

					<div id='divTab'>
					</div>

					<% if (lwbscFinalizada = "0") then %> 
						<div id='divBotoes' class='divBotoes' >  
                            <%
                            lbtnsTela = ""
                            ev = " onmouseover='this.className=""overBt"";' onmouseout='this.className=""outBt"";' "
                            if lwbscFinalizada = "0" then 
								'Se a condi��o acima foi confirmada, � pq ainda n�o foi finalizada a WBS
								lbtnsTela = lbtnsTela & "<input class='cpBt' " & ev & " type='button' title='Criar FCN' value='Criar FCN' onclick=""javascript:_openfcn('0','','');"" />"
							end if
                            response.write lbtnsTela
							%>
							<!-- <input class='cpBt' type='button' value='TESTA' onclick='javascript:teste_tratarxmlretorno();' /> -->
						</div>
					<%
                        end if
                        session("msg")=""
                    %>
                    <div id='divLegenda' style='width:100%;height:0px;margin-top:5px;'>
                        <table class='gridWBS'>
                            <tr>
                                <th colspan=8 style='text-align:left'>Legendas</th>
                            </tr>
                            <tr>
                                <td class='defLegenda'><span class='identlevel'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td><td class='descLegenda'>Agrupador</td>
                                <td class='defLegenda'><span class='impar'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><br><span class='par'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td><td class='descLegenda'>Item comum</td>
                                <td class='defLegenda'><span class='trDestaque'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td><td class='descLegenda'>Qtd. estimada diferente da qtd. real</td>
                                <td class='defLegenda'><span class='trFinalizado'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td><td class='descLegenda'>Item finalizado</td>
                            </tr>
                        </table>
					</div>
					<br><br><br>
					<div id='divErros' style='width:99%;height:0px;display:none;margin-top:5px;'>
					</div>
				</div>
			</form>
		</td>
	</tr>
<!--#include file="../includes/rod.inc"-->
</script>
</body>
</HTML>