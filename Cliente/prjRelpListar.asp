<% Response.Expires= -1%>
<% filename = "prjRelpListar.html" %>
<!--#include file="../includes/offline.inc"-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Controle de projetos pela internet</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<link rel="stylesheet" type="text/css" href="../EstiloMenu.css">
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<script language="JavaScript" type="text/javascript" src="../scripts/AbrirJanela.js"></script>
	</HEAD>
	<body class="cabecalho" >
<%  lPerfisAutorizados = "154"
	VerificarAcesso()

   Dim gmenuGeral
   Dim gMenuSelecionado
   Dim gOpcaoSelecionada
   gMenuSelecionado = "PrjRelatorios"
   gOpcaoSelecionada = "PrjRelHistorico"
   gmenuGeral = "Projetos"
   PastaSelecionada = "Projetos"
	projId = Session("projId")
	cntrNomeServArray = Session("cntrNomeServArray")
	cntrIdArray = Session("cntrIdArray")
	emprNomeArray = Session("emprNomeArray")
	emprIdArray = Session("emprIdArray")
	usuaIdContArray = Session("usuaIdContArray")
	TotalServ = Session("QtdServicos")
	servInd = 0
	area = ""
	Dim gConexao 
	AbrirConexao
%>
		<!--#include file="../includes/cab.inc"-->
			<tr>
				<td style="padding:0px" valign="top" class="blocoOpcoes">
					<!--#include file="../includes/opcProjX2.inc"-->
				</td>
				<td colspan="2" valign="top" style="padding:5px" class="blocoPagina">
				<form>
					<table width="100%">
					<tr>
						<td colspan="2" class="docTit"><%=Session("projNome")%> - Relatórios de progresso emitidos</td>
					</tr>
					<tr>
						<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
					</tr>
					<tr>
						<td colspan="2" style="height:5"></td>
					</tr>
					<tr>
						<td colspan="2">
							<table class="grid">
									<tr>
										<td width="20" class="GridCab">&nbsp;</td>
										<td width="400" class="GridCab">Semana</td>
										<td class="GridCab">&nbsp;</td>
									</tr> 
    <%
	xSQL = "select relpId, convert(varchar(10), relpSemIni, 3) + ' a ' + convert(varchar(10), relpSemFim, 3) relpSemana"
	xSQL = xSQL & " from RelatorioProjeto where projId = " & projId & "  order by relpSemFim desc"
	Set xRs = gConexao.Execute(xSQL)
	if xRs.EOF then
		Response.write "<tr>" & vbcrlf
		Response.write "<td colspan=""3"" class=""GridLinhaPar"">Nenhum relatório antigo disponível</td>" & vbcrlf
		Response.write "</tr>" & vbcrlf
	else
		estilo = "GridLinhaPar"
		while not xRs.EOF
			Response.write "<tr>" & vbcrlf
			if offline="" then
				response.write vbtab & "<td class=""" & estilo & """><a href=""prjRelpResumo.asp?relpId=" & xRs("relpId") & """><img src=""../images/icxxx.gif"" style=""border:0""></a></td>"
			else
				response.write vbtab & "<td class=""" & estilo & """><a href=""prjRelpResumo" & xRs("relpId") & ".html""><img src=""../images/icxxx.gif"" style=""border:0""></a></td>"
			end if
			response.write vbtab & "<td class=""" & estilo & """>" & xRs("relpSemana") & "</td>"
			response.write vbtab & "<td class=""" & estilo & """>&nbsp;</td>"
			Response.write "</tr>" & vbcrlf
			xRs.MoveNext
			if estilo = "GridLinhaPar" then
				estilo = "GridLinhaImpar"
			else
				estilo = "GridLinhaPar"
			end if
		wend
	end if
	%>
								<tr>
									<td colspan="3" class="GridRodape">&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
					</table>
				</form>
				</td>
			</tr>			
		<!--#include file="../includes/rod.inc"-->
	</body>
</HTML>