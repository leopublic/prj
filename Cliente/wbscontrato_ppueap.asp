<%
	Session.LCID = 1046 'Pt-Br'
	Response.Expires= 0
    Response.AddHeader "PRAGMA", "NO-CACHE" 
    if Session("usuaId") = "" then
   		response.redirect("../login.asp")
    end if
%>
<!--#include file="../includes/wbscontrato_init.inc"-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
    <% 
	'--- Instantiate the FileUpProgress object.
	'Set oFileUpProgress = Server.CreateObject("SoftArtisans.FileUpProgress")
 	'intProgressID = oFileUpProgress.NextProgressID
	%>
	<HEAD>
		<title>::PRJ:: Controle de WBS</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="JavaScript" type="text/javascript" src="tree/dtree.js"></script>
		<link rel="stylesheet" type="text/css" href="tree/dtree.css">

		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
        <!--#include file="../includes/funcoes.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
        
        <script language="JavaScript" type="text/javascript" src="../scripts/moving_div_on_screen.js"></script>
        <script language="JavaScript" type="text/javascript" src="../scripts/block_background_objects.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/functions.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/bubble-tooltip.js"></script>

		<script language="JavaScript" type="text/javascript" src="../scripts/AbrirJanela.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/NumberFormat.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/PreparaJanela.js"></script>

		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<link rel="stylesheet" type="text/css" href="../EstiloMenu.css">
        <link rel="stylesheet" type="text/css" href="../scripts/wbs_form.css">
        <link rel="stylesheet" type="text/css" href="../scripts/wbs_ppueap.css">
        <link rel="stylesheet" type="text/css" href="../scripts/bubble-tooltip.css">
        
		<SCRIPT language="JavaScript" type="text/javascript">
            var xml_erro = '';
			_xsl_ret=_gerarxsltretorno('LISTA/ERRO', 'LINHA', 'LINHA|DESCRICAO', 'Linha|Descri��o', '50|325');
			_xsl_fechar_eap=_gerarxsltretorno('EAP/ERRO', 'IND_ETAPA', 'ETAPA|LINHA|DESCRICAO', 'Etapa|Linha|Descri��o', '100|175|375');

			var _tabela = 'tbPPUEAP';
			if ('<%=Request.QueryString("fcn_Id")%>'!='') {
				_tabela = 'tbFCN<%=Request.QueryString("fcn_Id")%>';
			}
			
            if('<%=StringToJS(session("msg"))%>'!=''){
                alert('<%=StringToJS(session("msg"))%>');
            }
            
            function _limparwbs(ptipo){
				//
				var linha = 0;
                var w_obj = document.getElementById(_tabela);
				//
				xml_erro = '';
				//
                if (w_obj) {
                    for(lind_i=1;lind_i<w_obj.getElementsByTagName('TR').length;lind_i++){
                        linha++;
                        if(w_obj.getElementsByTagName('TR')[lind_i].className!='identlevel'){
                            //pegando id do item atual
                            var i_i = w_obj.getElementsByTagName('TR')[lind_i].id.substr(1,100);
                            //
                            document.getElementById("i"+i_i+"Complemento").value = '';
                            if(ptipo==1){
                                if (document.getElementById("i"+i_i+"QtdEst").disabled==false){
                                    document.getElementById("i"+i_i+"QtdEst").value = '';
                                    document.getElementById("i"+i_i+"ValUnitReal").value= '';
                                }                            
                            }else if(ptipo==2){
                                if (document.getElementById("i"+i_i+"DtInicioEst").disabled==false){
                                    document.getElementById("i"+i_i+"DtInicioEst").value='';
                                    document.getElementById("i"+i_i+"DuracaoEst").value='';
                                }
                            }else if(ptipo==3){
                                if (document.getElementById("i"+i_i+"FLConcForn").disabled==false){
                                    document.getElementById("i"+i_i+"FLConcForn").checked=false;
                                }
                            }else if(ptipo==4){
                                if (document.getElementById("i"+i_i+"FLConcVerif").disabled==false){
                                    document.getElementById("i"+i_i+"FLConcVerif").checked=false;
                                }
                            }else if(ptipo==6){
                                if (document.getElementById("i"+i_i+"QtdReal").disabled==false){
                                    document.getElementById("i"+i_i+"QtdReal").value='';
                                }
                            }
                        }
                    }
                } else {
                    alert('N�o existem itens a serem limpos!');
                }
			}
        
            function _carregagrid(p_cntrId, p_wbscId, p_tipo){
                var lfiltro_semanas='4';
                if (document.getElementById('filtro_semanas')){lfiltro_semanas=document.getElementById('filtro_semanas').value;}
                var pagina_c = '';
                if ('<%=Request.QueryString("fcn_Id")%>'!='') {
                    pagina_c = 'wbscontrato_ppueap_carrega.asp?cntrId='+p_cntrId+'&wbscId='+p_wbscId+'&tipo='+p_tipo+'&filtro_semanas='+lfiltro_semanas+'&flfcn=1&fcn_Id=<%=Request.QueryString("fcn_Id")%>';
                } else {
                    pagina_c = 'wbscontrato_ppueap_carrega.asp?cntrId='+p_cntrId+'&wbscId='+p_wbscId+'&tipo='+p_tipo+'&filtro_semanas='+lfiltro_semanas+'&flfcn=';
                }
                //return 1;
                var xmlhttp_c = _criaxmlhttprequest();
				xmlhttp_c.open("GET",pagina_c,false);
				xmlhttp_c.send(null);
				document.getElementById('divTab').innerHTML = xmlhttp_c.responseText;
                //document.getElementById('divBotoes').style.display = 'block';
				mesclar_tabela(_tabela);
                if(!document.getElementById(_tabela)){
                    alert('N�o existem itens dispon�veis!');
                }
			}
            
            function _salvarwbs(ptipo){
				//
				var msg_erro = ' Ocorreram erros durante o processamento. \n Por favor, verifique as mensagens de erro!';
                var xml_ret = '';
				//
                var w_obj = document.getElementById(_tabela);
                if (!w_obj) {
                    alert('N�o existem itens a serem salvos!');
                }else{
                    if(w_obj.getElementsByTagName('TR').length==1){
                        alert('N�o existem itens a serem salvos!');
                    }else{
        				if(_validarwbs(ptipo)){
        					//
							var qtdTotal=w_obj.getElementsByTagName('TR').length;

                            for(lind_i=1;lind_i<qtdTotal;lind_i++){
                                //pegando id do item atual
                                var i_i = w_obj.getElementsByTagName('TR')[lind_i].id.substr(1,100);
                                //pegando os campos da itemWbs atual
                                
                                var liwbcId = document.getElementById("i"+i_i+"Id").value;
								var liwbcComplemento = '';
                                
                                //PPU
                                var liwbcQtdEst = '';
                                var liwbcValUnitReal = '';
                                //TAKE-OFF
                                var liwbcQtdReal = '';
                                var liwdeDesNum = '';
                                var liwdeDesRev = '';
                                //Montar Cronograma
                                var liwbcDtInicioEst = '';
                                var liwbcDuracaoEst = '';
                                //Informar Avan�o - EAP
                                var liwbcFLConcForn = '';
                                //Validar Avan�o - EAP
                                var liwbcFLConcVerif = '';

                                var envia = 1;
                                if(w_obj.getElementsByTagName('TR')[lind_i].className!='identlevel'){
                                    if(ptipo==1){
                                        liwbcQtdEst = document.getElementById("i"+i_i+"QtdEst").value;
                                        liwbcValUnitReal = document.getElementById("i"+i_i+"ValUnitReal").value;
                                        if(liwbcQtdEst=='' && liwbcValUnitReal==''){envia=0;}
                                    }else if(ptipo==2){
                                        liwbcDtInicioEst = document.getElementById("i"+i_i+"DtInicioEst").value;
                                        liwbcDuracaoEst = document.getElementById("i"+i_i+"DuracaoEst").value;
                                        if(liwbcDtInicioEst=='' && liwbcDuracaoEst==''){envia=0;}
                                    }else if(ptipo==3){
                                        liwbcFLConcForn = (document.getElementById("i"+i_i+"FLConcForn").checked?'1':'0');
                                        //if(liwbcFLConcForn==''){envia=0;}
                                    }else if(ptipo==4){
                                        liwbcFLConcVerif = (document.getElementById("i"+i_i+"FLConcVerif").checked?'1':'0');
                                        //if(liwbcFLConcVerif==''){envia=0;}
                                    }else if(ptipo==6){
                                        liwbcQtdReal = document.getElementById("i"+i_i+"QtdReal").value;
                                        liwdeDesNum = document.getElementById("i"+i_i+"DesNum").value;
                                        liwdeDesRev = document.getElementById("i"+i_i+"DesRev").value;
                                        if((liwbcQtdReal=='')){envia=0;}
                                        if(toDecimalBR(document.getElementById("i"+i_i+"QtdEst").value)==toDecimalBR(document.getElementById("i"+i_i+"QtdReal").value)){
                                            envia=0;
                                        }
                                    }else{envia=0;}
                                }
                                //envia=0;
                                if(envia==1){
                                    //Atualizando item
                                    var pagina_i = "wbscontrato_bd.asp?tipo=I"+ptipo;
										pagina_i += "&iwbcId="+liwbcId;
										pagina_i += "&iwbcComplemento="+liwbcComplemento;
                                        pagina_i += "&iwbcQtdEst="+liwbcQtdEst;
										pagina_i += "&iwbcValUnitReal="+liwbcValUnitReal; //PPU
                                        pagina_i += "&iwbcDtInicioEst="+liwbcDtInicioEst;
										pagina_i += "&iwbcDuracaoEst="+liwbcDuracaoEst; //CRONO
                                        pagina_i += "&iwbcFLConcForn="+liwbcFLConcForn; //BM - F
                                        pagina_i += "&iwbcFLConcVerif="+liwbcFLConcVerif; //BM - W
                                        pagina_i += "&usuaIdVerif=<%=session("usuaId")%>"; //BM - W
                                        pagina_i += "&iwbcQtdReal="+liwbcQtdReal;
										pagina_i += "&iwdeDesNum="+liwdeDesNum;
										pagina_i += "&iwdeDesRev="+liwdeDesRev; //TAKE-OFF
									//alert(pagina_i);
									var itemAtual = document.getElementById('item'+liwbcId).value;
									window.status='Salvando '+itemAtual;
                                    var xmlhttp_i = _criaxmlhttprequest();
                                    xmlhttp_i.open("GET",pagina_i,false);
                                    xmlhttp_i.send(null);
                                    xml_ret += xmlhttp_i.responseText;
                                }
                            }

                            if(_tratarxmlretorno(xml_ret)){
                                _carregagrid(document.getElementById("curr_cntrId").value, document.getElementById("curr_wbscId").value, document.getElementById("curr_tipo").value)
                                alert('Dados salvos com sucesso!');
								window.status='';
                            }else{
                                alert(msg_erro);
                            }
                        }
                    }
                }
			}
            
            function _validarwbs(ptipo){
				//
				var linha = 0;
                var w_obj = document.getElementById(_tabela);
				//
				xml_erro = '';
				//
                for(lind_i=1;lind_i<w_obj.getElementsByTagName('TR').length;lind_i++){
                    linha++;
                    if(w_obj.getElementsByTagName('TR')[lind_i].className!='identlevel'){
                        //pegando id do item atual
                        var i_i = w_obj.getElementsByTagName('TR')[lind_i].id.substr(1,100);
                        //
                        if(ptipo==1){
                            if(isNumberJS(document.getElementById("i"+i_i+"QtdEst").value)==false){
                                xml_erro += '<ERRO><LINHA>'+linha+'</LINHA><DESCRICAO>Qtd estimada inv�lida!</DESCRICAO></ERRO>';
                            }
                            if(isNumberJS(document.getElementById("i"+i_i+"ValUnitReal").value)==false){
                                xml_erro += '<ERRO><LINHA>'+linha+'</LINHA><DESCRICAO>Valor Unit. inv�lido!</DESCRICAO></ERRO> ';
                            }
                        }else if(ptipo==2){
                            if(document.getElementById("i"+i_i+"DtInicioEst").value!=''){
                                if(isDateJS2(document.getElementById("i"+i_i+"DtInicioEst").value,1)==false){
                                    xml_erro += '<ERRO><LINHA>'+linha+'</LINHA><DESCRICAO>Data de in�cio estimada inv�lida!</DESCRICAO></ERRO> ';
                                }
                            }
                            if(document.getElementById("i"+i_i+"DuracaoEst").value!=''){
                                if(isNumberJS(document.getElementById("i"+i_i+"DuracaoEst").value)==false){
                                    xml_erro += '<ERRO><LINHA>'+linha+'</LINHA><DESCRICAO>Dura��o estimada inv�lida!</DESCRICAO></ERRO> ';
                                }
                            }
                        }else if(ptipo==3){
                            //if(isNumberJS(document.getElementById("i"+i_i+"QtdConcForn").value)==false){
                            //    xml_erro += '<ERRO><LINHA>'+linha+'</LINHA><DESCRICAO>Qtd. Atual!</DESCRICAO></ERRO> ';
                            //}
                        }else if(ptipo==4){
                            //if(isNumberJS(document.getElementById("i"+i_i+"QtdConcVerif").value)==false){
                            //    xml_erro += '<ERRO><LINHA>'+linha+'</LINHA><DESCRICAO>Qtd. Verificada!</DESCRICAO></ERRO> ';
                            //}
                        }else if(ptipo==6){
                            if(isNumberJS(document.getElementById("i"+i_i+"QtdReal").value)==false){
                                xml_erro += '<ERRO><LINHA>'+linha+'</LINHA><DESCRICAO>Qtd. Atual inv�lida!</DESCRICAO></ERRO> ';
                            }else{
                                if(toDecimalBR(document.getElementById("i"+i_i+"QtdAtual").value)>toDecimalBR(document.getElementById("i"+i_i+"QtdReal").value)){
                                        xml_erro += '<ERRO><LINHA>'+linha+'</LINHA><DESCRICAO>Quantidade Total Real n�o pode ser menor que a Quantidade Conclu�da!</DESCRICAO></ERRO> ';
                                }
                                if(toDecimalBR(document.getElementById("i"+i_i+"QtdEst").value)!=toDecimalBR(document.getElementById("i"+i_i+"QtdReal").value)){
                                    if (document.getElementById("i"+i_i+"DesNum").value=='') {
                                        xml_erro += '<ERRO><LINHA>'+linha+'</LINHA><DESCRICAO>O desenho � obrigat�rio para justificar altera��es entre Quantidade Total Atual e Quantidade Total Real!</DESCRICAO></ERRO> ';
                                    }
                                }
                            }
                        }
                    }
                }

				if(xml_erro!=''){
					alert('Houveram erros de preechimentos, verifique abaixo!');
					xml_erro='<LISTA>'+xml_erro+'</LISTA>';
					_carregaxmlxslt(xml_erro, _xsl_ret, 'divErros');
					return false
				}else{
					return true
				}
			}

            function _iniciarEAP(){
                if(confirm('Tem certeza que deseja inicializar os avan�os de EAP!')){
    				var lwbscId = document.getElementById('curr_wbscId').value;
    				var pagina_f = 'wbscontrato_ppueap_fecharPPUEAP.asp?wbscId='+lwbscId+'&tipo=EAP&fcn_Id=<%=Request.QueryString("fcn_Id")%>';
    				
                    var xmlhttp_f = _criaxmlhttprequest();
    				xmlhttp_f.open("GET",pagina_f,false);
    				xmlhttp_f.send(null);
    				retorno = xmlhttp_f.responseText;
    				if(retorno!=''){
    					retorno='<EAP>'+retorno+'</EAP>';
    					return _carregaxmlxslt(retorno, _xsl_fechar_eap, 'divErros');
    				}else{
    					var pagina_f = "wbscontrato_bd.asp?tipo=-IEAP&wbscId="+lwbscId+'&fcn_Id=<%=Request.QueryString("fcn_Id")%>';
    					
                        var xmlhttp_f = _criaxmlhttprequest();
    					xmlhttp_f.open("GET",pagina_f,false);
    					xmlhttp_f.send(null);
    					retorno = xmlhttp_f.responseText;
    					if(retorno!=''){
    						_tratarxmlretorno(retorno);
    					}else{
    						alert('EAP iniciada com sucesso!');
    						window.location.href=window.location.href;
    					}
    				}
                }
			}
            
            function _fecharPPU(){
                if(confirm('Tem certeza que deseja finalizar a PPU? Ap�s o fechamento, inclus�es de novas tarefas s� ser�o permitidas atrav�s de FCN. Confirma o fechamento?')){
    				var lwbscId  = document.getElementById('curr_wbscId').value;
					var lfcn_Id  = ('<%=Request.QueryString("fcn_Id")%>').trim();
					var lflfcn   = ((lfcn_Id!='') && (lfcn_Id!='0')?'1':'0');
    				var pagina_f = 'wbscontrato_ppueap_fecharPPUEAP.asp?wbscId='+lwbscId+'&tipo=PPU&fcn_Id=<%=Request.QueryString("fcn_Id")%>';
    				
                    var xmlhttp_f = _criaxmlhttprequest();
    				xmlhttp_f.open("GET",pagina_f,false);
    				xmlhttp_f.send(null);
    				retorno = xmlhttp_f.responseText;
					//alert(1);
					//alert(retorno);
    				if(retorno!=''){
    					retorno='<EAP>'+retorno+'</EAP>';
    					return _carregaxmlxslt(retorno, _xsl_fechar_eap, 'divErros');
    				}else{
    					var pagina_f = "wbscontrato_bd.asp?tipo=-PPU&wbscId="+lwbscId+'&fcn_Id=<%=Request.QueryString("fcn_Id")%>';
    					
                        var xmlhttp_f = _criaxmlhttprequest();
    					xmlhttp_f.open("GET",pagina_f,false);
    					xmlhttp_f.send(null);
    					retorno = xmlhttp_f.responseText;
						//alert(2);
						//alert(retorno);
    					if(retorno!=''){
    						_tratarxmlretorno(retorno);
    					}else{
    						alert('PPU fechada com sucesso!');
    						window.location.href=window.location.href;
    					}
    				}
                }
			}
            
            function _finalizarEAP(){
                if(confirm('Tem certeza que deseja finalizar esta etapa da EAP!')){
    				var lwbscId = document.getElementById('curr_wbscId').value;
    				
                    var pagina_f = "wbscontrato_bd.asp?tipo=-FEAP&wbscId="+lwbscId;
                    
                    var xmlhttp_f = _criaxmlhttprequest();
                    xmlhttp_f.open("GET",pagina_f,false);
                    xmlhttp_f.send(null);
                    retorno = xmlhttp_f.responseText;
                    
                    if(retorno!='' && (isNumberJS(retorno)==false)){
                        _tratarxmlretorno(retorno);
                    }else{
                        if(retorno=='0'){
                            alert('Contrato finalizado!!!');
                            retorno='<%=lwbscId%>';
                        }else{
                            alert('Avan�o finalizado com sucesso!');
                        }
                        window.location.href='wbscontrato.asp?projId=<%=lprojId%>&cntrId=<%=lcntrId%>&wbscId='+retorno+'&servInd='+<%=lservInd%>+'&tipo=0';
                        //window.location.href='wbscontrato_ppueap_boletim_lista.asp?projId=<%=lprojId%>&cntrId=<%=lcntrId%>&wbscId='+retorno+'&servInd='+<%=lservInd%>+'&tipo=<%=ltipo%>';
                    }
				}
			}
            
            function _expCrono(pwbscId){
				var pagina = 'wbscontrato_ppueap_exp_project.asp?cntrId='+<%=lcntrId%>+'&wbscId='+<%=lwbscId%>+'&flpenultimonivel=0&fcn_Id=<%=Request.QueryString("fcn_Id")%>';
                window.open(pagina,"WBSEXPMSPROJECT","directories=no,fullscreen=no,height=50,left=20,location=no,menubar=no,resizable=yes,scrollbars=no,status=no,titlebar=no,toolbar=no,top=50,width=50", true);
            }
            
            function _impCrono(pwbscId){
                if(document.getElementById('upd_arquivo').value==''){
                    alert('Arquivo inv�lido!');
                } else {
                    var frmUPD=document.getElementById('frmUPDMSProject');
                    frmUPD.action='wbscontrato_updmsproject.asp';
                    frmUPD.method='POST';
                    frmUPD.submit();
                }
            }
            
            function _expPPU(pwbscId){
				var lfcn_Id  = ('<%=Request.QueryString("fcn_Id")%>').trim();
				var lflfcn   = ((lfcn_Id!='') && (lfcn_Id!='0')?'1':'0');
                window.open('wbscontrato_ppueap_carrega.asp?cntrId='+<%=lcntrId%>+'&wbscId='+<%=lwbscId%>+'&tipo=1&expPPU=1&flfcn='+lflfcn+'&fcn_Id=<%=Request.QueryString("fcn_Id")%>',"WBSEXP","directories=no,fullscreen=no,height=50,left=20,location=no,menubar=no,resizable=yes,scrollbars=no,status=no,titlebar=no,toolbar=no,top=50,width=50", true);
            }
            
            function setadesenho(piwbcId){
                document.getElementById('dese_itemId').value = piwbcId;
                document.getElementById('dese_iwdeDesNum').value='';
                document.getElementById('dese_iwdeDesRev').value='0';
                _ocdiv('divDesenho');
                document.getElementById('dese_iwdeDesNum').focus();
            }
            
            function _salvadesenho(){
                //
                var l__itemId = document.getElementById('dese_itemId').value;
                var l__iwdeDesNum = document.getElementById('dese_iwdeDesNum').value;
                var l__iwdeDesRev = document.getElementById('dese_iwdeDesRev').value;
                //
                document.getElementById('i'+l__itemId+'DeseDesc').style.display='none';
                if(l__iwdeDesNum==''){
                    alert('Informe o n� do desenho!');
                    document.getElementById('dese_iwdeDesNum').focus();
                }else if((!isNumberJS(l__iwdeDesRev)) && (l__iwdeDesRev!='')){
                    alert('Revis�o deve ser num�rica!');
                    document.getElementById('dese_iwdeDesRev').focus();
                } else {
                    if(l__iwdeDesRev=='') {l__iwdeDesRev='0';}
                    document.getElementById('i'+l__itemId+'DesNum').value=l__iwdeDesNum;
                    document.getElementById('i'+l__itemId+'DesRev').value=l__iwdeDesRev;
                    document.getElementById('i'+l__itemId+'DeseDesc').innerHTML=l__iwdeDesNum+'.rev.'+l__iwdeDesRev;
                    document.getElementById('i'+l__itemId+'DeseDesc').style.display='block';
                    _ocdiv('divDesenho');
                }
                //
            }
            
        </SCRIPT>
	</HEAD>
<body class="full" onload='javascript:_carregagrid(<%=request.querystring("cntrId")%>,<%=request.querystring("wbscId")%>,<%=request.querystring("tipo")%>);'>
<div id="bubble_tooltip">
	<div class="bubble_top"><span></span></div>
	<div class="bubble_middle"><span id="bubble_tooltip_content">Content is comming here as you probably can see.Content is comming here as you probably can see.</span></div>
	<div class="bubble_bottom"></div>
</div>
<div id="over" class="over" style='display:none;'></div>
<!--#include file="../includes/varControleAcesso.inc"-->
<!--#include file="../includes/cab.inc"-->
	<tr>
		<td style="padding:0px" valign="top" class="blocoOpcoes">
			<!--#include file="../includes/opcWBS.inc"-->
		</td>
		<td colspan="2" valign="top" style="padding:5px" class="blocoPagina">
<%
select case request.querystring("tipo")
	case "1" 'Montando PPU
		Titulo =  "Planilha de Pre�os Unit�rios - PPU" 
		if lwbscStatusPPU = "0" then 
			Titulo = Titulo & " - Montagem"
		end if
	case "2" 'Motando cronograma
		Titulo ="Cronograma" 
	case "3" 'Informando avan�o de EAP
		Titulo= "EAP - Informando Avan�os" 
	case "4" 'Valida��o de avan�o de EAP
		Titulo= "EAP - Validando Avan�os" 
	case "6" 'Atulizando quantidades
		Titulo= "Atualiza��o de Quantidades"
	case "7" 'FCN
		Titulo= "FCN"
	case else
		Titulo= "&nbsp;"
end select 

%>

		<table style="width:100%">
				<tr>
					<td colspan="2" class="docTit"><%=session("projNome")%> - <%=cntrNomeServ%><br/><%=Titulo%></td>
				</tr>
				<tr>
					<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
				</tr>
			</table>
            <form id='frmUPDMSProject' action="wbscontrato_updmsproject.asp" method="post" enctype="multipart/form-data">
                <div id="divUPDMSProject" style="display:none; position:absolute; z-index:1000;" class="divwindow">
                    <div class='div_titulo' id='divItem_TIT2' onmousedown="javascript:dragStart(event, 'divUPDMSProject');">Upload de cronograma do MS Project</div>
                    <div class='sep'></div>
                    <table class="entradadados">
                        <tr>
                            <input type='hidden' id='upd_projId'  name='upd_projId'  value='<%=lprojId%>'/>
                            <input type='hidden' id='upd_cntrId'  name='upd_cntrId'  value='<%=lcntrId%>'/>
                            <input type='hidden' id='upd_wbscId'  name='upd_wbscId'  value='<%=lwbscId%>'/>
                            <input type='hidden' id='upd_servInd' name='upd_servInd' value='<%=lservInd%>'/>
                            <input type='hidden' id='upd_fcn_Id'  name='upd_fcn_Id'  value='<%=Request.QueryString("fcn_Id")%>'/>
                            <input type='hidden' id='upd_tipo'    name='upd_tipo'    value='<%=ltipo%>'/>
                            <th style="width:100px">Arquivo</th>
                            <th>:</th>
                            <td><input id='upd_arquivo' name='upd_arquivo' type='file' style='width:99%;'/></td>
                        </tr>
                    </table>
                    <div class='barrabotoes'>
                        <!--<input type='submit' value="Importar" title="Importar o cronograma selecionado">-->
                        <input type='button' onclick="javascript:_impCrono();" value="Importar" title="Importar o cronograma selecionado">
                        <input type='button' onclick="javascript:_ocdiv('divUPDMSProject');" value="Fechar" title="Fecha a tela upload de cronograma">
                    </div>
                </div>
            </form>
			<form action="" method="post" name="frmWBS" id="frmWBS">
				<div id="principal">
                    <div id="divDesenho" name="divItem" style="display:none; position:absolute; z-index:1000;" class="divwindow">
    					<div class='div_titulo' id='divItem_TIT' name='divItem_TIT' onmousedown="javascript:dragStart(event, 'divDesenho');">Selecionar desenho</div>
    					<div class='sep'></div>
    					<input id='dese_itemId' type='hidden'>
    					<table class="entradadados">
    						<tr>
    							<th style="width:100px">Desenho</th>
    							<th>:</th>
    							<td><input id='dese_iwdeDesNum' type='text' class='cpLCod' value='' maxlength=30/>
    							</td>
    						</tr>
                            <tr>
    							<th style="width:100px">Revis�o</th>
    							<th>:</th>
    							<td><input id='dese_iwdeDesRev' type='text' class='cpLValor' value='' maxlength=10 />
    							</td>
    						</tr>
    					</table>

    					<div class='barrabotoes'>
    						<input type='button' onclick="javascript:_salvadesenho();" value="Salvar" title="Seleciona o desenho para o item em quest�o">
    						<input type='button' onclick="javascript:_ocdiv('divDesenho');" value="Fechar" title="Fecha a tela de sele��o de desenho">
    					</div>
    				</div>
                
                
					<input type="hidden" id="curr_wbscId" name="curr_wbscId" value='<%=lwbscId%>' />
					<input type="hidden" id="curr_cntrId" name="curr_cntrId" value='<%=lcntrId%>' />
                    <input type="hidden" id="curr_wbscStatusPPU" name="curr_wbscStatusPPU" value='<%=lwbscStatusPPU%>' />
					<input type="hidden" id="curr_wbscStatusEAP" name="curr_wbscStatusEAP" value='<%=lwbscStatusEAP%>' />
					<input type='hidden' id='curr_tipo' value='<%=request.querystring("tipo")%>'/>
					<input type="hidden" id="curr_projNome" name="curr_cntrId" value='<%=projNome%>' />
					<input type="hidden" id="curr_cntrDesc" name="curr_cntrId" value='<%=cntrDesc%>' />
					<input type="hidden" id="curr_wbscStatusEAP" name="curr_wbscStatusEAP" value='<%=lwbscStatusEAP%>' />
					<input type="hidden" id="curr_wbscFinalizada" name="curr_wbscFinalizada" value='<%=lwbscFinalizada%>' />

					<div id='divTab'>
					</div>

					<% if (lwbscFinalizada = "0") then %> 
						<div id='divBotoes' class='divBotoes' style="border:none">  
							<br/>
                            <%
                            lbtnsTela = ""
                            lbtnsPrinc = ""
                            lbloqPrinc = ""
                            ev = " onmouseover='this.className=""overBt"";' onmouseout='this.className=""outBt"";' "
							ev = ""
							if request.querystring("tipo") = "1" then 'Gera��o da listagem WBS em XLS
                                'lbtnsTela = lbtnsTela & "<input class='cpBt' " & ev & " type='button' value='Exportar p/ XLS' title='Gerar planillha de quantidade' onclick='javascript:_expPPU(""" & lwbscId & """);' " & ldisabled & " />"
                                lbtnsTela = lbtnsTela & "<input " & ev & " type='button' value='Exportar p/ XLS' title='Gerar planillha de quantidade' onclick='javascript:_expPPU(""" & lwbscId & """);' " & ldisabled & " />"
                            end If
							if request.querystring("tipo") = "1" and lwbscStatusPPU = "0" then
								'Se a condi��o acima foi confirmada, � pq ainda n�o foi finalizada a PPU, sendo assim, ainda
								'n�o podemos montar cronograma, logo, mostraremos o bot�o respectivo de fechamento de PPU.
								lbtnsTela = lbtnsTela & "<input " & ev & " type='button' value='Fechar PPU' title='Finalizar planilha de pre&ccedil;os unit&aacute;rios(PPU)' onclick='javascript:_fecharPPU();' " & ldisabled & " />"
							end if
                            if request.querystring("tipo") = "1" and lwbscStatusPPU = "1" then 
                                'PPU j� finalizada... n�o podemos mais alterar nada nesta tela
                                lbloqPrinc = " style='display:none' "
                            end if
                            if request.querystring("tipo") = "2" then 'Gera��o/Recep��o de cronograma pro PROJECT
                                lbtnsTela = lbtnsTela & "<input " & ev & " type='button' value='Exp. Cronograma' title='Gerar espelho para MSProject' onclick='javascript:_expCrono(""" & lwbscId & """);' " & ldisabled & " />"
                                lbtnsTela = lbtnsTela & "<input " & ev & " type='button' value='Imp. Cronograma' title='Importar espelho do MSProject' onclick='javascript:_ocdiv(""divUPDMSProject"");' " & ldisabled & " />"
                            end If
							if request.querystring("tipo") = "2" and lwbscStatusEAP = "0" then
								'Se a condi��o acima foi confirmada, � pq ainda n�o foi iniciada a EAP, sendo assim, ainda
								'n�o podemos gerar avan�os, logo, mostraremos o bot�o respectivo para fechamento de cronograma.
								lbtnsTela = lbtnsTela & "<input " & ev & " type='button' value='Iniciar EAP' title='Iniciar EAP' onclick='javascript:_iniciarEAP();' " & ldisabled & " />"
							end if
                            if request.querystring("tipo") = "4" and lwbscFinalizada = "0" then
								'Se a condi��o acima foi confirmada, � pq ainda n�o foi finalizada a WBS
								lbtnsTela = lbtnsTela & "<input " & ev & " type='button' title='Finalizar EAP' value='Finalizar EAP' onclick='javascript:_finalizarEAP();' " & ldisabled & " />"
							end if
                            lbtnsPrinc = "<input " & ev & " type='button' value='Salvar' title='Salvar altera&ccedil;&otilde;es' onclick='javascript:_salvarwbs(" & request.querystring("tipo") & ");' " & ldisabled & " " & lbloqPrinc & " />"
							lbtnsPrinc = lbtnsPrinc & "<input " & ev & " type='button' value='Limpar' title='Limpar campos' onclick='javascript:_limparwbs(" & request.querystring("tipo") & ");' " & ldisabled & " " & lbloqPrinc & " />"
                            response.write lbtnsPrinc & lbtnsTela
							%>
							<!-- <input class='cpBt' type='button' value='TESTA' onclick='javascript:teste_tratarxmlretorno();' /> -->
						</div>
					<%
                        end if
                        session("msg")=""
                    %>
<!--
                    <div id='divLegenda' style='width:100%;height:0px;margin-top:5px;border:none;border-top:solid 1px #999'>
                        <table class='gridWBS' style="border:none">
                            <tr>
                                <th colspan=8 style='text-align:left'>Legendas</th>
                            </tr>
                            <tr>
                                <td class='defLegenda'><span class='identlevel'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td><td class='descLegenda'>Agrupador</td>
                                <td class='defLegenda'><span class='impar'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><br><span class='par'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td><td class='descLegenda'>Item comum</td>
                                <td class='defLegenda'><span class='trDestaque'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td><td class='descLegenda'>Qtd. estimada diferente da qtd. real</td>
                                <td class='defLegenda'><span class='trFinalizado'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td><td class='descLegenda'>Item finalizado</td>
                            </tr>
                        </table>
		</div>
-->
					<br><br><br>
					<div id='divErros' style='width:99%;height:0px;display:none;margin-top:5px;'>
					</div>
				</div>
			</form>
		</td>
	</tr>
<!--#include file="../includes/rod.inc"-->
</script>
</body>
</HTML>