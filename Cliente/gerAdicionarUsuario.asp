<% Response.Expires= -1%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Controle remoto de projetos</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<script language="JavaScript" type="text/javascript" src="../scripts/PreparaJanela.js"></script>
		<SCRIPT language="JavaScript">
			function submitform(){
			  document.form1.submit();
			}
			function validar(){
			  if (document.form1.txtLogin.value == '')
			  { alert('O preenchimento do login � obrigatorio.')}
			  else
			  {   if (document.form1.txtNome.value == '')
			      { alert('O preenchimento do nome � obrigatorio.')}
				  else
				  {    senha1 = document.form1.txtSenha1.value;
		  			   senha2 = document.form1.txtSenha2.value;
			  		   if (senha1 == senha2) {
						  	submitform();}
					  else
						  {alert('Senhas diferentes');}
					}
				}			  
			}
		</SCRIPT> 
	</HEAD>
	<body class="PopUpEntrada" onLoad="javascript:PreparaJanela(460,320);">
<% 
    lPerfisAutorizados = "23451"
	VerificarAcesso
	Dim gConexao 
	AbrirConexao
	usuaId = Request.Querystring("usuaID")
	if usuaId = 0 then
	    titulo = "Criar novo usu�rio"
		Session("TituloMsg") = titulo
	    login = ""
		nome = ""
		email = ""
		senha = ""
		perfil = ""
		emprId = "1"
	else
		xSQL = "select usuaNome, usuaLogi, usuaEmai, usuaSenh, perfId, emprId from Usuario where usuaId = " & usuaId
		set xrs = gConexao.execute(xSQL)
		titulo = "Alterar usu�rio"
		Session("TituloMsg") = titulo
	    login = xRs("usuaLogi")
		nome = xrs("usuaNome")
		email = xRs("usuaEmai")
		senha = xRs("usuaSenh")
		perfil = xRs("perfId")
		emprId = cstr(xRs("emprId"))
		if cstr(emprId) = "" then
			emprId = "1"
		end if
		xRs.CLose
		Set xRs = nothing
	end if
%>
	<form action="gerGravarUsuario.asp" method="post" name="form1">
		<input type="hidden" name="usuaId" value=" <% = usuaId %> " >
		<table width="100%">
			<tr>
				<td width="15"></td>
				<td></td>
			</tr>
			<tr>
				<td colspan="2" class="docTit"><% = titulo %></td>
			</tr>
			<tr>
				<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
			</tr>
			<tr>
				<td colspan="2" height="15"></td>
			</tr>
			<tr>
				<td width="15"></td>
				<td>
					<table class="doc">
						<tr>
							<td width="110" class="docLabel">Login</td>
							<td width="5" class="docLabel">:</td>
							<td class="docCmpLivre"><input type="text" name="txtLogin" class="docCmpLivre" value="<% = login %>"></td>
						</tr>
						<tr>
							<td class="docLabel">Nome</td>
							<td class="docLabel">:</td>
							<td class="docCmpLivre"><input type="text" name="txtNome" class="docCmpLivre" value="<% = nome %>"></td>
						</tr>
						<tr>
							<td class="docLabel">E-mail de contato</td>
							<td class="docLabel">:</td>
							<td class="docCmpLivre"><input type="text" name="txtEmail" class="docCmpLivre" value="<% = email %>"></td>
						</tr>
						<tr>
							<td class="docLabel">Empresa</td>
							<td class="docLabel">:</td>
							<td class="docSBorda">
								<select name="cmbEmpr" class="docCmpLivre">
					<%
					xSQL = ""
					xSQL = xSQL & "select emprId "
					xSQL = xSQL & "     , emprNome "
					xSQL = xSQL & "  from Empreiteira "
					xSQL = xSQL & " order by emprNome"
					set xRs = gConexao.execute(xsQL)
'					xEmpr = xEmpr & "<option value=""0"" class=""docCmpLivre"">(n�o definido)</option>" & vbcrlf
					while not xRs.eof
						xEmprId = cstr(xRs("emprId"))
						if xEmprId = emprId then
							xChecked = " selected "
						else
							xChecked = ""
						end if
						Response.Write ("							         <option value=""" & xRs("emprId") & """ " & xChecked & "class=""docCmpLivre"">" & xRs("emprNome") & "</option>" & vbcrlf)
						xRs.Movenext
					wend
					xRs.Close
					Set xRs = nothing
					%>
								</select>
							</td>
						</tr>
						<tr>
							<td class="docLabel">Senha de acesso</td>
							<td class="docLabel">:</td>
							<td class="docCmpLivre"><input type="password" name="txtSenha1" class="docCmpLivre" value="<% = senha %>"></td>
						</tr>
						<tr>
							<td class="docLabel" align="right">(repetir)</td>
							<td class="docLabel"></td>
							<td class="docCmpLivre"><input type="password" name="txtSenha2" class="docCmpLivre" value="<% = senha %>"></td>
						</tr>
						<tr>
							<td class="docLabel">Perfil</td>
							<td class="docLabel"></td>
							<td class="docSBorda">
								<select name="cmbPerfil" class="docCmpLivre">
	<%
	xSQL = "select perfId, perfNome"
	xSQL = xSQL & " from PerfilAcesso order by perfNome"
	Set xRs = gConexao.Execute(xSQL)
	while not xRs.EOF
		if perfil <> "" then
			if xRs("perfId") = perfil then
				xSelected = " selected "
			else
				xSelecionar = ""
			end if
		end if
		response.write vbtab & "<option value=""" & xRs("perfId") & """ class=""campo"" " & xSelected & ">" & xRs("perfNome") & "</option>"
		xSelected = ""
		xRs.MoveNext
	wend
	xrs.close
	set xRs = nothing
	%>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2" height="20"></td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<table>
						<tr>
							<td height="28" width="80" align="center"><img border="0" src="../images/btnSalvar.gif" class="botao" onclick="javascript:validar();"></td>
							<td width="10"></td>
							<td width="80" align="center"><img border="0" src="../images/btnCancelar.gif" class="botao" onclick="javascript:window.close();"></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
	</body>
</HTML>