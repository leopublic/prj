<% 
    Server.ScriptTimeout=60000 
%>
<!--#include file="../includes/clsBanco.inc"-->
<!--#include file="../includes/clsControleAcesso.inc"-->
<!--#include file="../includes/funcoes.inc"-->
<!--#include file="../includes/upload.inc"-->
<% 
Dim strFileName
'
'Pegando path f�sico do servidor - INI
xpath = server.mappath("../") & "\tmp\"
'Pegando path f�sico do servidor - FIM
'
'Instanciando classe de upload - INI
Dim MyUploader
Set MyUploader = New FileUploader
MyUploader.Upload()
'Instanciando classe de upload - FIM
'
'Pegando par�metros auxiliares - INI
lupd_projId  = MyUploader.Form("upd_projId")
lupd_cntrId  = MyUploader.Form("upd_cntrId")
lupd_wbscId  = MyUploader.Form("upd_wbscId")
lupd_servInd = MyUploader.Form("upd_servInd")
lupd_fcn_Id  = MyUploader.Form("upd_fcn_Id")
lupd_tipo    = MyUploader.Form("upd_tipo")
'Pegando par�metros auxiliares - FIM
'
'Pegand arquivo - INI
Dim lFile
For Each lFile In MyUploader.Files.Items
    '
    strFileName = lFile.FileName 
	'session("msg") = lFile.FileName & vbcrlf
	'session("msg") = ""
	'For nIndex = 1 to LenB(lFile.FileData)
	'	session("msg") = session("msg") & Chr(AscB(MidB(lFile.FileData,nIndex,1)))
	'Next
	'session("msg") = lFile.FileData & vbcrlf
	'exit for
    if strFileName = "" then
        session("msg") = "Arquivo informado � inv�lido!"
    else
        '
        'Salva csv no disco - INI
        strFileName = (xpath & "WBS_MSPROJECT_" & lupd_wbscId & "_" & Int((Rnd*100)) & ".csv")
        lFile.SaveAsFile strFileName
        'lFile = nothing
        'Salva csv no disco - FIM
        '
        'Importando arquivo - INI
        Dim gConexao
        AbrirConexao
        pseparador=";"
        dim fs,sr,lc
        Dim linha
        Dim campos
        set fs=Server.CreateObject("Scripting.FileSystemObject")
        if fs.FileExists(strFileName) then
            response.write " - In�cio leitura <br />"
            set sr=fs.OpenTextFile(strFileName,1,False)
            response.write " - Abriu arquivo <br />"
            lc=0
            lTX_Erros = ""
            do while not sr.AtEndOfStream
                lc=lc+1
                linha = ""
                linha=sr.ReadLine
                campos=split(linha,pseparador)
                response.write " - Linha atual: " & lc & " <br />"
                response.write " - Nr campos: " & ubound(campos) & " <br />"
                if (lc=1) then 'Estamos no cabe�alho
                    campos=split(ucase(linha),pseparador)
                    response.write " - Linha Cab: " & ucase(linha) & " <br />"
                    if (ubound(campos) <> 21) then
                        'response.write "Entrou no erro de n� de colunas <br />"
                        lTX_Erros = "N� de colunas de cabe�alho do arquivo informado � inv�lido!" & vbcrlf & "Verifique exporta��o feita no MS Project!"
                        exit do
                    else
                        Dim lHeaders 
                        lHeaders = Array("ID", "OUTLINE LEVEL", "IWBCCODIGO", "TAREFA", "BL.WORK", "BASELINE WORK 1", "BASELINE COST", "COST", "% COMPLETE", "% WORK COMPLETE", "ACTUAL COST", "DURATION", "START", "FINISH", "BASELINE 1 START", "BASELINE 1 FINISH", "START REPLANEJADO", "FINISH REPLANEJADO", "START REAL", "FINISH REAL", "PREDECESSORS", "IDSYS")
                        for i = 0 to ubound(lHeaders)
                            if (campos(i) <> lHeaders(i)) then
                                response.write campos(i) & " <> " & lHeaders(i) & " <br />"
                                'response.write "Entrou no erro de cabe�alho incorreto(coluna " & (i+1) & ") <br />"
                                lTX_Erros = "Cabe�alho do arquivo informado � inv�lido(coluna " & (i+1) & ")!" & vbcrlf & "Verifique exporta��o feita no MS Project!"
                                exit for
                            end if
                        next
                        if lTX_Erros <> "" then 
                            exit do
                        end if
                    end if
                else
                    response.write "Linha: " & linha & " <br />"
                    on error resume next
                    '
                    'Importando campos relevantes
                    lDuracao           = trim(lcase(campos(11)))
					licmpBaselineWork1 = trim(lcase(campos(5)))
					licmpCost          = trim(lcase(campos(7)))
                    Saltos = Array("days","day","dias","dia","hrs","hr")
                    For i = 0 to ubound(Saltos)
						lDuracao           = trim(replace(lDuracao,Saltos(i),""))
                        licmpBaselineWork1 = trim(replace(licmpBaselineWork1,Saltos(i),""))
						licmpCost          = trim(replace(licmpCost,Saltos(i),""))
                    Next
                    '
					lsql = ""
					lsql = lsql & "exec ICMP_Importar "
					lsql = lsql & "     @piwbcId                = " & NumberToBD(campos(21),0)
                    lsql = lsql & "   , @piwbcDuracaoEst        = " & NumberToBD(lDuracao,2)
                    lsql = lsql & "   , @piwbcDtInicioEst       = " & ProjectDateToBD(campos(12))
					lsql = lsql & "   , @picmpBaselineWork1     = " & NumberToBD(licmpBaselineWork1,2)
                    lsql = lsql & "   , @picmpCost              = " & NumberToBD(licmpCost,2)
					lsql = lsql & "   , @picmpBaseline1Start    = " & ProjectDateToBD(campos(14))
                    lsql = lsql & "   , @picmpBaseline1Finish   = " & ProjectDateToBD(campos(15))
                    lsql = lsql & "   , @picmpStartReplanejado  = " & ProjectDateToBD(campos(16))
                    lsql = lsql & "   , @picmpFinishReplanejado = " & ProjectDateToBD(campos(17))
                    lsql = lsql & "   , @picmpStartReal         = " & ProjectDateToBD(campos(18))
                    lsql = lsql & "   , @picmpFinishReal        = " & ProjectDateToBD(campos(19))
                    lsql = lsql & "   , @picmpPredecessor       = " & CurrencyToBD(campos(20),0)
					'response.write lsql & "<br>"
                    gConexao.execute(lsql)
                    if err.number <> 0 then
                        lTX_Erros = lTX_Erros & vbcrlf & err.description & " -> SQL: " & lsql
                        'exit do
                    end if
					'
                end if
            loop
            sr.close 
            session("msg") = lTX_Erros
            '
            'Eliminando arquivo do servidor
            if fs.FileExists(strFileName) then
                fs.DeleteFile(strFileName)
            end if
            '
        else
            session("msg") = "Falha no upload do arquivo " & replace(strFileName,"\","\\") & "!"
        end if
        set sr=nothing
        set fs=nothing
        'Importando arquivo - FIM
        '
    end if
    
Next 
'Pegand arquivo - FIM
'
response.write "session(""msg"") = " & session("msg")
if trim(session("msg")) = "" then
    session("msg") = "Sincroniza��o finalizada com sucesso!"
else
    session("msg") = "Sincroniza��o finalizada com erros: " & session("msg")
end if
'Retornando a p�gina de controle de WBS - INI
Response.Redirect "wbscontrato_ppueap.asp?projId=" & lupd_projId & "&cntrId=" & lupd_cntrId & "&wbscId=" & lupd_wbscId & "&servInd=" & lupd_servInd & "&tipo=2&fcn_Id=" & lupd_fcn_Id
'Retornando a p�gina de controle de WBS - FIM
'
%>