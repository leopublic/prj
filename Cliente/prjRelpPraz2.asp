<% Response.Expires= -1%>
<% Server.ScriptTimeout=60000 %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Controle remoto de projetos</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<!--#include file="../includes/basFormatador.inc"-->
	</HEAD>
	<body class="cabecalho" >
		<!--#include file="../includes/varControleAcesso.inc"-->
		<!--#include file="../includes/varBanco.inc"-->
		<% 
		Dim objConn
		Dim objRs
		Dim lngFileID
		Dim xNomeCampo 
		AbrirConexao
		'
		' Atualiza outros campos informados...
		Dim xTermReal(6)
		Dim xTermRepl(6)
		projId = Request.form("projId")
		usuaId = Request.form("usuaId")
		xQtdServicos = Request.form("QtdServicos")
		xQtdServicos = cInt(xQtdServicos)
		'response.write(xQtdServicos) & "<br>"
		for i = 1 to xQtdServicos
			cntrId = Request.form("cntrId" & cStr(i))
			PercAvanEst = Request.form("PercAvanEst" & cStr(i))
			PercAvanAtua = Request.form("PercAvanAtua" & cStr(i))
			PercAvanReal = Request.form("PercAvanReal" & cStr(i))
			usuaIdCont = Request.Form("cmbUsuaId" & cStr(i))
			for fase = 1 to 4
				x = Request.form("TermReal" & fase & cStr(i))
				if not isdate(x) then
					xTermReal(fase) = "null"
				else
					xTermReal(fase) = "'" & FmtDataBd(x) & "'"
				end if
				x = Request.form("TermRepl" & fase & cStr(i))
				if not isdate(x) then
					xTermRepl(fase) = "null"
				else
					xTermRepl(fase) = "'" & FmtDataBd(x) & "'"
				end if
			next 
			xSQL = ""
			xSQL = xSQL & "exec CNTR_AtualizarPrazos "
			xSQL = xSQL & "       @pusuaId = " & usuaId
			xSQL = xSQL & "     , @pcntrId = " & cntrId
			xSQL = xSQL & "     , @pTermReal1 = " & xTermReal(1)
			xSQL = xSQL & "     , @pTermReal2 = " & xTermReal(2)
			xSQL = xSQL & "     , @pTermReal3 = " & xTermReal(3)
			xSQL = xSQL & "     , @pTermReal4 = " & xTermReal(4)
			xSQL = xSQL & "     , @pTermReal5 = null"
			xSQL = xSQL & "     , @pTermRepl1 = " & xTermRepl(1)
			xSQL = xSQL & "     , @pTermRepl2 = " & xTermRepl(2)
			xSQL = xSQL & "     , @pTermRepl3 = " & xTermRepl(3)
			xSQL = xSQL & "     , @pTermRepl4 = " & xTermRepl(4)
			xSQL = xSQL & "     , @pTermRepl5 = null"
			xSQL = xSQL & "     , @pPercAvanEst  = " & NumFmtBd(PercAvanEst)
			xSQL = xSQL & "     , @pPercAvanAtua = " & NumFmtBd(PercAvanAtua)
			xSQL = xSQL & "     , @pPercAvanReal = " & NumFmtBd(PercAvanReal)
			xSQL = xSQL & "     , @pusuaIdCont   = " & cStr(usuaIdCont)
			'response.write xSQL & "<br>"
			gConexao.execute xSQL
		next
		'response.End()
		xComePraz = request.form("txtComePraz") 
		xComePraz = replace(xComePraz, "'", "''")
		xSQL = " update Projeto set projComePraz = '" & xComePraz& "' where projId = " & Cstr(projId)
		'Response.write xSQL
		'response.End()
		gConexao.execute xSQL
		Session("TituloMsg") = "ALTERAR ESTIMATIVAS DOS CONTRATOS"
		Session("Msg") = "Estimativas dos contratos alteradas com sucesso!"
		Response.redirect ("gerMensagem.asp")
		%>
	</body>
</HTML>