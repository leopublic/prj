<% Response.Expires= -1%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Controle de projetos pela internet</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<link rel="stylesheet" type="text/css" href="../EstiloMenu.css">
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsProjeto.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<script language="JavaScript" type="text/javascript" src="../scripts/AbrirJanela.js"></script>
	</HEAD>
	<body class="cabecalho" >
<%  lPerfisAutorizados = "12345"
	VerificarAcesso()

   Dim gmenuGeral
   Dim gMenuSelecionado
   Dim gOpcaoSelecionada
   gMenuSelecionado = "Projetos"
   gOpcaoSelecionada = "Projetos"
   gmenuGeral = "Projetos"
   PastaSelecionada = "Projetos"

%>
		
		<table width="100%" align="left" height="100%" cellpadding="0" cellspacing="0" style="border-style:solid;border-width:0;border-color:black;">
			<tr height="50" >
				<td colspan=2 style="background-repeat:repeat-y;background-position:right;background-color:#396D60;" background="../images/blend3.jpg">
					<table width="100%" height="100%" valing="middle">
						<tr>
							<td width="5"><img src="../images/logo.gif"></td>
							<td><span style="font-family:tahoma;font-size:16pt;font-weight:bold;color:#FFFFFF">PRJ Offline</span></td>
                            <td width="300" style="text-align:right" style='background:transparent'><img src="../images/logo_wm_portal2.gif"></td>
						</tr>
					</table>				
				</td>
			</tr>
			<tr>
				<td style="padding:5px; width:200px;" class="blocoOpcoes" valign='top'>
                    <table class="menu" width='100%'>
                        <tbody>
                            <tr>
                                <td style="height: 5px; font-size: 1px;" width="13">&nbsp;</td>
                                <td style="height: 5px; font-size: 1px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="opcSep">&nbsp;</td>
                                <td class="opcSep">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="opcSel">&nbsp;</td>
                                <td class="opcMenu" style='color:#FFF;'>Projetos</td>
                            </tr>
                        </tbody>
                    </table>
				</td>
				<td valign="top" style="padding:5px" class="blocoPagina">
    				<form>
    					<table>
        					<tr>
        						<td class="docTit">PROJETOS</td>
        					</tr>
        					<tr>
        						<td background="../images/pont_cinza_h.gif" style="height:2"></td>
        					</tr>
        					<tr>
        						<td style="height:5"></td>
        					</tr>
        					<tr>
        						<td>
        							<table class="grid">
                                        <tr>
                                            <td width="20px" class="GridCab">&nbsp;</td>
                                            <td width="90px" class="GridCab" valign="bottom">N�mero</td>
                                            <td width="230px" class="GridCab" valign="bottom">Nome</td>
                                            <td width="210px" class="GridCab" valign="bottom">Gerente do projeto</td>
                                            <td width="210px" class="GridCab" valign="bottom">Supervisor de constru��es</td>
                                            <td class="GridCab">&nbsp;</td>
    									</tr> 
	<%
	Dim gConexao 
	Dim gConexao2
	Dim projId() 
	i = -1
	xSQL = ""
	xSQL = xSQL & "select Projeto.projId"
	xSQL = xSQL & "     , projNum"
	xSQL = xSQL & "     , projNome"
	xSQL = xSQL & "     , isnull(U1.usuaNome, '(n�o definido)') projNomeGer"
	xSQL = xSQL & "     , isnull(U2.usuaNome, '(n�o definido)') projNomeSup"
	xSQL = xSQL & "     , isnull(emprNome, '(n�o definido)') emprNome"
	xSQL = xSQL & "  from (((Projeto left join Empreiteira on Empreiteira.emprId = Projeto.emprIdSupCmp )"
	xSQL = xSQL & "       left join Usuario U1 on U1.usuaId = usuaIdGer)"
	xSQL = xSQL & "       left join Usuario U2 on U2.usuaId = usuaIdSup)"
	xSQL = xsql & " where projAtivo = 0 "
	xSQL = xSQL & " order by projNum"
	AbrirConexao
	AbrirConexao2
	Set xRs = gConexao.Execute(xSQL)
	if xRs.EOF then
	    Response.write "<tr>" & vbcrlf
	    Response.write "<td colspan=""6"">Nenhum projeto cadastrado</td>" & vbcrlf
	    Response.write "</tr>" & vbcrlf
	else
		estilo = "GridLinhaPar"
		while not xRs.EOF
			Response.write "<tr>" & vbcrlf
			'response.write vbtab & "<td class=""" & estilo & """><a href=""PRJ" & xRs("projNum") & "/prjResumo.html""><img src=""../images/folder.gif"" style=""border:0""></a></td>"
            response.write vbtab & "<td class=""" & estilo & """><a name='LINKREL' href=""file:///projetos/PRJ" & xRs("projNum") & "/Cliente/prjResumo.html""><img src=""../images/folder.gif"" style=""border:0""></a></td>"
			response.write vbtab & "<td class=""" & estilo & """>" & xRs("projNum") & "</td>"
			response.write vbtab & "<td class=""" & estilo & """>" & xRs("projNome") & "</td>"
			response.write vbtab & "<td class=""" & estilo & """>" & xRs("projNomeGer") & "</td>"
			response.write vbtab & "<td class=""" & estilo & """>" & xRs("projNomeSup") & "</td>"
			response.write vbtab & "<td class=""" & estilo & """>&nbsp;</td>"
			Response.write "</tr>" & vbcrlf
			xRs.MoveNext
			if estilo = "GridLinhaPar" then
				estilo = "GridLinhaImpar"
			else
				estilo = "GridLinhaPar"
			end if
		wend
	end if
	gConexao2.close

	%>
    								<tr>
    									<td colspan="6" class="GridRodape">&nbsp;</td>
    								</tr>
    							</table>
    						</td>
    					</tr>
    					<tr>
    						<td class="opcSubMenu">&nbsp;</td>
    					</tr>
    					</table>
    				</form>
				</td>
			</tr>			
		<!--#include file="../includes/rod.inc"-->
	</body>
    <script language="JavaScript" type="text/javascript">
        if(window.location.href.substr((window.location.href.length-3))!='asp'){ //para acertar as refs dos links dos projetos
            var _prj = document.getElementsByName('LINKREL');
            var _ixc = 0;
            var _newref = window.location.href.replace('prjIndiceCD.html','').replace('prjIndiceCD.htm','');
            for (_ixc=0;_ixc<_prj.length;_ixc++) {
                _prj[_ixc].href = _prj[_ixc].href.replace('file:///', _newref);
            }
        }
    </script>        
</HTML>