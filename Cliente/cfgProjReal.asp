<% Response.Expires= -1%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Projetos</title><%%>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<!--#include file="../includes/basFormatador.inc"-->
		<!--#include file="../includes/basValidacao.inc"-->
		<script language="JavaScript" type="text/javascript" src="../scripts/NumberFormat.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/Mid.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/ValidacaoDatas.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/PreparaJanela.js"></script>
		<SCRIPT language="JavaScript">
			function CamposOk(){
				var QtdCntr = document.form.QtdServicos.value
			    for(i=1;i<=QtdCntr;i++)
				{
					for(j=1;j<=4;j++)
					{   eval('var Cmp = document.form.cntrTermEst'+j+i)
					    if(Cmp.value!='')
						{	if(isDate(Cmp)==false)
 							{	Cmp.select();
								Cmp.focus();
								return false;
							}
						}
					}
				}
				return true
			}
			function FormatarPuro(campo){
				var inputValue = campo;
				inputValue.value = FormatarNumero(inputValue.value, '.', ',', ',');
			}
			function Formatar(campo){
				var inputValue = document.getElementById(campo+'Fmt');
				var inputHidden = document.getElementById(campo);
				inputValue.value = FormatarNumero(inputValue.value, '.', ',', ',');
				inputHidden.value = FormatarNumero(inputValue.value, '', ',', '.');
				var Total = Math.round(parseFloat(document.form.GerReal.value)*100 )
				Total = Total + Math.round(parseFloat(document.form.SupcReal.value)*100)
				Total = Total + Math.round(parseFloat(document.form.Etc.value)*100)
				Total = Total / 100
				Total = Total + '';
				document.form.TotalEst.value = FormatarNumero(Total, '.', '.', ',');
			}


			function Validar(){
                if (CamposOk()==true)
			    { document.form.submit();}
			}
		</SCRIPT>
	</HEAD>
	<body class="PopUpEntrada" onLoad="javascript:PreparaJanela(680, 800);">
		<!--#include file="../includes/varControleAcesso.inc"-->
		<!--#include file="../includes/varBanco.inc"-->
<%
    lPerfisAutorizados = "51"
	VerificarAcesso
	AbrirConexao
	projId = Request.Querystring("projId")
	xSQL = "select top 1 relpId, relpCustGerReal, relpCustSupcReal, relpCustEtc, relpSemIni, relpSemFim "
	xSQL = xSQL & " from RelatorioProjeto where projId = " & projId & " order by relpSemFim desc"
	set xRS = gConexao.execute(xSQL)
	if not xRs.EOF then
		relpId = xRs(0)
		GerReal = xRs("relpCustGerReal")
		SupcReal = xRs("relpCustSupcReal")
		Etc = xRs("relpCustEcn")
		SemIni = xRs("relpSemIni")
		SemFim = xRs("relpSemFim")
	else
		relpId = 0
	end if
	xRS.close
	Set xRs = nothing

	xsql = ""
	xSQL = xsql & "select cntrNomeServ"
	xsql = xsql & "     , isnull(relcVlrOrig, 0) relcVlrOrig"
	xsql = xsql & "     , isnull(relcVlrAtu,  0) relcVlrAtu"
	xsql = xsql & "     , isnull(relcVlrFcn,  0) relcVlrFcn"
	xsql = xsql & "     , isnull(relcVlrPlei, 0) relcVlrPlei"
	xsql = xsql & "     , convert(varchar(10), relcTermReal1, 3) relcTermReal1"
	xsql = xsql & "     , convert(varchar(10), relcTermReal2, 3) relcTermReal2"
	xsql = xsql & "     , convert(varchar(10), relcTermReal3, 3) relcTermReal3"
	xsql = xsql & "     , convert(varchar(10), relcTermReal4, 3) relcTermReal4"
	xSQL = xsql & "     , Contrato.cntrId"
	xsql = xsql & "  from (Contrato left join RelatorioContrato on RelatorioContrato.cntrId = Contrato.cntrId and RelatorioContrato.relpId = " & relpId & ")"
	xsql = xsql & " where Contrato.projId = " & projId
	xsql = xsql & " order by servOrd"
	set xRs = gConexao.execute(xsQL)
	xServ = ""
	i = 0
	while not xRs.Eof
		i = i + 1
		xTermReal1 = xRs("relcTermReal1")
		xTermReal2 = xRs("relcTermReal2")
		xTermReal3 = xRs("relcTermReal3")
		xTermReal4 = xRs("relcTermReal4")
		estilo = "class=""docCmpLivre"" style=""height:20;border:1;border-style:solid;border-color:#CCCCCC;text-align:center"" onblur=""javascript:this.value=FormatarData(this);"""
		xServ = xServ & "<tr>" & vbcrlf
		xServ = xServ & "<input type=""hidden"" name=""cntrId"& cStr(i) & """ value=""" & xRs("cntrId") & """>"
		xServ = xServ & vbtab & "<td class=""gridcablinha"" align=""left"">" & xRs("cntrNomeServ") & "</td>" & vbcrlf
		xServ = xServ & vbtab & "<td class=""gridlinha""><input type=""text"" name=""TermReal1" & cStr(i) & """ value=""" & xTermReal1 & """ " & estilo & "></td>" & vbcrlf
		xServ = xServ & vbtab & "<td class=""gridlinha""><input type=""text"" name=""TermReal2" & cStr(i) & """ value=""" & xTermReal2 & """ " & estilo & "></td>" & vbcrlf
		xServ = xServ & vbtab & "<td class=""gridlinha""><input type=""text"" name=""TermReal3" & cStr(i) & """ value=""" & xTermReal3 & """ " & estilo & "></td>" & vbcrlf
		xServ = xServ & vbtab & "<td class=""gridlinha""><input type=""text"" name=""TermReal4" & cStr(i) & """ value=""" & xTermReal4 & """ " & estilo & "></td>" & vbcrlf
		xServ = xServ & "</tr>" & vbcrlf

		xCust = xCust & "<tr>" & vbcrlf
		xCust = xCust & vbtab & "<td class=""gridcablinha"" align=""left"">" & xRs("cntrNomeServ") & "</td>" & vbcrlf
		xCust = xCust & vbtab & "<td class=""gridlinha""><input type=""text"" name=""relcVlrOrig" & cStr(i) & """ value=""" & FormatNumber(xRs("relcVlrOrig")) & """ class=""docCmpLivre"" style=""height:20;border:1;border-style:solid;border-color:#CCCCCC;text-align:right"" onBlur=""javascript:FormatarPuro(this);""></td>" & vbcrlf
		xCust = xCust & vbtab & "<td class=""gridlinha""><input type=""text"" name=""relcVlrAtu" & cStr(i) & """ value=""" & FormatNumber(xRs("relcVlrAtu")) & """ class=""docCmpLivre"" style=""height:20;border:1;border-style:solid;border-color:#CCCCCC;text-align:right"" onBlur=""javascript:FormatarPuro(this);""></td>" & vbcrlf
		xCust = xCust & vbtab & "<td class=""gridlinha""><input type=""text"" name=""relcVlrFcn" & cStr(i) & """ value=""" & FormatNumber(xRs("relcVlrFcn")) & """ class=""docCmpLivre"" style=""height:20;border:1;border-style:solid;border-color:#CCCCCC;text-align:right"" onBlur=""javascript:FormatarPuro(this);""></td>" & vbcrlf
		xCust = xCust & vbtab & "<td class=""gridlinha""><input type=""text"" name=""relcVlrPlei" & cStr(i) & """ value=""" & FormatNumber(xRs("relcVlrPlei")) & """ class=""docCmpLivre"" style=""height:20;border:1;border-style:solid;border-color:#CCCCCC;text-align:right"" onBlur=""javascript:FormatarPuro(this);""></td>" & vbcrlf
		xCust = xCust & "</tr>" & vbcrlf

		xRs.Movenext
	wend
	xRs.Close
	set xRs = Nothing

%>

	<form onSubmit="Validar();" action="cfgProjReal2.asp" method="post" name="form">
		<input type="hidden" name="projId" value="<% = projId %>" >
		<input type="hidden" name="GerReal"  value="<% = NumFmtBd(GerReal) & " "%>">
		<input type="hidden" name="SupcReal" value="<% = NumFmtBd(SupcReal) & " " %>">
		<input type="hidden" name="Etc"  value="<% = NumFmtBd(Etc) & " " %>">
		<input type="hidden" name="QtdServicos" value="<% = i %>">
		<table width="100%">
			<tr>
				<td width="15"></td>
				<td></td>
			</tr>
			<tr>
				<td colspan="2" class="docTit"><%=Session("projNome")%> - Atualização de status</td>
			</tr>
			<tr>
				<td colspan="2" height="15"></td>
			</tr>
			<tr>
				<td colspan="2" class="docTit">Custos de construções</td>
			</tr>
			<tr>
				<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td align="left">
					<table class="doc" width="100%">
						<tr>
							<td class="docLabel">Hh gerenciamento comprometido</td>
							<td width="10" class="docLabel">:</td>
							<td width="100"  class="docCmpLivre"><input type="text" name="GerRealFmt" value="<% = FormatNumber(GerReal) %>" class="docCmpLivre" style="text-align:right" onBlur="javascript:Formatar('GerReal');"></td>
						</tr>
						<tr>
							<td class="docLabel">Hh sup. campo Comprometido</td>
							<td class="docLabel">:</td>
							<td align="right" class="docCmpLivre"><input type="text" name="SupcRealFmt" value="<% = FormatNumber(SupcReal) %>" class="docCmpLivre" style="text-align:right" onBlur="javascript:Formatar('SupcReal');"></td>
						</tr>
						<tr>
							<td class="docLabel">ETC</td>
							<td class="docLabel">:</td>
							<td align="right" class="docCmpLivre"><input type="text" name="EtcFmt" value="<% = FormatNumber(Etc) %>" class="docCmpLivre" style="text-align:right" onBlur="javascript:Formatar('Etc');"></td>
						</tr>
						<tr>
							<td colspan="3" class="docLabel" height="5" style="font-size:1;border-bottom-style:solid;border-bottom-width:1;border-bottom-color:#94bdd1">&nbsp;</td>
						</tr>
						<tr>
							<td class="docLabel">Total hh previsto:</td>
							<td class="docLabel">:</td>
							<td align="right" class="docSBorda"><input type="text" name="TotalReal"  value="<% = FormatNumber(TotalReal) %>" class="docCmpLivre"  readonly="read-only" style="text-align:right;font-weight:bold"></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="docLabel">Comentários sobre as variações:</td>
			</tr>
			<tr>
				<td colspan="2" class="docCmpLivre"><textarea name="ComeCust" class="docCmpLivre" rows="3"  wrap="soft"></textarea> </td>
			</tr>
			<tr>
				<td colspan="2" height="15"></td>
			</tr>
			<tr>
				<td colspan="2" class="docTit">Prazos dos contratos</td>
			</tr>
			<tr>
				<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td align="left">
					<table class="grid" style="padding:5" width="100%">
						<tr>
							<td class="GridCab" align="Left">Contrato</td>
							<td width="90" class="GridCab" align="center">Emissão MD</td>
							<td width="90" class="GridCab" align="center">Contratação</td>
							<td width="90" class="GridCab" align="center">Mobilização</td>
							<td width="90" class="GridCab" align="center">Desmobilização</td>
						</tr>
						<% Response.write(xServ) %>
						<tr>
							<td class="Gridrodape">&nbsp;</td>
							<td class="Gridrodape">&nbsp;</td>
							<td class="Gridrodape">&nbsp;</td>
							<td class="Gridrodape">&nbsp;</td>
							<td class="Gridrodape">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="docLabel">Comentários sobre os prazos dos contratos:</td>
			</tr>
			<tr>
				<td colspan="2" class="docCmpLivre"><textarea name="ComePraz" class="docCmpLivre" rows="3"  wrap="soft"></textarea> </td>
			</tr>
			<tr>
				<td colspan="2" height="15"></td>
			</tr>
			<tr>
				<td colspan="2" class="docTit">Custos dos contratos</td>
			</tr>
			<tr>
				<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td align="left">
					<table class="grid" style="padding:5" width="100%">
								<tr>
									<td class="GridCab" align="Right">&nbsp;</td>
									<td width="70" class="GridCab" align="right">Valor<br>Original<br>(<%=Session("moedSigla")%>)</td>
									<td width="70" class="GridCab" align="right">Valor<br>Atual<br>(<%=Session("moedSigla")%>)</td>
									<td width="70" class="GridCab" align="right">FCN<br>(<%=Session("moedSigla")%>)</td>
									<td width="70" class="GridCab" align="right">Pleitos<br>(<%=Session("moedSigla")%>)</td>
									<td width="70" class="GridCab" align="right">Valor<br>Final<br>(<%=Session("moedSigla")%>)</td>
								</tr>
						<% Response.write(xCust) %>
						<tr>
							<td class="Gridrodape">&nbsp;</td>
							<td class="Gridrodape">&nbsp;</td>
							<td class="Gridrodape">&nbsp;</td>
							<td class="Gridrodape">&nbsp;</td>
							<td class="Gridrodape">&nbsp;</td>
							<td class="Gridrodape">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="docLabel">Comentários sobre os custos dos contratos:</td>
			</tr>
			<tr>
				<td colspan="2" class="docCmpLivre"><textarea name="ComeCusc" class="docCmpLivre" rows="3"  wrap="soft"></textarea> </td>
			</tr>
			<tr>
				<td colspan="2" height="15"></td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<table>
						<tr>
							<td height="28" width="80" align="center"><img border="0" src="../images/btnSalvar.gif" class="botao" onclick="javascript:Validar();"></td>
							<td width="10"></td>
							<td width="80" align="center"><img border="0" src="../images/btnCancelar.gif" class="botao" onclick="javascript:window.close();"></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
	</body>
</HTML>