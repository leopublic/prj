<!--#include file="../includes/clsBanco.inc"-->
<!--#include file="../includes/funcoes.inc"-->
<%
Session.LCID = 1046 'Pt-Br'
Response.Expires= 0
Response.AddHeader "PRAGMA", "NO-CACHE"
Response.Charset = "iso-8859-1"

'Conex�o
'on error resume next
Dim gConexao
AbrirConexao

'Fun��es
function f_cria_said(pprojId, pusuaId, pop, pls, pnc, psl)
'on error goto erro_exec
	retorno = "" 
	if(trim(psl)="") then
		retorno = NovoErroXML("00000004", "Solicitante n�o informado", chamada)
	else 
		if ((trim(pprojId)="") or (trim(pprojId)="0")) then
			retorno = NovoErroXML("00000006", "Projeto n�o identificado", chamada)
		else
			lnconId="0" 'N�o-Conformidade
			if (pOPCAO="2") then lnconId=f_valida_naoconformidade(pnc, pprojId)

			if (not isnumeric(trim(lnconId))) then
				retorno = lnconId
			else
				'Inicializando recebimento
				if ((trim(pnc)="") or (trim(pnc)="0")) then pnc = "null"
				if ((trim(pls)="") or (trim(pls)="0")) then pls = "null"
				lsaidId = ""
				
				lsql = ""
				lsaidId = pCad_NovoId("SaidaMaterial", gConexao)
				lsql = lsql & "insert into saidamaterial(saidId, projId, listId, nconId, saidDtHrCad, usuaIdCad, saidRecebedor) "
				lsql = lsql & "values (" & lsaidId & ", " & lprojId & ", " & pls & ", " & pnc & ", getdate(), " & pusuaId & ", '" & psl & "') "
				'response.write lsql 
				gConexao.execute(lsql)
				
				retorno = lsaidId
			end if
		end if
	end if
	f_cria_said = retorno
'	exit function
'erro_exec:
'	f_cria_rece = NovoErroXML("[" & err.number & "] " & err.description, chamada)
end function

function f_said_item(psaidId, pitemId, pitelId, pitesQtd, punidId)
'on error goto erro_exec
	retorno = "" 
	if ((trim(pitemId) = "") or (not isNumeric(pitemId))) then
		retorno = NovoErroXML("00000003", "Item n�o identificado", chamada)
	else
		if ((trim(pitelId)="") or (trim(pitelId)="0")) then pitelId = "null"
		if ((trim(punidId)="") or (trim(punidId)="0")) then punidId = "null"
		
		'Inserindo o item em si
		lsql = ""
		litesId = pCad_NovoId("ItemSaida", gConexao)
		lsql = lsql & "insert into itemsaida (itesId, saidId, itemId, itelId, itesQtd) "
		'lsql = lsql & "values (" & litesId & ", " & psaidId & ", " & pitemId & ", " & litelId & ", " & punidId & ", " & CurrencyToBD(pitesQtd,2) & ") "
		lsql = lsql & "values (" & litesId & ", " & psaidId & ", " & pitemId & ", " & litelId & ", " & CurrencyToBD(pitesQtd,2) & ") "
		'response.write lsql 
		gConexao.execute(lsql)

		if ((pitelId <> "") and (pitelId <> "null")) then 'Atualizando saldo do item na lista
			lsql = ""
			lsql = lsql & "update ItemLista "
			lsql = lsql & "   set itelQtdUtilizada = isnull(itelQtdUtilizada,0) + " & CurrencyToBD(pitesQtd,2)
			lsql = lsql & " where itelId = " & pitelId
			gConexao.execute(lsql)
		end if
	end if
	
	f_said_item = retorno
'	exit function
'erro_exec:
'	f_rec_item = NovoErroXML("[" & err.number & "] " & err.description, chamada)
end function

function f_monta_lista(pprojId, pls, pnc, psl, pop, pitems, pqtds, plins, pscript)
'on error goto erro_exec
	retorno = ""
	if (pitems <> "") then
		cab = ""
		rod = ""
		'
		cab = cab & "<html>" & vbcrlf
		cab = cab & "<head>" & vbcrlf
		cab = cab & "<title>:: PRJ ::</title>" & vbcrlf
		cab = cab & "<style type='text/css'>" & vbcrlf
		cab = cab & 	"table.grid 	      {border-collapse:collapse;width:100%} " & vbcrlf
		cab = cab & 	"tr.GridCab td        {font-family:tahoma;font-size:8pt;font-weight:bold;border-bottom:2px;border-bottom-style:solid;border-bottom-color:#999999;vertical-align:bottom;} " & vbcrlf
		cab = cab & 	"tr.GridLinhaPar td   {font-family:tahoma; font-size:8pt; font-weight:normal; padding:2px; background-color:white; border-bottom-width:1px; border-top-width:1px; border-top-style:solid; border-bottom-style:solid; border-color:#F0F0F0;} " & vbcrlf
		cab = cab & 	"tr.GridLinhaImpar td {font-family:tahoma; font-size:8pt; font-weight:normal; padding:2px; background-color:white; border-bottom-width:1px; border-top-width:1px; border-top-style:solid; border-bottom-style:solid; border-color:#F0F0F0;} " & vbcrlf
		cab = cab & 	"td.docTit 			  {color:red;text-align:left;padding-left:4pt;font-family:tahoma;font-size:10pt;font-weight:bold;letter-spacing:2px} " & vbcrlf
		cab = cab & "</style>" & vbcrlf
		cab = cab & "<script type='text/javascript' language='javascript'>" & vbcrlf
		cab = cab & pscript & vbcrlf
		cab = cab & "</script>" & vbcrlf
		cab = cab & "</head>" & vbcrlf
		cab = cab & "<body>" & vbcrlf
		cab = cab & 	"<table cellspacing=0 cellpadding=0 width='99%' class='grid'>" & vbcrlf
		cab = cab & 		"<tr>" & vbcrlf
		cab = cab & 			"<td colspan='8' class='docTit'>" & Session("projNome") & " - NOVA SA�DA<br><br></td>" & vbcrlf
		cab = cab & 		"</tr>" & vbcrlf
		cab = cab & 		"<tr class='GridCab'>" & vbcrlf
		cab = cab & 			"<td style='width:5px'>&nbsp;</td>" & vbcrlf
		cab = cab & 			"<td style='width:100px'>C�d. JDE</td>" & vbcrlf
		cab = cab & 			"<td style='width:330px'>Descri��o</td>" & vbcrlf
		cab = cab & 			"<td style='width:70px' >Unidade</td>" & vbcrlf
		cab = cab & 			"<td style='width:100px'>Qtd pedida</td>" & vbcrlf
		cab = cab & 			"<td style='width:100px'>Qtd dispon�vel</td>" & vbcrlf
		cab = cab & 			"<td style='width:100px'>Qtd requerida</td>" & vbcrlf
		cab = cab & 			"<td style='width:100px'>Conferido</td>" & vbcrlf
		cab = cab & 		"</tr>" & vbcrlf
		'
		rod = rod & 		"<tr>" & vbcrlf
		rod = rod & 			"<td colspan='8' class='docTit' style='font-size:12px; text-align:center'><br>Declaro estarem corretas as quantidades acima.</td>" & vbcrlf
		rod = rod & 		"</tr>" & vbcrlf
		rod = rod & 		"<tr>" & vbcrlf
		rod = rod & 			"<td colspan='8' class='docTit' style='font-size:11px; color:#000; text-align:center'><br>_________________________________________________</td>" & vbcrlf
		rod = rod & 		"</tr>" & vbcrlf
		rod = rod & 		"<tr>" & vbcrlf
		rod = rod & 			"<td colspan='8' class='docTit' style='font-size:11px; color:#000; text-align:center'>" & lsl & "</td>" & vbcrlf
		rod = rod & 		"</tr>" & vbcrlf
		rod = rod & 	"</table>" & vbcrlf
		rod = rod & "</body>" & vbcrlf
		rod = rod & "</html>" & vbcrlf
		'
		lclass = "GridLinhaImpar"
		pitems = split(pitems,"|")
		pitels = split(pitels,"|")
		pqtds  = split(pqtds,"|")
		plins  = split(plins,"|")
		i = 0
		'
		for i = 0 to ubound(pitems)
			if (retorno="") then retorno = retorno & cab
			xSQL = ""
			if(pop="1")then 'Lista
				xSQL = xSQL & "select i.itelId, i.itemId, isnull(i.itelQtdAtual,0) itelQtdAtual, it.ItemDescricao, it.ItemCodJde, isnull(i.itelQtdUtilizada,0) itelQtdUtilizada, isnull(u.unidSigla,'-') unidSigla, isnull(u.unidId,0) unidId "
				xSQL = xSQL & "  from ItemLista i, Item it, Unidade U "
				xSQL = xSQL & " where it.itemId  = i.itemId "
				xSQL = xSQL & "   and i.unidId  *= u.unidId "
				xSQL = xSQL & "   and i.listId   = " & pls
				xSQL = xSQL & "   and i.itemId   = " & pitems(i)
			elseif(pop="2")then 'N�o-Conformidade
				xSQL = xSQL & "select '' itelId, j.itemId, isnull(j.itrcQtd,0) itelQtdAtual, it.ItemDescricao, it.ItemCodJde, isnull(k.itrcQtdUtilizada,0) itelQtdUtilizada, isnull(u.unidSigla,'-') unidSigla, isnull(u.unidId,0) unidId "
				xSQL = xSQL & "  from Item    it "
				xSQL = xSQL & "     , Unidade u "
				xSQL = xSQL & "     , (select r.nconId, i.itemId, i.unidId, sum(isnull(i.itrcQtd,0)) as itrcQtd "
				xSQL = xSQL &         "  from itemrecebimento i "
				xSQL = xSQL &         "     , recebimento     r "
				xSQL = xSQL &         " where i.receId = r.receId "
				xSQL = xSQL &         "   and r.nconId = " & pnc 
				xSQL = xSQL &         "   and i.itemId = " & pitems(i)
				xSQL = xSQL &         "   group by r.nconId, i.itemId, i.unidId) j "
				xSQL = xSQL & "     , (select r.nconId, i.itemId, i.unidId, sum(isnull(ia.itesQtd,0)) as itrcQtdUtilizada "
				xSQL = xSQL &         "  from itemrecebimento i "
				xSQL = xSQL &         "     , recebimento     r "
				xSQL = xSQL &         "     , itemsaida      ia " 
				xSQL = xSQL &         " where i.receId = r.receId "
				xSQL = xSQL &         "   and r.nconId = " & pnc 
				xSQL = xSQL &         "   and i.itemId = " & pitems(i)
				xSQL = xSQL &         "   and i.itemId = ia.itemId "
				xSQL = xSQL &         "   group by r.nconId, i.itemId, i.unidId) k "
				xSQL = xSQL & " where it.itemId = j.itemId "
				xSQL = xSQL & "   and j.itemId *= k.itemId "
				xSQL = xSQL & "   and j.unidId *= u.unidId "
			end if
			Set xrs = gConexao.Execute(xSQL)
			if not xrs.eof then	
				'
				if lclass = "GridLinhaPar" then 
					lclass = "GridLinhaImpar" 
				else
					lclass = "GridLinhaPar" 
				end if
				'
				retorno = retorno & "<tr class='" & lclass & "'>" & vbcrlf
				retorno = retorno &		"<td style='width:5px'>" & llins(i) & "</td>" & vbcrlf
				retorno = retorno & 	"<td style='width:100px'>" & xrs("itemCodJde")    & "</td>" & vbcrlf
				retorno = retorno & 	"<td style='width:330px'>" & xrs("itemDescricao") & "</td>" & vbcrlf
				retorno = retorno & 	"<td style='width:70px' >" & xrs("unidSigla")     & "</td>" & vbcrlf
				retorno = retorno & 	"<td style='width:100px'>" & formatnumber(xrs("itelQtdAtual"),2) & "</td>" & vbcrlf
				retorno = retorno & 	"<td style='width:100px'>" & formatnumber((cdbl(xrs("itelQtdAtual"))-cdbl(xrs("itelQtdUtilizada"))),2) & "</td>" & vbcrlf
				retorno = retorno & 	"<td style='width:100px'>" & pqtds(i) & "</td>" & vbcrlf
				retorno = retorno & 	"<td style='width:100px'>&nbsp;</td>" & vbcrlf
				retorno = retorno & "</tr>" & vbcrlf
				'
			end if
			xrs.close
		next
		if (retorno<>"") then retorno = retorno & rod
		'		
	end if
	f_monta_lista = retorno
'	exit function
'erro_exec:
'	f_rec_item = NovoErroXML("[" & err.number & "] " & err.description, chamada)
end function

'
'Pegando par�metros de request
'	Dados de cabe�alho
lprojId  = trim(request.querystring("projId")) 	'Id do projeto
lls      = trim(request.querystring("ls"))		'Id da lista (caso seja uma sa�da de lista)
lnc      = trim(request.querystring("nc"))		'Id da n�o-conformidade(caseo seja uma sa�da por n�o-conformidade)
lsl      = trim(request.querystring("sl"))		'Nome do solicitante da retirada
lop      = trim(request.querystring("op"))		'Modo de opera��o: 1- Lista; 2 - N�o-conformidade

'	Dados de item
lsaidId  = trim(request.querystring("saidId")) 	'Id da sa�da de material
lunidId  = trim(request.querystring("unidId"))	'Id da unidade de medida do item
litelId  = trim(request.querystring("itelId"))	'Id do item na lista
litemId  = trim(request.querystring("itemId"))	'Id do item na tabela item (serve p/ ambos os casos)
litesQtd = trim(request.querystring("itesQtd"))	'Qtd. requerida

'Dados agrupados
litems  = trim(request.querystring("items"))	'Id dos itens da tabela item (serve p/ ambos os casos)  selecionados(RELAT�RIO)
lqtds   = trim(request.querystring("qtds"))	    'Qtds. requeridas dos itens selecionados para sa�da (RELAT�RIO)
llins   = trim(request.querystring("lins"))	    'Linhas da tela dos itens selecionados para sa�da (RELAT�RIO)

'	Dado geral
lacao    = trim(request.querystring("acao")) 	'A��o a ser tomada neste m�dulo

'Zerando vari�veis de apoio
lR = ""

'Tomada de decis�o
select case lacao
	case "CS" 'Cria sa�da
		chamada = "f_cria_said(" & lprojId & ", " & session("usuaId") & ", " & lop & ", " & lls & ", " & lnc & ", " & lsl & ")"
		lR = f_cria_said(lprojId, session("usuaId"), lop, lls, lnc, lsl)
	case "SI" 'Salva item
		chamada = "f_said_item(" & lsaidId & ", " & litemId & ", " & litelId & ", " & litesQtd & ", " & lunidId & ")"
		lR = f_said_item(lsaidId, litemId, litelId, litesQtd, lunidId)
	case "GE" 'Gerar excel
		Response.Charset     = "iso-8859-1"
		Response.ContentType = "application/vns.ms-excel"
		Response.AddHeader   "Content-Disposition","attachment;filename=saida_de_material.xls"
		chamada = "f_monta_lista(" & lprojId & ", " & lls & ", " & lnc & ", " & lsl & ", " & lop & ", " & litems & ", " & lqtds & ", " & llins & ", )"
		lR = f_monta_lista(lprojId, lls, lnc, lsl, lop, litems, lqtds, llins, "")
	case "IL" 'Imprimir lista
		chamada = "f_monta_lista(" & lprojId & ", " & lls & ", " & lnc & ", " & lsl & ", " & lop & ", " & litems & ", " & lqtds & ", " & llins & ", ""window.print();"")"
		lR = f_monta_lista(lprojId, lls, lnc, lsl, lop, litems, lqtds, llins, "window.print(); window.close();")
	case else
		lR  = NovoErroXML("00000010", "A��o inexistente!", "--")
end select

set gConexao = nothing

'Tratamento de erro
if err.number <> 0 then lR = NovoErroXML(err.number, err.description,"Query n�o dispon�vel. A��o: " & lacao & ". Chamada: " & chamada )

'Retorno
response.write lR
%>