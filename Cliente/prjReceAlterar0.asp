<% 	
	Session.LCID = 1046 'Pt-Br'
	Response.Expires= 0
    Response.AddHeader "PRAGMA", "NO-CACHE"
	Response.Charset = "iso-8859-1"
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ::</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<link rel="stylesheet" type="text/css" href="../EstiloMenu.css">
		<link rel="stylesheet" type="text/css" href="../.css">
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<!--#include file="../includes/funcoes.inc"-->
<%  lPerfisAutorizados = "15"
	VerificarAcesso()

	Dim gmenuGeral
	Dim gMenuSelecionado
	Dim gOpcaoSelecionada
	'gOpcaoSelecionada = "Servicos"
	gmenuGeral = "Projetos"
	gmenuGeral = "Projetos"
	gMenuSelecionado = "prjReceListar"
	
	projId     = request.querystring("projId")
	xreceId    = request.querystring("receId")
	protegido  = request.querystring("pr")
	numero     = request.querystring("nr")
	pedcId     = request.querystring("pedcId")
	tipo       = request.querystring("op")
	acao       = request.querystring("ac")
	notafiscal = request.querystring("nf")
	'dia        = request.querystring("dt")
	dia        = right("00" & day(now()), 2) & "/" & right("00" & month(now()), 2) & "/" &  year(now())
	
	msgerro = ""
	
	Dim gConexao
	AbrirConexao

	if ((pedcId<>"") and (ucase(trim(tipo))="PEDIDO")) then
		xSQL = ""
		xSQL = xSQL & "select isnull(count(1),0) as existe "
		xSQL = xSQL & "  from Recebimento RC "
		xSQL = xSQL & " where RC.pedcId         = " & pedcId
		xSQL = xSQL & "   and RC.receNotaFiscal = '" & trim(notafiscal) & "'"
		Set xrs = gConexao.Execute(xSQL)
		if not xrs.eof then	
			if (xrs(0) > 0) then
				acao="erro"
				msgerro = "Nota fiscal j� recebida para o pedido indicado!"
			end if
		end if
		xrs.close	
	end if 
 %>
		<script language="JavaScript" type="text/javascript" src="../scripts/moving_div_on_screen.js"></script>
        <script language="JavaScript" type="text/javascript" src="../scripts/block_background_objects.js"></script>
        <script language="JavaScript" type="text/javascript" src="../scripts/functions.js"></script>
        <link rel="stylesheet" type="text/css" href="../scripts/block_background_objects.css">
		<link rel="stylesheet" type="text/css" href="../scripts/wbs_form.css">
		<style type="text/css">
			p {
				text-align:center; 
				width:99%;
			}
		</style>
		<script language="JavaScript" type="text/javascript" src="../scripts/AbrirJanela.js"></script>
		<script language="JavaScript" type="text/javascript">
			var _xsl_ret=_gerarxsltretorno('LISTA/ERRO', 'LINHA', 'LINHA|DESCRICAO', 'Linha|Descri��o', '50|325');
			
			function _alterar_item_nc(_indrow){
				_ocdiv2('divItem');
				var _tr = document.getElementById('tbItens').rows[_indrow];
				document.getElementById('add_modo').value=1;
				document.getElementById('add_itemCodJDE').value=_tr.cells[0].innerHTML;
				document.getElementById('add_itemDescricao').value=_tr.cells[1].innerHTML;
				document.getElementById('add_unidId').value=document.getElementById('unidId'+_indrow).value;
				document.getElementById('add_unidSigla').value=_tr.cells[2].innerHTML;
				document.getElementById('add_itrcQtd').value=document.getElementById('itrcQtd'+_indrow).value;
				setavaluecombo('cmbunidId',document.getElementById('add_unidId').value);
				document.getElementById('cmbunidId').focus();
				document.getElementById('add_itemCodJDE').ReadOnly=true;
			}
			
			function _excluir_item_nc(_indrow){
				document.getElementById('tbItens').rows[_indrow].style.display='none';
			}
			
			function _ocdiv2(_obj_name){
				var _ooc = document.getElementById(_obj_name);
				_ooc.style.display=(_ooc.style.display=='block'?'none':'block');
				document.getElementById('over').style.display=_ooc.style.display;
				if(_ooc.style.display=='block'){
					document.getElementById('add_modo').value=0;
					document.getElementById('add_itemCodJDE').ReadOnly=false;
					document.getElementById('add_itemCodJDE').focus();
				}
			}
			function _altori(_opt){
				var _td = document.getElementById('tdPedConf');
				if (_opt.value=='pedido') {
					_td.innerHTML='Pedido';
					document.getElementById('cmbPedido').style.display='inline';
					document.getElementById('txtNumero').style.display='none';
				}else{
					_td.innerHTML='N�o-Conformidade';
					document.getElementById('cmbPedido').style.display='none';
					document.getElementById('txtNumero').style.display='inline';
				}
			}
	
			function _limpar(){
				window.location.href='prjReceAlterar0.asp?projId=<%=projId%>&receId=<%=xreceId%>';
			}
	
			function _validar_itens(){
				var _ret='N�o existem itens.'
				if(document.getElementById('tbItens')){
					_ret = '';
					var _v_trs = document.getElementById('tbItens').getElementsByTagName('TR');
					var _v_op = (document.getElementById('tdPedConf').innerHTML.toUpperCase()=='PEDIDO'?'PEDIDO':'NAO-CONFORMIDADE');
					var _v_itrs=0;
					var _v_itrcQtd='';					
					for(_v_itrs=1;_v_itrs<_v_trs.length;_v_itrs++){
						if(_v_trs[_v_itrs].style.display!='none'){
							_v_itrcQtd=document.getElementById('itrcQtd'+_v_itrs).value;
							_v_rest = _v_trs[_v_itrs].cells[4].innerHTML;
							if(isNumberJS(_v_itrcQtd,0)==false){
								_ret += '<ERRO><LINHA>'+_v_itrs+'</LINHA><DESCRICAO>Qtd. recebida inv�lida!</DESCRICAO></ERRO> ';
							}else{
								_v_itrcQtd = _v_itrcQtd.replace(',','.').replace(' ','');
								if(eval(_v_itrcQtd+'<0')){
									_ret += '<ERRO><LINHA>'+_v_itrs+'</LINHA><DESCRICAO>Qtd. recebida n�o pode ser negativa!</DESCRICAO></ERRO> ';
								}else{
									if(_v_op=='PEDIDO'){
										_v_rest    = _v_rest.replace(',','.').replace(' ','');
										if(isNumberJS(_v_rest,1)){
											if(eval(_v_itrcQtd+'>'+_v_rest)){
												_ret += '<ERRO><LINHA>'+_v_itrs+'</LINHA><DESCRICAO>Qtd. recebida n�o pode ser maior que a qtd. restante!</DESCRICAO></ERRO> ';
											}
										}
									}
								}
							}
						}
					}
					
					if(_ret!=''){
						alert('Houveram erros de preechimentos, verifique abaixo!');
						_ret='<LISTA>'+_ret+'</LISTA>';
						//alert(_ret);
						_carregaxmlxslt(_ret, _xsl_ret, 'divErros');
						_ret = false;
					}else{
						_ret = true;
					}
				}
				return _ret;
			}
				
			function _limpar_itens(){
				if(document.getElementById('tbItens')){
					var _l_trs = document.getElementById('tbItens').getElementsByTagName('TR');
					var _l_itrs=0;
					for(_l_itrs=1;_l_itrs<_l_trs.length;_l_itrs++){
						if(_l_trs[_l_itrs].style.display!='none'){
							document.getElementById('itrcQtd'+_l_itrs).value='';
						}
					}
				}
			}
			
			function _receber_itens(_receId){
				if(document.getElementById('tbItens')){
					//
					var msg_erro = ' Ocorreram erros durante o processamento. \n Por favor, verifique as mensagens de erro!';
					var _ret= '';
					//
					var _s_trs = document.getElementById('tbItens').getElementsByTagName('TR');
					var _s_itrs=0;
					var _s_unidId='';
					var _s_itepId='';
					var _s_itrcQtd='';
					//
					for(_s_itrs=1;_s_itrs<_s_trs.length;_s_itrs++){
						// --
						if(_s_trs[_s_itrs].style.display!='none'){
							_s_itepId     = '';
							_s_unidId     = document.getElementById('unidId'+_s_itrs).value;
							_s_itrcQtd    = document.getElementById('itrcQtd'+_s_itrs).value;
							_s_itemCodJDE = _s_trs[_s_itrs].cells[0].innerHTML;
							// --
							if(_s_itrcQtd!=''){_s_itepId=document.getElementById('itepId'+_s_itrs).value;}
							if(_s_itepId!=''){
								//salvando item
								var _pagina = 'prjReceAlterar0_BD.asp?acao=SI&pedcId=<%=pedcId%>&receId='+_receId+'&unidId='+_s_unidId+'&itepId='+_s_itepId+'&itrcQtd='+_s_itrcQtd+'&itemCodJDE='+_s_itemCodJDE;
								//alert(_pagina);
								var _xmlhttp_i = _criaxmlhttprequest();
								_xmlhttp_i.open("GET",_pagina,false);
								_xmlhttp_i.send(null);
								_ret += _xmlhttp_i.responseText;
							}
						}
					}
					if(_tratarxmlretorno(_ret)){
						alert('Dados salvos com sucesso!');
						window.location.href='prjReceListar.asp?projId=<%=projId%>';
					}else{
						alert(msg_erro);
					}
				}
			}
			
			function _salvar_itens(){
				if(document.getElementById('tbItens')){
					document.getElementById('procover').style.display='block';
					var msg_erro = ' Ocorreram erros durante o processamento. \n Por favor, verifique as mensagens de erro!';
					if(_validar_itens()){
						//
						var _ret= '';
						var _s_nf = document.getElementById('txtNotaFiscal').value;
						var _s_nr = document.getElementById('txtNumero').value;
						var _s_op = (document.getElementById('tdPedConf').innerHTML.toUpperCase()=='PEDIDO'?'PEDIDO':'NAO-CONFORMIDADE');
						// criando o id do recebimento
						var _pagina = 'prjReceAlterar0_BD.asp?acao=CR&projId=<%=projId%>&pedcId=<%=pedcId%>&nf='+_s_nf+'&nr='+_s_nr+'&op='+_s_op;
						//alert(_pagina);
						var _xmlhttp_i = _criaxmlhttprequest();
						_xmlhttp_i.open("GET",_pagina,false);
						_xmlhttp_i.send(null);
						_ret += _xmlhttp_i.responseText;
						if (isNumberJS(_ret,1)){
							_receber_itens(_ret);
						}else{
							if(_tratarxmlretorno(_ret)){
								alert(msg_erro);
							}
						}
					}
					document.getElementById('procover').style.display='none';
				}
			}
			
			function _item_existe(pes_itemCodJDE){
				var _ret = 0;
				if(document.getElementById('tbItens')){
					var _p_trs = document.getElementById('tbItens').getElementsByTagName('TR');
					var _p_itrs=0;
					for(_p_itrs=1;_p_itrs<_p_trs.length;_p_itrs++){
						if(_p_trs[_p_itrs].style.display!='none'){
							if(_p_trs[_p_itrs].cells[0].innerHTML==pes_itemCodJDE){
								_ret = _p_itrs;
								break;
							}
						}
					}
				}
				return _ret;
			}
			
			function _adicionar_item(){
				//
				var _itemCodJDE    = document.getElementById('add_itemCodJDE').value;
				var _itemDescricao = document.getElementById('add_itemDescricao').value;
				var _unidId        = document.getElementById('add_unidId').value;
				var _unidSigla     = document.getElementById('add_unidSigla').value;
				var _itrcQtd       = document.getElementById('add_itrcQtd').value;
				var _modo          = document.getElementById('add_modo').value;
				var _indrowatu     = 0;
				var _msg           = '';
				//
				if(isNumberJS(_itemCodJDE,1)==false){
					_msg+=', C�digo JDE';
				}
				//
				if(_unidId==0){
					_msg+=', Unidade';
				}
				//
				if(isNumberJS(_itrcQtd,1)==false){
					_msg+=', Qtd. recebida';
				}else if(_itrcQtd<=0){
					_msg+=', Qtd. recebida n�o pode ser menor ou igual que 0 (zero)';
				}
				//
				_indrowatu=_item_existe(_itemCodJDE);
				if(_indrowatu>0){
					if(_modo!=1){//Altera��o
						_msg+=', Item j� existente no recebimento atual';
					}
				}
				//
				if(_itemDescricao==''){
					_msg+=', Item n�o indicado';
				}
				//
				if(_msg!=''){
					alert('Ocorreram os seguintes erros no preenchimento dos dados do novo item:\n'+_msg.substr(2));
				}else{
					if(_indrowatu>0){
						var _tr_atu = document.getElementById('tbItens').rows;
						_tr_atu[_indrowatu].cells[2].innerHTML              = _unidSigla;
						document.getElementById('unidId'+_indrowatu).value  = _unidId;
						document.getElementById('itrcQtd'+_indrowatu).value = _itrcQtd;
						_tr_atu = null;
					}else{
						//criando nova linha
						var _i_l = document.getElementById('tbItens').rows.length;
						var _trped = document.getElementById('tbItens').insertRow(_i_l);
						_trped.className = 'GridLinhaImpar';
						if(_trped.parentNode.rows.length>1){_trped.className = (_trped.parentNode.rows[_trped.rowIndex-1].className=='GridLinhaPar'?'GridLinhaImpar':'GridLinhaPar');}
						
						//criando c�lulas
						var _clped;
						var _campos = [[_itemCodJDE,'100px'], [_itemDescricao,'330px'], [_unidSigla,'70px'], ['0','100px'], ['0','100px']];
						for (_i_c=0;_i_c<_campos.length;_i_c++){
							_clped = _trped.insertCell(_trped.cells.length);
							_clped.innerHTML=_campos[_i_c][0];
							_clped.style.width=_campos[_i_c][1];
						}
						
						//criando c�lula de input
						_clped = _trped.insertCell(_trped.cells.length);
						_clped.style.width='100px';
						_clped.innerHTML='<input type="hidden" id="unidId'+_i_l+'"  name="unidId'+_i_l+'"  value="'+_unidId+'" /><input type="hidden" id="itepId'+_i_l+'" name="itepId'+_i_l+'" value="0" /><input type="text" id="itrcQtd'+_i_l+'" name="itrcQtd'+_i_l+'" value="'+_itrcQtd+'" maxlength="15" class="cpNum" style="width:80px;border:solid 1px black" /><img src=\'../images/mini_g.gif\' class=\'link\' onclick=\'javascript:_alterar_item_nc("' + _i_l + '")\' /><img src=\'../images/Crossm.gif\' class=\'link\' onclick=\'javascript:_excluir_item_nc("' + _i_l + '")\' />';
						
						_zebra_linhas('tbItens', 'GridLinhaImpar', 'GridLinhaPar', 1);
					}				
					document.getElementById('add_itemCodJDE').value='';
					document.getElementById('add_itemDescricao').value='';
					document.getElementById('add_unidId').value='';
					document.getElementById('add_unidSigla').value='';
					document.getElementById('add_itrcQtd').value='';
					document.getElementById('cmbunidId').selectedIndex=0;
					
					_ocdiv2('divItem');
				}
			}
			
			function _procura_item(){
				var _itemCodJDE    = document.getElementById('add_itemCodJDE').value;
				var _itemDescricao = '';
				var _ret = '';
				//
				if(isNumberJS(_itemCodJDE,1)==false){
					alert('C�digo JDE n�o informado!');
				}else{
					document.getElementById('procover').style.display='block';
					var _pag_bus = 'prjReceAlterar0_BD.asp?acao=PI&itemCodJDE='+_itemCodJDE;
					var _xmlhttp_b = _criaxmlhttprequest();
					_xmlhttp_b.open("GET",_pag_bus,false);
					_xmlhttp_b.send(null);
					_ret = _xmlhttp_b.responseText;
					//
					if(_ret.substr(0,4)=='ERRO'){
						alert(_ret.substr(4));
					}else{
						_itemDescricao = _ret;
					}
					//
					document.getElementById('procover').style.display='none';
					//
				}
				//
				document.getElementById('add_itemDescricao').value=_itemDescricao;
			}
					
			function _exibir_itens(){
				var _nf = document.getElementById('txtNotaFiscal').value;
				var _nr = document.getElementById('txtNumero').value.replace(/^\s*/, "").replace(/\s*$/, "");
				var _ped = document.getElementById('cmbPedido').value;
				document.getElementById('txtNumero').value = _nr;
				var _dt = document.getElementById('txtData').value;
				var _op = [document.getElementById('optTipo_PED'), document.getElementById('optTipo_NCO')];
				//var _op = document.getElementsByName('optTipo');
				var _pr = 'readonly';
				var _ac = 'informar_quantidades';
				var _msg = '';
				//
				if (_nf==''){
					_msg+=', Nota Fiscal';
				}
				//
				if (_op[1].checked)
				{
					if (isNumberJS(_nr,1)==false){
						_msg+=', '+'N�o-Conformidade';
					}
				}
				//
				//if (isDateJS2(_dt, 1)==false){
				//	_msg+=', Data';
				//}
				//
				if(_op[0].checked){
					_op='Pedido';
				}else if(_op[1].checked){
					_op='N�o-Conformidade';
				}else{
					_msg+=', Tipo de Origem';
				}
				//
				if(_msg!=''){
					alert('Os seguintes campos est�o errados ou foram preenchidos incorretamente:\n'+_msg.substr(2));
				}else{
					window.location.href='prjReceAlterar0.asp?projId=<%=projId%>&receId=<%=xreceId%>&pr='+_pr+'&nr='+_nr+'&ac='+_ac+'&op='+_op+'&nf='+_nf+'&dt='+_dt+'&pedcId='+_ped;
				}
			}
			
			function _uni_change(){
				var _opt = document.getElementById('cmbunidId').options[document.getElementById('cmbunidId').selectedIndex];
				document.getElementById('add_unidId').value    = _opt.value;
				document.getElementById('add_unidSigla').value = _opt.title;
			}
		</script>
	</HEAD>
	<body class="cabecalho" >
		<div id="over"     style="display:none; z-index:500; width:99%; height:99%; position:absolute; background: #464; -moz-opacity:0.4;filter:alpha(opacity=40)"></div>
		<div id='procover' style='background: #464 url(../images/aguarde.gif) no-repeat fixed center; display:none'></div>
		<!--#include file="../includes/cab.inc"-->
		<tr>
			<td style="padding:0px" valign="top" class="blocoOpcoes">
			<!--#include file="../includes/opcProjx2.inc"-->
			</td>
			<td colspan="2" valign="top" style="padding:5px" class="blocoPagina">
				<table width="100%">
				<tr>
					<td colspan="2" class="docTit"><%=session("projNome")%><br/>NOVO RECEBIMENTO</td>
				</tr>
				<tr>
					<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
				</tr>
				<tr>
					<td colspan="2" style="height:5"></td>
				</tr>
		<tr>
			<td></td>
			<td>
				<div id="divItem" name="divItem" style="display:none; position:absolute; z-index:1000;" class="divwindow">
					<div class='div_titulo' id='divItem_TIT' name='divItem_TIT' onmousedown="javascript:dragStart(event, 'divItem');">Adi��o de itens n�o previstos</div>
					<div class='sep'></div>
					<table class="entradadados">
						<tr>
							<th style="width:100px">C�digo JDE</th>
							<th style="width:1px">:</th>
							<td><input id='add_itemCodJDE' type='text' class='cpNum' maxlength='10'><input type='button' value='Procurar' onclick='javascript:_procura_item();'><input id='add_itemDescricao' type='text' class='docSBorda' readonly style='width:100%'></td>
						</tr>
						<tr>
							<th style="width:100px">Unidade</th>
							<th style="width:1px">:</th>
							<td>
								<input type='hidden' id='add_modo'      name='add_modo' />
								<input type='hidden' id='add_unidId'    name='add_unidId' />
								<input type='hidden' id='add_unidSigla' name='add_unidSigla' />
								<select id="cmbunidId" style="display:block" onchange="javascript:_uni_change();">
									<option value="0" class="campo" selected>(selecione uma unidade)</option>
								<%
								lsql = ""
								lsql = lsql & "select u.unidId, u.unidNome, u.unidSigla, (isnull(u.unidSigla,'-') + ' - ' + isnull(u.unidNome,'-')) unidNomeLongo "
								lsql = lsql & "  from unidade u "
								lsql = lsql & " order by 4 "
								Set lRS_1 = gConexao.Execute(lsql)
								while not lRS_1.EOF
									response.write "<option value=""" & lRS_1("unidId") & """ class=""campo"" title='" & lRS_1("unidSigla") & "'>" & lRS_1("unidNomeLongo") & "</option>" & vbcrlf
									lRS_1.MoveNext
								wend
								lRS_1.close
								set lRS_1 = nothing
								%>
								</select>
							</td>
						</tr>
						<tr>
							<th style="width:100px">Qtd. recebida</th>
							<th style="width:1px">:</th>
							<td><input id='add_itrcQtd' type='text' class='cpNum' maxlength='15'></td>
						</tr>
					</table>
					<div class='barrabotoes'>
						<input type='button' onclick="javascript:_adicionar_item();" value="Adicionar" title="Adicionar item em quest�o" />
						<input type='button' onclick="javascript:_ocdiv2('divItem');" value="Fechar" title="Fechar" />
					</div>
				</div>
				<input id='pedcId' name='pedcId' type="hidden" value="<%=pedcId%>" />
				<input id="acao"   name="acao"   type="hidden" value="<%=acao%>" />
				<input id="tipo"   name="tipo"   type="hidden" value="<%=tipo%>" />
				<table class="doc">
					<tr>
						<td width="130">Nota fiscal</td>
						<td width="5">:</td>
						<td class="docSBorda"><input id="txtNotaFiscal" name="txtNotaFiscal" type="text" <%=protegido%> value="<%=notaFiscal%>" maxlength="20" class="docCmpLivre" style="width:100px;border:solid 1px #999;"></td>
					</tr>
					<tr>
						<td id='tdPedConf'><%if (tipo="") then %>Origem<% else response.write tipo end if%></td>
						<td>:</td>
						<td class="docSBorda">
							<select id="cmbPedido" class="docCmpLivre" style="width:300px;display:inline">
<%
		xSQL = "select pedcId, pedcNum, fornNome"
		xSQL = xSQL & " from PedidoCompra P, Fornecedor F"
		xSQL = xSQL & " where P.fornId = F.fornId "
		xSQL = xSQL & " and projId = " & session("projId")

		Set xRs = gConexao.Execute(xSQL)
		while not xRS.EOF
			response.write "<option value=""" & xRs("pedcId") & """ class=""campo"" >" & xRS("pedcNum") & " - " & xRS("fornNome") & "</option>" & vbcrlf
			xRs.MoveNext
		wend
%>							
							</select>
							<input id="txtNumero" name="txtNumero" type="text" value="<%=numero%> " maxlength="10" class="docCmpLivre" <%=protegido%> style="width:100px;border:solid 1px #999;display:none" >
							<%if (tipo="") then%>
								<input id='optTipo_PED' name="optTipo" type="radio" class="docCmpLivre" value="pedido"  checked          <%=protegido%> style="width:auto" onclick='javascript:_altori(this);'>Pedido&nbsp;&nbsp;&nbsp;
								<input id='optTipo_NCO' name="optTipo" type="radio" class="docCmpLivre" value="nao-conformidade" <%=protegido%> style="width:auto" onclick='javascript:_altori(this);'>N�o-Conformidade
							<%end If%>
						</td>
					</tr>
					<tr>
						<td>Data</td>
						<td>:</td>
						<td class="docSBorda"><input id="txtData" name="txtData" type="text" value="<%=dia%> " maxlength="10" class="docCmpLivre" readonly style="width:100px;border:solid 1px #999;" ></td>
					</tr>

<%
if ((acao="informar_quantidades") and (tipo="Pedido")) then
	xSQL = ""
	xSQL = xSQL & "select fornNome  "
	xSQL = xSQL & "  from fornecedor F, pedidocompra PC "
	xSQL = xSQL & " where F.fornId  = PC.fornId "
	xSQL = xSQL & "   and PC.pedcId = " & pedcId
	Set xRs = gConexao.Execute(xSQL)
	if not xrs.eof then	%>
					<tr>
						<td>Fornecedor</td>
						<td>:</td>
						<td class="docCmpLivre" style="border:solid 1px black"><%=xrs("fornNome")%></td>
					</tr>
<%
	end if
	xrs.close
end if
%>
				</table>
				<p>
<% 'if acao="informar_quantidades" then 
	'<input type="button" value="Mudar pedido/n�o-conformidade" />
 'else
  if acao="erro" then %>
	<span class='erro'><%=msgerro%></span><br/>
<% else %>
	<input type="button" value="Exibir itens" onclick='javascript:_exibir_itens();' />
<% end if %>
					<input type="button" value="Limpar" onclick='javascript:_limpar();'>
				</p>
<%
if acao="informar_quantidades" then
	tabela=""
	tabela=tabela&"<br>"
	tabela=tabela&"<p style='text-align:left;'>"
	if(tipo="Pedido")then
		tabela=tabela&"Informe as quantidades recebidas de cada item: "
	else
		tabela=tabela&"<input type='button' value='Adicionar Item' onclick=""javascript:_ocdiv2('divItem');"" style='float:right'>Informe os itens da n�o-conformidade: "
	end if
	tabela=tabela&"</p>"
	tabela=tabela&"<table width='99%' class='grid' id='tbItens' name='tbItens'>"
	tabela=tabela&"	<tr class='GridCab'>"
	tabela=tabela&"		<td style='width:100px'>C�d. JDE</td>"
	tabela=tabela&"		<td style='width:330px'>Descri��o</td>"
	tabela=tabela&"		<td style='width:70px' >Unidade</td>"
	tabela=tabela&"		<td style='width:100px'>Qtd pedida</td>"
	tabela=tabela&"		<td style='width:100px'>Qtd restante</td>"
	tabela=tabela&"		<td style='width:100px'>Qtd recebida</td>"
	tabela=tabela&"	</tr>"
	
	rod_tabela=""
	rod_tabela=rod_tabela&"</table>"
	rod_tabela=rod_tabela&"<p>"
	rod_tabela=rod_tabela&"		<input type='button' value='Receber Itens' onclick='javascript:_salvar_itens();'>"
	rod_tabela=rod_tabela&"		<input type='button' value=""Limpar Qtd's"" onclick='javascript:_limpar_itens();'>"
	rod_tabela=rod_tabela&"</p>"
		
	if(tipo="Pedido")then
		xSQL = ""
		xSQL = xSQL & "select IP.itepId, isnull(IP.itepQtd,0) itepQtd, IT.ItemDescricao, IT.ItemCodJde, isnull(J.itrcQtd_R,0) itrcQtd_R, isnull(u.unidSigla,'-') unidSigla, isnull(u.unidId,0) unidId "
		xSQL = xSQL & "  from ItemPedido IP, Item IT, (select isnull(sum(IR.itrcQtd),0) itrcQtd_R, IR.itepId from ItemRecebimento IR group by IR.itepId) J, Unidade U "
		xSQL = xSQL & " where IT.itemId  = IP.itemId "
		xSQL = xSQL & "   and IP.unidId *= U.unidId "
		xSQL = xSQL & "   and IP.itepId *= J.itepId  "
		'xSQL = xSQL & "   and IP.itepId  > J.itrcQtd_R "
		xSQL = xSQL & "   and IP.pedcId  = " & pedcId
		Set xRs = gConexao.Execute(xSQL)
		if not xrs.eof then	
			response.write tabela

			estilo = "GridLinhaImpar"
			i = 0
			while not xrs.EOF 
				if (cdbl(xrs("itepQtd"))>cdbl(xrs("itrcQtd_R"))) then
					i = i + 1
			%>
					<tr class="<%=estilo%>">
						<td style='width:100px'><%=xrs("itemCodJde")%></td>
						<td style='width:330px'><%=xrs("itemDescricao")%></td>
						<td style='width:70px' ><%=xrs("unidSigla")%></td>
						<td style='width:100px'><%=xrs("itepQtd")%></td>
						<td style='width:100px'><%=(cdbl(xrs("itepQtd"))-cdbl(xrs("itrcQtd_R"))) %></td>
						<td style='width:100px'>
							<input type="hidden" id="unidId<%=i%>"  name="unidId<%=i%>"  value="<%=cstr(xrs("unidId"))%>" />
							<input type="hidden" id="itepId<%=i%>"  name="itepId<%=i%>"  value="<%=cstr(xrs("itepId"))%>" />
							<input type="text"   id="itrcQtd<%=i%>" name="itrcQtd<%=i%>" value="" maxlength="15" class="cpNum" style="width:80px;border:solid 1px black" />
						</td>
					</tr>
	<%				if estilo = "GridLinhaPar" then
						estilo = "GridLinhaImpar"
					else
						estilo = "GridLinhaPar"
					end if
				end if
				xrs.movenext
			wend
			%>
			</table>
			<%
			response.write rod_tabela
		    xrs.close
		else %>
			<p>N�o existem itens a serem recebidos para este pedido!</p>
<%		end if
	else
		response.write tabela & rod_tabela
	end if
end if
%>						<div id='divErros' style='width:99%;height:0px;display:none;margin-top:5px;'>
					</td>
				</tr>
				<tr>
					<td colspan="2" height="20"></td>
				</tr>
			</table>
		</tr>
		<!--#include file="../includes/rod.inc"-->
	</body>
</HTML>