<% Response.Expires= -1%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Controle remoto de projetos</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<script language="JavaScript" type="text/javascript" src="../scripts/PreparaJanela.js"></script>
		<SCRIPT language="JavaScript">
			function submitform(){
			  document.form1.submit();
			}
			function validar(){
			  	submitform();
			}
		</SCRIPT>
	</HEAD>
	<body class="PopUpEntrada" onLoad="javascript:PreparaJanela(460,320);">
<%
    lPerfisAutorizados = "23451"
	VerificarAcesso
	Dim gConexao
	AbrirConexao
	usuaId = Request.Querystring("usuaId")
	xSQL = "select usuaNome, usuaLogi, usuaEmai, usuaSenh, perfNome, emprNome"
	xSQL = xSQL & "  from Usuario "
	xSQL = xSQL & "     , PerfilAcesso"
	xSQL = xSQL & "     , Empreiteira"
	xSQL = xSQL & " where usuaId = " & usuaId
    xSQL = xSQL & "   and PerfilAcesso.perfId = Usuario.perfId"
    xSQL = xSQL & "   and Empreiteira.emprId  = Usuario.emprId"
	set xrs = gConexao.execute(xSQL)
	Session("TituloMsg") = "Excluir usu�rio"
	login = xRs("usuaLogi")
	nome = xrs("usuaNome")
	email = xRs("usuaEmai")
	senha = xRs("usuaSenh")
	perfNome = xRs("perfNome")
	emprNome = xRs("emprNome")
	if cstr(emprId) = "" then
		emprId = "1"
	end if
	xRs.CLose
	Set xRs = nothing
%>
	<form action="cfgUsuaExcluir2.asp" method="post" name="form1">
		<input type="hidden" name="usuaId" value=" <% = usuaId %> " >
		<table width="100%">
			<tr>
				<td width="15"></td>
				<td></td>
			</tr>
			<tr>
				<td colspan="2" class="docTit">Excluir usu�rio</td>
			</tr>
			<tr>
				<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
			</tr>
			<tr>
				<td colspan="2" height="15"></td>
			</tr>
			<tr>
				<td width="15"></td>
				<td>
					<table class="doc">
						<tr>
							<td width="110" class="docLabel">Login</td>
							<td width="5" class="docLabel">:</td>
							<td class="docCmpLivre"><% = login %></td>
						</tr>
						<tr>
							<td class="docLabel">Nome</td>
							<td class="docLabel">:</td>
							<td class="docCmpLivre"><% = nome %></td>
						</tr>
						<tr>
							<td class="docLabel">E-mail de contato</td>
							<td class="docLabel">:</td>
							<td class="docCmpLivre"><% = email %></td>
						</tr>
						<tr>
							<td class="docLabel">Perfil</td>
							<td class="docLabel"></td>
							<td class="docCmpLivre"><% = perfNome %></td>
						</tr>
						<tr>
							<td class="docLabel">Empresa</td>
							<td class="docLabel"></td>
							<td class="docCmpLivre"><% = emprNome %></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2" height="20"></td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<p align="center" style="font-weight:bold;color:red">Confirma a exclus�o do usu�rio acima?</p>
					<table>
						<tr>
							<td height="28" width="80" align="center"><img border="0" src="../images/btnExcluir.gif" class="botao" onclick="javascript:validar();"></td>
							<td width="10"></td>
							<td width="80" align="center"><img border="0" src="../images/btnCancelar.gif" class="botao" onclick="javascript:window.close();"></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
	</body>
</HTML>
