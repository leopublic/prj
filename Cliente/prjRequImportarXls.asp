<!--#include file="../includes/clsBanco.inc"-->
<!--#include file="../includes/clsControleAcesso.inc"-->
<!--#include file="../includes/varControleAcesso.inc"-->
<!--#include file="../includes/varBanco.inc"-->
<!--#include file="../includes/funcoes.inc"-->
<%
Dim debug 
Dim ximpoId
Dim objConn
Dim objRs
Dim xPagina 
xPagina = "prjRequImportarXls"
AbrirConexao
'=================================================
'== Obt�m arquivo a importar
debug = request.querystring("debug")
ximpoId = request.querystring("impoId")
xPath = Server.MapPath("..\listas")

destinoErro = "prjRequAlterar0.asp?impoId=" & ximpoId
'=================================================
'== Limpa mensagens da �ltima importacao
xSQL = "delete from MensagemImportacao where impoId =  " & ximpoId
gConexao.execute(xSQL)
'=================================================
'== Obtem c�digo da lista
xSQL = "select reviId, impoExtensao from importacao where impoId = " & ximpoId
debuga xsql 
set xrs = gConexao.execute(xSQL)
reviId = xrs("reviId")
xExtensao = xrs("impoExtensao")
xrs.close
set xrs = nothing
if reviId = "" or isnull(reviId) then
	retornarComErro xPagina & ":ObterLista", "Revis�o n�o encontrada na importa��o", destinoErro
end if
'=================================================
'== Confirma a existencia do arquivo
xArquivo = xPath & "\" & ximpoId & "." & xExtensao
Dim FSO
set FSO = server.createObject("Scripting.FileSystemObject")
if not FSO.FileExists(xArquivo) Then
	retornarComErro xPagina & ":VerificarArquivo", "Arquivo n�o encontrado no servidor", destinoErro 
end if 
Set FSO = nothing
'=================================================
'== Inicia importa��o do arquivo
' = Abre conexao com o arquivo
Set cnnExcel = Server.CreateObject("ADODB.Connection")
cnnExcel.Open "DBQ=" & xArquivo & ";DRIVER={Microsoft Excel Driver (*.xls)};"
'== Obt�m os nomes das planilhas
dim planilhas(10)
Set rs = cnnExcel.OpenSchema(20)
qtdPlanilhas = -1
while not rs.eof
	qtdPlanilhas = qtdPlanilhas + 1
	planilhas(qtdPlanilhas) = rs.Fields("TABLE_NAME").Value
	debuga "Encontrou a planilha " & rs.Fields("TABLE_NAME").Value
	rs.MoveNext
wend
rs.close
'== Verifica em qual planilha est�o os dados
xEncontrou = 0
indP = 0
planilha = ""
dim colunas(9)
NUM_ORIGINAL = "N�  Original"
NUM_PEDIDO = "N�mero  Pedido"
TIPO_PEDIDO = "Tp  Pd"
FORNECEDOR = "Fornec. "
CUSTO_UNIT = "Custo  Unit�rio"
NUMERO_ITEM = "2� N�mero  de Item"
DT_ENTREGA = "Dt Entr. Promet."
QTD_RECEBER = "Quant. a Receber"
UNIDADE = "UM  "
TAG_REF = "Tag  Reference"

colunas(0) = NUM_ORIGINAL
colunas(1) = NUM_PEDIDO
colunas(2) = TIPO_PEDIDO
colunas(3) = FORNECEDOR
colunas(4) = CUSTO_UNIT
colunas(5) = NUMERO_ITEM
colunas(6) = DT_ENTREGA
colunas(7) = QTD_RECEBER
colunas(8) = UNIDADE
colunas(9) = TAG_REF
xmsg = ""
while indP <= qtdPlanilhas and xEncontrou <> 1
	Set rstExcel = Server.CreateObject("ADODB.Recordset")
	xSQL = "SELECT * FROM [" & planilhas(indP) & "];"
	debuga "SQL:" & xSQL 
	rstExcel.Open xSQL, cnnExcel	

	iCols = rstExcel.Fields.Count

	nomesColunas = ""
	for i = 0 to iCols -1
		nomesColunas = nomesColunas & "|" & rstExcel.Fields(i).Name 
	next
	debuga nomesColunas
	xEncontrou = 1
	for iCol = 0 to ubound(colunas)
		if instr(1, nomescolunas, replace(colunas(iCol), ".", "#")) = 0 then
			xEncontrou= 0
			xmsg = "Campo " & replace(colunas(iCol), ".", "#") & " n�o encontrado nos nomes das colunas"
			debuga xmsg
		else
			debuga "Campo " & replace(colunas(iCol), ".", "#") & " encontrado"
		end if
	next 
	rstExcel.close
	
	if xEncontrou = 1 then
		planilha = planilhas(indP)
	end if
	indP = indP + 1
wend

if xEncontrou <> 1 then
	retornarComErro xPagina & ":VerificarPlanilha", "Nenhuma das planilhas do arquivo atende ao formato esperado. Verifique o arquivo e tente novamente.\n (" & xmsg & ")", destinoErro 
	debuga "Nenhuma planilha encontrada"
else
	debuga "Planilha " & planilha & " ser� utilizada"
end if

'== Abre cursor para a leitura da planilha
Set rstExcel = Server.CreateObject("ADODB.Recordset")
xSQL = "SELECT * FROM [" & planilha & "];"
debuga "SQL:" & xSQL 
rstExcel.Open xSQL, cnnExcel	
on error resume next
while not rstExcel.eof 
	if not isnull(rstExcel.Fields(replace(QTD_RECEBER, ".", "#")).value) then
		iteqQtd = campoConvertido(rstExcel.Fields(replace(QTD_RECEBER, ".", "#")).value)
	else
		iteqQtd = "null"
	end if

	if rstExcel.Fields(replace(UNIDADE, ".", "#")).value = "EA" then
		iteqQtd = 1
	end if
	xSQL = " exec PCAD_ItemRequisicao"
	xSQL = xSQL & "   @prequNum       = " & rstExcel.Fields(replace(NUM_ORIGINAL, ".", "#")).value
	xSQL = xSQL & "  ,@prequNumOrig   = " & rstExcel.Fields(replace(NUM_ORIGINAL, ".", "#")).value
	xSQL = xSQL & "  ,@prequNumTag    = '" & rstExcel.Fields(replace(TAG_REF, ".", "#")).value & "'"
	xSQL = xSQL & "  ,@pitemCodJde    = " & rstExcel.Fields(replace(NUMERO_ITEM, ".", "#")).value
	xSQL = xSQL & "  ,@piteqQtd       = " & iteqQtd
	xSQL = xSQL & "  ,@piteqValorUnitario = " & campoConvertido(rstExcel.Fields(replace(CUSTO_UNIT, ".", "#")).value)
	xSQL = xSQL & "  ,@previId        = " & reviId
	xSQL = xSQL & "  ,@punidSigla     ='" & rstExcel.Fields(replace(UNIDADE, ".", "#")).value & "'"
	debuga xSQL 
	set xrs = gConexao.execute(xSQL)
	If Err.Number <> 0 then
		erro = Err.Description
		debuga "SQL:" & xSQL
		debuga "ERRO: " & erro
		mensagem = "Erro na importa��o.\nSQL:" & xSQL & "\nErro:" & erro
		xSQL = " exec PCAD_RequisicaoRollback"
		xSQL = xSQL & "   @prequNum       = " & rstExcel.Fields(replace(NUM_ORIGINAL, ".", "#")).value
		xSQL = xSQL & "  ,@previId        = " & reviId
		set xrs = gConexao.execute(xSQL)
		retornarComErro xPagina & ":PCAD_ItemRequisicao", mensagem , destinoErro 
	end if
	retorno = xrs("retorno")
	if retorno <> 0 then
		mensagem = "(" & retorno & ") " & xRs("msg")
		xSQL = " exec PCAD_RequisicaoRollback"
		xSQL = xSQL & "   @prequNum       = " & rstExcel.Fields(replace(NUM_ORIGINAL, ".", "#")).value
		xSQL = xSQL & "  ,@previId        = " & reviId
		set xrs = gConexao.execute(xSQL)
		retornarComErro xPagina & ":PCAD_ItemRequisicao", mensagem, destinoErro 
	end if
	set xrs = nothing

	rstExcel.MoveNext
wend
rstExcel.close
Set rstExcel = nothing
cnnExcel.close
set cnnExcel = nothing

Session("msg") = "Requisi��o carregada com sucesso!"

if not debug="on" then
	response.redirect("gerMensagem.asp")
end if

%>