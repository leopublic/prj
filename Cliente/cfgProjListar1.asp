<% Response.Expires= -1%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<!--#include file="../includes/header-padrao.inc"-->
		<!--#include file="../includes/clsProjeto.inc"-->
		<SCRIPT language="javascript">
		<!--
			function AbrirJanela(url) { //v2.0
				var w = screen.width; // Get the width of the screen
				var h = screen.height; // Get the height of the screen

				// The size of the Window
				var win_width = 470;
				var win_height = 450;

				// Where to place the Window
				var left = (w - win_width)/2;
				var top = (h - win_height)/2;

				posicao = "width=" + win_width + ",height=" + win_height ;
				posicao += ",top=" + top + ",left=" + left + ",screenX=" + left + ",screenY=" + top;			
				parametros = posicao + ",directories=no,location=no,menubar=no,scrollbars=no,status=no,toolbar=no,resizable=no;"; 
				titulo = "IncluirUsuario" ; 
				window.open(url,titulo,parametros);
			}
			//--> 
		</SCRIPT>
	</HEAD>
	<body class="cabecalho" >
<%  lPerfisAutorizados = "15"
	VerificarAcesso()

   Dim gmenuGeral
   Dim gMenuSelecionado
   Dim gOpcaoSelecionada
   gMenuSelecionado = "Projetos"
'   gOpcaoSelecionada = "Projetos"
   gmenuGeral = "Configuracao"
   PastaSelecionada = "Configura��o"
%>
		<!--#include file="../includes/cab.inc"-->
			<tr>
				<td valign="top" class="blocoOpcoes">
					<!--#include file="../includes/opcConfig.inc"-->
				</td>
				<td colspan="2" valign="top" style="padding:5px" class="blocoPagina">
				<form>
					<table width="100%">
						<tr>
							<td class="docTit">PROJETOS CADASTRADOS</td>
				<% if TemAcesso("PRJADC") then %>
							<td align="right"><a href="javascript:AbrirJanela('cfgProjAlterar.asp?projId=0');"><img src="../images/newitem.gif" class="botao">&nbsp;Criar novo</a></td>
				<% else %>
							<td align="right">&nbsp;</td>
				<% end if %>
						</tr>
						<tr>
							<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
						</tr>
						<tr>
							<td colspan="2" style="height:5"></td>
						</tr>
						<tr>
							<td colspan="2">
								<table class="grid">
									<% 	xCampoOrdem = Request.QueryString("CampoOrdem") 
										If TemAcesso("PRJALT") then
											acessoOk = true
										else
											acessoOk = false
										end if
										if xCampoOrdem = "" then
											xCampoOrdem = "projNum"
										end if
										SortNum = ""
										SortNome = ""
										SortNomeGer = ""
										SortNomeSup = ""
										SortSupCmp = ""
										if xCampoOrdem = "projNum" then SortNum = "<img src=""../images/sort.gif"">"
										if xCampoOrdem = "projNome" then SortNome = "<img src=""../images/sort.gif"">"
										if xCampoOrdem = "projNomeGer" then SortNomeGer = "<img src=""../images/sort.gif"">"
										if xCampoOrdem = "projNomeSup" then SortNomeSup = "<img src=""../images/sort.gif"">"
										if xCampoOrdem = "emprNome" then SortSupCmp = "<img src=""../images/sort.gif"">"
									%>
									<tr>
										<td width="20" class="GridCab">&nbsp;</td>
										<td width="90" class="GridCab"><a href="cfgProjListar.asp?CampoOrdem=projNum" class="GridCab">N�mero</a><%=SortNum%></td>
										<td width="230" class="GridCab"><a href="cfgProjListar.asp?CampoOrdem=projNome" class="GridCab">Nome</a><%=SortNome%></td>
										<td width="200" class="GridCab"><a href="cfgProjListar.asp?CampoOrdem=projNomeGer" class="GridCab">Gerente de<br>projeto</a><%=SortNomeGer%></td>
										<td width="200" class="GridCab"><a href="cfgProjListar.asp?CampoOrdem=projNomeSup" class="GridCab">Supervisor de<br>constru��es</a><%=SortNomeSup%></td>
										<td width="120" class="GridCab"><a href="cfgProjListar.asp?CampoOrdem=emprNome" class="GridCab">Sup. Campo</a><%=SortSupCmp%></td>
										<td class="GridCab">&nbsp;</td>
										<td class="GridCab">&nbsp;</td>
									</tr> 
									<%
									Dim gConexao 
									Dim gConexao2
									xSQL = "select projID, projNum, projNome, isnull(U1.usuaNome, '(n�o definido)')  projNomeGer, isnull(U2.usuaNome, '(n�o definido)') projNomeSup"
									xSQL = xSQL & " from ((Projeto left join Usuario U1 on U1.usuaId = usuaIdGer)"
									xSQL = xSQL & "     left join Usuario U2 on U2.usuaId = usuaIdSup)"
									xSQL = xSQL & "  where projAtivo = 1"
									xSQL = xSQL & " order by " & xCampoOrdem
								
									AbrirConexao
									AbrirConexao2
									Set xRs = gConexao.Execute(xSQL)
									if xRs.EOF then
										Response.write "<tr>" & vbcrlf
										Response.write "<td colspan=""7"">Nenhum projeto cadastrado</td>" & vbcrlf
										Response.write "</tr>" & vbcrlf
									else
										estilo = "GridLinhaPar"
										while not xRs.EOF
											Response.write "<tr>" & vbcrlf
											if acessoOk then
												response.write vbtab & "<td class=""" & estilo & """><a href=""cfgProjDetalhe.asp?projId=" & xRs("projId") & """ title=""Clique para alterar os detalhes do projeto"">" & IconEdicao() & "</td>"
											else
												response.write vbtab & "<td class=""" & estilo & """><img src=""../images/folder.gif"" style=""border:0""></td>"
											end if
											response.write vbtab & "<td class=""" & estilo & """>" & xRs("projNum") & "</td>"
											response.write vbtab & "<td class=""" & estilo & """>" & xRs("projNome") & "</td>"
											response.write vbtab & "<td class=""" & estilo & """>" & xRs("projNomeGer") & "</td>"
											response.write vbtab & "<td class=""" & estilo & """>" & xRs("projNomeSup") & "</td>"
											response.write vbtab & "<td class=""" & estilo & """>" & SupervisorCampo(xrs("projId")) & "</td>"
										    response.write vbtab & "<td class=""" & estilo & """><a href=""javascript:AbrirJanela('cfgProjArq.asp?projId=" & xRs("projId") & "',500,340);"" title=""Clique para arquivar o projeto"">" & IconArquivar() & "</a></td>"
										    response.write vbtab & "<td class=""" & estilo & """>&nbsp;</td>"
											Response.write "</tr>" & vbcrlf
											xRs.MoveNext
											if estilo = "GridLinhaPar" then
												estilo = "GridLinhaImpar"
											else
												estilo = "GridLinhaPar"
											end if
										wend
									end if
									%>
									<tr>
										<td colspan="8" class="GridRodape">&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="2" class="opcSubMenu">(<b>dica</b>: clique no t�tulo das colunas para mudar a ordena��o da consulta.)</td>
						</tr>
					</table>
				</form>
				</td>
			</tr>	
		<!--#include file="../includes/rod.inc"-->
	</body>
</HTML>