<% Response.Expires= -1%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Controle de projetos pela internet</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<link rel="stylesheet" type="text/css" href="../EstiloMenu.css">
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<!--#include file="../includes/basFormatador.inc"-->
		<!--#include file="../includes/clsContrato.inc"-->
		<!--#include file="../includes/clsProjeto.inc"-->
		<script language="JavaScript" type="text/javascript" src="../scripts/AbrirJanela.js"></script>
	</HEAD>
	<body class="cabecalho" >
		<!--#include file="../includes/varControleAcesso.inc"-->
		<!--#include file="../includes/varBanco.inc"-->
		<!--#include file="../includes/varDatas.inc"-->

<%  lPerfisAutorizados = "15"
	VerificarAcesso()

   gMenuSelecionado = "Projetos"
   gOpcaoSelecionada = "Projetos"
   gItemSelecionado = "Projeto"
   gmenuGeral = "Configuracao"
   PastaSelecionada = "Configura��o"

	AbrirConexao
	projId = Request.Querystring("projID")
	set xrs = Projetos(projId)
	projNome = xRs("projNome")
	projNum = xRs("projNum")
	projNomeSup = xRs("projNomeSup")
	projNomeGer = xRs("projNomeGer")
	projNomePla = xRs("projNomePla")
	projCustGerEst = xRs("projCustGerEst")
	projCustSupcEst = xRs("projCustSupcEst")
	tpprNome = xRs("tpprNome")
	moedNome = xRs("moedNome")
	moedSigla = xRs("moedSigla")
	Session("projId") = projId
	Session("projNome") = projNome
	Session("moedSigla") = moedSigla
	xRs.CLose
	Dim gConexao2
	AbrirConexao2
	emprNome = SupervisorCampo(projId)


	TotalEst = projCustGerEst + projCustSupcEst

	Set xRs = nothing



    PastaSelecionada = "Configura��o"
%>
		<!--#include file="../includes/cab.inc"-->
			<tr>
				<td valign="top" class="blocoOpcoes">
					<!--#include file="../includes/opcConfig.inc"-->
				</td>
				<td colspan="2" valign="top" style="padding:5px" class="blocoPagina">
				<form>
					<table width="100%">
						<tr>
							<td width="15"></td>
							<td width="460"></td>
							<td width="60"></td>
						</tr>
						<tr>
							<td colspan="3" class="docTit"><%=ProjNome%> - Detalhes do projeto</td>
						</tr>
						<tr>
							<td colspan="3" style="height:5"></td>
						</tr>
						<tr>
							<td colspan="2" class="docTit">Informa��es b�sicas</td>
							<td class="GridSLinha" align="right"><a href="javascript:AbrirJanela('cfgProjAlterar.asp?projId=<% = projId %>',470,270);"><img src="../images/btnAlterar.gif" class="botao"></a></td>
						</tr>
						<tr>
							<td colspan="3" background="../images/pont_cinza_h.gif" style="height:2"></td>
						</tr>
						<tr>
							<td colspan="3" style="height:10"></td>
						</tr>
						<tr>
							<td></td>
							<td colspan="2" align="left">
								<table class="doc">
									<tr>
										<td width="17%" class="docLabel">Nome</td>
										<td width="2%" class="docLabel">:</td>
										<td width="31%" class="docCmpLivre" ><% = projNome %></td>
										<td width="17%" class="docLabel" align="right">N�mero</td>
										<td width="2%" class="docLabel">:</td>
										<td width="31%"class="docCmpLivre"><% = projNum %></td>
									</tr>
									<tr>
										<td class="docLabel">Gerente</td>
										<td class="docLabel">:</td>
										<td class="docCmpLivre"><% = projNomeGer %></td>
										<td class="docLabel" align="right">Sup. constru��es</td>
										<td class="docLabel">:</td>
										<td class="docCmpLivre"><% = projNomeSup %></td>
									</tr>
									<tr>
										<td class="docLabel" align="right">Tipo</td>
										<td class="docLabel">:</td>
										<td class="docCmpLivre" ><%= tpprNome %></td>
										<td class="docLabel">Planejador</td>
										<td class="docLabel">:</td>
										<td class="docCmpLivre"><% = projNomePla %></td>
									</tr>
									<tr>
										<td class="docLabel">Sup. campo</td>
										<td class="docLabel">:</td>
										<td colspan="4" class="docCmpLivre"><% = emprNome %></td>
									</tr>
									<tr>
										<td class="docLabel" align="right">Moeda</td>
										<td class="docLabel">:</td>
										<td class="docCmpLivre" ><%= moedNome %> (<%= moedSigla %>)</td>
										<td class="docLabel">&nbsp</td>
										<td class="docLabel">&nbsp</td>
										<td class="docLabel">&nbsp</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="3" style="height:15"></td>
						</tr>
						<tr>
							<td colspan="2" class="docTit">Custos de constru��es</td>
							<td class="GridSLinha" align="right"><a href="javascript:AbrirJanela('cfgProjAltCustConstr.asp?projId=<% = projId %>',500,600);"><img src="../images/btnAlterar.gif" class="botao"></a></td>
						</tr>
						<tr>
							<td colspan="3" background="../images/pont_cinza_h.gif" style="height:2"></td>
						</tr>
						<tr>
							<td></td>
							<td colspan="2">
								<table class="doc">
									<tr>
										<td width="200" class="docLabel">Hh gerenciamento estimado</td>
										<td width="5" class="docLabel">:</td>
										<td width="100" align="right" class="docCmpLivre"><% = FormatNumber(projCustGerEst) %></td>
										<td></td>
									</tr>
									<tr>
										<td class="docLabel">Hh sup. campo estimado</td>
										<td class="docLabel">:</td>
										<td align="right" class="docCmpLivre"><% = FormatNumber(projCustSupcEst) %></td>
										<td></td>
									</tr>
									<tr>
										<td colspan="3" class="docLabel" height="5" style="font-size:1;border-bottom-style:solid;border-bottom-width:1;border-bottom-color:#94bdd1">&nbsp;</td>
										<td></td>
									</tr>
									<tr>
										<td class="docLabel">Total hh estimado:</td>
										<td class="docLabel">:</td>
										<td align="right" class="docSBorda" style="font-weight:bold"><% = FormatNumber(TotalEst) %></td>
										<td></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="3" style="height:10"></td>
						</tr>
						<tr>
							<td colspan="3" style="height:15"></td>
						</tr>
						<tr>
							<td colspan="2" class="docTit">Estimativas de custos e prazos de contratos</td>
							<td class="GridSLinha" align="right"><a href="javascript:AbrirJanela('cfgProjCntrPraz.asp?projId=<% = projId %>',680,340);"><img src="../images/btnAlterar.gif" class="botao"></a></td>
						</tr>
						<tr>
							<td colspan="3" background="../images/pont_cinza_h.gif" style="height:2"></td>
						</tr>
						<tr>
							<td colspan="3" style="height:10"></td>
						</tr>
						<tr>
							<td></td>
							<td colspan="2">
								<table width="100%" class="grid">
									<tr>
										<td width="130"></td>
										<td width="2"></td>
										<td width="80"></td>
										<td width="2"></td>
										<td width="90"></td>
										<td width="90"></td>
										<td width="90"></td>
										<td width="90"></td>
										<td width="90"></td>
									</tr>
									<tr>
										<td class="docSBorda"></td>
										<td></td>
										<td class="GridCabGrupo" align="center">Custo</td>
										<td></td>
										<td class="GridCabGrupo" align="center" colspan="5">Prazos estimados</td>
									</tr>
									<tr>
										<td class="GridCab">&nbsp;</td>
										<td></td>
										<td class="GridCab" align="center" valign="middle">Valor<br>estimado<br>(<%=Session("moedSigla")%>)</td>
										<td></td>
										<td class="GridCab" align="center" valign="middle">Emiss�o<br>MD</td>
										<td class="GridCab" align="center" valign="middle">Contrata��o</td>
										<td class="GridCab" align="center" valign="middle">Mobiliza��o</td>
										<td class="GridCab" align="center" valign="middle">Desmob.</td>
										<td class="GridCab" ></td>
									</tr>
										<%
										i = 0
										xTermEst = ""
										set xRs = gConexao.execute("Select *, isnull(TermEst1, '2099-12-31') TermEstx from vContratosBaseline v where projId = " & projId & " order by TermEstx, servOrd")
										if xRs.eof then
											xTermEst = xTermEst & "<tr>" & vbcrlf
											xTermEst = xTermEst & vbtab & "<td colspan=""9"" class=""GridLinha"">(nenhum servico alocado ao projeto)</td>" & vbcrlf
											xTermEst = xTermEst & "<tr>" & vbcrlf
										else
											while not xRs.Eof
												i = i + 1
												if isnull(xRs("usuaNome")) then
													fornecedor = "(n�o definido)"
												else
													fornecedor = "(" & xRs("emprNome") & ")"
												end if
												if xRs("cntrCanc") then
												    classCab = "GridCabLinhaCanc"
													classLinha = "GridLinhaCanc"
												else
												    classCab = "GridCabLinha"
													classLinha = "GridLinha"
												end if
												xTermEst = xTermEst & "<tr>" & vbcrlf
												xTermEst = xTermEst & vbtab & "<td class=""" & classCab & """ style=""font-weight:normal""><b>" & xRs("servNome") & "</b><br>" & fornecedor & "</td>" & vbcrlf
												xTermEst = xTermEst & vbtab & "<td></td>" & vbcrlf
												xTermEst = xTermEst & vbtab & "<td class=""" & classLinha & """ align=""right"">" &  FormatNumber(xRs("cntrVlrEst")) & "</td>" & vbcrlf
												xTermEst = xTermEst & vbtab & "<td></td>" & vbcrlf
												xTermEst = xTermEst & vbtab & "<td class=""" & classLinha & """ align=""center"">" &  DtFmt(xRs("cntrTermEst1")) & "</td>" & vbcrlf
												xTermEst = xTermEst & vbtab & "<td class=""" & classLinha & """ align=""center"">" &  DtFmt(xRs("cntrTermEst2")) & "</td>" & vbcrlf
												xTermEst = xTermEst & vbtab & "<td class=""" & classLinha & """ align=""center"">" &  DtFmt(xRs("cntrTermEst3")) & "</td>" & vbcrlf
												xTermEst = xTermEst & vbtab & "<td class=""" & classLinha & """ align=""center"">" &  DtFmt(xRs("cntrTermEst4")) & "</td>" & vbcrlf
												if xRs("cntrCanc") then
													xTermEst = xTermEst & vbtab & "<td class=""" & classLinha & """ align=""right"">(cancelado)</td>"
												else
													xTermEst = xTermEst & vbtab & "<td class=""" & classLinha & """ align=""right""><a href=""javascript:AbrirJanela('cfgProjCntrExcl.asp?cntrId=" & xRs("cntrId") & "',500,300);"" class=""gridLinha""><img src=""../images/btnExcluirMini.gif""></a></td>"
												end if
												xTermEst = xTermEst & "<tr>" & vbcrlf
												xRs.Movenext
											wend
										end if
										xRs.Close
										set xRs = Nothing
										Response.Write(xTermEst)
										TamJanela = 240 + (25 * i)
									 %>
									<tr>
										<td class="GridRodape">&nbsp;</td>
										<td></td>
										<td class="GridRodape">&nbsp;</td>
										<td></td>
										<td class="GridRodape">&nbsp;</td>
										<td class="GridRodape">&nbsp;</td>
										<td class="GridRodape">&nbsp;</td>
										<td class="GridRodape">&nbsp;</td>
										<td class="GridRodape">&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="3" style="height:15"></td>
						</tr>
					</table>
				</form>
				</td>
			</tr>

		</table>
	</body>
</HTML>