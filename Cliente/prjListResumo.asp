<% Response.Expires= -1%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ::</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<link rel="stylesheet" type="text/css" href="../EstiloMenu.css">
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<script language="JavaScript" type="text/javascript" src="../scripts/AbrirJanela.js"></script>
	</HEAD>
	<body class="cabecalho" >
<%  lPerfisAutorizados = "157"
	VerificarAcesso()

	function formataQtd(pqtd)
		if  pqtd = 0 then
			xqtd = "-"
		else
			xqtd = FormatNumber(pqtd,1,-1,0)
		end if
		formataQtd = xqtd
	end function 
	
	projId = session("projId")
  	xlistId = request.querystring("listId")
	Dim gmenuGeral
	Dim gMenuSelecionado
	Dim gOpcaoSelecionada
	'gOpcaoSelecionada = "Servicos"
	gmenuGeral = "Projetos"
	gMenuSelecionado = "prjListas"

   	Dim gConexao 
	AbrirConexao
	
	xSQL = "select count(*) from revisao where listId = " & xlistId
	Set xRs = gConexao.Execute(xSQL)
	if not xRs.EOF then
		xQtdRevisoes = xrs(0)
	end if
	xrs.close
	Set xrs = nothing
	
	xSQL =        "select tpliPrefixo , projNum , tpliNome , tpliCodigo, reviNumero, requNum, listSequencial"
	xSQL = xSQL & "  from lista , tipolista, projeto , revisao left join requisicao on requisicao.reviId = revisao.reviId "
	xSQL = xSQL & " where lista.tpliId = tipolista.tpliId"
	xSQL = xSQL & "   and projeto.projId = lista.projId"
	xSQL = xSQL & "   and revisao.listId = lista.listId"
	xSQL = xSQL & "   and lista.listId = " & xlistId
	'response.write xSQL
	Set xRs = gConexao.Execute(xSQL)
	if not xRs.EOF then
		xLista =  xrs("tpliPrefixo") & "-" & xRs("projNum") & "-" & xRs("tpliCodigo") & "-" & xRs("listSequencial")
		xtpliPrefixo = xrs("tpliPrefixo") 
		xtpliNome = xrs("tpliNome")
	end if
    gOpcaoSelecionada = xrs("tpliPrefixo") 

	xrs.close
   
   
 %>
		<!--#include file="../includes/cab.inc"-->
			<tr>
				<td style="padding:0px" valign="top" class="blocoOpcoes">
					<!--#include file="../includes/opcProjx2.inc"-->
				</td>
				<td colspan="2" valign="top" style="padding:5px" class="blocoPagina">
				<form>
					<table width="100%">
					<tr>
						<td class="docTit"><%=session("projNome")%><br><img src="../images/0066_double_arrow.png" class="botao" style="margin-right:3px"><%=xtpliNome %>: <%=xlista%><br><%=xrequisicao%> </td>
						<td align="right">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
					</tr>
					<tr>
						<td colspan="2" style="height:5"></td>
					</tr>
					<tr>
						<td colspan="2">
							<table width="100%" class="grid" >
							<tr>
<%
	xCampoOrdem = Request.QueryString("CampoOrdem")
	if xCampoOrdem = "" then
		xCampoOrdem = Session("CampoOrdemResumo")
		if xCampoOrdem = "" then
			xCampoOrdem = "itemCodJde"
		end if
	end if
	Session("CampoOrdemResumo") = xCampoOrdem

	if xCampoOrdem = "itemCodJde" then SortCod = "<img src=""../images/sort.gif"">"
	if xCampoOrdem = "itemTag" then SortTag = "<img src=""../images/sort.gif"">"
	if xCampoOrdem = "itemDescricao" then SortDescricao = "<img src=""../images/sort.gif"">"

%>

							<td width="70px" class="GridCab"><a href="prjListResumo.asp?listId=<%=xlistId%>&CampoOrdem=itemCodJde" class="GridCab" title="Clique para ordenar por essa coluna">Cod JDE</a><%=SortCod%></td>
							<td width="80px" class="GridCab"><a href="prjListResumo.asp?listId=<%=xlistId%>&CampoOrdem=itemTag" class="GridCab" title="Clique para ordenar por essa coluna">TAG</a><%=SortTag%></td>
							<td class="GridCab" ><a href="prjListResumo.asp?listId=<%=xlistId%>&CampoOrdem=itemDescricao" class="GridCab" title="Clique para ordenar por essa coluna">Descri��o</a><%=SortDescricao%></td>
							<td width="80px" class="GridCab" style="text-align:right">Lista</td>
							<td width="80px" class="GridCab" style="text-align:right">Requerido</td>
							<td width="80px" class="GridCab" style="text-align:right">Pedido</td>
							<td width="80px" class="GridCab" style="text-align:right">Excedente<br/>de Compra</td>
							<td width="80px" class="GridCab" style="text-align:right">Recebido</td>
							<td width="80px" class="GridCab" style="text-align:right">Retirado</td>
							<td width="80px" class="GridCab" style="text-align:right">Em estoque</td>
						</tr>
	<%
	xsql = "        select IT.itemId, itemCodJde, itemDescricao, itemTag, itelQtd"
	xSQL = xSQL & "      , isnull(IRX.iterQtd, 0) iterQtd"
	xSQL = xSQL & "      , isnull(max(IQ.iteqQtd), 0) iteqQtd"
	xSQL = xSQL & "      , isnull(sum(IP.itepQtd), 0) itepQtd"
	xSQL = xSQL & "      , isnull(sum(IC.itrcQtd), 0) itrcQtd"
	xSQL = xSQL & "      , isnull(sum(IA.itesQtd), 0) itesQtd"
	xSQL = xSQL & "      , isnull((sum(IC.itrcQtd) - sum(IA.itesQtd)), 0) saldo"
	xSQL = xSQL & "      , isnull((sum(IP.itepQtd) - max(IR.iterQtd)), 0) sobra"
	xsql = xSQL & "   from (((((((itemLista IL join item IT on IT.itemId = IL.itemId) "
	xSQL = xSQL & "   left join (select itemId, iterQtd from itemRevisao IR where reviId = (select reviId from revisao where listId = " & xlistId & " and reviNumero = (select max(reviNumero) from revisao where listId= " & xlistId & "))) IRX on IRX.itemId = IL.itemId)"
	xsql = xSQL & "   left join itemRevisao IR on IR.itemId = IT.itemId)"
	xsql = xSQL & "   left join (itemrequisicao IQ join requisicao RQ   on RQ.requId = IQ.requId) on IQ.iterId = IR.iterId )"
	xsql = xSQL & "   left join (itempedido IP join pedidocompra P      on P.pedcId = IP.pedcId)  on IP.iteqId = IQ.iteqId )"
	xsql = xSQL & "   left join (itemrecebimento IC join recebimento RC on RC.receId = IC.receId) on IC.itepId = IP.itepId )"
	xsql = xSQL & "   left join (itemsaida IA join saidaMaterial S      on S.saidId = IA.saidId)  on IA.itelId = IL.itelId )"
	xSQL = xSQL & "  where IL.listId = " & xlistId
	xsql = xSQL & "  group by IT.itemId, itemCodJde, itemDescricao, itemTag, itelQtd, IRX.iterQtd"
	xSQL = xSQL & " order by " & xCampoOrdem
	'response.write xsql
	Set xRs = gConexao.Execute(xSQL)
	Dim xQtd 
	xQtd = 0
	while not xRs.eof
		response.write "<tr onMouseOver=""javascript:this.style.backgroundColor='#F0F0F0';"" onMouseOut=""javascript:this.style.backgroundColor='#FFFFFF';"">"
		response.write "<td>" & xRs("itemCodJde") & "</td>"
		response.write "<td>" & xRs("itemTag") & "</td>"
		response.write "<td>" & xRs("itemDescricao") & "</td>"
		xvalor = cdbl(xRS("itelQtd"))
		response.write "<td style=""text-align:right;"">" & FormataQtd(xvalor) & "</td>"
		xvalor = cdbl(xRS("iteqQtd"))
		response.write "<td style=""text-align:right;"">" & FormataQtd(xvalor) & "</td>"
		xvalor = cdbl(xRS("itepQtd"))
		response.write "<td style=""text-align:right;"">" & FormataQtd(xvalor) & "</td>"
		xvalor = cdbl(xRS("itepQtd")) - cdbl(xRS("iterQtd"))
		if xvalor < 0 then
			xvalor = 0
		end if
		response.write "<td style=""text-align:right;"">" & FormataQtd(xvalor) & "</td>"
		xvalor = cdbl(xRS("itrcQtd"))
		response.write "<td style=""text-align:right;"">" & FormataQtd(xvalor) & "</td>"
		xvalor = cdbl(xRS("itesQtd"))
		response.write "<td style=""text-align:right;"">" & FormataQtd(xvalor) & "</td>"
		xvalor = cdbl(xRS("itrcQtd")) - cdbl(xRS("itesQtd"))
		response.write "<td style=""text-align:right;"">" & FormataQtd(xvalor) & "</td>"
		xrs.MoveNext
	wend
	
	%>
								<tr>
									<td class="GridRodape">&nbsp;</td>
									<td class="GridRodape">&nbsp;</td>
									<td class="GridRodape">&nbsp;</td>
									<td class="GridRodape">&nbsp;</td>
									<td class="GridRodape">&nbsp;</td>
									<td class="GridRodape">&nbsp;</td>
									<td class="GridRodape">&nbsp;</td>
									<td class="GridRodape">&nbsp;</td>
									<td class="GridRodape">&nbsp;</td>
									<td class="GridRodape">&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
					</table>
				</form>
				</td>
			</tr>
		<!--#include file="../includes/rod.inc"-->
	</body>
</HTML>