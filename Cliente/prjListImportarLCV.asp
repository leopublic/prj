<!--#include file="../includes/clsBanco.inc"-->
<!--#include file="../includes/clsControleAcesso.inc"-->
<!--#include file="../includes/varControleAcesso.inc"-->
<!--#include file="../includes/varBanco.inc"-->
<%
Dim debug 
Dim ximpoId
Function trimaAspas(campo)
	campo = left(campo, len(campo) - 1)
	campo = right(campo, len(campo) - 1)
	trimaAspas = campo
end function

Sub debuga (msg)
	xSQLD = "exec PCAD_MensagemImportacao @pimpoId = " & ximpoId & ", @pmsgiTexto = '" & replace(msg, "'", "''") & "'"
	gConexao.execute(xSQLD)
	
	if debug="on" then
		response.write "<br>" & msg
	end if
end sub

Function campoConvertido(campo) 
	valor = trim(replace(campo, ",", "."))
	campoConvertido = valor
end function

Dim objConn
Dim objRs
AbrirConexao
'== Obt�m arquivo a importar
debug = request.querystring("debug")
ximpoId = request.querystring("impoId")
xPath = Server.MapPath("..\listas")
xArquivo = xPath & "\" & ximpoId & ".txt"
'== Limpa mensagens da �ltima importacao
xSQL = "delete from MensagemImportacao where impoId =  " & ximpoId
gConexao.execute(xSQL)
'== Obtem dados do arquivo
xSQL = "select tpliPrefixo "
xSQL = xSQL & " from TipoLista TL, Importacao I"
xSQL = xSQL & " where TL.tpliId = I.tpliId "
xSQL = xSQL & "   and I.impoId = " & ximpoId
'response.write xSQL 
set xrs = gConexao.execute(xSQL)
If Err.Number <> 0 then
	debuga "<br/>SQL:" & xSQL
	debuga "<br/>ERRO: " & Err.Description
	Error.Clear
	response.end
end if
xprefixo = xrs("tpliPrefixo")
xrs.close
set xrs = nothing
'== Inicia importa��o do arquivo
	Const ForReading = 1, ForWriting = 2, ForAppending = 3
	Const TristateUseDefault = -2, TristateTrue = -1, TristateFalse = 0
	' Create a filesystem object
	Dim FSO
	set FSO = server.createObject("Scripting.FileSystemObject")

	if FSO.FileExists(xArquivo) Then
	    ' Get a handle to the file
	    Dim file    
	    set file = FSO.GetFile(xArquivo)

	    ' Open the file
	    Dim TextStream
	    Set TextStream = file.OpenAsTextStream(ForReading, TristateUseDefault)

	    ' Read the file line by line
		regNum = 0
		erro = 0
	    if ( not TextStream.AtEndOfStream) then
	        reg = TextStream.readline
			regNum = regNum + 1
			'==============================================================================================
			'== Procura identificador do arquivo
			'==============================================================================================
			posIdentificacao = instr(1, reg, xprefixo & "-", 1)
			debuga "Procurando header com identifica��o da lista...(" & xprefixo & "-" & ")"
			Do While (posIdentificacao = 0) and (not TextStream.AtEndOfStream)
		        reg = TextStream.readline
				regNum = regNum + 1		
				posIdentificacao = instr(1, reg, xprefixo & "-", 1)
			loop

			if (posIdentificacao = 0) then
				debuga "ERRO! Identifica��o da lista n�o encontrada."
				erro = 1
			else
				nomeLista = mid(reg, posIdentificacao,16)
				debuga "Identifica��o encontrada na linha " & regNum & " -->" & nomeLista
				xSplit = split(nomeLista, "-")
				xlistSequencial = xsplit(3)
				xNumProjeto = xsplit(1)
			end if
			'==============================================================================================
			'== Procura a revis�o do arquivo
			'==============================================================================================
			debuga "Procurando n�mero da revis�o..."
			pos = instr(1, reg, "Rev: ", 1)
			Do While (pos = 0) and (not TextStream.AtEndOfStream)
		        reg = TextStream.readline
				regNum = regNum + 1
				posIdentificacao = instr(1, reg, xprefixo & "-", 1)
			loop

			if (pos = 0) then
				debuga "ERRO! N�mero da revis�o da lista n�o encontrado."
				erro = 1
			else
				reviNumero = mid(reg, pos+5,2)
				debuga "N�mero da revis�o encontrada na linha " & regNum & " -->" & reviNumero
			end if
			reviNumero = cInt(reviNumero)
			'==============================================================================================
			'== Cria a revis�o no banco
			'==============================================================================================
			xSQL = "exec LIST_Criar "
			xSQL = xSQL & "   @pprojId         = " & session("projId")
			xSQL = xSQL & " , @pimpoId         = " & ximpoId
			xSQL = xSQL & " , @previNumero     = " & reviNumero
			xSQL = xSQL & " , @plistSequencial = " & xlistSequencial
			xSQL = xSQL & " , @pusuaId         = " & session("usuaId")
			xSQL = xSQL & " , @plistNumProjeto = " & xNumProjeto
			debuga xSQL 
			on error resume next
			set xrs = gConexao.execute(xSQL)
			If Err.Number <> 0 then
				debuga "<br/>SQL:" & xSQL
				debuga "<br/>ERRO: " & Err.Description
				Error.Clear
				response.end
			end if
			xreviId = xrs("reviId")
			
			'==============================================================================================
			'== Procura o cabe�alho das colunas
			'==============================================================================================
			debuga "Procurando cabe�alho das colunas..."
			pos = instr(1, reg, "Descri��o Resumida", 1)
			Do While (pos = 0) and (not TextStream.AtEndOfStream)
		        reg = TextStream.readline
				regNum = regNum + 1		
				pos = instr(1, reg, "Descri��o Resumida", 1)
			loop
			if (pos = 0) then
				debuga "ERRO! Cabe�alho das linhas n�o encontrado."
				erro = 1
			else
				revisao = mid(reg, pos+5,2)
				debuga "Cabe�alho das colunas encontrado na linha " & regNum 
		        reg = TextStream.readline  ' Pula uma linha				
		        reg = TextStream.readline  ' Pula uma linha				
		        reg = TextStream.readline  ' Pula uma linha				
			end if
			'== Inicia a importa��o dos itens
			Do While (not TextStream.AtEndOfStream)
				'== Pula linhas em branco
				if not (trim(replace(reg, vbtab, "")) = "") then
					'
					' Obtem campos
					coluna = Split(reg, vbtab)
					tag = trimaAspas(coluna(0))
					linha = trimaAspas(coluna(1))
					codJde = coluna(2)
					descricao = trimaAspas(coluna(3))
					revisao = coluna(4)

					debuga "<br>Item encontrado: " & ordem
					debuga "Tag    -->" & tag & "<--"
					debuga "Linha   -->" & linha & "<--"
					debuga "codJDE    -->" & codJde & "<--"
					debuga "descricao -->" & descricao & "<--"
					' 
					' Cadastra ou obtem ID da linha
					campos = split(linha, "-")
					xBitola = campos(0)
					if ubound(campos) <1 then
						xCod1 = xBitola
						xCod2 = "-"
					else
						xCod1 = campos(1)
						if ubound(campos) <2 then
							xCod2 = "-"
						else
							xCod2 = campos(2)
						end if
					end if
					'== Cria nova linha
					xSQL = "exec PCAD_Linha "
					xsql = xsql & "   @previId     =  " & xreviId
					xsql = xsql & " , @plinhNome   = '" & linha & "'"
					xsql = xsql & " , @plinhBitola = '" & xBitola & "'"
					xsql = xsql & " , @plinhCod1   = '" & xCod1 & "'"
					xsql = xsql & " , @plinhCod2   = '" & xCod2 & "'"
					debuga xSQL 
					set xrs = gConexao.execute(xSQL)
					If Err.Number <> 0 then
						debuga "<br/>SQL:" & xSQL
						debuga "<br/>ERRO: " & Err.Description
						Error.Clear
						response.end
					end if
					xlinhId = xrs("linhId")
					xrs.close
					set xrs = nothing

					xSQL = " exec PCAD_ItemLista"
					xSQL = xSQL & "   @plinhId       = " & xlinhId
					xSQL = xSQL & "  ,@punidSigla    = 'UN'"
					xSQL = xSQL & "  ,@pitelOrdem    = 0"
					xSQL = xSQL & "  ,@pitemCodJde   = " & codJde
					xSQL = xSQL & "  ,@pitelTag      = '" & tag & "'"
					xSQL = xSQL & "  ,@previId       = " & xreviId
					xSQL = xSQL & "  ,@pitelQtd      = 1"
					xSQL = xSQL & "  ,@pitemDescricao = '" & descricao & "'"
					debuga xSQL 
					set xrs = gConexao.execute(xSQL)
					If Err.Number <> 0 then
						debuga "<br/>SQL:" & xSQL
						debuga "<br/>ERRO: " & Err.Description
						Error.Clear
						response.end
					end if
					xrs.close
					set xrs = nothing
				end if
				'== L� proximo registro
				reg = TextStream.readline
				regNum = regNum + 1
			loop
	    
	    end if
		TextStream.close
	    Set TextStream = nothing
		Session("msg") = "Lista carregada com sucesso!"
		Set FSO = nothing
		if not debug="on" then
			response.redirect("gerMensagem.asp")
		end if
	Else
		Session("msg") = "Arquivo (" & xArquivo & ") n�o encontrado!"	    
		debuga "Arquivo (" & xArquivo & ") n�o encontrado!"
		Set FSO = nothing
		if not debug="on" then
			response.redirect("gerMensagem.asp")
		end if
	End If
%>