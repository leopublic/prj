<% Response.Expires= -1%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Controle remoto de projetos</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<script language="JavaScript" type="text/javascript" src="../scripts/PreparaJanela.js"></script>
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<script language="JavaScript" type="text/javascript" src="../scripts/CheckNumeric.js"></script>
		<SCRIPT language="JavaScript">
			function submitform(){
			  document.form1.submit();
			}
			function validar(){
				submitform();
			}
		</SCRIPT> 
	</HEAD>
	<body class="PopUpEntrada" onLoad="PreparaJanela(430,400);">
<% 
    lPerfisAutorizados = "51"
	VerificarAcesso
	Dim gConexao 
	AbrirConexao
	emprId = Request.Querystring("emprId")
	xSQL = "select emprNome, emprIdJde, emprCnpj , emprEnd, emprCep, emprTel, perfNome from (Empreiteira left join PerfilAcesso on PerfilAcesso.perfId = Empreiteira.perfId) where emprId = " & emprId 
	set xrs = gConexao.execute(xSQL)
	titulo = "Excluir contratado"
	Session("TituloMsg") = titulo
	emprNome = xrs("emprNome")
	emprIdJde = xrs("emprIdJde")
	emprCnpj = xrs("emprCnpj")
	emprEnd = xrs("emprEnd")
	emprCep = xrs("emprCep")
	emprTel = xrs("emprTel")
	perfNome = xrs("perfNome")
	xRs.CLose
	Set xRs = nothing
%>

	<form action="cfgEmprExcluir2.asp" method="post" name="form1">
		<input type="hidden" name="emprId" value=" <% = emprId %> " >
		<table width="100%">
			<tr>
				<td width="15"></td>
				<td></td>
			</tr>
			<tr>				
				<td colspan="2" class="docTit"><% = titulo %></td>
			</tr>
			<tr>
				<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
			</tr>
			<tr>
				<td colspan="2" height="15"></td>
			</tr>
			<tr>
				<td></td>
				<td>
					<table class="doc">
						<tr>
							<td width="100" class="docLabel">Id</td>
							<td width="5" class="docLabel">:</td>
							<td class="docCmpLivre"><% = emprIdJde %></td>
						</tr>
						<tr>
							<td width="100" class="docLabel">Nome</td>
							<td width="5" class="docLabel">:</td>
							<td class="docCmpLivre"><% = emprNome %></td>
						</tr>
						<tr>
							<td width="100" class="docLabel">CNPJ</td>
							<td width="5" class="docLabel">:</td>
							<td class="docCmpLivre"><% = emprCnpj %></td>
						</tr>
						<tr>
							<td width="100" class="docLabel">Endere�o</td>
							<td width="5" class="docLabel">:</td>
							<td class="docCmpLivre"><% = emprEnd %></td>
						</tr>
						<tr>
							<td width="100" class="docLabel">CEP</td>
							<td width="5" class="docLabel">:</td>
							<td class="docCmpLivre"><% = emprCep %></td>
						</tr>
						<tr>
							<td width="100" class="docLabel">Telefone</td>
							<td width="5" class="docLabel">:</td>
							<td class="docCmpLivre"><% = emprTel %></td>
						</tr>
						<tr>
							<td class="docLabel">Perfil</td>
							<td class="docLabel">:</td>
							<td class="docCmpLivre"><% = perfNome %></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2" height="20"></td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<table>
						<tr>
							<td height="28" width="80" align="center"><img border="0" src="../images/btnExcluir.gif" class="botao" onClick="javascript:validar();"></td>
							<td width="10%"></td>
							<td width="45%" align="center"><a href="javascript:window.close();"><img src="../images/btnCancelar.gif" class="botao"></a></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
	</body>
</HTML>