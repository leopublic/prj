<%
	Response.Expires= 0
    Response.AddHeader "PRAGMA", "NO-CACHE" 
    if Session("usuaId") = "" then
   		response.redirect("../login.asp")
    end if
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Controle de WBS</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="JavaScript" type="text/javascript" src="tree/dtree.js"></script>
		<link rel="stylesheet" type="text/css" href="tree/dtree.css">
		<script language="JavaScript" type="text/javascript" src="../scripts/moving_div_on_screen.js"></script>
        <script language="JavaScript" type="text/javascript" src="../scripts/block_background_objects.js"></script>
        <script language="JavaScript" type="text/javascript" src="../scripts/functions.js"></script>
        <link rel="stylesheet" type="text/css" href="../scripts/block_background_objects.css">
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<!--#include file="../includes/funcoes.inc"-->
		<script language="JavaScript" type="text/javascript" src="../scripts/PreparaJanela.js"></script>

		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<link rel="stylesheet" type="text/css" href="../EstiloMenu.css">
        <link rel="stylesheet" type="text/css" href="../scripts/wbs_form.css">
        <script language="javascript">
			function limpa()
			{
				d = null;
				document.getElementById("divLista").innerHTML = 'zerou';
			}
		</script>
	</HEAD>
<body id="sandbox" class="full">
<!--#include file="../includes/varControleAcesso.inc"-->
<!--#include file="../includes/varBanco.inc"-->
	<input type="button" onclick="javascript:d.openAll();" value="Expandir at� �ltimo n�vel"/>
	<input type="button" onclick="javascript:d.closeAll();" value="Fechar todos os n�veis"/>
	<input type="button" onclick="javascript:limpa();" value="Limpa"/>
				<input type='hidden' id='counter' name='counter'>
				<div class='divtree' id="divLista">
					Montando �rvore... <br/>(dependendo da quantidade de ramos e folhas, isso pode demorar alguns segundos)				
				</div>
				<div id='divErros' style='width:100%;height:0px;display:none;margin-top:5px;'></div>
	<script language="JavaScript">
		d = new dTree('d');
		_carregaitensNovo('d', 'divLista', 'PRTX', 'WBS Modelo Impressao', '', '', '', '');
		d.openAll();
	</script>
</body>
</HTML>