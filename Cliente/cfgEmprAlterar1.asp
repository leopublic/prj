<% Response.Expires= -1%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Controle remoto de projetos</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<script language="JavaScript" type="text/javascript" src="../scripts/PreparaJanela.js"></script>
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<script language="JavaScript" type="text/javascript" src="../scripts/CheckNumeric.js"></script>
		<SCRIPT language="JavaScript">
			function submitform(){
			  document.form1.submit();
			}
			function validar(){
			  if (document.form1.txtNome.value == '')
			  {   alert('O preenchimento do nome � obrigatorio.');
			  	  document.form1.txtNome.select();
				  document.form1.txtNome.focus(); }
			  else
  			  {    if (document.form1.cmbPerfil.value == '')
				  {   alert('O perfil � obrigatorio.');
					  document.form1.cmbPerfil.focus(); }
					else
					{submitform();}
			   }
			}
		</SCRIPT> 
	</HEAD>
	<body class="PopUpEntrada" onLoad="PreparaJanela(430,400);">
<% 
    lPerfisAutorizados = "51"
	VerificarAcesso
	Dim gConexao 
	AbrirConexao
	emprId = Request.Querystring("emprId")
	if emprId = 0 then
	    titulo = "Criar novo contratado"
		session("Msg") = "Contratado inclu�do com sucesso!"
		Session("TituloMsg") = titulo
		emprNome = ""
		emprIdJde = ""
		emprCnpj = ""
		perfId = ""
	else
		xSQL = "select emprNome, emprIdJde, emprCnpj , emprEnd, emprCep, emprTel, perfId from Empreiteira where emprId = " & emprId
		set xrs = gConexao.execute(xSQL)
		titulo = "Alterar contratado"
		session("Msg") = "Contratado alterado com sucesso!"
		Session("TituloMsg") = titulo
		emprNome = xrs("emprNome")
		emprIdJde = xrs("emprIdJde")
		emprCnpj = xrs("emprCnpj")
		emprEnd = xrs("emprEnd")
		emprCep = xrs("emprCep")
		emprTel = xrs("emprTel")
		perfId = xrs("perfId")
		xRs.CLose
		Set xRs = nothing
	end if
%>

	<form action="cfgEmprAlterar2.asp" method="post" name="form1">
		<input type="hidden" name="emprId" value=" <% = emprId %> " >
		<table width="100%">
			<tr>
				<td width="15"></td>
				<td></td>
			</tr>
			<tr>				
				<td colspan="2" class="docTit"><% = titulo %></td>
			</tr>
			<tr>
				<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
			</tr>
			<tr>
				<td colspan="2" height="15"></td>
			</tr>
			<tr>
				<td></td>
				<td>
					<table class="doc">
						<tr>
							<td width="100" class="docLabel">Id</td>
							<td width="5" class="docLabel">:</td>
							<td class="docCmpLivre"><input type="text" name="txtIdJde" class="docCmpLivre" value="<% = emprIdJde %>"></td>
						</tr>
						<tr>
							<td width="100" class="docLabel">Nome</td>
							<td width="5" class="docLabel">:</td>
							<td class="docCmpLivre"><input type="text" name="txtNome" class="docCmpLivre" value="<% = emprNome %>"></td>
						</tr>
						<tr>
							<td width="100" class="docLabel">CNPJ</td>
							<td width="5" class="docLabel">:</td>
							<td class="docCmpLivre"><input type="text" name="txtCnpj" class="docCmpLivre" value="<% = emprCnpj %>"></td>
						</tr>
						<tr>
							<td width="100" class="docLabel">Endere�o</td>
							<td width="5" class="docLabel">:</td>
							<td class="docCmpLivre"><textarea type="text" name="txtEnd" class="docCmpLivre" rows="3" wrap="soft"><% = emprEnd %></textarea></td>
						</tr>
						<tr>
							<td width="100" class="docLabel">CEP</td>
							<td width="5" class="docLabel">:</td>
							<td class="docCmpLivre"><input type="text" name="txtCep" class="docCmpLivre" value="<% = emprCep %>"></td>
						</tr>
						<tr>
							<td width="100" class="docLabel">Telefone</td>
							<td width="5" class="docLabel">:</td>
							<td class="docCmpLivre"><input type="text" name="txtTel" class="docCmpLivre" value="<% = emprTel %>"></td>
						</tr>
						<tr>
							<td class="docLabel">Perfil</td>
							<td class="docLabel">:</td>
							<td class="docSBorda">
								<select name="cmbPerfil" class="docCmpLivre">
	<%
	xSQL = "select perfId, perfNome"
	xSQL = xSQL & " from PerfilAcesso"
	xSQL = xSQL & "  order by perfNome"
	Set xRs = gConexao.Execute(xSQL)
	response.write vbtab & "<option value="""" class=""campo"" " & xSelected & ">(n�o informado)</option>"
	while not xRs.EOF
		if perfil <> "" then
			if xRs("perfId") = perfil then
				xSelected = " selected "
			else
				xSelected = ""
			end if
		end if
		response.write vbtab & "<option value=""" & xRs("perfId") & """ class=""campo"" " & xSelected & ">" & xRs("perfNome") & "</option>"
		xSelected = ""
		xRs.MoveNext
	wend
	xrs.close
	set xRs = nothing
	%>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2" height="20"></td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<table>
						<tr>
							<td height="28" width="45%" align="center"><a href="javascript:validar();"><img src="../images/btnSalvar.gif" class="botao"></a></td>
							<td width="10%"></td>
							<td width="45%" align="center"><a href="javascript:window.close();"><img src="../images/btnCancelar.gif" class="botao"></a></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
	</body>
</HTML>