<% 
    pStr = "private, no-cache, must-revalidate" 
    Response.ExpiresAbsolute = #2000-01-01# 
    Response.AddHeader "pragma", "no-cache" 
    Response.AddHeader "cache-control", pStr 

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<% 
		servidor = Request.ServerVariables("HTTP_HOST")
		if servidor = "www.prj.com.br" then
			'--- Instantiate the FileUpProgress object.
			Set oFileUpProgress = Server.CreateObject("SoftArtisans.FileUpProgress")
		 	intProgressID = oFileUpProgress.NextProgressID
		end if
	%>
	<HEAD>
		<title>::PRJ:: Controle remoto de projetos xx</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<script language="JavaScript" type="text/javascript" src="../scripts/PreparaJanela.js"></script>
		<SCRIPT language="JavaScript">
<%
		if servidor = "www.prj.com.br" then
%>
			function submitform(){
			  startupload();
			  document.form.submit();
			}
			function validar(){
			  if (document.form.txtArquivo.value == '')
			  {   alert('Informe a localização do arquivo.')}
			  else
			  {   submitform();}
			}
			function startupload() {
				winstyle="height=150,width=500,status=no,toolbar=no,menubar=no,location=no";
				window.open("progress.asp?progressid=<%=intProgressID%>",null,winstyle);
				document.form.action="prjPedcAlterar1.asp?progressid=<%=intProgressID%>";
			}
<%
		else
%>
			function submitform(){
			  document.form.submit();
			}
			function validar(){
			  if (document.form.txtArquivo.value == '')
			  {   alert('Informe a localização do arquivo.')}
			  else
			  {   submitform();}
			}
<%
		end if
%>

		</SCRIPT> 
	</HEAD>
	<body class="PopUpEntrada" onLoad="javascript:PreparaJanela(600,350);">
<%
	On error resume next
    lPerfisAutorizados = "123457"
	'VerificarAcesso
	'* Abrir conexao
	'*==========================================================
	Dim gConexao 
	AbrirConexao
	tituloPagina = "Carregar novo pedido"
	Session("TituloMsg") = tituloPagina
	projId = Session("projId")
%>
	<div style="padding-right:25px">
	<form action="prjPedcAlterar1.asp" method="post" name="form" encType="multipart/form-data" onSubmit="startupload();" >
		<input type="hidden" name="projId" value="<%=projId%>" >
		<input type="hidden" name="usuaId" value="<%=Session("usuaId")%>" >
		<table width="100%">
			<tr>
				<td width="15"></td>
				<td>&nbsp;</td>
			</tr>
			<tr>				
				<td colspan="2" class="docTit"><% = tituloPagina %></td>
			</tr>
			<tr>
				<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
			</tr>
			<tr>
				<td colspan="2" height="15"></td>
			</tr>
			<tr>
				<td></td>
				<td>
					<table class="doc">
						<tr>
							<td width="110">Projeto</td>
							<td width="5">:</td>
							<td class="docCmpLivre">(<% =Session("projNum") %>) <% =Session("projNome") %></td>
						</tr>
						<tr>
							<td>Arquivo</td>
							<td>:</td>
							<td class="docCmpLivre"><input type="file" name="txtArquivo" class="docCmpLivre" style="width:100%"></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2" height="20"></td>
			</tr>
		</table>
		<div id="comandos" style="text-align:center">
			<img src="../images/btnSalvar.gif"   onclick="javascript:validar();">
			<img src="../images/btnCancelar.gif" onclick="javascript:window.close();">
		</div>
	</form>
	</div>
	</body>
<%
if session("msgErro") <> "" then
%>
<script language="javascript" type="text/javascript">
	alert('<%=session("msgErro")%>');
</script>
<%
	session("msgErro") = ""
end if
%>
</HTML>