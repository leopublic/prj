<% Response.Expires= -1%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Controle remoto de projetos</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<script language="JavaScript" type="text/javascript" src="../scripts/PreparaJanela.js"></script>
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<script language="JavaScript" type="text/javascript" src="../scripts/CheckNumeric.js"></script>
		<SCRIPT language="JavaScript">
			function submitform(){
			  document.form1.submit();
			}
			function validar(){
			  if (document.form1.areaNome.value == '')
			  {   alert('O preenchimento do nome � obrigatorio.');
			  	  document.form1.areaNome.select();
				  document.form1.areaNome.focus(); }
  			  {   if (chkNumeric(document.form1.areaOrdem,0,9999999999,'','','')==false)
			      {   alert('A ordem deve ser um valor num�rico');
				      document.form1.areaOrdem.select();
					  document.form1.areaOrdem.focus();}
				  else
					  { submitform();}
			   }
			}
		</SCRIPT> 
	</HEAD>
	<body class="PopUpEntrada" onLoad="PreparaJanela(430,330);">
<% 
    lPerfisAutorizados = "51"
	VerificarAcesso
	Dim gConexao 
	AbrirConexao
	areaId = Request.Querystring("areaId")
	if areaId = 0 then
	    titulo = "Cadastrar uma nova �rea"
		Session("TituloMsg") = titulo
		Session("Msg") = "�rea adicionada com sucesso!"
		areaNome = ""
	else
		xSQL = "select * from AreaAnexo where areaId = " & areaId
		set xrs = gConexao.execute(xSQL)
		titulo = "Alterar �rea de anexo"
		Session("TituloMsg") = titulo
		Session("Msg") = "�rea alterada com sucesso!"
		areaNome = xrs("areaNome")
		areaOrdem = xrs("areaOrdem")
		areaIdPai = xrs("areaIdPai")
		xRs.CLose
		Set xRs = nothing
	end if
%>

	<form action="cfgAreaAlterar2.asp" method="post" name="form1">
		<input type="hidden" name="areaId" value="<% = areaId %>" >
		<table width="100%">
			<tr>
				<td width="15"></td>
				<td></td>
			</tr>
			<tr>				
				<td colspan="2" class="docTit"><% = titulo %></td>
			</tr>
			<tr>
				<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
			</tr>
			<tr>
				<td colspan="2" height="15"></td>
			</tr>
			<tr>
				<td></td>
				<td>
					<table class="doc">
						<tr>
							<td width="100" class="docLabel">Nome</td>
							<td width="5" class="docLabel">:</td>
							<td class="docCmpLivre"><input type="text" name="areaNome" class="docCmpLivre" value="<% = areaNome %>"></td>
						</tr>
						<tr>
							<td width="100" class="docLabel">Ordem</td>
							<td width="5" class="docLabel">:</td>
							<td class="docCmpLivre"><input type="text" name="areaOrdem" class="docCmpLivre" value="<% = areaOrdem %>"></td>
						</tr>
						<tr>
							<td width="100" class="docLabel">�rea pai</td>
							<td width="5" class="docLabel">:</td>
							<td class="docCmpLivre">
					<%
						xSQL = ""
						xSQL = xSQL & "select areaId "
						xSQL = xSQL & "     , areaNome "
						xSQL = xSQL & "  from AreaAnexo"
						xSQL = xSQL & " where areaIdPai = 0"
						xSQL = xSQL & " order by areaNome"
						set xRs = gConexao.execute(xsQL)
						xEmpr = "<select name=""areaIdPai"" class=""docCmpLivre"">" & vbcrlf
						' xEmpr = xEmpr & "<option value=""0"" class=""docCmpLivre"">(n�o definido)</option>" & vbcrlf
						while not xRs.eof
							xareaId = cstr(xRs("areaId"))
							if xareaId = cStr(areaIdPai) then
								xChecked = " selected "
							else
								xChecked = ""
							end if
							xEmpr = xEmpr & "<option value=""" & xRs("areaId") & """ " & xChecked & " class=""docCmpLivre"">" & xRs("areaNome") & "</option>" & vbcrlf
							xRs.Movenext
						wend
						xRs.Close
						Set xRs = nothing
						xEmpr = xEmpr & "</select>" & vbcrlf
						Response.write xEmpr
					%>
								</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2" height="20"></td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<table>
						<tr>
							<td height="28" width="45%" align="center"><a href="javascript:validar();"><img src="../images/btnSalvar.gif" class="botao"></a></td>
							<td width="10%"></td>
							<td width="45%" align="center"><a href="javascript:window.close();"><img src="../images/btnCancelar.gif" class="botao"></a></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
	</body>
</HTML>