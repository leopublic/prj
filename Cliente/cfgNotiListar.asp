<% Response.Expires= -1%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Controle remoto de projetos</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<link rel="stylesheet" type="text/css" href="../EstiloMenu.css">
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<script language="JavaScript" type="text/javascript" src="../scripts/AbrirJanela.js"></script>
	</HEAD>
	<body class="cabecalho" >
		<!--#include file="../includes/varControleAcesso.inc"-->
		<!--#include file="../includes/varBanco.inc"-->
<%  lPerfisAutorizados = "15"
	VerificarAcesso()

   gMenuSelecionado = "Notificacoes"
  ' gOpcaoSelecionada = "Usuarios"
   gmenuGeral = "Configuracao"
   PastaSelecionada = "Configuração"
%>
		<!--#include file="../includes/cab.inc"-->
			<tr>
				<td style="padding:0px" valign="top" class="blocoOpcoes">
					<!--#include file="../includes/opcConfig.inc"-->
				</td>
				<td colspan="2" valign="top" style="padding:5px" class="blocoPagina">
				<form>
					<table width="100%">
					<tr>
						<td class="docTit">CONTROLE DE NOTIFICAÇÕES</td>
						<td align="right"><a href="javascript:AbrirJanela('cfgNotiAlterar1.asp?notiId=0',440,270);"><img src="../images/newitem.gif" class="botao">&nbsp;Adicionar e-mail</a></td>
						<td align="right">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
					</tr>
					<tr>
						<td colspan="2" style="height:5"></td>
					</tr>
					<tr>
						<td colspan="2">
							<table class="grid">
<% 	xCampoOrdem = Request.QueryString("CampoOrdem") 
	if xCampoOrdem = "" then
		xCampoOrdem = "notiNome"
	end if
	SortNome = ""
	SortEmail = ""
	if xCampoOrdem = "notiNome" then SortNome = "<img src=""../images/sort.gif"">"
	if xCampoOrdem = "notiEmail" then SortEmail = "<img src=""../images/sort.gif"">"
%>
								<tr>
									<td width="20"  class="GridCab">&nbsp;</td>
									<td width="80"  class="GridCab"><a href="cfgNotiListar.asp?CampoOrdem=notiNome" class="GridCab">Nome</a><%=SortNome%></td>
									<td width="150"   class="GridCab"><a href="cfgNotiListar.asp?CampoOrdem=notiEmail" class="GridCab">E-mail</a><%=SortEmail%></td>
									<td class="GridCab">&nbsp;</td>
									<td class="GridCab">&nbsp;</td>
								</tr>
	<%
	xSQL = "select notiId, notiEmail, notiNome "
	xSQL = xSQL & " from Notificado"
	xSQL = xSQL & " order by " & xCampoOrdem
	AbrirConexao
	Set xRs = gConexao.Execute(xSQL)
	if xRs.EOF then
	    Response.write "<tr>" & vbcrlf
	    Response.write "<td colspan=""3"">Nenhum e-mail cadastrado</td>" & vbcrlf
	    Response.write "</tr>" & vbcrlf
	else
		estilo = "GridLinhaPar"
		while not xRs.EOF
			Response.write "<tr>" & vbcrlf
			response.write vbtab & "<td class=""" & estilo & """><img src=""../images/user_suit.gif"" style=""border:0""></td>"
			response.write vbtab & "<td class=""" & estilo & """>" & xRs("notiNome") & "</td>"
			response.write vbtab & "<td class=""" & estilo & """>" & xRs("notiEmail") & "</td>"
			response.write vbtab & "<td align=""right"" class=""" & estilo & """><a href=""javascript:AbrirJanela('cfgNotiExcluir1.asp?notiId=" & xRs("notiId") & "',600,250);"" class=""GridCab"" onmouseover=""javascript:document['excluir" & xRs("notiId")  & "'].src='../images/botaoXA.jpg';"" onmouseout=""javascript:document['excluir" & xRs("notiId") & "'].src='../images/botaoX.jpg';""><img name=""excluir" & xRs("notiId") & """ id=""excluir" & xRs("notiId") & """ src=""../images/botaoX.jpg""></a></td>"
			response.write vbtab & "<td class=""" & estilo & """>&nbsp;</td>"
			Response.write "</tr>" & vbcrlf
			xRs.MoveNext
			if estilo = "GridLinhaPar" then
				estilo = "GridLinhaImpar"
			else
				estilo = "GridLinhaPar"
			end if
		wend
	end if
	%>
								<tr>
									<td colspan="6" class="GridRodape">&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2" class="opcSubMenu">(<b>dica</b>: clique no título das colunas para mudar a ordenação da consulta.)</td>
					</tr>
					</table>
				</form>
				</td>
			</tr>
			
		<!--#include file="../includes/rod.inc"-->
	</body>
</HTML>