<% Response.Expires= -1%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Projetos</title><%%>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<script language="JavaScript" type="text/javascript" src="../scripts/PreparaJanela.js"></script>
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<!--#include file="../includes/clsProjeto.inc"-->
		<SCRIPT language="JavaScript">
			function submitform(){
			  document.form1.submit();
			}
			function validar(){
				submitform();
			}
		</SCRIPT> 
	</HEAD>
	<body class="PopUpEntrada" onLoad="javascript:PreparaJanela(500,500);">
<% 
    lPerfisAutorizados = "23451"
	VerificarAcesso
	Dim gConexao , gConexao2
	AbrirConexao
	AbrirConexao2
	projId = Request.Querystring("projID")
	titulo = "Arquivar projeto"
	Session("Msg") = "Projeto arquivado com sucesso!"
	set xrs = Projetos(projId)
	Session("TituloMsg") = titulo
	projNome = xRs("projNome")
	projNum = xrs("projNum")
	projNomeGer = xrs("projNomeGer")
	projNomeSup = xrs("projNomeSup")
	usuaNomeSup = xrs("projNomeSup")
	usuaNomeGer = xrs("projNomeGer")
	tpprNome = xrs("tpprNome")
	xRs.CLose
	Set xRs = nothing
	emprNome = SupervisorCampo(projId)
%>

	<form action="cfgProjArq2.asp" method="post" name="form1">
		<input type="hidden" name="projId" value="<% = projId %>" >
		<table width="100%">
			<tr>
				<td width="15"></td>
				<td></td>
			</tr>
			<tr>
				<td colspan="2" class="docTit"><% = titulo %></td>
			</tr>
			<tr>
				<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
			</tr>
			<tr>
				<td colspan="2" height="15"></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td align="left">
					<table class="doc">
						<tr>
							<td width="120" class="docLabel">N�mero</td>
							<td width="05" class="docLabel">:</td>
							<td class="docCmpLivre"><% = projNum %></td>
						</tr>
						<tr>
							<td class="docLabel">Nome</td>
							<td class="docLabel">:</td>
							<td class="docCmpLivre"><% = projNome %></td>
						</tr>
						<tr>
							<td class="docLabel">Gerente</td>
							<td class="docLabel">:</td>
							<td class="docCmpLivre"><%= usuaNomeGer%></td>
						</tr>
						<tr>
							<td class="docLabel">Supervisor</td>
							<td class="docLabel">:</td>
							<td class="docCmpLivre"><% = usuaNomeSup %></td>
						</tr>
						<tr>
							<td class="docLabel">Supervisor de campo</td>
							<td class="docLabel">:</td>
							<td class="docCmpLivre"><%= emprNome %></td>
						</tr>
						<tr>
							<td class="docLabel">Tipo de projeto</td>
							<td class="docLabel">:</td>
							<td class="docCmpLivre"><%= tpprNome %></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2" height="15"></td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<p align="center" style="font-weight:bold;color:red">Confirma o arquivamento do projeto acima?</p>
					<p align="center" style="font-weight:bold;color:red">ATEN��O: O arquivamento exclui todos os anexos e congela os dados do projeto.</p>
					<table>
						<tr>
							<td height="28" width="80" align="center"><img border="0" src="../images/btnArquivar.jpg" class="botao" onclick="javascript:validar();"></td>
							<td width="10"></td>
							<td width="80" align="center"><img border="0" src="../images/btnCancelar.gif" class="botao" onclick="javascript:window.close();"></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
	</body>
</HTML>