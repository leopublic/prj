<% Response.Expires= -1%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Controle remoto de projetos</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<script language="JavaScript" type="text/javascript" src="../scripts/PreparaJanela.js"></script>
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<script language="JavaScript" type="text/javascript" src="../scripts/CheckNumeric.js"></script>
		<SCRIPT language="JavaScript">
			function submitform(){
			  document.form1.submit();
			}
			function validar(){
			  if (document.form1.tpanDesc.value == '')
			  {   alert('O preenchimento do nome � obrigat�rio.');
			  	  document.form1.txtNome.select();
				  document.form1.txtNome.focus(); 
			  }
			  else{
				  if (document.form1.tpanDescTela.value == '')
				  {   alert('O preenchimento do t�tulo � obrigat�rio.');
				  	  document.form1.txtNome.select();
					  document.form1.txtNome.focus(); 
				  }
				  else{
				     if (chkNumeric(document.form1.tpanOrdem,0,9999999999,'','','')==false)
				     {   alert('A ordem deve ser um valor num�rico');
					      document.form1.txtOrd.select();
						  document.form1.txtOrd.focus();}
					 else{ 
					 	submitform();
					 }			  	
				  }
			  }
			}
		</SCRIPT> 
	</HEAD>
	<body class="PopUpEntrada" onLoad="PreparaJanela(430,400);">
<% 
    lPerfisAutorizados = "51"
	VerificarAcesso
	Dim gConexao 
	AbrirConexao
	tpanId = Request.Querystring("tpanId")
	if tpanId = 0 then
	    titulo = "Cadastrar novo tipo de anexo"
		Session("TituloMsg") = titulo
		Session("Msg") = "Tipo de anexo inclu�do com sucesso!"
		tpanDesc = ""
	else
		xSQL = "select * from TipoAnexo where tpanId = " & tpanId
		set xrs = gConexao.execute(xSQL)
		titulo = "Alterar servi�o"
		Session("TituloMsg") = titulo
		Session("Msg") = "Tipo de anexo alterado com sucesso!"
		areaId = xrs("areaId")
		tpanDesc = xrs("tpanDesc")
		tpanDescTela = xrs("tpanDescTela")
		tpanOrdem = xrs("tpanOrdem")
		xRs.Close
		Set xRs = nothing
	end if
%>

	<form action="cfgTipoAnexoAlterar2.asp" method="post" name="form1">
		<input type="hidden" name="tpanId" value=" <% = tpanId %> " >
		<table width="100%">
			<tr>
				<td width="15"></td>
				<td></td>
			</tr>
			<tr>				
				<td colspan="2" class="docTit"><% = titulo %></td>
			</tr>
			<tr>
				<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
			</tr>
			<tr>
				<td colspan="2" height="15"></td>
			</tr>
			<tr>
				<td></td>
				<td>
					<table class="doc">
						<tr>
							<td width="100" class="docLabel">Nome</td>
							<td width="5" class="docLabel">:</td>
							<td class="docCmpLivre"><input type="text" name="tpanDesc" class="docCmpLivre" value="<% = tpanDesc %>"></td>
						</tr>
						<tr>
							<td width="100" class="docLabel">T�tulo na tela</td>
							<td width="5" class="docLabel">:</td>
							<td class="docCmpLivre"><input type="text" name="tpanDescTela" class="docCmpLivre" value="<% = tpanDescTela %>"></td>
						</tr>
						<tr>
							<td width="100" class="docLabel">Ordem</td>
							<td width="5" class="docLabel">:</td>
							<td class="docCmpLivre"><input type="text" name="tpanOrdem" class="docCmpLivre" value="<% = tpanOrdem %>"></td>
						</tr>
						<tr>
							<td width="100" class="docLabel">�rea</td>
							<td width="5" class="docLabel">:</td>
							<td class="docCmpLivre">
					<%
						xSQL = ""
						xSQL = xSQL & "select a1.areaId "
						xSQL = xSQL & "     , a1.areaNome area, a2.areaNome grupo "
						xSQL = xSQL & "  from AreaAnexo a1 join AreaAnexo a2 on a2.areaId = a1.areaIdPai"
						xSQL = xSQL & " where a1.areaIdPai > 0"
						xSQL = xSQL & " order by a2.areaOrdem, a1.areaIdPai, a1.areaOrdem"
						set xRs = gConexao.execute(xsQL)
						xEmpr = "<select name=""areaId"" class=""docCmpLivre"">" & vbcrlf
						while not xRs.eof
							xareaId = cstr(xRs("areaId"))
							if xareaId = cStr(areaId) then
								xChecked = " selected "
							else
								xChecked = ""
							end if
							xEmpr = xEmpr & "<option value=""" & xRs("areaId") & """ " & xChecked & " class=""docCmpLivre"">" & xRs("grupo") & " - " & xRs("area") & "</option>" & vbcrlf
							xRs.Movenext
						wend
						xRs.Close
						Set xRs = nothing
						xEmpr = xEmpr & "</select>" & vbcrlf
						Response.write xEmpr
					%>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2" height="20"></td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<table>
						<tr>
							<td height="28" width="45%" align="center"><a href="javascript:validar();"><img src="../images/btnSalvar.gif" class="botao"></a></td>
							<td width="10%"></td>
							<td width="45%" align="center"><a href="javascript:window.close();"><img src="../images/btnCancelar.gif" class="botao"></a></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
	</body>
</HTML>