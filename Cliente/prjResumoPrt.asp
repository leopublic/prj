<% Response.Expires= -1%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Controle remoto de projetos</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<link rel="stylesheet" type="text/css" href="../EstiloMenu.css">
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/basFormatador.inc"-->
		<!--#include file="../includes/clsContrato.inc"-->
		<!--#include file="../includes/clsProjeto.inc"-->
		<script language="JavaScript" type="text/javascript" src="../scripts/AbrirJanela.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/NumberFormat.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/PreparaJanela.js"></script>
		<style type="text/css">
		<!--
		td.titulo1 {background-color:#1D4F68;color:#FFFFFF;text-align:center;padding:4pt;font-family:tahoma;font-size:10pt;font-weight:bold}
		td.titulo {color:red;text-align:left;padding-left:4pt;font-family:tahoma;font-size:10pt;font-weight:bold;letter-spacing:2px}
		td.servicos {text-align:left;font-family:tahoma;font-size:8pt;font-weight:bold;border:1;border-style:solid;padding:2;}
		td.servicosCustos {text-align:left;font-family:tahoma;font-size:8pt;font-weight:bold;border:1;border-style:solid;padding:1;}
		td.servicosTit{text-align:center;font-family:tahoma;font-size:10pt;font-weight:bold;border:1;border-style:solid;padding:2;}
		td.dataCustos{vertical-align:center;text-align:right;font-family:tahoma;font-size:7pt;font-weight:normal;border:1;border-style:solid;letter-spacing:-0.6px}
		td.separador{height:20pt;font-size:1pt}
		-->
		</style>
	</HEAD>
	<body class="cabecalho" onLoad="javascript:PreparaJanela(800,600);">
		<!--#include file="../includes/varBanco.inc"-->
		<!--#include file="../includes/varDatas.inc"-->
<%
   Dim gmenuGeral
   Dim gMenuSelecionado
   Dim gOpcaoSelecionada
   gmenuGeral = "Projetos"
   gMenuSelecionado = "PrjResumo"
   gOpcaoSelecionada = "PrjResumo"
   gItemSelecionado = "Resumo"

	AbrirConexao
	projId = Session("projId")
	if projId = "" then
		Response.Redirect("prjListarAtivos.asp")
	end if

	set xRS = gConexao.execute("exec PROJ_ListarProjeto @pprojId = " & projId & ", @pLiberado = 1, @prelpId = 0")
	projNome = xRs("projNome")
	projNum = xRs("projNum")
	emprNomeSupCmp = xRs("NomeSupCmp")
	projNomeSup = xRs("NomeSup")
	projNomeGer = xRs("NomeGer")
	projCustGerEst = xRs("CustGerEst")
	projCustSupcEst = xRs("CustSupcEst")
	GerReal = xRs("CustGerReal")
	SupcReal = xRs("CustSupcReal")
	Ecn = xRs("CustEcn")
	Etc = xRs("CustEtc")
	SemIni = xRs("SemIni")
	SemFim = xRs("SemFim")
	Semana = SemIni & " a " & SemFim
	ComeCust = xRs("ComeCust")
	ComePraz = xRs("ComePraz")
	ComeCusc = xRs("ComeCusc")
	ComeCUst = replace(ComeCust, vbcrlf, "<br>")
	ComePraz = replace(ComePraz, vbcrlf, "<br>")
	ComeCUsc = replace(ComeCusc, vbcrlf, "<br>")
	xRS.close
	Set xRs = nothing

	TotalEst = projCustGerEst + projCustSupcEst + Ecn
	TotalReal = GerReal + SupcReal + Etc
	if TotalEst = 0 then
		Variacao = "(n/a)"
	else
		if TotalReal = 0 then
			Variacao = "-"
		else
			Variacao = ((TotalEst - TotalReal) / TotalEst) * 100
			if Variacao < 0 then
				estiloVari = "style=""color:red"""
			end if
			Variacao = FormatNumber(Variacao,2,-1,-1) & "%"
		end if
	end if
'	response.write totalreal & " " & totalest
'	response.end()
	PastaSelecionada = "Projetos"
%>
		<table width="100%">
			<tr>
				<td colspan="2" valign="top" style="padding:5px" class="blocoPagina">
				<form>
					<table width="100%">
					<tr>
						<td colspan="2" class="docTit"><% = Session("projNome") %> - SITUA��O DO PROJETO: <%=Semana%></td>
					</tr>
					<tr>
						<td colspan="2" height="15"></td>
					</tr>
					<tr>
						<td colspan="2" class="docTit">Informa��es b�sicas</td>
					</tr>
					<tr>
						<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
					</tr>
					<tr>
						<td colspan="2" >
							<table class="doc" >
									<tr>
										<td width="80" class="docLabel">Projeto</td>
										<td width="05" class="docLabel">:</td>
										<td width="150" class="docCmpLivre" ><% = projNome %></td>
										<td width="100" class="docLabel" align="right">N�mero</td>
										<td width="05" class="docLabel">:</td>
										<td width="150"class="docCmpLivre"><% = projNum %></td>
									</tr>
									<tr>
										<td class="docLabel">Gerente</td>
										<td class="docLabel">:</td>
										<td class="docCmpLivre"><% = projNomeGer %></td>
										<td class="docLabel" align="right" >Sup. constru��es</td>
										<td class="docLabel">:</td>
										<td class="docCmpLivre"><% = projNomeSup %></td>
									</tr>
									<tr>
										<td class="docLabel">Sup. campo</td>
										<td class="docLabel">:</td>
										<td class="docCmpLivre"><% = emprNomeSupCmp %></td>
										<td class="docLabel">&nbsp;</td>
										<td class="docLabel">&nbsp;</td>
										<td class="docLabel" >&nbsp;</td>
									</tr>
							</table>
						</td>
					</tr>
					<tr><td colspan="2" class="separador">&nbsp;</td></tr>
					<tr>
						<td colspan="2" class="docTit">Custos de constru��es</td>
					</tr>
					<tr>
						<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
					</tr>
					<tr>
						<td colspan="2">
							<table class="doc">
								<tr>
									<td width="200pt" align="right" class="docLabel">Hh gerenciamento estimado:</td>
									<td width="80pt"  align="right"  class="docCmpLivre"><% = FormatNumber(projCustGerEst) %></td>
									<td>&nbsp;</td>
									<td width="200pt" align="right" class="docLabel">Hh gerenciamento comprometido:</td>
									<td width="80pt"  align="right" class="docCmpLivre"><% = FormatNumber(GerReal) %></td>
								</tr>
								<tr>
									<td align="right" class="docLabel">Hh sup. campo estimado:</td>
									<td align="right" class="docCmpLivre"><% = FormatNumber(projCustSupcEst) %></td>
									<td>&nbsp;</td>
									<td align="right" class="docLabel">Hh sup. campo comprometido:</td>
									<td align="right" class="docCmpLivre"><% = FormatNumber(SupcReal) %></td>
								</tr>
								<tr>
									<td align="right" class="docLabel">ECN:</td>
									<td align="right" class="docCmpLivre"><% = FormatNumber(Ecn) %></td>
									<td>&nbsp;</td>
									<td align="right" class="docLabel">ETC:</td>
									<td align="right" class="docCmpLivre"><% = FormatNumber(Etc) %></td>
								</tr>
								<tr>
									<td align="right" class="docLabel">Total hh estimado:</td>
									<td align="right" class="docCmpLivre"><% = FormatNumber(TotalEst) %></td>
									<td>&nbsp;</td>
									<td align="right" class="docLabel">Total hh previsto:</td>
									<td align="right" class="docCmpLivre"><% = FormatNumber(TotalReal) %></td>
								</tr>
								<tr>
									<td colspan="4" align="right" class="docLabel" style="align:right">Varia��o(%):</td>
									<td align="right" class="docCmpLivre" <%=estiloVari%>><% = Variacao %></td>
								</tr>
								<tr><td colspan="5">&nbsp;</td></tr>
								<tr><td colspan="5" class="docLabel">Coment�rios sobre as varia��es</td></tr>
								<tr><td colspan="5" class="docCmpLivre"><%=ComeCust%></td></tr>
							</table>
						</td>
					</tr>
					<tr><td colspan="2"  height="85" class="separador">&nbsp;</td></tr>
					<tr>
						<td colspan="2" class="docTit">Prazo de execu��o dos contratos</td>
					</tr>
					<tr>
						<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
					</tr>
					<tr>
						<td colspan="2">
								<% = MontarContratos(projId, "0", "1") %>
								  <tr>
									<td colspan="7" class="GridRodape">&nbsp;</td>
								  </tr>
								  <tr>
									<td colspan="7">&nbsp;</td>
								  </tr>
								  <tr>
									<td colspan="7" class="docLabel">Coment�rios sobre os
									  prazos dos contratos:</td>
								  </tr>
								  <tr>
									<td colspan="7" class="docCmpLivre"><%=ComePraz%></td>
								  </tr>
							</table>
						</td>
					</tr>
					<tr><td colspan="2" class="separador">&nbsp;</td></tr>
					<tr>
						<td colspan="2" class="docTit">Custo dos contratos</td>
					</tr>
					<tr>
						<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
					</tr>
					<tr>
						<td colspan="2">
							<table width="100%">
								<tr>
									<td></td>
									<td width="90"></td>
									<td width="90"></td>
									<td width="90"></td>
									<td width="90"></td>
									<td width="90"></td>
									<td width="90"></td>
									<td width="60"></td>
								</tr>
								<tr>
									<td class="GridCab" align="center">&nbsp;</td>
									<td class="GridCab" align="center">Valor<br>Estimado<br>(<%=Session("moedSigla")%>)</td>
									<td class="GridCab" align="center">Valor<br>Original<br>(<%=Session("moedSigla")%>)</td>
									<td class="GridCab" align="center">Valor<br>Atual<br>(<%=Session("moedSigla")%>)</td>
									<td class="GridCab" align="center">FCN<br>(<%=Session("moedSigla")%>)</td>
									<td class="GridCab" align="center">Pleitos<br>(<%=Session("moedSigla")%>)</td>
									<td class="GridCab" align="center">Valor<br>Final<br>(<%=Session("moedSigla")%>)</td>
									<td class="GridCab" align="center">Varia��o<br>(%)</td>
								</tr>
								<% = xCusto%>
								<tr>
									<td colspan="8" class="GridRodape">&nbsp;</td>
								</tr>
								<tr><td colspan="8">&nbsp;</td></tr>
								<tr><td colspan="8" class="docLabel">Coment�rios sobre os custos dos contratos:</td></tr>
								<tr><td colspan="8" class="docCmpLivre"><%=ComeCusc%></td></tr>
							</table>
						</td>
					</table>
				</form>
				</td>
			</tr>
		</table>
	</body>
</HTML>