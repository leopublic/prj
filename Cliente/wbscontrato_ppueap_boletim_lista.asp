<%
	Session.LCID = 1046 'Pt-Br'
	Response.Expires= 0
    Response.AddHeader "PRAGMA", "NO-CACHE"
    if Session("usuaId") = "" then
   		response.redirect("../login.asp")
    end if
    response.charset = "iso-8859-1"
%>
<!--#include file="../includes/wbscontrato_init.inc"-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Controle de WBS</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="JavaScript" type="text/javascript" src="tree/dtree.js"></script>
		<link rel="stylesheet" type="text/css" href="tree/dtree.css">

		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<!--#include file="../includes/funcoes.inc"-->

		<script language="JavaScript" type="text/javascript" src="../scripts/functions.js"></script>

		<script language="JavaScript" type="text/javascript" src="../scripts/AbrirJanela.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/NumberFormat.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/PreparaJanela.js"></script>

		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<link rel="stylesheet" type="text/css" href="../EstiloMenu.css">
        <link rel="stylesheet" type="text/css" href="../scripts/wbs_form.css">
        <link rel="stylesheet" type="text/css" href="../scripts/wbs_ppueap.css">

		<SCRIPT language="JavaScript" type="text/javascript">
            function _imprimirBM(pwbscId){
                alert('Em desenvolvimento!');
            }
		</SCRIPT>
		<style type='text/css'>
            tr.TOTAL td{
                background-color:#477;
                color:#FFF;
                font-weight:bold;
            }

            .gridWBS a{
                text-decoration:underline;
                font-weight:bold;
            }

		</style>
	</HEAD>
<body class="full" >
<!--#include file="../includes/varControleAcesso.inc"-->
<!--#include file="../includes/cab.inc"-->
	<tr>
		<td style="padding:0px" valign="top" class="blocoOpcoes">
			<!--#include file="../includes/opcWBS.inc"-->
		</td>
		<td colspan="2" valign="top" style="padding:5px" class="blocoPagina">
			<form action="" method="post" name="frmWBS" id="frmWBS">
				<div id="principal">
					<input type="hidden" id="curr_wbscId" name="curr_wbscId" value='<%=lwbscId%>' />
					<input type="hidden" id="curr_cntrId" name="curr_cntrId" value='<%=lcntrId%>' />
					<input type='hidden' id='curr_tipo' value='<%=request.querystring("tipo")%>'/>
					<input type="hidden" id="curr_projNome" name="curr_cntrId" value='<%=projNome%>' />
					<input type="hidden" id="curr_cntrDesc" name="curr_cntrId" value='<%=cntrDesc%>' />

					<div id='divTab'>
						<!--
						<div style='width:100%'>
							<%
							lalerta = "wbscontrato_ppueap_boletim.asp?cntrId=" & lcntrId & "&wbscId=" & lwbscId & "&servInd=" & lservInd & "&tipo=4&criarnovo=1"
							if lwbscFinalizada = "0" then
								lalerta = "javascript:alert(""O boletim de medi��o atual ainda n�o foi finalizado.\nN�o � poss�vel criar um novo boletim!!!"");"
							end if
							%>
							<a href='<%=lalerta%>' style='float:right'>Criar novo boletim</a><br />
						</div>
						-->
<%

	xTotFi = 0

	response.write "<table class='gridWBS'>" & vbcrlf
	response.write "<tr>" & vbcrlf
	response.write "<th style='width:100px'>N� Boletim</th>"  & vbcrlf
	response.write "<th style='width:70px'>Qtd. itens conclu�dos</th>"  & vbcrlf
	response.write "<th style='width:70px'>Qtd. itens verificados</th>"  & vbcrlf
	response.write "<th style='width:70px'>Valor total(" & Session("moedSigla") & ")<br/>conclu�do</th>"  & vbcrlf
	response.write "<th style='width:70px'>Valor total(" & Session("moedSigla") & ")<br/>verificado</th>"  & vbcrlf
	response.write "<th style='width:70px'>&nbsp;</th>"  & vbcrlf
	response.write "<th style='width:70px'>&nbsp;</th>" & vbcrlf
    response.write "</tr>"  & vbcrlf
	lTX_Sql3 = ""
	lTX_Sql3 = lTX_Sql3 & "SELECT w.wbscId "
	lTX_Sql3 = lTX_Sql3 & "     , isnull(w.wbscSequencial,1) as wbscSequencial "
	lTX_Sql3 = lTX_Sql3 & "     , isnull(w.wbscFinalizada,0) as wbscFinalizada "
	lTX_Sql3 = lTX_Sql3 & "     , sum(isnull(i.iwbcQtdConcForn,0))  as Tot_Itens_Concluidos  "
	lTX_Sql3 = lTX_Sql3 & "     , sum(isnull(i.iwbcQtdConcVerif,0)) as Tot_Itens_Verificados  "
	lTX_Sql3 = lTX_Sql3 & "     , sum(isnull(i.iwbcQtdConcForn,0) * isnull(i.iwbcValUnitReal,0)) as Vl_Total_C  "
	lTX_Sql3 = lTX_Sql3 & "     , sum(isnull(i.iwbcQtdConcVerif,0) * isnull(i.iwbcValUnitReal,0)) as Vl_Total_V  "
	lTX_Sql3 = lTX_Sql3 & "  FROM ItemWbsContrato       i "
	lTX_Sql3 = lTX_Sql3 & "     , WbsContrato           w "
	lTX_Sql3 = lTX_Sql3 & " WHERE i.wbscId = w.wbscId "
	lTX_Sql3 = lTX_Sql3 & "   AND w.cntrId = " & request.querystring("cntrId")
	lTX_Sql3 = lTX_Sql3 & "   AND coalesce(w.wbscStatusEAP,0) = 1 "
	lTX_Sql3 = lTX_Sql3 & " GROUP BY w.wbscId, w.wbscSequencial, w.wbscFinalizada "

	Set lRS_3 = gConexao.execute(lTX_Sql3)
	if(Not lRS_3.BOF) and (Not lRS_3.EOF)then
		li = "" & vbcrlf

		lordem = "impar"
		lni = 0
		lind_wbs = 0

		xTotIC = 0
		xTotIV = 0
		xVlBMC = 0
		xVlBMV = 0

		xTotIC_G = 0
		xTotIV_G = 0
		xVlBMC_G = 0
		xVlBMV_G = 0

		While Not lRS_3.EOF

			'lind_wbs = lind_wbs + 1
			lind_wbs = lRS_3("wbscSequencial")

			if lordem = "impar" then
				lordem = "par"
			else
				lordem = "impar"
			end if

			response.write "<tr class='" & lordem & "'>" & vbcrlf
			response.write "<td style='text-align:center'><a href='#' onclick='javascript:_imprimirBM(" & lRS_3("wbscId") & ");' title='Relat�rio de avan�o at� este boletim'>" & right("0000000000" & lind_wbs,10) & "</td>" & vbcrlf
			response.write "<td style='text-align:center'>" & FormataNumero(lRS_3("Tot_Itens_Concluidos"),2) & "</td>" & vbcrlf
			response.write "<td style='text-align:center'>" & FormataNumero(lRS_3("Tot_Itens_Verificados"),2) & "</td>" & vbcrlf
			response.write "<td style='text-align:center'>" & FormataNumero(lRS_3("Vl_Total_C"),2) & "</td>" & vbcrlf
			response.write "<td style='text-align:center'>" & FormataNumero(lRS_3("Vl_Total_V"),2) & "</td>" & vbcrlf

			if(lRS_3("wbscFinalizada") = 0) then
				xTotFi = 1
				response.write "<td style='text-align:center'><a href='wbscontrato_ppueap.asp?projId=" & lprojId & "&cntrId=" & lcntrId & "&wbscId=" & lRS_3("wbscId") & "&servInd=" & lservInd & "&tipo=4' title='Informar avan�o'>Atualizar</a></td>" & vbcrlf
				response.write "<td style='text-align:center'><a href='wbscontrato_ppueap.asp?projId=" & lprojId & "&cntrId=" & lcntrId & "&wbscId=" & lRS_3("wbscId") & "&servInd=" & lservInd & "&tipo=5' title='Avaliar avan�o informado'>Avaliar</a></td>" & vbcrlf
			else
				response.write "<td style='text-align:center'>--</td>" & vbcrlf
				response.write "<td style='text-align:center'>--</td>" & vbcrlf
			end if

			response.write "</tr>" & vbcrlf

			xTotIC = lRS_3("Tot_Itens_Concluidos")
			xTotIV = lRS_3("Tot_Itens_Verificados")
			xVlBMC = lRS_3("Vl_Total_C")
			xVlBMV = lRS_3("Vl_Total_V")

			xTotIC_G = xTotIC_G + xTotIC
			xTotIV_G = xTotIV_G + xTotIV
			xVlBMC_G = xVlBMC_G + xVlBMC
			xVlBMV_G = xVlBMV_G + xVlBMV

			lRS_3.MoveNext
		wend
	End If
	lRS_3.Close

    response.write "<tr class='TOTAL'>" & vbcrlf
    response.write "<td style='text-align:center'>Total Geral</td>" & vbcrlf
    response.write "<td style='text-align:center'>" & FormataNumero(xTotIC_G,2) & "</td>" & vbcrlf
    response.write "<td style='text-align:center'>" & FormataNumero(xTotIV_G,2) & "</td>" & vbcrlf
    response.write "<td style='text-align:center'>" & FormataNumero(xVlBMC_G,2) & "</td>" & vbcrlf
    response.write "<td style='text-align:center'>" & FormataNumero(xVlBMV_G,2) & "</td>" & vbcrlf
    response.write "<td style='text-align:center'>--</td>" & vbcrlf
    response.write "<td style='text-align:center'>--</td>" & vbcrlf

    response.write "</tr>" & vbcrlf

	response.write "</table>" & vbcrlf

	if xTotFi = 0 then
		response.write "<br /><div style='color:#F00; font-weight:bold; font-size:12px'>Wbs finalizada. N�o existem mais itens a serem verificados!!!</div>"
	end if
%>

					</div>
<!--
					<div class='divBotoes'>
						<input class='cpBt' type='button' value='Salvar' onclick='javascript:_salvarwbs(<%=request.querystring("tipo")%>);' onmouseover='javascript:_overbts(this);' onmouseout='javascript:_outbts(this);' <%=ldisabled%> />
						<input class='cpBt' type='button' value='Limpar' onclick='javascript:_limparwbs(<%=request.querystring("tipo")%>);' onmouseover='javascript:_overbts(this);' onmouseout='javascript:_outbts(this);' <%=ldisabled%> />
						 <input class='cpBt' type='button' value='TESTA' onclick='javascript:teste_tratarxmlretorno();' />
					</div>

					<div id='divErros' style='width:100%;height:0px;display:none;margin-top:5px;'>
					</div>
-->
				</div>
			</form>
		</td>
	</tr>
<!--#include file="../includes/rod.inc"-->
</script>
</body>
</HTML>