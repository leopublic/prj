<!--#include file="../includes/clsBanco.inc"-->
<!--#include file="../includes/clsControleAcesso.inc"-->
<!--#include file="../includes/varControleAcesso.inc"-->
<!--#include file="../includes/varBanco.inc"-->
<%
Dim debug 
Dim ximpoId
Function trimaAspas(campo)
	campo = left(campo, len(campo) - 1)
	campo = right(campo, len(campo) - 1)
	trimaAspas = campo
end function

Sub debuga (msg)
	'xSQLD = "exec PCAD_MensagemImportacao @pimpoId = " & ximpoId & ", @pmsgiTexto = '" & replace(msg, "'", "''") & "'"
	'gConexao.execute(xSQLD)
	
	if debug="on" then
		response.write "<br>" & msg
	end if
end sub

Function campoConvertido(campo) 
	valor = trim(replace(campo, ",", "."))
	campoConvertido = valor
end function

Dim objConn
Dim objRs
AbrirConexao
'== Obt�m arquivo a importar
debug = request.querystring("debug")
ximpoId = request.querystring("impoId")
xPath = Server.MapPath("..\listas")
xArquivo = xPath & "\" & ximpoId & ".txt"
'== Limpa mensagens da �ltima importacao
xSQL = "delete from MensagemImportacao where impoId =  " & ximpoId
gConexao.execute(xSQL)
'== Obtem c�digo da lista
xSQL = "select reviId from importacao where impoId = " & ximpoId
xrs = gConexao.execute(xSQL)
reviId = xrs("reviId")
xrs.close
set xrs = nothing
'== Inicia importa��o do arquivo
	Const ForReading = 1, ForWriting = 2, ForAppending = 3
	Const TristateUseDefault = -2, TristateTrue = -1, TristateFalse = 0
	' Create a filesystem object
	Dim FSO
	set FSO = server.createObject("Scripting.FileSystemObject")

	if FSO.FileExists(xArquivo) Then
	    ' Get a handle to the file
	    Dim file    
	    set file = FSO.GetFile(xArquivo)

	    ' Open the file
	    Dim TextStream
	    Set TextStream = file.OpenAsTextStream(ForReading, TristateUseDefault)

	    ' Read the file line by line
		regNum = 0
		erro = 0
	    if ( not TextStream.AtEndOfStream) then
	        reg = TextStream.readline
			regNum = regNum + 1
			' Pula primeira linha
	        reg = TextStream.readline
			'== Inicia a importa��o dos itens
			Do While (not TextStream.AtEndOfStream)
				'== Pula linhas em branco
				if not (trim(replace(reg, vbtab, "")) = "") then
					'
					' Obtem campos
					colunas = Split(reg, vbtab)
					requNumOrig = coluna(0)
					requNum     = coluna(1)
					fornNome = coluna(3)
					itemcodJde     = coluna(4)
					dtPromEntr = coluna(5)
					xData = split(dtPromEntr, "/")
					dtPromEntr = xdata(2) & "-" & xdata(1) & "-" & xdata(0)
					iteqQtd = campoConvertido(coluna(6))
					unidMed = coluna(7)
					requNumTag = coluna(8)

					debuga "<br>Item encontrado: " & ordem
					debuga "Ordem    -->" & ordem & "<--"
					debuga "Tag    -->" & tag & "<--"
					debuga "Linha   -->" & linha & "<--"
					debuga "codJDE    -->" & codJde & "<--"
					debuga "descricao -->" & descricao & "<--"
					
					xSQL = " exec PCAD_ItemRequisicao"
					xSQL = xSQL & "   @prequNum       = " & requNum
					xSQL = xSQL & "  ,@prequNumOrig   = " & requNumOrig
					xSQL = xSQL & "  ,@prequNumTag    = " & requNumTag
					xSQL = xSQL & "  ,@pitemCodJde    = " & itemCodJde
					xSQL = xSQL & "  ,@pfornNome      ='" & fornNome & "'"
					xSQL = xSQL & "  ,@piteqQtd       = " & iteqQtd
					xSQL = xSQL & "  ,@previId        = " & reviId
					debuga xSQL 
					set xrs = gConexao.execute(xSQL)
					If Err.Number <> 0 then
						debuga "<br/>SQL:" & xSQL
						debuga "<br/>ERRO: " & Err.Description
						Error.Clear
						response.end
					end if
					xrs.close
					set xrs = nothing
				end if
				'== L� proximo registro
				reg = TextStream.readline
				regNum = regNum + 1
			loop
	    
	    end if
		TextStream.close
	    Set TextStream = nothing
		Session("msg") = "Lista carregada com sucesso!"
		Set FSO = nothing
		if not debug="on" then
			response.redirect("gerMensagem.asp")
		end if
	Else
		Session("msg") = "Arquivo (" & xArquivo & ") n�o encontrado!"	    
		debuga "Arquivo (" & xArquivo & ") n�o encontrado!"
		Set FSO = nothing
		if not debug="on" then
			response.redirect("gerMensagem.asp")
		end if
	End If
%>