<%
	Response.Expires= 0
    Response.AddHeader "PRAGMA", "NO-CACHE" 
    if Session("usuaId") = "" then
   		response.redirect("../login.asp")
    end if
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<!--#include file="../includes/wbscontrato_init.inc"-->
		<title>::PRJ:: Controle de WBS</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="JavaScript" type="text/javascript" src="tree/dtree.js"></script>
		<link rel="stylesheet" type="text/css" href="tree/dtree.css">
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<script language="JavaScript" type="text/javascript" src="../scripts/moving_div_on_screen.js"></script>
        <script language="JavaScript" type="text/javascript" src="../scripts/block_background_objects.js"></script>

		<script language="JavaScript" type="text/javascript" src="../scripts/functions.js"></script>
 
		<script language="JavaScript" type="text/javascript" src="../scripts/AbrirJanela.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/NumberFormat.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/PreparaJanela.js"></script>

		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<link rel="stylesheet" type="text/css" href="../EstiloMenu.css">
        <link rel="stylesheet" type="text/css" href="../scripts/wbs_form.css">

		<SCRIPT language="JavaScript" type="text/javascript">
			function recriar()
			{
				d = null;
				document.getElementById("divDestino").innerHTML = '<span style="color:#396D60;font-family:Verdana, Tahoma, Arial;font-size:8pt;font-weight:normal;">Aguarde enquanto a estrutura da WBS � montada... <br/>(dependendo da quantidade de itens e subitens isso pode demorar alguns segundos)</span>';
				window.status = 'Montando...';
				//_carregaitensNovissimo('d', 'divLista', 'CADM', 'WBS Modelo', '', '', '', '');
                _carregaitensNovissimo('d', 'divDestino', 'IWBC', 'PROJETO', '<%=lwbscId%>', '<%=lcntrId%>', '<%=lservInd%>', '<%=ltipo%>', '<%=request.querystring("fcn_Id")%>');
			}
			function setadestino(piwbcId_Pai){
				document.getElementById("curr_iwbcId_Pai").value=piwbcId_Pai;
                //alert(document.getElementById("curr_iwbcId_Pai").value);
			}

            function maozero(){
				document.getElementById("imgrootdest").style.cursor='pointer';
			}

			function setadestinozero(){
				document.getElementById("curr_iwbcId_Pai").value='';
			}
            
            function AddItemWbs(piwbcId){
                if('<%=lwbscStatusPPU%>'=='1' && '<%=request.querystring("fcn_Id")%>'==''){
                    alert('A PPU deste contrato j� foi fechada!\nPara inclus�o de novos itens, favor utilizar FCN.');
                }else{
                	document.body.style.cursor='wait';
                    var pagina = "wbscontrato_bd.asp?iwbcId="+piwbcId+"&tipo=-INC&iwbcId_Pai="+document.getElementById("curr_iwbcId_Pai").value+"&wbscId="+document.getElementById("curr_wbscId").value+"&fcn_Id="+document.getElementById("curr_fcn_Id").value;
                	var xmlhttp = _criaxmlhttprequest();
                	xmlhttp.open("GET",pagina,true);
                	xmlhttp.onreadystatechange = function() {
                		if(xmlhttp.readyState == 4) {
                			if(xmlhttp.status == 200){
                				if(_tratarxmlretorno(xmlhttp.responseText)){
                					alert('Item inserido c/ sucesso!');
                					recriar();
                				}else{
                					alert('Falha ao inserir o item!');
                				}
                			}
                		}
                	}

                	xmlhttp.send(null);
                	document.body.style.cursor='auto';
                }
            }

            function ExcluirItem(piwbcId){
                if('<%=lwbscStatusPPU%>'=='1' && '<%=request.querystring("fcn_Id")%>'==''){
                    alert('A PPU deste contrato j� foi fechada!\nPara remo��o de itens, favor utilizar FCN.');
                }else{
                	if(confirm("Tem certeza que deseja excluir este item e todos os demais itens relacionados a ele?")==false){return -1}

                	document.body.style.cursor='wait';
                	var pagina = "wbscontrato_bd.asp?iwbcId="+piwbcId+'&tipo=-DEL';
                	//
                	var xmlhttp = _criaxmlhttprequest();

                	xmlhttp.open("GET",pagina,true);
                	xmlhttp.onreadystatechange = function() {
                		if(xmlhttp.readyState == 4) {
                			if(xmlhttp.status == 200){
                            //'-- alert(xmlhttp.responseText);
                				if(_tratarxmlretorno(xmlhttp.responseText)){
                					alert('Item removido c/ sucesso!');
                					recriar();
                				}else{
                					alert('Falha ao remover o item!');
                				}
                			}
                		}
                	}

                	xmlhttp.send(null);
                	document.body.style.cursor='auto';
                	//window.location.href=window.location.href;
                }
            }
			
			function AbrirDetalheItem(_itemId, _itemNome, _itemComplemento){
				document.getElementById("tituloPopUp").innerHTML = 'Complementar descri��o do item';	
				_ocdiv('divItem'); //Exibindo div

				//Preenchendo campos do div
				//Chaves
				document.getElementById("_itemId").value = _itemId;
				//Descri��es
				document.getElementById("_itemNome").value = _itemNome;
                document.getElementById("_itemComplemento").value = _itemComplemento;
			}

			function SalvaItem(){
                //alert ('entrou SalvaItem');
                //return false
				//Chaves
				var _itemId = document.getElementById("_itemId").value;
				//Descri��es
                var _itemComplemento = document.getElementById("_itemComplemento").value;
				//Validando campos
                _itemComplemento = _itemComplemento.replace("&", "|AMP|")
                _ocdiv('divItem'); //Fechando div
                
				//Atualizando item
				document.body.style.cursor='wait';
				var pagina = 'wbsContrato_atualiza_item.asp?itemId='+_itemId+'&itemComplemento='+_itemComplemento;
                
                //document.getElementById('divErros').innerHTML = 'wbslistar_atualiza_item.asp?itemId='+_itemId+'&itemId_Pai='+_itemId_Pai+'&tiwbId='+_tiwbId+'&unimId='+_unimId+'&itemCodigo='+_itemCodigo+'&itemNome='+_itemNome+'&itemPercPai='+_itemPercPai+'&itemFatorHH='+_itemFatorHH+'&itemValUnitEst='+_itemValUnitEst+'&itemFLFilho='+_itemFLFilho+'&itemFLFCN='+_itemFLFCN;
                //document.getElementById('divErros').style.display='block';
                //return true;
				//alert(pagina);
                    
                var xmlhttp = _criaxmlhttprequest();

				xmlhttp.open("GET",pagina,true);
				xmlhttp.onreadystatechange = function() {
					if(xmlhttp.readyState == 4) {
						if(xmlhttp.status == 200){
                            _ocdiv('over');
							if(_tratarxmlretorno(xmlhttp.responseText)){
                                alert('Descri��o atualizada com sucesso!');
                                if (document.getElementById("atualiza").checked)
								{	recriar();}
							}else{
								alert('Falha ao atualizar o item!');
								return false
							}
						}
					}
				}
				xmlhttp.send(null);
				document.body.style.cursor='auto';
			}
			
		</SCRIPT>
		<style type='text/css'>
			.divtree div {
				font-size: 7pt;
				font-family: Verdana, Tahoma, arial;
				color: #467;
			}

			.divtree a{
				font-size: 7pt;
				font-family: Verdana, Tahoma, arial;
				color: #467;
			}
		</style>
	</HEAD>
<body id="sandbox" class="full">
<div id="over" style="display:none;"></div>
<!--#include file="../includes/varControleAcesso.inc"-->
<!--#include file="../includes/cab.inc"-->
	<tr>
		<td style="padding:0px" valign="top" class="blocoOpcoes">
			<!--#include file="../includes/opcWBS.inc"-->
		</td>
		<td colspan="2" valign="top" style="padding:5px" class="blocoPagina">
			<table style="width:100%">
				<tr>
					<td colspan="2" class="docTit"><%=session("projNome")%> - <%=cntrNomeServ%><br/>WBS do Contrato</td>
				</tr>
				<tr>
					<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
				</tr>
			</table>
			<form action="" method="post" name="frmDetalhe" id="frmDetalhe">
				<div id="divItem" name="divItem" style="display:none; position:absolute; z-index:1000;" class="divwindow">
					<div class='div_titulo' id='divItem_TIT' name='divItem_TIT' onmousedown="javascript:dragStart(event, 'divItem');">WBS - <%=Session("projNome")%></div>
					<div class='sep'></div>
					<input id='_itemId' type='hidden'>
					<input id='_itemFLFilho' type='hidden'>
					<table style="width:97%">
						<tr>				
							<td class="docTit" id="tituloPopUp"></td>
						</tr>
						<tr>
							<td background="../images/pont_cinza_h.gif" style="height:2"></td>
						</tr>					
					</table>
					<table class="entradadados">
						<tr>
							<th>Item</th>
							<th>:</th>
							<td><input id='_itemNome' type='text' class='cpTexto' maxlength='250' readonly='readonly' style='border:none'></td>
						</tr>
                        <tr>
							<th>Complemento</th>
							<th>:</th>
							<td><textarea id="_itemComplemento" class="cpTexto" maxlength="100" Rows="2" style="color:#396D60;"></textarea></td>
						</tr>
					</table>

					<div class="barrabotoes">
						<input type="button" onclick="javascript:SalvaItem();" value="Salvar" title="Salva dados do item em quest�o">
						<input type="button" onclick="javascript:_ocdiv('divItem');" value="Fechar" title="Fecha o cadastro de itens">
					</div>
				</div>
				<input type='hidden' id='counter' name='counter'>
			</form>
			<form action="" method="post" name="frmWBS" id="frmWBS">
				<div id="principal" style='width:99%'>
					<input type="hidden" id="curr_wbscId" name="curr_wbscId" value='<%=lwbscId%>' />
					<input type="hidden" id="curr_cntrId" name="curr_cntrId" value='<%=lcntrId%>' />
                    <input type="hidden" id="curr_fcn_Id" name="curr_fcn_Id" value='<%=Request.QueryString("fcn_Id")%>' />
					<input type="hidden" id="curr_projNome" name="curr_projNome" value='<%=projNome%>' />
					<input type="hidden" id="curr_cntrDesc" name="curr_cntrDesc" value='<%=cntrDesc%>' />
					<input type="hidden" id="curr_iwbcId_Pai" name="curr_iwbcId_Pai" value='' />
					<input type="hidden" id="n_start" name="n_start" value='<%=lStart%>' />
					<div class='divtree' id='xxx' style='width:49%; height:600px; float:right; overflow:auto;'>
						<input type="button" onclick="javascript:recriar();" value="Recarregar �rvore"/>
						<div class='divtree' id='divDestino' style='width:100%;'>
	                        <script language="JavaScript">
	                            //_carregaitensNovissimo('d', 'divDestino', 'IWBC', 'PROJETO', '<%=lwbscId%>', '<%=lcntrId%>', '<%=lservInd%>', '<%=ltipo%>', '<%=request.querystring("fcn_Id")%>');
								recriar();
	                        </script>
						</div>
					</div>
					<div class='divtree' id='xxx' style='width:49%; height:600px; float:right; overflow:auto;'>
						<div class='divtree' id='divOrigem' style='width:100%; text-shadow: color'>
	                        <script language="JavaScript">
	                            _carregaitensNovissimo('o', 'divOrigem', 'CWBS', 'WBS Modelo', '', '', '', '', '');
	                        </script>
						</div>
					</div>
					<div id='divErros' style='width:100%;height:0px;display:none;margin-top:5px;'></div>
				</div>
			</form>
		</td>
	</tr>
<!--#include file="../includes/rod.inc"-->
</script>
</body>
</HTML>