<% Response.Expires= -1%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Projetos</title><%%>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<!--#include file="../includes/basFormatador.inc"-->
		<!--#include file="../includes/basValidacao.inc"-->
		<!--#include file="../includes/clsContrato.inc"-->
		<script language="JavaScript" type="text/javascript" src="../scripts/NumberFormat.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/Mid.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/ValidacaoDatas.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/PreparaJanela.js"></script>
		<SCRIPT language="JavaScript">
			function CamposOk(){
				var QtdCntr = document.form.QtdServicos.value
			    for(i=1;i<=QtdCntr;i++)
				{
					for(j=1;j<=4;j++)
					{   eval('var Cmp = document.form.TermEst'+j+i)
					    if(Cmp.value!='')
						{	if(isDate(Cmp)==false)
 							{	Cmp.select();
								Cmp.focus();
								return false;
							}
						}
					}
				}
				return true
			}
			function Formatar(campo, i){
				//var inputValue = document.getElementById(campo+'Fmt');
				var inputValue = campo
				var nome = campo.id
				var nomeCampoVal = nome.substring(0,nome.length - 1)+"Val"+i
				//alert(nomeCampoVal)
				var inputHidden = document.getElementById(nomeCampoVal);
				inputValue.value = FormatarNumero(inputValue.value, '.', ',', ',');
				inputHidden.value = FormatarNumero(inputValue.value, '', ',', '.');
				eval("var TotalAtu = Math.round(parseFloat(document.form.VlrAtuVal"+i+".value)*100)")
				eval("var TotalOrig = Math.round(parseFloat(document.form.VlrOrigVal"+i+".value)*100)")
				var Total
				if (TotalAtu==0)
				{   Total = TotalOrig}
				else
				{   Total = TotalAtu}
				//alert(Total)
				eval("var Total = Total + Math.round(parseFloat(document.form.VlrFcneVal"+i+".value)*100)")
				//alert(Total)
				eval("var Total = Total + Math.round(parseFloat(document.form.VlrPleiVal"+i+".value)*100)")
				//alert(Total)
				var TotalEst = 0
				eval("TotalEst = parseFloat(document.form.VlrOrigVal"+i+".value)")
				//alert('TotalEst = '+TotalEst)
				Total = Total / 100
				//alert('Total = '+Total)
				var cor='#1D4F68'
				if ((TotalEst==0))
				{   eval("document.form.Variacao"+i+".value='(n/a)';")}
				else
				{	if ((Total!=0))
					{	var variacao = TotalEst	 - Total
						variacao = Math.round((variacao/ TotalEst) * 10000)
						variacao = variacao / 100
						if (variacao<0) {cor='red';}
						variacao = variacao + '';
						variacao = FormatarNumero(variacao, '.', '.', ',');
						variacao = variacao + '%'
						eval("document.form.Variacao"+i+".value = variacao;")
					}
					else
					{	eval("document.form.Variacao"+i+".value='-';")}
				}
				eval("document.form.Variacao"+i+".style.color='"+cor+"'")
				Total = Total + '';
				eval("document.form.VlrTot"+i+".value = FormatarNumero(Total, '.', '.', ',');")
				//
				// Se for informado o valor original e o valor atual estiver zerado, colocar o valor original no valor atual
				var nomeRed = nome.substring(0, 7)
				if(nomeRed=='VlrOrig')
				{
					if ((document.getElementById('VlrAtuVal'+i).value==0)||document.getElementById('VlrAtuVal'+i).value=='')
					{
						document.getElementById('VlrAtu'+i).value = document.getElementById('VlrOrig'+i).value
						document.getElementById('VlrAtuVal'+i).value = document.getElementById('VlrOrigVal'+i).value
					}
				}

			}
			function FormatarPuro(campo){
				var inputValue = campo;
				inputValue.value = FormatarNumero(inputValue.value, '.', ',', ',');
			}

			function Validar(){
//                if (CamposOk()==true)
			    { document.form.submit();}
			}
		</SCRIPT>
	</HEAD>
	<body class="PopUpEntrada" onLoad="javascript:PreparaJanela(680, 290 + (document.form.QtdServicos.value) * 40);">
		<!--#include file="../includes/varControleAcesso.inc"-->
		<!--#include file="../includes/varBanco.inc"-->
<%
    lPerfisAutorizados = "514"
	VerificarAcesso
	AbrirConexao
	projId = Request.Querystring("projId")
	xSQL = ""
	xSQL = xSQL & "select isnull(projComeCusC, '&nbsp') ComeCusC from Projeto where projId = " & projId
	set xrs = gConexao.execute(xSQL)
	ComeCusc = xRs("ComeCusC")
	xRs.CLose
	Set xRs = nothing
%>

	<form onSubmit="Validar();" action="prjRelcCust2.asp" method="post" name="form">
		<input type="hidden" name="usuaId" value="<% = Session("usuaId") %>" >
		<input type="hidden" name="projId"   value="<% = projId %>" >
		<table width="100%">
			<tr>
				<td width="15"></td>
				<td></td>
			</tr>
			<tr>
				<td colspan="2" class="docTit"><%= Session("projNome")%> - Atualizar custos dos contratos</td>
			</tr>
			<tr>
				<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
			</tr>
			<tr>
				<td colspan="2" height="15"></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td align="left">
							<table width="100%" class="grid">
								<tr>
									<td></td>
									<td width="70"></td>
									<td width="70"></td>
									<td width="70"></td>
									<td width="70"></td>
									<td width="70"></td>
									<td width="70"></td>
								</tr>
								<tr>
									<td class="GridCab" align="Right">&nbsp;</td>
									<td class="GridCab" align="right">Valor<br>Estimado<br>(<%=Session("moedSigla")%>)</td>
									<td class="GridCab" align="right">Valor<br>Original<br>(<%=Session("moedSigla")%>)</td>
									<td class="GridCab" align="right">Valor<br>Atual<br>(<%=Session("moedSigla")%>)</td>
									<td class="GridCab" align="right">FCN<br>(<%=Session("moedSigla")%>)</td>
									<td class="GridCab" align="right">Pleitos<br>(<%=Session("moedSigla")%>)</td>
									<td class="GridCab" align="right">Valor<br>Final<br>(<%=Session("moedSigla")%>)</td>
									<td class="GridCab" align="right">Varia��o<br>Contratual<br>(%)</td>
								</tr>
<%
	set xRs = gConexao.execute("exec PROJ_ListarContratos @pprojId = " & projId & ", @pLiberado = 0, @prelpId=0")
	estilo = "GridLinhaPar"
	i = 0
	estiloClass = "docCmpLivrePar"
	while not xRs.eof
		estiloVari = ""
		if xRs("VlrAtu") = 0 then
			xVlrTot = xRs("VlrTotOrig")
		else
			xVlrTot = xRs("VlrTotAtu")
		end if
		if  xRs("VlrOrig") = 0 then
			cntrVari = "(n/a)"
		else
			if xVlrTot <> 0 then
				cntrVari = ((xRs("VlrOrig") - xVlrTot) / xRs("VlrOrig")) * 100
				if cntrVari < 0 then
					estiloVari = ";color:red"
				else
					estiloVari = ""
				end if
				if isnull(cntrVari) then
					cntrVari = "-"
				else
					cntrVari = FormatNumber(cntrVari,2,-1,-1)
					cntrVari = cntrVari & "%"
				end if
			else
				cntrVari = "-"
			end if
		end if
		estiloCmp = "class=""" & estiloClass & """ style=""height:20;border:1;border-style:solid;border-color:#CCCCCC;text-align:right"" "
		estiloCmpS = "class=""" & estiloClass & """ style=""height:20;border:0;text-align:right"" "
		estiloCmpV = "class=""" & estiloClass & """ style=""height:20;border:0;text-align:right" & estiloVari & """ "
		i = i + 1
		Response.write "<tr>" & vbcrlf
		Response.write "<input type=""hidden"" id=""cntrId"& cStr(i) & """ name=""cntrId"& cStr(i) & """ value=""" & xRs("cntrId") & """>" & vbcrlf
		Response.write vbtab & "<td class=""GridCabLinha"" height=""33"">" & xRs("servNome") & "</td>" & vbcrlf
		Response.write vbtab & "<td class=""" & estilo & """ align=""right"">" & FormatNumber(xRs("VlrEst")) & "<input type=""hidden"" id=""VlrEstVal" & cStr(i) & """ name=""VlrEstVal" & cStr(i) & """ value=""" & NumFmtBd(xRs("VlrEst")) & """ tabindex=""-1""></td>" & vbcrlf
		Response.write vbtab & "<td class=""" & estilo & """ align=""right""><input type=""text"" id=""VlrOrig" & cStr(i) & """ name=""VlrOrig" & cStr(i) & """ value=""" & FormatNumber(xRs("VlrOrig")) & """ " & estiloCmp & "onblur=""javascript:Formatar(this," & cStr(i) & ");""><input type=""hidden"" id=""VlrOrigVal" & cStr(i) & """ value=""" & NumFmtBd(xRs("VlrOrig")) & """></td>" & vbcrlf
		Response.write vbtab & "<td class=""" & estilo & """ align=""right""><input type=""text"" id=""VlrAtu" & cStr(i) & """ name=""VlrAtu" & cStr(i) & """ value=""" & FormatNumber(xRs("VlrAtu")) & """ " & estiloCmp & "onblur=""javascript:Formatar(this," & cStr(i) & ");""><input type=""hidden"" id=""VlrAtuVal" & cStr(i) & """ value=""" & NumFmtBd(xRs("VlrAtu")) & """></td>" & vbcrlf
		Response.write vbtab & "<td class=""" & estilo & """ align=""right""><input type=""text"" id=""VlrFcne" & cStr(i) & """ name=""VlrFcne" & cStr(i) & """ value=""" & FormatNumber(xRs("VlrFcn")) & """ " & estiloCmp & "onblur=""javascript:Formatar(this," & cStr(i) & ");""><input type=""hidden"" id=""VlrFcneVal" & cStr(i) & """ value=""" & NumFmtBd(xRs("VlrFcn")) & """></td>" & vbcrlf
		Response.write vbtab & "<td class=""" & estilo & """ align=""right""><input type=""text"" id=""VlrPlei" & cStr(i) & """ name=""VlrPlei" & cStr(i) & """ value=""" & FormatNumber(xRs("VlrPlei")) & """ " & estiloCmp & "onblur=""javascript:Formatar(this," & cStr(i) & ");""><input type=""hidden"" id=""VlrPleiVal" & cStr(i) & """ value=""" & NumFmtBd(xRs("VlrPlei")) & """></td>" & vbcrlf
		Response.write vbtab & "<td class=""" & estilo & """ align=""right""><input type=""text"" readonly=""readonly"" id=""VlrTot" & cStr(i) & """ value=""" & FormatNumber(xVlrTot) & """ " & estiloCmpS & " tabindex=""-1""></td>" & vbcrlf
		Response.write vbtab & "<td class=""" & estilo & """ align=""right""><input type=""text"" readonly=""readonly"" id=""Variacao" & cStr(i) & """ value=""" & cntrVari & """ " & estiloCmpV & " tabindex=""-1""></td>" & vbcrlf
		Response.write "</tr>" & vbcrlf
		xRs.MoveNext
		if estilo = "GridLinhaPar" then
			estilo = "GridLinhaImpar"
			estiloClass = "docCmplivreImpar"
		else
			estilo = "GridLinhaPar"
			estiloClass = "docCmplivrePar"
		end if
	wEnd
	Session("QtdServicos") = i
%>
								<tr>
									<td colspan="8" class="GridRodape">&nbsp;</td>
								</tr>
							</table>
				</td>
				<input type="hidden" name="QtdServicos" value="<% = i %>">
			</tr>
			<tr>
				<td colspan="2" class="docLabel">Coment�rios sobre os custos dos contratos:</td>
			</tr>
			<tr>
				<td colspan="2" class="docCmpLivre"><textarea name="txtComeCusC" class="docCmpLivre" rows="5" wrap="soft"><%=ComeCusc%></textarea></td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<table>
						<tr>
							<td height="28" width="80" align="center"><img border="0" src="../images/btnSalvar.gif" class="botao" onclick="javascript:Validar();"></td>
							<td width="10"></td>
							<td width="80" align="center"><img border="0" src="../images/btnCancelar.gif" class="botao" onclick="javascript:window.close();"></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
	</body>
</HTML>