<% Response.Expires= -1%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ::</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<link rel="stylesheet" type="text/css" href="../EstiloMenu.css">
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<script language="JavaScript" type="text/javascript" src="../scripts/AbrirJanela.js"></script>
	</HEAD>
	<body class="cabecalho" >
<%  lPerfisAutorizados = "157"
	VerificarAcesso()
	projId = Session("projId")

	Dim gmenuGeral
	Dim gMenuSelecionado
	Dim gOpcaoSelecionada
	cntrNomeServArray = Session("cntrNomeServArray")
	cntrIdArray = Session("cntrIdArray")
	emprNomeArray = Session("emprNomeArray")
	emprIdArray = Session("emprIdArray")
	TotalServ = Session("QtdServicos")
	servInd = 0
	area = request.querystring("area")
    gmenuGeral = "Projetos"
    gMenuSelecionado = "prjNaoConformidade"
    gOpcaoSelecionada = area

    Dim gConexao
	AbrirConexao

    icone = request.querystring("icone")

 %>
		<!--#include file="../includes/cab.inc"-->
			<tr>
				<td style="padding:0px" valign="top" class="blocoOpcoes">
					<!--#include file="../includes/opcProjx2.inc"-->
				</td>
				<td colspan="2" valign="top" style="padding:5px" class="blocoPagina">
				<form>
					<table width="100%">
					<tr>
						<td class="docTit"><%=Session("projNome")%><br><img src="../images/0066_double_arrow.png" class="botao" style="margin-right:3px">N�o-Conformidades</td>
					</tr>
					<tr>
						<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
					</tr>
					<tr>
						<td colspan="2" style="height:5"></td>
					</tr>
					<tr>
						<td colspan="2">
							<table width="100%" class="grid" >
								<tr>
<%

	xCampoOrdem = Request.QueryString("CampoOrdem")
	if xCampoOrdem = "" then
		xCampoOrdem = Session("CampoOrdemNaoConformidade")
		if xCampoOrdem = "" then
			xCampoOrdem = "nconNumero"
		end if
	end if
	Session("CampoOrdemNaoConformidade") = xCampoOrdem

	if xCampoOrdem = "n.nconNumero" then SortNumero = "<img src=""../images/sort.gif"">"
	if xCampoOrdem = "j.itens" then SortItens = "<img src=""../images/sort.gif"">"
%>
									<td width="100px" class="GridCab" >Resumo</td>
									<td width="250px" class="GridCab"><a href="prjNconListar.asp?CampoOrdem=n.nconNumero" class="GridCab" title="Clique para ordenar por essa coluna">N�mero</a><%=SortNumero%></td>
									<td width="250px" class="GridCab"><a href="prjNconListar.asp?CampoOrdem=j.itens" class="GridCab" title="Clique para ordenar por essa coluna">Itens</a><%=SortItens%></td>
									<td class="GridSLinha" >&nbsp;</td>
								</tr>
	<%


	xSQL = ""
	xSQL = xSQL & "select n.nconId, n.nconNumero, isnull(j.itens,0) itens "
	xSQL = xSQL & "  from NaoConformidade n "
	xSQL = xSQL & "     , (select count(distinct ir.itemId) itens, r.nconId from ItemRecebimento ir, Recebimento r where ir.receId = r.receId and r.nconId is not null group by r.nconId) j "
	xSQL = xSQL & " where n.nconId *= j.nconId "
	xSQL = xSQL & " and n.projId = " & projId
	xSQL = xSQL & " order by " & xCampoOrdem
	'response.write xSQL
	Set xRs = gConexao.Execute(xSQL)
	if xRs.EOF then
	    Response.write "<tr>" & vbcrlf
	    Response.write 		"<td colspan=""6"">Nenhuma n�o-conformidade cadastrada</td>" & vbcrlf
	    Response.write "</tr>" & vbcrlf
	else
		estilo = "GridLinhaPar"
		xlistIdAnt = ""
		while not xRs.EOF
			if estilo = "GridLinhaPar" then
				estilo = "GridLinhaImpar"
			else
				estilo = "GridLinhaPar"
			end if
			Response.write "<tr>" & vbcrlf
			response.write 		"<td class=""" & estilo & """><a href=""prjNconResumo.asp?nconId=" & xrs("nconId") & """><img src=""../images/folder.gif"" style=""border:0"" title=""Consultar resumo da n�o-conformidade""></a></td>"
			response.write 		"<td class=""" & estilo & """>" & right(("00000000" & xrs("nconNumero")),8) & "</td>"
			response.write 		"<td class=""" & estilo & """>" & FormatNumber(xrs("itens"),0) & "</td>"
			response.write 		"<td class=""GridSLinha"">&nbsp;</td>"
			Response.write "</tr>" & vbcrlf
			xRs.MoveNext
		wend
	end if
	%>
								<tr>
									<td class="GridRodape">&nbsp;</td>
									<td class="GridRodape">&nbsp;</td>
									<td class="GridRodape">&nbsp;</td>
									<td class="GridSLinha">&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
					</table>
				</form>
				</td>
			</tr>
		<!--#include file="../includes/rod.inc"-->
	</body>
</HTML>