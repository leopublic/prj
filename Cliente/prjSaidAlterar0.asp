<% 	
	Session.LCID = 1046 'Pt-Br'
	Response.Expires= 0
    Response.AddHeader "PRAGMA", "NO-CACHE"
	Response.Charset = "iso-8859-1"
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ::</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<link rel="stylesheet" type="text/css" href="../EstiloMenu.css">
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<!--#include file="../includes/funcoes.inc"-->
<%  lPerfisAutorizados = "15"
	VerificarAcesso()

	Dim gmenuGeral
	Dim gMenuSelecionado
	Dim gOpcaoSelecionada
	'gOpcaoSelecionada = "Servicos"
	gmenuGeral = "Projetos"
	gMenuSelecionado = "prjSaidListar"
	
	projId      = request.querystring("projId")
	protegido   = request.querystring("pr")
	solicitante = request.querystring("sl")
	numero      = trim(request.querystring("nr"))
	listId      = request.querystring("ls")
	nconId      = ""
	tipo        = trim(request.querystring("op"))
	tipo2       = "Origem"
	if(tipo="1") then tipo2 = "Lista"
	if(tipo="2") then tipo2 = "N�o-Conformidade"
	acao        = request.querystring("ac")
	dia         = now()
	
	msgerro = ""
	
	Dim gConexao
	AbrirConexao

	if((trim(session("projNome"))="") and (trim(projId)<>""))then
		xSQL = ""
		xSQL = xSQL & "select isnull(p.projNome,'') "
		xSQL = xSQL & "  from projeto p "
		xSQL = xSQL & " where p.projId = " & projId
		Set xrs = gConexao.Execute(xSQL)
		if not xrs.eof then	
			session("projNome") = xrs(0)
		end if
		xrs.close	
	end if
	
	if (tipo="2") then
		if(numero<>"")then
			xSQL = ""
			xSQL = xSQL & "select n.nconId "
			xSQL = xSQL & "  from naoconformidade n "
			xSQL = xSQL & " where n.nconNumero = " & trim(numero)
			Set xrs = gConexao.Execute(xSQL)
			if not xrs.eof then	
				nconId = xrs(0)
			else
				acao="erro"
				msgerro = "N�o-conformidade inexistente para o n�mero informado!"
			end if
			xrs.close	
		else
			acao="erro"
			msgerro = "N�mero da n�o-conformidade n�o foi informado!"
		end if
	end if 
 %>
		<script language="JavaScript" type="text/javascript" src="../scripts/moving_div_on_screen.js"></script>
        <script language="JavaScript" type="text/javascript" src="../scripts/block_background_objects.js"></script>
        <script language="JavaScript" type="text/javascript" src="../scripts/functions.js"></script>
        <link rel="stylesheet" type="text/css" href="../scripts/block_background_objects.css">
		<link rel="stylesheet" type="text/css" href="../scripts/wbs_form.css">
		<style type="text/css">
			p {
				text-align:center; 
				width:99%;
			}
		</style>
		<script language="JavaScript" type="text/javascript" src="../scripts/AbrirJanela.js"></script>
		<script language="JavaScript" type="text/javascript">
			var _xsl_ret=_gerarxsltretorno('LISTA/ERRO', 'LINHA', 'LINHA|DESCRICAO', 'Linha|Descri��o', '50|325');
			
			function _altori(_opt){
				var _td = document.getElementById('tdSaiConf');
				//
				_td.innerHTML='Origem';
				document.getElementById('cmbLista').style.display='none';
				document.getElementById('txtNumero').style.display='none';
				//
				if(_opt=='1'){ //lista
					_td.innerHTML='Lista';
					document.getElementById('cmbLista').style.display='block';
				}else if(_opt=='2'){ //n�o conformidade
					_td.innerHTML='N�o-Conformidade';
					document.getElementById('txtNumero').style.display='block';
				}
				//
			}
	
			function _limpar(){
				window.location.href='prjSaidAlterar0.asp?projId=<%=projId%>';
			}
	
			function _validar_itens(){
				var _ret='N�o existem itens.';
				if(document.getElementById('tbItens')){
					_ret = '';
					var _v_trs         = document.getElementById('tbItens').getElementsByTagName('TR');
					var _v_itrs        = 0;
					var _v_itesQtd     = '';
					var _v_itesQtdDisp = '';
					for(_v_itrs=1;_v_itrs<_v_trs.length;_v_itrs++){
						if(_v_trs[_v_itrs].style.display!='none'){
							_v_itesQtd     = document.getElementById('itesQtd'+_v_itrs).value;
							_v_itesQtdDisp = _v_trs[_v_itrs].cells[4].innerHTML;
							if(isNumberJS(_v_itesQtd,0)==false){
								_ret += _erro_xml_comum(_v_itrs, 'Qtd. recebida inv�lida!');
							}else{
								if(_v_itesQtd!=''){
									_v_itesQtd     = _v_itesQtd.replace('.','').replace(',','.');
									_v_itesQtdDisp = _v_itesQtdDisp.replace('.','').replace(',','.');
									if(eval(_v_itesQtd+'>'+_v_itesQtdDisp)){
										_ret += _erro_xml_comum(_v_itrs, 'Qtd. requerida maior que a qtd. dispon�vel!');
									}
								}
							}
						}
					}
					
					if(_ret!=''){
						alert('Houveram erros de preechimentos, verifique abaixo!');
						_ret='<LISTA>'+_ret+'</LISTA>';
						//alert(_ret);
						_carregaxmlxslt(_ret, _xsl_ret, 'divErros');
						_ret = false;
					}else{
						_ret = true;
					}
				}
				return _ret;
			}
				
			function _limpar_itens(){
				if(document.getElementById('tbItens')){
					var _l_trs = document.getElementById('tbItens').getElementsByTagName('TR');
					var _l_itrs=0;
					for(_l_itrs=1;_l_itrs<_l_trs.length;_l_itrs++){
						if(_l_trs[_l_itrs].style.display!='none'){
							document.getElementById('itesQtd'+_l_itrs).value='';
						}
					}
				}
			}
			
			function _sair_itens(_saidId){
				if(document.getElementById('tbItens')){
					//
					var msg_erro = ' Ocorreram erros durante o processamento. \n Por favor, verifique as mensagens de erro!';
					var _ret= '';
					//
					var _s_trs = document.getElementById('tbItens').getElementsByTagName('TR');
					var _s_itrs=0;
					var _s_unidId='';
					var _s_itelId='';
					var _s_itemId='';
					var _s_itesQtd='';
					//
					for(_s_itrs=1;_s_itrs<_s_trs.length;_s_itrs++){
						// --
						if(_s_trs[_s_itrs].style.display!='none'){
							_s_itelId  = document.getElementById('itelId'+_s_itrs).value;
							_s_unidId  = document.getElementById('unidId'+_s_itrs).value;
							_s_itesQtd = document.getElementById('itesQtd'+_s_itrs).value;
							_s_itemId  = document.getElementById('itemId'+_s_itrs).value;
							// --
							if(_s_itesQtd!=''){
								//salvando item
								var _pagina = 'prjSaidAlterar0_BD.asp?acao=SI&saidId='+_saidId+'&unidId='+_s_unidId+'&itelId='+_s_itelId+'&itesQtd='+_s_itesQtd+'&itemId='+_s_itemId;
								//alert(_pagina);
								var _xmlhttp_i = _criaxmlhttprequest();
								_xmlhttp_i.open("GET",_pagina,false);
								_xmlhttp_i.send(null);
								_ret += _xmlhttp_i.responseText;
							}
						}
					}
					
					if(_tratarxmlretorno(_ret)){
						alert('Dados salvos com sucesso!');
						window.location.href='prjSaidListar.asp?projId=<%=projId%>';
					}else{
						alert(msg_erro);
					}
				}
			}
			
			function _salvar_itens(){
				if(confirm('Ao solicitar uma sa�das de material, voc� estar� atestado a veracidade dos dados informados, bem como concordando com os mesmos.\nDeseja continuar?')){
					if(document.getElementById('tbItens')){
						document.getElementById('procover').style.display='block';
						var msg_erro = ' Ocorreram erros durante o processamento. \n Por favor, verifique as mensagens de erro!';
						if(_validar_itens()){
							//
							var _ret= '';
							// criando o id da sa�da
							var _pagina = 'prjSaidAlterar0_BD.asp?acao=CS&projId=<%=projId%>&ls=<%=listId%>&nc=<%=nconId%>&sl=<%=solicitante%>&op=<%=tipo%>';
							//alert(_pagina);
							var _xmlhttp_i = _criaxmlhttprequest();
							_xmlhttp_i.open("GET",_pagina,false);
							_xmlhttp_i.send(null);
							_ret += _xmlhttp_i.responseText;
							if (isNumberJS(_ret,1)){
								_sair_itens(_ret);
							}else{
								if(_tratarxmlretorno(_ret)){
									alert(msg_erro);
								}
							}
						}
						document.getElementById('procover').style.display='none';
					}
				}
			}
			
			function _emitir(_modo){
				if(document.getElementById('tbItens')){
					//
					var msg_erro = ' Ocorreram erros durante o processamento. \n Por favor, verifique as mensagens de erro!';
					var _ret= '';
					//
					var _g_trs = document.getElementById('tbItens').getElementsByTagName('TR');
					var _g_itrs=0;
					var _g_lins  = '';
					var _g_items = '';
					var _g_qtds  = '';
					var _g_qtd   = '';
					var _g_lins  = '';
					//
					for(_g_itrs=1;_g_itrs<_g_trs.length;_g_itrs++){
						if(_g_trs[_g_itrs].style.display!='none'){
							_g_qtd=document.getElementById('itesQtd'+_g_itrs).value;
							if(isNumberJS(_g_qtd,1)){
								if(eval(_g_qtd.replace('.','').replace(',','.')+'>0')){
									_g_qtds  += '|' + _g_qtd;
									_g_items += '|' + document.getElementById('itemId'+_g_itrs).value;
									_g_lins  += '|' + (_g_itrs+1);
								}
							}
						}
					}
					//
					_g_qtds  = _g_qtds.substr(1);
					_g_items = _g_items.substr(1);
					_g_lins  = _g_lins.substr(1);
					//
					if(_g_items!=''){
						window.open('prjSaidAlterar0_BD.asp?acao='+_modo+'&projId=<%=projId%>&ls=<%=listId%>&nc=<%=nconId%>&sl=<%=solicitante%>&op=<%=tipo%>&qtds='+_g_qtds+'&items='+_g_items+'&lins='+_g_lins);
					}else{
						alert('Nenhuma quantidade foi infornmada!');
					}
				}
			}
			
			function _gerar_xls(){_emitir('GE');}
			function _imprimir(){_emitir('IL');}
			
			function _solic_itens(){
				var _sl = document.getElementById('txtSolicitante').value;
				var _nr = document.getElementById('txtNumero').value;
				var _ls = _valor_atual_combo('cmbLista');
				var _dt = document.getElementById('txtData').value;
				var _op = [document.getElementById('optTipo_LIS'), document.getElementById('optTipo_NCO')];
				var _pr = 'readonly';
				var _ac = 'informar_quantidades';
				var _msg = '';
				//
				if ((_sl=='') || (_sl.length<3)){
					_msg+=', Solicitante';
				}
				//
				if ((isNumberJS(_nr,1)==false) && (_op[1].checked)){
					_msg+=', N�o-Conformidade';
				}else if ((_ls=='0') && (_op[0].checked)){
					_msg+=', Lista';
				}
				//
				if(_op[0].checked){
					_op='1'; //lista
				}else if(_op[1].checked){
					_op='2'; //N�o-Conformidade
				}else{
					_msg+=', Tipo de Origem';
				}
				//
				if(_msg!=''){
					alert('Os seguintes campos est�o errados ou foram preenchidos incorretamente:\n'+_msg.substr(2));
				}else{
					window.location.href='prjSaidAlterar0.asp?projId=<%=projId%>&pr='+_pr+'&sl='+_sl+'&nr='+_nr+'&ls='+_ls+'&dt='+_dt+'&ac='+_ac+'&op='+_op;
				}
			}
		</script>
	</HEAD>
	<body class="cabecalho" >
		<div id="over"     style="display:none; z-index:500; width:99%; height:99%; position:absolute; background: #464; -moz-opacity:0.4;filter:alpha(opacity=40)"></div>
		<div id='procover' style='background: #464 url(../images/aguarde.gif) no-repeat fixed center; display:none'></div>
		<!--#include file="../includes/cab.inc"-->
		<tr>
			<td style="padding:0px" valign="top" class="blocoOpcoes">
			<!--#include file="../includes/opcProjx2.inc"-->
			</td>
			<td colspan="2" valign="top" style="padding:5px" class="blocoPagina">
				<table width="100%">
				<tr>
					<td colspan="2" class="docTit"><%=session("projNome")%> - NOVA SA�DA</td>
				</tr>
				<tr>
					<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
				</tr>
				<tr>
					<td colspan="2" style="height:5"></td>
				</tr>
		<tr>
			<td></td>
			<td>
				<input id='listId' name='listId' type="hidden" value="<%=listId%>" />
				<input id='nconId' name='nconId' type="hidden" value="<%=nconId%>" />
				<input id="acao"   name="acao"   type="hidden" value="<%=acao%>" />
				<input id="tipo"   name="tipo"   type="hidden" value="<%=tipo%>" />
				<table class="doc">
					<tr>
						<td width="130">Solicitante</td>
						<td width="5">:</td>
						<td class="docSBorda"><input id="txtSolicitante" name="txtSolicitante" type="text" <%=protegido%> value="<%=solicitante%>" maxlength="200" class="docCmpLivre" style="width:99%;border:solid 1px black"></td>
					</tr>
					<tr>
						<td id='tdSaiConf'><%=tipo2%></td>
						<td>:</td>
						<td class="docSBorda">
							<%if (tipo="") then %>
								<input id='optTipo_LIS' name="optTipo" type="radio" class="docCmpLivre" value="1" <%=protegido%> style="width:auto;" onclick='javascript:_altori(this.value);'>Lista&nbsp;&nbsp;&nbsp;
								<input id='optTipo_NCO' name="optTipo" type="radio" class="docCmpLivre" value="2" <%=protegido%> style="width:auto;" onclick='javascript:_altori(this.value);'>N�o-Conformidade
							<%end if %>
							<select id='cmbLista' name='cmbLista' class="docCmpLivre" <%=protegido%> style="width:99%;border:solid 1px black; display:none">
<%							xSQL = ""
							xSQL = xSQL & "select l.listId, isnull(l.listSequencial,0), isnull(r.reviNumero,0), isnull(t.tpliNome,'') "
							xSQL = xSQL & "  from (lista l left join revisao r on l.reviIdAtual = r.reviId) left join tipolista t on t.tpliId = l.tpliId "
							xSQL = xSQL & " where l.projId = " & projId
							'pedcId = ""
							%>	<option value='0' selected>Selecione uma lista</option> <%
							set xrs = gConexao.Execute(xSQL)
							while not xrs.eof 
								'pedcId=xrs("pedcId")
								response.write "<option value='" & xrs(0) & "'>" & xrs(3) & " - seq.: " & xrs(1) & " - rev.: " & xrs(2) & "</option>"
								xrs.movenext
							wend
							xrs.close %>
							</select>
							<input id="txtNumero" name="txtNumero" type="text" value="<%=numero%>" maxlength="10" class="docCmpLivre" <%=protegido%> style="width:200px;border:solid 1px black; display:none" >
						</td>
					</tr>
					<tr>
						<td>Data</td>
						<td>:</td>
						<td class="docSBorda"><input id="txtData" name="txtData" type="text" value="<%=dia%> " maxlength="10" class="docCmpLivre" readonly style="width:200px;border:solid 1px black" ></td>
					</tr>
				</table>
				<p>
<% if acao="informar_quantidades" then 
	'<input type="button" value="Mudar LISTA/n�o-conformidade" />
   elseif acao="erro" then %>
	<span class='erro'><%=msgerro%></span><br/>
<% else %>
	<input type="button" value="Solicitar itens" onclick='javascript:_solic_itens();' />
<% end if %>
					<input type="button" value="Limpar" onclick='javascript:_limpar();'>
				</p>
<%
if acao="informar_quantidades" then
	tabela=""
	tabela=tabela&"<br>"
	tabela=tabela&"<p style='text-align:left;'>Informe as quantidades requeridas de cada item:</p>"
	tabela=tabela&"<table width='99%' class='grid' id='tbItens' name='tbItens'>"
	tabela=tabela&"	<tr class='GridCab'>"
	tabela=tabela&"		<td style='width:5px'>&nbsp;</td>"
	tabela=tabela&"		<td style='width:100px'>C�d. JDE</td>"
	tabela=tabela&"		<td style='width:330px'>Descri��o</td>"
	tabela=tabela&"		<td style='width:70px' >Unidade</td>"
	tabela=tabela&"		<td style='width:100px'>Qtd pedida</td>"
	tabela=tabela&"		<td style='width:100px'>Qtd dispon�vel</td>"
	tabela=tabela&"		<td style='width:100px'>Qtd requerida</td>"
	tabela=tabela&"	</tr>"
	
	rod_tabela=""
	rod_tabela=rod_tabela&"</table>"
	rod_tabela=rod_tabela&"<p>"
	rod_tabela=rod_tabela&"		<input type='button' value='Imprimir' onclick='javascript:_imprimir();'>"
	rod_tabela=rod_tabela&"		<input type='button' value='Solicitar Itens' onclick='javascript:_salvar_itens();'>"
	rod_tabela=rod_tabela&"		<input type='button' value=""Limpar Qtd's"" onclick='javascript:_limpar_itens();'>"
	rod_tabela=rod_tabela&"		<input type='button' value='Gerar xls' onclick='javascript:_gerar_xls();'>"
	rod_tabela=rod_tabela&"</p>"
	
	xSQL = ""
	if(tipo="1")then 'Lista
		xSQL = xSQL & "select i.itelId, i.itemId, isnull(i.itelQtdAtual,0) itelQtdAtual, it.ItemDescricao, it.ItemCodJde, isnull(i.itelQtdUtilizada,0) itelQtdUtilizada, isnull(u.unidSigla,'-') unidSigla, isnull(u.unidId,0) unidId "
		xSQL = xSQL & "  from ItemLista i, Item it, Unidade U "
		xSQL = xSQL & " where it.itemId  = i.itemId "
		xSQL = xSQL & "   and i.unidId  *= u.unidId "
		xSQL = xSQL & "   and i.listId   = " & listId
	elseif(tipo="2")then 'N�o-Conformidade
		xSQL = xSQL & "select '' itelId, j.itemId, isnull(j.itrcQtd,0) itelQtdAtual, it.ItemDescricao, it.ItemCodJde, isnull(k.itrcQtdUtilizada,0) itelQtdUtilizada, isnull(u.unidSigla,'-') unidSigla, isnull(u.unidId,0) unidId "
		xSQL = xSQL & "  from Item    it "
		xSQL = xSQL & "     , Unidade u "
		xSQL = xSQL & "     , (select r.nconId, i.itemId, i.unidId, sum(isnull(i.itrcQtd,0)) as itrcQtd "
		xSQL = xSQL &         "  from itemrecebimento i "
		xSQL = xSQL &         "     , recebimento     r "
		xSQL = xSQL &         " where i.receId = r.receId "
		xSQL = xSQL &         "   and r.nconId = " & nconId 
		xSQL = xSQL &         "   group by r.nconId, i.itemId, i.unidId) j "
		xSQL = xSQL & "     , (select r.nconId, i.itemId, i.unidId, sum(isnull(ia.itesQtd,0)) as itrcQtdUtilizada "
		xSQL = xSQL &         "  from itemrecebimento i "
		xSQL = xSQL &         "     , recebimento     r "
		xSQL = xSQL &         "     , itemsaida      ia " 
		xSQL = xSQL &         " where i.receId = r.receId "
		xSQL = xSQL &         "   and r.nconId = " & nconId 
		xSQL = xSQL &         "   and i.itemId = ia.itemId "
		xSQL = xSQL &         "   group by r.nconId, i.itemId, i.unidId) k "
		xSQL = xSQL & " where it.itemId = j.itemId "
		xSQL = xSQL & "   and j.itemId *= k.itemId "
		xSQL = xSQL & "   and j.unidId *= u.unidId "
	end if
	if (xSQL<>"") then
		Set xrs = gConexao.Execute(xSQL)
		if not xrs.eof then	
			response.write tabela

			estilo = "GridLinhaImpar"
			i = 0
			while not xrs.EOF 
				if (cdbl(xrs("itelQtdAtual"))>cdbl(xrs("itelQtdUtilizada"))) then
					i = i + 1
			%>
					<tr class="<%=estilo%>">
						<td style='width:5px'><%=i%></td>
						<td style='width:100px'><%=xrs("itemCodJde")%></td>
						<td style='width:330px'><%=xrs("itemDescricao")%></td>
						<td style='width:70px' ><%=xrs("unidSigla")%></td>
						<td style='width:100px'><%=formatnumber(xrs("itelQtdAtual"),2)%></td>
						<td style='width:100px'><%=formatnumber((cdbl(xrs("itelQtdAtual"))-cdbl(xrs("itelQtdUtilizada"))),2)%></td>
						<td style='width:100px'>
							<input type="hidden" id="unidId<%=i%>"  name="unidId<%=i%>"  value="<%=cstr(xrs("unidId"))%>" />
							<input type="hidden" id="itelId<%=i%>"  name="itelId<%=i%>"  value="<%=cstr(xrs("itelId"))%>" />
							<input type="hidden" id="itemId<%=i%>"  name="itemId<%=i%>"  value="<%=cstr(xrs("itemId"))%>" />
							<input type="text"   id="itesQtd<%=i%>" name="itesQtd<%=i%>" value="" maxlength="15" class="cpNum" style="width:80px;border:solid 1px black" />
						</td>
					</tr>
	<%				if estilo = "GridLinhaPar" then
						estilo = "GridLinhaImpar"
					else
						estilo = "GridLinhaPar"
					end if
				end if
				xrs.movenext
			wend
			%>
			</table>
			<%
			response.write rod_tabela
		    xrs.close
		else %>
			<p>N�o existem itens dispon�veis para retirada!</p>
<%		end if
	end if
	
end if
%>						<div id='divErros' style='width:99%;height:0px;display:none;margin-top:5px;'>
					</td>
				</tr>
				<tr>
					<td colspan="2" height="20"></td>
				</tr>
			</table>
			<script language='javascript' type='text/javascript'>
				_altori('<%=tipo%>');
			</script>
		</tr>
		<!--#include file="../includes/rod.inc"-->
	</body>
</HTML>