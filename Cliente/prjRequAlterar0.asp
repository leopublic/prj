<% 
    pStr = "private, no-cache, must-revalidate" 
    Response.ExpiresAbsolute = #2000-01-01# 
    Response.AddHeader "pragma", "no-cache" 
    Response.AddHeader "cache-control", pStr 
	'Server.ScriptTimeout = 10000
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<% 
	'--- Instantiate the FileUpProgress object.
	Set oFileUpProgress = Server.CreateObject("SoftArtisans.FileUpProgress")
 	intProgressID = oFileUpProgress.NextProgressID
	%>
	<HEAD>
		<title>::PRJ:: Controle remoto de projetos</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<script language="JavaScript" type="text/javascript" src="../scripts/PreparaJanela.js"></script>
		<SCRIPT language="JavaScript">
			function submitform(){
			  startupload();
			  document.form.submit();
			}
			function validar(){
				if (document.form.txtArquivo.value == ''){   
					alert('Informe a localiza��o do arquivo.');
				}else 
				{
					if (document.form.cmbRevisao.value == '0'){   
						alert('Informe a revis�o a que refere essa requisi��o.');
					}else 
					{
						document.form.btnEnviar.style.display='none';
						submitform();
					}
				}
			}
			function startupload() {
				winstyle="height=150,width=500,status=no,toolbar=no,menubar=no,location=no";
				window.open("progress.asp?progressid=<%=intProgressID%>",null,winstyle);
				document.form.action="prjRequAlterar1.asp?progressid=<%=intProgressID%>";
			}
		</SCRIPT> 
	</HEAD>
	<body class="PopUpEntrada" onLoad="javascript:PreparaJanela(600,350);">
<%
    lPerfisAutorizados = "123457"
	'VerificarAcesso
	'* Abrir conexao
	'*==========================================================
	Dim gConexao 
	AbrirConexao
	tituloPagina = "Carregar nova requisi��o"
	Session("TituloMsg") = tituloPagina
	projId = Session("projId")
%>
	<div style="padding-right:25px">
	<form action="prjRequAlterar1.asp" method="post" name="form" encType="multipart/form-data" onSubmit="startupload();" >
		<input type="hidden" name="projId" value="<%=projId%>" >
		<input type="hidden" name="usuaId" value="<%=Session("usuaId")%>" >
		<table width="100%">
			<tr>
				<td width="15"></td>
				<td>&nbsp;</td>
			</tr>
			<tr>				
				<td colspan="2" class="docTit"><% = tituloPagina %></td>
			</tr>
			<tr>
				<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
			</tr>
			<tr>
				<td colspan="2" height="15"></td>
			</tr>
			<tr>
				<td></td>
				<td>
					<table class="doc">
						<tr>
							<td width="110">Projeto</td>
							<td width="5">:</td>
							<td class="docCmpLivre">(<% =Session("projNum") %>) <% =Session("projNome") %></td>
						</tr>
						<tr>
							<td>Arquivo</td>
							<td>:</td>
							<td class="docCmpLivre"><input type="file" name="txtArquivo" class="docCmpLivre" style="width:100%"></td>
						</tr>
						<tr>
							<td>Lista</td>
							<td>:</td>
							<td class="docCmpLivre">
								<select id="cmbRevisao" name="cmbRevisao" class="docCmpLivre" class="docCmpLivre" >
									<option value="0" class="campo">--</option>
<%								
	xSQL = "select lista.listId, listSequencial, reviId, reviNumero, tpliCodigo, tpliPrefixo, listNumProjeto"
	xSQL = xSQL & " from lista, revisao, tipoLista "
	xSQL = xSQL & " where projId = " & projId
	xSQL = xSQL & " and lista.listId = revisao.listId"
	xSQL = xSQL & " and tipolista.tpliId = lista.tpliId"
	xSQL = xSQL & " order by tpliNome, listSequencial"
	'response.write xSQL
	Set xRs = gConexao.Execute(xSQL)
	while not xRs.EOF
		response.write vbtab & "<option value=""" & xRs("reviId") & """ class=""campo"">" & xRs("tpliPrefixo")  & "-" & xRs("listNumProjeto") & "-" & xRs("tpliCodigo") & "-" & xRs("listSequencial") & " - Rev:" & xrs("reviNumero") & "</option>"
		xRs.MoveNext
	wend
	xrs.close
	set xRs = nothing
%>
								</select>
							</td>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2" height="20"></td>
			</tr>
		</table>
		<div id="comandos" style="text-align:center">
			<img src="../images/btnSalvar.gif"  id="btnEnviar" onclick="javascript:validar();">
			<img src="../images/btnCancelar.gif" onClick="javascript:window.close();">
		</div>
	</form>
	</div>
	</body>
<%
if session("msgErro") <> "" then
%>
<script language="javascript" type="text/javascript">
	alert('<%=session("msgErro")%>');
	document.form.btnEnviar.style.display='inline';
</script>
<%
	session("msgErro") = ""
end if
%>
</HTML>