<% Response.Expires= -1%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ::</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<link rel="stylesheet" type="text/css" href="../EstiloMenu.css">
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<script language="JavaScript" type="text/javascript" src="../scripts/AbrirJanela.js"></script>
	</HEAD>
	<body class="cabecalho" >
<%  lPerfisAutorizados = "157"
	VerificarAcesso()

	projId = session("projId")
	xrequId = request.querystring("requId")
   Dim gmenuGeral
   Dim gMenuSelecionado
   Dim gOpcaoSelecionada
   'gOpcaoSelecionada = "Servicos"
   gmenuGeral = "Projetos"
    gmenuGeral = "Projetos"
   gMenuSelecionado = "prjRequListar"

   	Dim gConexao 
	AbrirConexao
	xSQL =        "select RV.reviId, RQ.requId, tpliPrefixo , tpliNome , tpliCodigo, reviNumero, listSequencial, requNum, pedcId, pedcNum, listNumProjeto"
	xSQL = xSQL & "  from lista     LI "
	xSQL = xSQL & "      , tipolista TL "
	xSQL = xSQL & "      , revisao   RV "
	xSQL = xSQL & "      , (requisicao RQ left join pedidoCompra PC on PC.requId = RQ.requId)"
	xSQL = xSQL & " where LI.tpliId = TL.tpliId"
	xSQL = xSQL & "   and RV.listId = LI.listId"
	xSQL = xSQL & "   and RV.reviId = RQ.reviId"
	xSQL = xSQL & "   and RQ.requId = " & xrequId
	xSQL = xSQL & " order by requNum"
	Set xRs = gConexao.Execute(xSQL)
	if not xRs.Eof then
		xLista =  xrs("tpliPrefixo") & "-" & xrs("listNumProjeto") & "-" & xRs("tpliCodigo") & "-" & xRs("listSequencial") & " (revis�o " & xrs("reviNumero") & ")"
		xtpliPrefixo = xrs("tpliPrefixo") 
		xtpliNome = xrs("tpliNome")
		xreviId = xrs("reviId")
		xrequNum = xrs("requNum")
		xpedido = ""
		xvirgula = ""
		while not xRs.EOF 
			xpedido = xpedido & xvirgula &  "<a href=""prjPedcConsultar.asp?pedcId=" & xrs("pedcId") & """>" & xrs("pedcNum") & "</a>"
			xvirgula = ", "
			xrs.MoveNext
		wend 
		if xpedido <>"" then
			xpedido = "<tr><td>Pedido(s)</td><td>:</td><td>" & xpedido & "</td></tr>"
		end if
	end if
	xrs.close
   
 %>
		<!--#include file="../includes/cab.inc"-->
			<tr>
				<td style="padding:0px" valign="top" class="blocoOpcoes">
					<!--#include file="../includes/opcProjx2.inc"-->
				</td>
				<td colspan="2" valign="top" style="padding:5px" class="blocoPagina">
				<form>
					<table width="100%">
					<tr>
						<td class="docTit">
							<%=session("projNome")%><br>
							<table>
								<tr><td>Requisi��o de compra</td><td>:</td><td><%= xrequNum %></td></tr>
								<tr><td><%=xtpliNome%></td><td>:</td><td><a href="prjListConsultar.asp?reviId=<%=xreviId%>"><%=xlista%></a></td></tr>
								<%=xpedido%>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
					</tr>
					<tr>
						<td colspan="2" style="height:5"></td>
					</tr>
					<tr>
						<td colspan="2">
							<table width="100%" class="grid" >
								<tr>
									<td width="80px" class="GridCab"><a href="prjListListar.asp" class="GridCab">Cod JDE<%=SortNome%></a></td>
									<td class="GridCab"><a href="prjListListar.asp" class="GridCab">Descri��o<%=SortNome%></a></td>
									<td width="70px"  class="GridCab" STYLE="text-align:right">Qtd<br>Requisi��o</td>
									<td width="70px"  class="GridCab" STYLE="text-align:right">Qtd<br>Lista</td>
									<td width="70px"  class="GridCab" STYLE="text-align:right">Qtd<br>Pedida</td>
									<td class="GridSLinha">&nbsp;</td>
								</tr>
	<%
	xSQL = " select itemCodJde, itemDescricao, iteqQtd, xxx.iterQtd , yyy.itepQtd"
	xSQL = xSQL & "  from requisicao     RQ "
	xSQL = xSQL & "     , revisao        RE"
	xSQL = xSQL & "     , item           IT"
	xSQL = xSQL & "     , unidade        UN"
	xSQL = xSQL & "     , (((itemRequisicao IQ left join itempedido IP on IP.iteqId = IQ.iteqId)"
	xSQL = xSQL & "      left join (select itemId, iterId, sum(iterQtd) iterQtd from itemRevisao IR, requisicao RQ"
	xSQL = xSQL & "         where IR.reviId = RQ.reviId and RQ.requId  = " & xrequId & " group by itemId, iterId) xxx on xxx.iterId = IQ.iterId)"
	xSQL = xSQL & "      left join (select itemId, iteqId, sum(itepQtd) itepQtd from itemPedido IP, pedidoCompra PC"
	xSQL = xSQL & "         where IP.pedcId = PC.pedcId and PC.requId  = " & xrequId & " group by itemId, iteqId) yyy on yyy.iteqId = IQ.iteqId)"
	xSQL = xSQL & " where RQ.requId = IQ.requId"
	xSQL = xSQL & "   and IT.itemId = IQ.itemId"
	xSQL = xSQL & "   and UN.unidId = IQ.unidId"
	xSQL = xSQL & "   and RE.reviId = RQ.reviId"
	xSQL = xSQL & "   and RQ.requId =  " & xrequId
	'response.write xsql
	Set xRs = gConexao.Execute(xSQL)
	if xRs.EOF then
	    Response.write "<tr>" & vbcrlf
	    Response.write "<td colspan=""5"">Requisi��o sem itens</td>" & vbcrlf
	    Response.write "</tr>" & vbcrlf
		xPovoou = false
	else
		estilo = "GridLinhaPar"
		while not xRs.EOF
			Response.write "<tr>" & vbcrlf
			response.write vbtab & "<td class=""" & estilo & """>" & xRs("itemCodJde") & "</td>"
			response.write vbtab & "<td class=""" & estilo & """>" & xRs("itemDescricao") & "</td>"
			response.write vbtab & "<td class=""" & estilo & """ style=""text-align:right"">" & xRs("iteqQtd") & "</td>"
			response.write vbtab & "<td class=""" & estilo & """ style=""text-align:right"">" & xRs("iterQtd") & "</td>"
			response.write vbtab & "<td class=""" & estilo & """ style=""text-align:right"">" & xRs("itepQtd") & "</td>"
			response.write vbtab & "<td class=""GridSLinha"">&nbsp;</td>"
			Response.write "</tr>" & vbcrlf
			xRs.MoveNext
			if estilo = "GridLinhaPar" then
				estilo = "GridLinhaImpar"
			else
				estilo = "GridLinhaPar"
			end if
		wend
	end if
	%>
								<tr>
									<td class="GridRodape">&nbsp;</td>
									<td class="GridRodape">&nbsp;</td>
									<td class="GridRodape">&nbsp;</td>
									<td class="GridRodape">&nbsp;</td>
									<td class="GridRodape">&nbsp;</td>
									<td class="GridSLinha">&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
					</table>
				</form>
				</td>
			</tr>
		<!--#include file="../includes/rod.inc"-->
	</body>
</HTML>