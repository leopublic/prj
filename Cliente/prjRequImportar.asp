<!--#include file="../includes/clsBanco.inc"-->
<!--#include file="../includes/clsControleAcesso.inc"-->
<!--#include file="../includes/varControleAcesso.inc"-->
<!--#include file="../includes/varBanco.inc"-->
<!--#include file="../includes/funcoes.inc"-->
<%
Dim debug 
Dim ximpoId
Dim objConn
Dim objRs
Dim xPagina 
xPagina = "prjRequImportar"
AbrirConexao
'== Obt�m arquivo a importar
debug = request.querystring("debug")
ximpoId = request.querystring("impoId")
xPath = Server.MapPath("..\listas")
xArquivo = xPath & "\" & ximpoId & ".txt"
destinoErro = "prjRequAlterar0.asp?impoId=" & ximpoId
'== Limpa mensagens da �ltima importacao
xSQL = "delete from MensagemImportacao where impoId =  " & ximpoId
gConexao.execute(xSQL)
'== Obtem c�digo da lista
xSQL = "select reviId from importacao where impoId = " & ximpoId
debuga xsql 
set xrs = gConexao.execute(xSQL)
reviId = xrs("reviId")
xrs.close
set xrs = nothing
if reviId = "" or isnull(reviId) then
	retornarComErro(xPagina & ":ObterLista", "Revis�o n�o encontrada na importa��o", destinoErro)
end if
'== Inicia importa��o do arquivo
	Const ForReading = 1, ForWriting = 2, ForAppending = 3
	Const TristateUseDefault = -2, TristateTrue = -1, TristateFalse = 0
	' Create a filesystem object
	Dim FSO
	set FSO = server.createObject("Scripting.FileSystemObject")

	if FSO.FileExists(xArquivo) Then
	    ' Get a handle to the file
	    Dim file    
	    set file = FSO.GetFile(xArquivo)

	    ' Open the file
	    Dim TextStream
	    Set TextStream = file.OpenAsTextStream(ForReading, TristateUseDefault)

	    ' Read the file line by line
		regNum = 0
		erro = 0
	    if ( not TextStream.AtEndOfStream) then
	        reg = TextStream.readline
			regNum = regNum + 1
			' Pula primeira linha
	        reg = TextStream.readline
			'== Inicia a importa��o dos itens
			Do While (not TextStream.AtEndOfStream)
				'== Pula linhas em branco
				if not (trim(replace(reg, vbtab, "")) = "") then
					'
					' Obtem campos
					coluna = Split(reg, vbtab)
					requNumOrig = coluna(0)
					requNum     = coluna(1)
					fornNome = coluna(3)
					itemcodJde     = coluna(4)
					dtPromEntr = coluna(5)
					xData = split(dtPromEntr, "/")
					dtPromEntr = xdata(2) & "-" & xdata(1) & "-" & xdata(0)
					iteqQtd = campoConvertido(coluna(6))
					unidSigla = coluna(7)
					requNumTag = coluna(8)

					'debuga "<br>Item encontrado: " & ordem
					'debuga "Ordem    -->" & ordem & "<--"
					'debuga "Tag    -->" & tag & "<--"
					'debuga "Linha   -->" & linha & "<--"
					'debuga "codJDE    -->" & codJde & "<--"
					'debuga "descricao -->" & descricao & "<--"
					
					xSQL = " exec PCAD_ItemRequisicao"
					xSQL = xSQL & "   @prequNum       = " & requNum
					xSQL = xSQL & "  ,@prequNumOrig   = " & requNumOrig
					xSQL = xSQL & "  ,@prequNumTag    = '" & requNumTag & "'"
					xSQL = xSQL & "  ,@pitemCodJde    = " & itemCodJde
					xSQL = xSQL & "  ,@piteqQtd       = " & iteqQtd
					xSQL = xSQL & "  ,@previId        = " & reviId
					xSQL = xSQL & "  ,@punidSigla     ='" & unidSigla & "'"
					'on error resume next
					debuga xSQL 
					set xrs = gConexao.execute(xSQL)
					If Err.Number <> 0 then
						debuga "<br/>SQL:" & xSQL
						debuga "<br/>ERRO: " & Err.Description
						Error.Clear
						response.end
					end if
					retorno = xrs("retorno")
					if retorno <> 0 then
						retornarComErro(xPagina & ":PCAD_ItemRequisicao", xrs("msg"), destinoErro)
					end if
					set xrs = nothing
				end if
				'== L� proximo registro
				reg = TextStream.readline
				regNum = regNum + 1
			loop
	    
	    end if
		TextStream.close
	    Set TextStream = nothing
		Session("msg") = "Requisi��o carregada com sucesso!"
		Set FSO = nothing
		if not debug="on" then
			response.redirect("gerMensagem.asp")
		end if
	Else
		retornarComErro(xPagina & ":AbrirArquivo", "Arquivo (" & xArquivo & ") n�o encontrado!" , destinoErro)
		debuga "Arquivo (" & xArquivo & ") n�o encontrado!"
		Set FSO = nothing
		if not debug="on" then
			response.redirect("gerMensagem.asp")
		end if
	End If
%>