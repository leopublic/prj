<%@ Language=VBScript EnableSessionState="False" %> 
<% 
'-----------------------------------------------------------------------
'--- This is the progress indicator itself.  It refreshes every second
'--- to re-read the file progress properties, which are updated thoughout
'--- the upload.
'-----------------------------------------------------------------------
'--- Declarations
Dim oFileUpProgress
Dim intProgressID
Dim intPercentComplete
Dim intBytesTransferred
Dim intTotalBytes
Dim bDone

intPercentComplete = 0
intBytesTransferred = 0
intTotalBytes = 0

'--- Instantiate the FileUpProgress object
Set oFileUpProgress = Server.CreateObject("Softartisans.FileUpProgress")

'--- Set the ProgressID with the value we submitted from the form page
oFileUpProgress.ProgressID = CInt(Request.QueryString("progressid"))

'--- Read the values of the progress indicator's properties
intPercentComplete = oFileUpProgress.Percentage
intBytesTransferred = oFileUpProgress.TransferredBytes
intTotalBytes = oFileUpProgress.TotalBytes

%>
<html>
<Head>
		<script language="JavaScript" type="text/javascript" src="../scripts/PreparaJanela.js"></script>
		<link rel="stylesheet" type="text/css" href="../Styles.css">
<%
	'--- If the upload isn't complete, continue to refresh
	If intPercentComplete < 100 Then
		bDone = False
		Response.Write("<Meta HTTP-EQUIV=""Refresh"" CONTENT=1>")
	Else
		bDone = True
	End If
%>
</head>
<Body bgcolor="#DDE4DC" onload="javascript:PreparaJanela(350,100);" style="background-color:#DDE4DC">
<TABLE border=0 width="100%" bgcolor="#DDE4DC">
	<tr>
		<td colspan="3" style="font-family:tahoma;font-size:10pt;font-weight:normal;color:#414342;"><b>Recebendo arquivo... </b>(<%=intPercentComplete%>%)</td>
	</tr>
	<TR>
		<TD colspan="3" style="border-style:solid;border-width:1;">
			<TABLE width="100%" border=1 cellspacing=0 bordercolor="#396D60" style="border-collapse:collapse">
				<TR>
          			<TD align=right height="10" width="<%=intPercentComplete%>%" BGCOLOR="red" border=0></TD>					
          			<TD align=right height="10" width="<%=100 - intPercentComplete%>%" BGCOLOR="white" border=0 ></TD>					
				</TR>
			</TABLE>
		</td>
	</tr>
</Table>
<% if bDone = true then %>
		<SCRIPT language="javascript">
			 setTimeout("window.close()",500); 
		</SCRIPT>
<% end if%>
</body>
</Html>