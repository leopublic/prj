<% Response.Expires= -1%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Controle remoto de projetos</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<script language="JavaScript" type="text/javascript" src="../scripts/PreparaJanela.js"></script>
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<script language="JavaScript" type="text/javascript" src="../scripts/CheckNumeric.js"></script>
		<SCRIPT language="JavaScript">
			function submitform(){
			  document.form1.submit();
			}
			function validar(){
			  if (document.form1.txtNome.value == '')
			  {   alert('O preenchimento do nome � obrigatorio.');
			  	  document.form1.txtNome.select();
				  document.form1.txtNome.focus(); }
			  else
			  {   submitform();}
			}
		</SCRIPT> 
	</HEAD>
	<body class="PopUpEntrada" onLoad="PreparaJanela(600,300);">
<% 
    lPerfisAutorizados = "51"
	VerificarAcesso
	Dim gConexao 
	AbrirConexao
	tpliId = Request.Querystring("tpliId")
	if tpliId = "0" then
	    titulo = "Cadastrar novo tipo de lista de material"
		Session("TituloMsg") = titulo
		Session("Msg") = "Novo tipo inclu�do com sucesso!"
		tpliNome = ""
		tpliPrefixo = ""
		tpliCodigo = ""
	else
		xSQL = "select tpliNome, tpliPrefixo, tpliCodigo from TipoLista where tpliId = " & tpliId
		set xrs = gConexao.execute(xSQL)
		titulo = "Alterar tipo de lista"
		Session("TituloMsg") = titulo
		Session("Msg") = "Tipo de lista alterado com sucesso!"
		tpliNome = xrs("tpliNome")
		tpliPrefixo = xrs("tpliPrefixo")
		tpliCodigo = xrs("tpliCodigo")
		xRs.CLose
		Set xRs = nothing
	end if
%>

	<form action="cfgTipoListaAlterar1.asp" method="post" name="form1">
		<input type="hidden" name="tpliId" value=" <% = tpliId %> " >
		<table width="100%">
			<tr>
				<td width="15"></td>
				<td></td>
			</tr>
			<tr>				
				<td colspan="2" class="docTit"><% = titulo %></td>
			</tr>
			<tr>
				<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
			</tr>
			<tr>
				<td colspan="2" height="15"></td>
			</tr>
			<tr>
				<td></td>
				<td>
					<table class="doc">
						<tr>
							<td width="100px" class="docLabel">Nome</td>
							<td width="5px" class="docLabel">:</td>
							<td class="docCmpLivre"><input type="text" name="txtNome" maxlength="100" class="docCmpLivre" value="<% = tpliNome %>"></td>
						</tr>
						<tr>
							<td class="docLabel">Prefixo</td>
							<td class="docLabel">:</td>
							<td class="docCmpLivre"><input type="text" name="txtPrefixo" maxlength="4" class="docCmpLivre" value="<% = tpliPrefixo %>"></td>
						</tr>
						<tr>
							<td class="docLabel">C�digo</td>
							<td class="docLabel">:</td>
							<td class="docCmpLivre"><input type="text" name="txtCodigo" maxlength="2" class="docCmpLivre" value="<% = tpliCodigo %>"></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2" height="20"></td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<table>
						<tr>
							<td height="28" width="45%" align="center"><a href="javascript:validar();"><img src="../images/btnSalvar.gif" class="botao"></a></td>
							<td width="10%"></td>
							<td width="45%" align="center"><a href="javascript:window.close();"><img src="../images/btnCancelar.gif" class="botao"></a></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
	</body>
</HTML>