<% Response.Expires= -1%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Controle remoto de projetos</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<link rel="stylesheet" type="text/css" href="../EstiloMenu.css">
		<script language="JavaScript" type="text/javascript" src="../scripts/PreparaJanela.js"></script>
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<script language="JavaScript" type="text/javascript" src="../scripts/AbrirJanela.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/CheckNumeric.js"></script>
		<SCRIPT language="JavaScript">
			function submitform(){
			  document.form1.submit();
			}
			function validar(){
			  if (document.form1.txtNome.value == '')
			  {   alert('O preenchimento do nome � obrigatorio.');
			  	  document.form1.txtNome.select();
				  document.form1.txtNome.focus(); }
			  else
  			  {   if (chkNumeric(document.form1.txtIdJde,0,9999999999,'','','')==false)
			      {   alert('O ID deve ser um valor num�rico');
				      document.form1.txtIdJde.select();
					  document.form1.txtIdJde.focus();}
				  else
					  { submitform();}
			   }
			}
		</SCRIPT> 
	</HEAD>
	<body class="cabecalho" >
<%  lPerfisAutorizados = "15"
	VerificarAcesso()
	VerificarAcesso
   gMenuSelecionado = "Fornecedores"
   gOpcaoSelecionada = "Empreiteiras"
   gmenuGeral = "Configuracao"
   PastaSelecionada = "Configura��o"
	Dim gConexao 
	AbrirConexao
	emprId = Request.Querystring("emprId")
	xSQL = "select emprNome, emprIdJde, emprCnpj , emprEnd, emprCep, emprTel, isnull(perfNome, '(n�o informado)') perfNome "
	xSQL = xSQL & "from (Empreiteira left join PerfilAcesso on PerfilAcesso.perfId = Empreiteira.perfId) where emprId = " & emprId
	set xrs = gConexao.execute(xSQL)
	titulo = "Alterar contratado"
	session("Msg") = "Contratado alterado com sucesso!"
	Session("TituloMsg") = titulo
	emprNome = xrs("emprNome")
	emprIdJde = xrs("emprIdJde")
	emprCnpj = xrs("emprCnpj")
	emprEnd = xrs("emprEnd")
	emprCep = xrs("emprCep")
	emprTel = xrs("emprTel")
	perfNome = xrs("perfNome")
	xRs.CLose
	Set xRs = nothing
%>
		<!--#include file="../includes/cab.inc"-->
			<tr>
				<td style="padding:0px" valign="top" class="blocoOpcoes">
					<!--#include file="../includes/opcConfig.inc"-->
				</td>
				<td colspan="2" valign="top" style="padding:5px" class="blocoPagina">
					<form>
						<table width="100%">
							<tr>
							<td width="15"></td>
							<td width="460"></td>
							<td width="60"></td>
							</tr>
							<tr>				
								<td colspan="3" class="docTit"><%= emprNome %></td>
							</tr>
							<tr>
								<td colspan="3" height="15"></td>
							</tr>
							<tr>				
								<td colspan="2" class="docTit">Dados cadastrais</td>
								<td align="right"><a href="javascript:AbrirJanela('cfgEmprAlterar1.asp?emprId=<%=emprId%>',600,250);"><img src="../images/newitem.gif" class="botao">&nbsp;Alterar</a></td>
							</tr>
							<tr>
								<td colspan="3" background="../images/pont_cinza_h.gif" style="height:2"></td>
							</tr>
							<tr>
								<td colspan="3" height="15"></td>
							</tr>
							<tr>
								<td></td>
								<td colspan="2">
									<table class="doc">
										<tr>
											<td width="100" class="docLabel" style="text-align:left">Id</td>
											<td width="5" class="docLabel">:</td>
											<td class="docCmpLivre"><% = emprIdJde %></td>
										</tr>
										<tr>
											<td width="100" class="docLabel" style="text-align:left">Nome</td>
											<td width="5" class="docLabel">:</td>
											<td class="docCmpLivre"><% = emprNome %></td>
										</tr>
										<tr>
											<td width="100" class="docLabel" style="text-align:left">CNPJ</td>
											<td width="5" class="docLabel">:</td>
											<td class="docCmpLivre"><% = emprCnpj %>&nbsp;</td>
										</tr>
										<tr>
											<td width="100" class="docLabel" style="text-align:left">Endere�o</td>
											<td width="5" class="docLabel">:</td>
											<td class="docCmpLivre"><% = emprEnd %>&nbsp;</td>
										</tr>
										<tr>
											<td width="100" class="docLabel" style="text-align:left">CEP</td>
											<td width="5" class="docLabel">:</td>
											<td class="docCmpLivre"><% = emprCep %>&nbsp;</td>
										</tr>
										<tr>
											<td width="100" class="docLabel" style="text-align:left">Telefone</td>
											<td width="5" class="docLabel">:</td>
											<td class="docCmpLivre"><% = emprTel %>&nbsp;</td>
										</tr>
										<tr>
											<td width="100" class="docLabel" style="text-align:left">Perfil</td>
											<td width="5" class="docLabel">:</td>
											<td class="docCmpLivre"><% = perfNome %></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td colspan="3" height="20"></td>
							</tr>
							<tr>				
								<td colspan="2" class="docTit">Usu�rios</td>
								<td align="right"><a href="javascript:AbrirJanela('cfgFornAlterar1.asp?emprId=<%=emprId%>&usuaId=0',600,250);"><img src="../images/newitem.gif" class="botao">&nbsp;Incluir novo</a></td>
							</tr>
							<tr>
								<td colspan="3" background="../images/pont_cinza_h.gif" style="height:2"></td>
							</tr>
							<tr>
								<td></td>
								<td colspan="2">
									<table class="grid">
		<% 	xCampoOrdem = Request.QueryString("CampoOrdem") 
			if xCampoOrdem = "" then
				xCampoOrdem = "usuaLogi"
			end if
			SortLogi = ""
			SortNome = ""
			SortPerf = ""
			SortEmpr = ""
			SortDt = ""
			SortIp = ""
			if xCampoOrdem = "usuaLogi" then SortLogi = "<img src=""../images/sort.gif"">"
			if xCampoOrdem = "usuaNome" then SortNome = "<img src=""../images/sort.gif"">"
			if xCampoOrdem = "perfNome" then SortPerf = "<img src=""../images/sort.gif"">"
			if xCampoOrdem = "emprNome" then SortEmpr = "<img src=""../images/sort.gif"">"
			if xCampoOrdem = "usuaDtUltimoAcesso" then SortDt = "<img src=""../images/sort.gif"">"
			if xCampoOrdem = "usuaIpUltimoAcesso" then SortIp = "<img src=""../images/sort.gif"">"
		%>
										<tr>
											<td width="20"  class="GridCab">&nbsp;</td>
											<td width="80"  class="GridCab"><a href="cfgFornListar.asp?CampoOrdem=usuaLogi" class="GridCab">Login</a><%=SortLogi%></td>
											<td    class="GridCab"><a href="cfgFornListar.asp?CampoOrdem=usuaNome" class="GridCab">Nome usu�rio</a><%=SortNome%></td>
											<td width="200" class="GridCab"><a href="cfgFornListar.asp?CampoOrdem=perfNome" class="GridCab">Perfil</a><%=SortPerf%></td>
											<td width="120" class="GridCab"><a href="cfgUsuaListar.asp?CampoOrdem=usuaDtUltimoAcesso" class="GridCab">�ltimo acesso</a><%=SortDt%></td>
											<td width="120" class="GridCab"><a href="cfgUsuaListar.asp?CampoOrdem=usuaIpUltimoAcesso" class="GridCab">Origem</a><%=SortIp%></td>
											<td class="GridCab">&nbsp;</td>
										</tr>
			<%
		
			if TemAcesso("USRALT") then
				acessoOk = true
			else
				acessoOk = false
			end if
		
			xSQL = "select usuaID, usuaNome, usuaLogi, perfNome, CONVERT(VARCHAR(10), usuaDtUltimoAcesso, 103) + ' ' + CONVERT(VARCHAR(8), usuaDtUltimoAcesso, 108) as usuaDtUltimoAcesso, usuaIpUltimoAcesso "
			xSQL = xSQL & " from Usuario, PerfilAcesso"
			xSQL = xSQL & " where PerfilAcesso.perfId = Usuario.perfId"
			xSQL = xSQL & "   and Usuario.emprId = " & emprId
			xSQL = xSQL & "   and Usuario.emprId <> 1"
			xSQL = xSQL & "   and Usuario.usuaAtivo = 1"
			xSQL = xSQL & " order by " & xCampoOrdem
			AbrirConexao
			Set xRs = gConexao.Execute(xSQL)
			if xRs.EOF then
				Response.write "<tr>" & vbcrlf
				Response.write "<td colspan=""5"" class=""gridLinhaPar"">Nenhum fornecedor cadastrado</td>" & vbcrlf
				Response.write "</tr>" & vbcrlf
			else
				estilo = "GridLinhaPar"
				while not xRs.EOF
					Response.write "<tr>" & vbcrlf
					if acessoOk then
						response.write vbtab & "<td class=""" & estilo & """><a href=""javascript:AbrirJanela('cfgFornAlterar1.asp?usuaId=" & xRs("usuaId") & "&emprId=" & emprId & "',440,270);""><img src=""../images/folder.gif"" style=""border:0""></a></td>"
					else
						response.write vbtab & "<td class=""" & estilo & """><img src=""../images/folder.gif"" style=""border:0""></td>"
					end if
					response.write vbtab & "<td class=""" & estilo & """>" & xRs("usuaLogi") & "</td>"
					response.write vbtab & "<td class=""" & estilo & """>" & xRs("usuaNome") & "</td>"
					response.write vbtab & "<td class=""" & estilo & """>" & xRs("perfNome") & "</td>"
					response.write vbtab & "<td class=""" & estilo & """>" & xRs("usuaDtUltimoAcesso") & "</td>"
					response.write vbtab & "<td class=""" & estilo & """>" & xRs("usuaIpUltimoAcesso") & "</td>"
					response.write vbtab & "<td class=""" & estilo & """><a href=""javascript:AbrirJanela('cfgUsuaExcluir1.asp?usuaId=" & xRs("usuaId") & "',600,250);"" class=""GridCab"" onmouseover=""javascript:document['excluir" & xRs("usuaId")  & "'].src='../images/botaoXA.jpg';"" onmouseout=""javascript:document['excluir" & xRs("usuaId") & "'].src='../images/botaoX.jpg';""><img name=""excluir" & xRs("usuaId") & """ id=""excluir" & xRs("usuaId") & """ src=""../images/botaoX.jpg""></a></td>"
					Response.write "</tr>" & vbcrlf
					xRs.MoveNext
					if estilo = "GridLinhaPar" then
						estilo = "GridLinhaImpar"
					else
						estilo = "GridLinhaPar"
					end if
				wend
			end if
			%>
										<tr>
											<td colspan="7" class="GridRodape">&nbsp;</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
							<tr>
								<td colspan="3" height="20"></td>
							</tr>
							<tr>				
								<td colspan="2" class="docTit">Servi�os homologados</td>
								<td align="right"><a href="javascript:AbrirJanela('cfgEmprServ1.asp?emprId=<%=emprId%>',460,450);"><img src="../images/newitem.gif" class="botao">&nbsp;Alterar</a></td>
							</tr>
							<tr>
								<td colspan="3" background="../images/pont_cinza_h.gif" style="height:2"></td>
							</tr>
							<tr>
								<td></td>
								<td colspan="2">
									<table class="doc">
										<tr>
											<td width="10%"></td><td width="40%"></td>
											<td width="10%"></td><td width="40%"></td>
										</tr>
				<%
					xSQL = ""
					xSQL = xSQL & "select servNome, Servico.servId, emprId"
					xSQL = xSQL & "  from Servico left join EmpreiteiraServico on EmpreiteiraServico.servId = Servico.servId and EmpreiteiraServico.emprId = " & emprId 
					xSQL = xSQL & " order by servNome"
					
					set xrs = gConexao.execute(xSQL)
					x = 1
					Linha = ""
					while not xRs.eof 
						if xRs("emprId") <> "" then
							xSelected = "checked=""1""" 
						else
							xSelected = " "
						end if
						Campo = "<td class=""docSBorda"" align=""right"" style=""vertical-align:top""><input type=""checkbox""  disabled  name=""servId" & xRs("servId") & """ id=""servId" & xRs("servId") & """ " & xSelected & "></td><td class=""docSBorda"" style=""vertical-align:top"">" & xRs("servNome") & "</td>"
						if x = 1 then
							x = 2
							Linha = Campo
						else
							Linha = Linha & Campo
							Linha = "<tr>" & vbcrlf & Linha & vbcrlf & "</tr>" & vbcrlf
							response.write Linha
							x = 1
						end if
						xRs.MoveNext
					wend 
					if x = 2 then
						Linha = "<tr>" & vbcrlf & Linha & vbcrlf & "<td>&nbsp;</td><td>&nbsp;</td></tr>" & vbcrlf
						response.write Linha
					end if		
					xRs.CLose
					Set xRs = nothing
				%>
									</table>

								</td>
							<tr>
								<td colspan="3" height="20"></td>
							</tr>
						</table>
					</form>
				</td>
			</tr>
		<!--#include file="../includes/rod.inc"-->
	</body>
</HTML>