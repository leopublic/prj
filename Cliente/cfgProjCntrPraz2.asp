<% Response.Expires= -1%>
<% Server.ScriptTimeout=60000 %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Controle remoto de projetos</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<!--#include file="../includes/basFormatador.inc"-->
	</HEAD>
	<body class="cabecalho" >
		<!--#include file="../includes/varControleAcesso.inc"-->
		<!--#include file="../includes/varBanco.inc"-->
		<% 
		Dim objConn
		Dim objRs
		Dim lngFileID
		Dim xNomeCampo 
		AbrirConexao
		'
		' Atualiza outros campos informados...
		Dim xTermEst(6)
		xQtdServicos = Request.form("QtdServicos")
		xQtdServicos = cInt(xQtdServicos)
		projId = Request.Form("projId")
		for i = 1 to xQtdServicos
			if Request.form("cntrId" & cStr(i)) <> "" then
				xVlrEst = Request.Form("cntrVlrEst" & cStr(i))
				if xVlrEst = "-" then
					xVlrEst = "0"
				end if
				response.write xVlrEst & "<br>"
				for fase = 1 to 4
					x = Request.form("cntrTermEst" & fase & cStr(i))
					if not isdate(x) then
						xTermEst(fase) = "null"
					else
						xTermEst(fase) = "'" & FmtDataBd(x) & "'"
					end if
				next 
				xSQL = " exec PROJ_AtualizarContrato"
				xSQL = xSQL & "   @pusuaId    = " & Session("usuaId")
				xSQL = xSQL & " , @pcntrId    = " & Request.form("cntrId" & cStr(i))
				xSQL = xSQL & " , @pVlrEst    = " & NumFmtBd(xVlrEst)
				xSQL = xSQL & " , @pTermEst1  = " & xTermEst(1)
				xSQL = xSQL & " , @pTermEst2  = " & xTermEst(2)
				xSQL = xSQL & " , @pTermEst3  = " & xTermEst(3)
				xSQL = xSQL & " , @pTermEst4  = " & xTermEst(4)
				xSQL = xSQL & " , @pTermEst5  = null"
				xSQL = xSQL & " , @pusuaIdCont  = null"
				'response.write (xSQL)
				'response.end()
				gConexao.execute xSQL
			else
				exit for
			end if
		next
		Session("Msg") = "Contratos alterados com sucesso!"
		Response.redirect ("gerMensagem.asp")
		%>
	</body>
</HTML>