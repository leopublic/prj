<% Response.Expires= -1%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Controle de projetos pela internet</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<link rel="stylesheet" type="text/css" href="../EstiloMenu.css">
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<!--#include file="../includes/basFormatador.inc"-->
		<script language="JavaScript" type="text/javascript" src="../scripts/AbrirJanela.js"></script>
		<SCRIPT language="javascript">
		<!--
			//-->
		</SCRIPT>
	</HEAD>
	<body class="cabecalho" >
		<!--#include file="../includes/varControleAcesso.inc"-->
		<!--#include file="../includes/varBanco.inc"-->
		<!--#include file="../includes/varDatas.inc"-->

<%  lPerfisAutorizados = "15"
	VerificarAcesso()

   gMenuSelecionado = "ProjetoX"
   gOpcaoSelecionada = "Projetos"
   gItemSelecionado = "Projeto"
   gmenuGeral = "Projetos"

	AbrirConexao
	projId = Request.Querystring("projId")
	xSQL = ""
	xSQL = xSQL & "select projNome, projNum, projNomeSup, projNomeGer"
	xSQL = xSQL & " from Projeto  "
	xSQL = xSQL & " where Projeto.projId = " & projId
	set xrs = gConexao.execute(xSQL)
	projNome = xRs("projNome")
	projNum = xRs("projNum")
	projNomeSup = xRs("projNomeSup")
	projNomeGer = xRs("projNomeGer")
	xRs.CLose
	Set xRs = nothing
%>
		<table width="790" align="center" height="90%" cellpadding="0" cellspacing="0" style="border-style:solid;border-width:0;border-color:black">
			<tr>
				<td width="9" bgcolor="#363459" height="1px" style="font-size:1px" >&nbsp;</td>
				<td width="170" bgcolor="#363459" style="font-size:1px"  >&nbsp;</td>
				<td width="600" bgcolor="#363459" style="font-size:1px"  >&nbsp;</td>
				<td width="19"  bgcolor="#363459"  style="font-size:1px">&nbsp;</td>
			</tr>
			<tr>
				<td height="50" colspan="4" background="../images/fundo2.jpg" STYLE="background-position:top left; background-repeat:no-repeat">&nbsp;</td>
			</tr>
			<tr>
				<td height="12" colspan="4" bgcolor="black" style="padding:0;color:white;font-size:8pt;">&nbsp;&nbsp;<% = "Usu�rio: " & Session("usuaNome") %></td>
			</tr>
			<tr>
				<td width="auto" colspan="4" height="8px" style="font-size:1px" >
					<!--#include file="../includes/menuAreas.inc"-->
				</td>
			</tr>
			<tr>
				<td colspan="3" width="auto" height="5px" style="text-align:left;font-size:1px;font-weight:bold;font-family:verdana;color:white;padding-left:6px"  bgcolor="#5B89A4" bordercolor="#000000">&nbsp;</td>
				<td width="auto" style="font-size:1px"   bgcolor="#94BDD1" background="../images/barraDirTope2.jpg">&nbsp;</td>
			</tr>
			<tr>
				<td width="auto" height="9px" style="font-size:1px" background="../images/barraEsqMenu2.jpg">&nbsp;</td>
				<td width="auto" style="font-size:1px"  background="../images/barraMenu2.jpg">&nbsp;</td>
				<td width="auto" style="font-size:1px"  background="../images/barra2.jpg" align="right">&nbsp;</td>
				<td width="auto" style="font-size:1px"  background="../images/barraDir2.jpg">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2" rowspan="2" width="auto" style="padding:0px" valign="top" class="blocoOpcoes">
					<!--#include file="../includes/opcProjX2.inc"-->
				</td>
				<td colspan="2" width="auto" valign="top" style="padding:5px" class="blocoPagina">
				<form>
					<table width="100%">
						<tr>
							<td width="15"></td>
							<td width="460"></td>
							<td width="60"></td>
						</tr>
						<tr>
							<td colspan="3" class="docTit">DETALHES DO PROJETO</td>
						</tr>
						<tr>
							<td colspan="3" style="height:5"></td>
						</tr>
						<tr>
							<td colspan="2" class="docTit">Informa��es b�sicas</td>
							<td class="GridBotao" background="../images/btnAlterar.gif" onClick="javascript:AbrirJanela('gerAdicionarProjeto.asp?projId=<% = projId %>',470,450);">&nbsp;</td>
						</tr>
						<tr>
							<td colspan="3" background="../images/pont_cinza_h.gif" style="height:2"></td>
						</tr>
						<tr>
							<td colspan="3" style="height:10"></td>
						</tr>
						<tr>
							<td></td>
							<td colspan="2" align="left">
								<table class="doc">
									<tr>
										<td width="120" class="docLabel">N�mero</td>
										<td width="05" class="docLabel">:</td>
										<td class="docCmpLivre"><% = projNum %></td>
									</tr>
									<tr>
										<td class="docLabel">Nome</td>
										<td class="docLabel">:</td>
										<td class="docCmpLivre"><% = projNome %></td>
									</tr>
									<tr>
										<td class="docLabel">Gerente do projeto</td>
										<td class="docLabel">:</td>
										<td class="docCmpLivre"><% = projNomeGer %></td>
									</tr>
									<tr>
										<td class="docLabel">Supervisor de constru��es</td>
										<td class="docLabel">:</td>
										<td class="docCmpLivre"><% = projNomeSup %></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="3" style="height:15"></td>
						</tr>
						<tr>
							<td colspan="3" class="docTit">Estimativas iniciais (baselines)</td>
						</tr>
						<tr>
							<td colspan="3" background="../images/pont_cinza_h.gif" style="height:2"></td>
						</tr>
						<tr>
							<td colspan="3" style="height:10"></td>
						</tr>
						<tr>
							<td></td>
							<td colspan="2">
								<table width="100%">
									<tr>
										<td></td>
										<td width="2"></td>
										<td width="80"></td>
										<td width="2"></td>
										<td width="90"></td>
										<td width="90"></td>
										<td width="90"></td>
										<td width="90"></td>
									</tr>
									<tr>
										<td class="docSBorda"></td>
										<td></td>
										<td class="GridCabGrupo" align="center">CUSTO</td>
										<td></td>
										<td class="GridCabGrupo" align="center" colspan="4">PRAZOS</td>
									</tr>
									<tr>
										<td class="GridCab">&nbsp;</td>
										<td></td>
										<td class="GridCab" align="center" valign="middle">Valor<br>estimado<br>(<%=Session("moedSigla")%>)</td>
										<td></td>
										<td class="GridCab" align="center" valign="middle">Emiss�o<br>MD</td>
										<td class="GridCab" align="center" valign="middle">Contrata��o</td>
										<td class="GridCab" align="center" valign="middle">Mobiliza��o</td>
										<td class="GridCab" align="center" valign="middle">Desmobil.</td>
									</tr>
										<%
										i = 0
										xTermEst = ""
										xsql = ""
										xSQL = xsql & "select Contrato.cntrNomeServ"
										xSQL = xSQL & "     , Contrato.cntrVlrEst"
										xSQL = xSQL & "     , convert(varchar(8), Contrato.cntrTermEst1, 12) cntrTermEst1"
										xSQL = xSQL & "     , convert(varchar(8), Contrato.cntrTermEst2, 12) cntrTermEst2"
										xSQL = xSQL & "     , convert(varchar(8), Contrato.cntrTermEst3, 12) cntrTermEst3"
										xSQL = xSQL & "     , convert(varchar(8), Contrato.cntrTermEst4, 12) cntrTermEst4"
										xsql = xsql & "     , isnull(Empreiteira.emprNome, '(n�o definido)') emprNome"
										xsql = xsql & "  from (Contrato left join Empreiteira on Empreiteira.emprId = Contrato.emprId)"
										xSQL = xSQL & " where Contrato.projId = " & projId
										xsql = xsql & " order by cntrId"
										set xRs = gConexao.execute(xsQL)
										if xRs.eof then
											xTermEst = xTermEst & "<tr>" & vbcrlf
											xTermEst = xTermEst & vbtab & "<td colspan=""6"" class=""GridLinha"">(nenhum servico alocado ao projeto)</td>" & vbcrlf
											xTermEst = xTermEst & "<tr>" & vbcrlf
										else
											while not xRs.Eof
												i = i + 1
												xTermEst = xTermEst & "<tr>" & vbcrlf
												xTermEst = xTermEst & vbtab & "<td class=""GridCabLinha"">" & xRs("cntrNomeServ") & "</td>" & vbcrlf
												xTermEst = xTermEst & vbtab & "<td></td>" & vbcrlf
												xTermEst = xTermEst & vbtab & "<td class=""GridLinha"" align=""right"">" &  NumFmt(xRs("cntrVlrEst")) & "</td>" & vbcrlf
												xTermEst = xTermEst & vbtab & "<td></td>" & vbcrlf
												xTermEst = xTermEst & vbtab & "<td class=""GridLinha"" align=""center"">" &  DtFmt(xRs("cntrTermEst1")) & "</td>" & vbcrlf
												xTermEst = xTermEst & vbtab & "<td class=""GridLinha"" align=""center"">" &  DtFmt(xRs("cntrTermEst2")) & "</td>" & vbcrlf
												xTermEst = xTermEst & vbtab & "<td class=""GridLinha"" align=""center"">" &  DtFmt(xRs("cntrTermEst3")) & "</td>" & vbcrlf
												xTermEst = xTermEst & vbtab & "<td class=""GridLinha"" align=""center"">" &  DtFmt(xRs("cntrTermEst4")) & "</td>" & vbcrlf
												xTermEst = xTermEst & "<tr>" & vbcrlf
												xRs.Movenext
											wend
										end if
										xRs.Close
										set xRs = Nothing
										Response.Write(xTermEst)
										TamJanela = 240 + (25 * i)
									 %>
									<tr>
										<td class="GridRodape">&nbsp;</td>
										<td></td>
										<td class="GridRodape">&nbsp;</td>
										<td></td>
										<td class="GridRodape">&nbsp;</td>
										<td class="GridRodape">&nbsp;</td>
										<td class="GridRodape">&nbsp;</td>
										<td class="GridRodape">&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="3" style="height:15"></td>
						</tr>
						<tr>
							<td colspan="3" class="docTit">Documentos</td>
						</tr>
						<tr>
							<td colspan="3" background="../images/pont_cinza_h.gif" style="height:2"></td>
						</tr>
						<tr>
							<td colspan="3" style="height:10"></td>
						</tr>
						<tr>
							<td></td>
							<td colspan="2">
								<table width="100%">
									<tr>
										<td width="90"></td>
										<td width="150"></td>
										<td width="120"></td>
										<td width="90"></td>
										<td width="60"></td>
										<td width="60"></td>
									</tr>
									<tr>
										<td class="GridCab">&nbsp;</td>
										<td class="GridCab">Nome do arquivo</td>
										<td class="GridCab">Carregado por</td>
										<td class="GridCab">em</td>
										<td class="GridCab" align="center" valign="middle">&nbsp;</td>
										<td class="GridCab" align="center" valign="middle">&nbsp;</td>
									</tr>
									<tr>
										<td class="GridCabLinha">Cronograma</td>
										<td class="GridLinha" valign="middle"><% = NomeOrigPrazo%></td>
										<td class="GridLinha" valign="middle"><% = UsuaPrazo %></td>
										<td class="GridLinha" valign="middle"><% = dtUpldPrazo %></td>
									<% If AcaoPrazo <> "" then %>
										<td class="GridBotao"><img src="../images/btnAbrir.gif" onclick="<% = AcaoPrazo %>" class="botao"></td>
									<% else %>
									    <td class="GridLinha">&nbsp;</td>
									<% end if %>
										<td class="GridBotao" background="../images/btnAlterar.gif" onClick="javascript:AbrirJanela('gerAdicionarBSP.asp?projId=<% = projId  %>&Carregar=1',600,<% = TamJanela %>);" ></td>
									</tr>
									<tr>
										<td class="GridCabLinha">PCHC</td>
										<td class="GridLinha" valign="middle"><% = NomeOrigCusto%></td>
										<td class="GridLinha" valign="middle"><% = UsuaCusto %></td>
										<td class="GridLinha" valign="middle"><% = dtUpldCusto %></td>
									<% If AcaoCusto <> "" then %>
										<td class="GridBotao"><img src="../images/btnAbrir.jpg" onclick="<% = AcaoCusto %>" class="botao"></td>
									<% else %>
									    <td class="GridLinha">&nbsp;</td>
									<% end if %>
										<td class="GridBotao" background="../images/btnAlterar.gif" onClick="javascript:AbrirJanela('gerAdicionarBSC.asp?projId=<% = projId  %>&Carregar=1',600,<% = TamJanela %>);" ></td>
									</tr>
									<tr>
										<td class="GridRodape">&nbsp;</td>
										<td class="GridRodape">&nbsp;</td>
										<td class="GridRodape">&nbsp;</td>
										<td class="GridRodape">&nbsp;</td>
									<!--	<td class="GridRodape">&nbsp;</td>-->
										<td class="GridRodape">&nbsp;</td>
										<td class="GridRodape">&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
<!--						<tr>
							<td></td>
							<td colspan="2" align="left">
								<table class="doc">
									<tr>
										<td width="120" class="docLabel">Cronograma</td>
										<td width="05" class="docLabel">:</td>
										<td class="docCmpLivre"><a <% = PathAnexPrazo %> class="docAnex"><% = NomeOrigPrazo %></a></td>
										<td width="60" class="docBotao" background="../images/btnAlterar.gif" onClick="javascript:AbrirJanela('gerAltBaseCrono.asp?projId=<% = projId  %>&Carregar=1',600,300);" ></td>
									</tr>
									<tr>
										<td colspan="4" height="10"></td>
									</tr>
									<tr>
										<td class="docLabel">PCHC</td>
										<td class="docLabel">:</td>
										<td class="docCmpLivre"><a <% = PathAnexCusto %> class="docAnex"><% = NomeOrigCusto %></a></td>
										<td class="docBotao" background="../images/btnAlterar.gif" onClick="javascript:AbrirJanela('gerAltBaseCusto.asp?projId=<% = projId  %>');" ></td>
									</tr>
								</table>
							</td>
						</tr>-->
					</table>
				</form>
				</td>
			</tr>

		</table>
	</body>
</HTML>