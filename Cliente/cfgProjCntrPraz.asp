<% Response.Expires= -1%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Projetos</title><%%>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<!--#include file="../includes/basFormatador.inc"-->
		<!--#include file="../includes/basValidacao.inc"-->
		<!--#include file="../includes/clsContrato.inc"-->
		<script language="JavaScript" type="text/javascript" src="../scripts/NumberFormat.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/Mid.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/ValidacaoDatas.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/PreparaJanela.js"></script>
		<SCRIPT language="JavaScript">
			function AbrirJanela(url, win_width, win_height) { //v2.0
				var w = screen.width; // Get the width of the screen
				var h = screen.height; // Get the height of the screen
			
				// Where to place the Window
				var left = (w - win_width)/2;
				var top = (h - win_height)/2;
			
				posicao = "width=" + win_width + ",height=" + win_height ;
				posicao += ",top=" + top + ",left=" + left + ",screenX=" + left + ",screenY=" + top;			
				parametros = posicao + ",directories=no,location=no,menubar=no,scrollbars=no,status=yes,toolbar=no,resizable=no;"; 
				titulo = "JanelaAdicionar" ; 
				window.open(url,titulo,parametros);
			}
			function CamposOk(){
				var QtdCntr = document.form.QtdServicos.value
			    for(i=1;i<=QtdCntr;i++)
				{
					for(j=1;j<=4;j++)
					{   eval('var Cmp = document.form.cntrTermEst'+j+i)
					    if(Cmp.value!='')
						{	if(isDate(Cmp)==false)
 							{	Cmp.select();
								Cmp.focus();
								return false;
							}
						}
					}
				}
				return true
			}
			function Formatar(campo){
				var inputValue = campo;
				inputValue.value = FormatarNumero(inputValue.value, '.', ',', ',');
			}

			function Validar(){
                if (CamposOk()==true)
			    { document.form.submit();}
			}
		</SCRIPT> 
<%
			Public Function MontaComboEmpr(pServInd, pEmprId)
				xEmpr = "<select name=""cmbEmprId" & pServInd & """ class=""docCmpLivre"">" & vbcrlf
				xEmpr = xEmpr & "<option value=""null"" class=""docCmpLivre"">(n�o definido)</option>" & vbcrlf
				for k = 1 to qtdForn
					if emprIdArray(k) = pEmprId then
						xChecked = " selected "
					else
						xChecked = ""
					end if
					xEmpr = xEmpr & "<option value=""" & emprIdArray(k) & """ " & xChecked & " class=""docCmpLivre"">" & emprNomeArray(k) & "</option>" & vbcrlf
				next
				xEmpr = xEmpr & "</select>" & vbcrlf
				MontaComboEmpr = xEmpr
			End Function

			Public Function MontaComboServ(pServInd, pServId)
				xServ = "<select name=""cmbServId" & pServInd & """ class=""docCmpLivre"" style=""width:120"">" & vbcrlf
				for k = 1 to qtdServ
					if servIdArray(k) = pServId then
						xChecked = " selected "
					else
						xChecked = ""
					end if
					xServ = xServ & "<option value=""" & ServIdArray(k) & """ " & xChecked & " class=""docCmpLivre"">" & servNomeArray(k) & "</option>" & vbcrlf
				next
				xServ = xServ & "</select>" & vbcrlf
				MontaComboServ = xServ
			End Function
%>		
	</HEAD>
	<body class="PopUpEntrada" onLoad="javascript:PreparaJanela(680, 200 + (document.form.QtdServicos.value) * 30);">
		<!--#include file="../includes/varControleAcesso.inc"-->
		<!--#include file="../includes/varBanco.inc"-->
<% 
    lPerfisAutorizados = "51"
	VerificarAcesso
	AbrirConexao
	projId = Request.Querystring("projID")
	xSQL = ""
	xSQL = xSQL & "select projNome, projNum"
	xSQL = xSQL & " from Projeto "
	xSQL = xSQL & " where Projeto.projId = " & projId
	set xrs = gConexao.execute(xSQL)
	projNome = xRs("projNome")
	projNum = xRs("projNum")
	Session("projId") = projId
	Session("projNome") = projNome
	Session("TituloMsg") = Session("projNome") & " - Alterar contratos"
	xRs.CLose
	Set xRs = nothing
%>

	<form onSubmit="Validar();" action="cfgProjCntrPraz2.asp" method="post" name="form">
		<input type="hidden" name="projId" value="<% = projId %>" >
		<table width="100%">
			<tr>
				<td width="15"></td>
				<td></td>
			</tr>
			<tr>
				<td colspan="2">
					<table width="100%">
						<tr>
							<td width="400" class="docTit"><%=Session("TituloMsg")%></td>
							<td align="right"><a href="javascript:AbrirJanela('cfgProjCntrNovo.asp?projId=<%=projId%>',500,250);"><img src="../images/newitem.gif">&nbsp;Adicionar novo</a></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
			</tr>
			<tr>
				<td colspan="2" height="15"></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td align="left">
					<table class="grid" style="padding:5" width="100%">
						<tr>
							<td width="25%" class="GridCab" align="Left">Servi�o</td>
							<td width="23%" class="GridCab" align="center">Valor est.</td>
							<td width="13%" class="GridCab" align="center">Emiss�o MD</td>
							<td width="13%" class="GridCab" align="center">Contrata��o</td>
							<td width="13%" class="GridCab" align="center">Mobiliza��o</td>
							<td width="13%" class="GridCab" align="center">Desmob.</td>
						</tr>
				<%
				xSQL= "select count(*) from Empreiteira"
				set xRs = gConexao.execute(xSQL)
				qtdForn = xRs(0)
				xRs.close
				Dim emprNomeArray()
				Redim emprNomeArray(qtdForn)
				Dim emprIdArray()
				Redim emprIdArray(qtdForn)
				xSQL = ""
				xSQL = xSQL & "select emprId "
				xSQL = xSQL & "     , emprNome "
				xSQL = xSQL & "  from Empreiteira "
				xSQL = xSQL & " where emprEdit = 1"
				xSQL = xSQL & " order by emprNome"
				set xRs = gConexao.execute(xsQL)
				i = 0
				while not xRs.eof
					i = i + 1
					emprIdArray(i) = cstr(xRs("emprId"))
					emprNomeArray(i) = cstr(xRs("emprNome"))
					xRs.Movenext
				wend
				xRs.Close
				Set xRs = nothing

				xSQL= "select count(*) from Servico"
				set xRs = gConexao.execute(xSQL)
				qtdServ = xRs(0)
				xRs.close
				Dim servNomeArray()
				Redim servNomeArray(qtdServ)
				Dim servIdArray()
				Redim servIdArray(qtdServ)
				xSQL = ""
				xSQL = xSQL & "select servId "
				xSQL = xSQL & "     , servNome "
				xSQL = xSQL & "  from Servico "
				xSQL = xSQL & " order by servOrd"
				set xRs = gConexao.execute(xsQL)
				i = 0
				while not xRs.eof
					i = i + 1
					servIdArray(i) = cstr(xRs("servId"))
					servNomeArray(i) = cstr(xRs("servNome"))
					xRs.Movenext
				wend
				xRs.Close
				Set xRs = nothing
				
				xsql = "select * from vContratosBaseline where projId = " & projId & " order by TermEst1, servOrd"
				set xRs = gConexao.execute(xSQL)
				xServ = ""
				i = 0
				on error resume next
				while not xRs.Eof
					i = i + 1
					j = cStr(i)
					xcntrTermEst1 = xRs("cntrTermEst1")
					xcntrTermEst2 = xRs("cntrTermEst2")
					xcntrTermEst3 = xRs("cntrTermEst3")
					xcntrTermEst4 = xRs("cntrTermEst4")
					cntrVlrEst = xRs("cntrVlrEst")
					emprId = xRs("emprId")
					emprId = cStr(emprId)
					servId = xRs("servId")
					servId = cStr(servId)
					xServ = xServ & "<tr>" & vbcrlf
					xServ = xServ & "<input type=""hidden"" name=""cntrId"& j & """ value=""" & xRs("cntrId") & """>"
					xServ = xServ & vbtab & "<td class=""gridcablinha"" align=""left"">" & xRs("servNome") & "</td>" & vbcrlf
					xServ = xServ & vbtab & "<td class=""gridlinha""><input type=""text"" name=""cntrVlrEst" & j & """ value=""" & FormatNumber(xRs("cntrVlrEst")) & """ class=""docCmpLivre"" style=""height:20;border:1;border-style:solid;border-color:#CCCCCC;text-align:right"" onBlur=""javascript:Formatar(this);""></td>" & vbcrlf
					xServ = xServ & vbtab & "<td class=""gridlinha""><input type=""text"" name=""cntrTermEst1" & j & """ value=""" & xcntrTermEst1 & """ class=""docCmpLivre"" style=""height:20;border:1;border-style:solid;border-color:#CCCCCC;text-align:center"" onblur=""javascript:this.value=FormatarData(this);""></td>" & vbcrlf
					xServ = xServ & vbtab & "<td class=""gridlinha""><input type=""text"" name=""cntrTermEst2" & j & """ value=""" & xcntrTermEst2 & """ class=""docCmpLivre"" style=""height:20;border:1;border-style:solid;border-color:#CCCCCC;text-align:center"" onblur=""javascript:this.value=FormatarData(this);""></td>" & vbcrlf
					xServ = xServ & vbtab & "<td class=""gridlinha""><input type=""text"" name=""cntrTermEst3" & j & """ value=""" & xcntrTermEst3 & """ class=""docCmpLivre"" style=""height:20;border:1;border-style:solid;border-color:#CCCCCC;text-align:center"" onblur=""javascript:this.value=FormatarData(this);""></td>" & vbcrlf
					xServ = xServ & vbtab & "<td class=""gridlinha""><input type=""text"" name=""cntrTermEst4" & j & """ value=""" & xcntrTermEst4 & """ class=""docCmpLivre"" style=""height:20;border:1;border-style:solid;border-color:#CCCCCC;text-align:center"" onblur=""javascript:this.value=FormatarData(this);""></td>" & vbcrlf
					xServ = xServ & "</tr>" & vbcrlf
					xRs.Movenext
				wend
				xRs.Close
				set xRs = Nothing								
				Response.write xServ
				%>
				<input type="hidden" name="QtdServicos" value="<% = i %>">
						<tr>
							<td colspan="6" class="Gridrodape">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2" height="15"></td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<table>
						<tr>
							<td height="28" width="80" align="center"><img border="0" src="../images/btnSalvar.gif" class="botao" onclick="javascript:Validar();"></td>
							<td width="10"></td>
							<td width="80" align="center"><img border="0" src="../images/btnCancelar.gif" class="botao" onclick="javascript:window.close();"></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
	</body>
</HTML>