<% Server.ScriptTimeout=60000 %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Controle remoto de projetos</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
	</HEAD>
	<body class="cabecalho" >
		<!--#include file="../includes/varControleAcesso.inc"-->
		<!--#include file="../includes/varBanco.inc"-->
		<% 
		Dim objUpload
		Dim strFileName
		Dim objConn
		Dim objRs
		Dim lngFileID
		AbrirConexao
		' Instantiate Upload Class
		Set objUpload = Server.CreateObject("SoftArtisans.FileUp")
		objUpload.ProgressID = CInt(Request.QueryString("progressid"))
		servidor = Request.ServerVariables("HTTP_HOST")
		if servidor = "www.prj.com.br" then
			xPath = "e:\home\prj\web\Anexos"
		elseif servidor = "www.m2software.com.br" then
			xPath = "e:\home\m2software\web\whitemartins\prj\Anexos"
		end if
		objUpload.Path = xpath
			
		' Grab the file name
		tpanId = objUpload.form("tpanId")
		projId = objUpload.form("projId")
		cntrId = objUpload.form("cntrId")
		anexId = objUpload.form("anexId")
		usuaId = objUpload.form("usuaId")
		anexRevNum = objUpload.form("txtRevNum")
		anexNome = objUpload.form("txtNome")
		anexNum = objUpload.form("txtNumero")
		if anexNum = "" then
			anexNum = "0"
		end if
		if anexId = "" then
			anexId = "0"
		end if
		strFileName = objUpload.form("txtArquivo").ShortFileName
		if strFileName = "" then
			Session("MsgErro") = "Arquivo n�o informado."
			Session("txtDesc") = anexDesc
			Response.Redirect ("anxUpload1.asp?tpanId=" & tpanId & "&projId=" & projId & "&cntrId=" & cntrId & "anexId=" & anexId)
		end if
		'
		' Cria o anexo
		xSQL = ""
		xSQL = xSQL & "exec ANEX_CriarNovo "
		xSQL = xSQL & "  @ptpanId       =  " & tpanId 
		xSQL = xSQL & ", @pprojId       =  " & projId
		xSQL = xSQL & ", @pcntrId       =  " & cntrId 
		xSQL = xSQL & ", @panexDesc     =  null"
		xSQL = xSQL & ", @pusuaIdUpl    =  " & usuaId
		xSQL = xSQL & ", @panexNomeOrig = '" & replace(strFileName, "'", "''") & "'"
		xSQL = xSQL & ", @panexTam      =      0"
		xSQL = xSQL & ", @panexNum      = '" & anexNum & "'"
		xSQL = xSQL & ", @panexNome     = '" & replace(anexNome, "'", "''") & "'"
		xSQL = xSQL & ", @panexRevNum   = '" & anexRevNum & "'"
		xSQL = xSQL & ", @panexId       =  " & anexId 
		'Response.write(xSQL)
		'Response.End()
		Set xRs = gConexao.execute(xSQL)
		xId = xRs("anexId")
		anexId = Right("00000000" & cStr(xId), 8)
		objUpload.form("txtArquivo").SaveAs anexId '& "." & tparExte
		xTam = objUpload.TotalBytes 
		xSQL = "update Anexo set anexTam = " & xTam & " where anexId = " & xId
		gConexao.execute xSQL 
		Session("TituloMsg") = "Arquivo carregado com sucesso! em " & xpath
		Session("Msg") = "Arquivo carregado com sucesso! em " & xpath
		Response.redirect ("gerMensagem.asp")
		%>
	</body>
</HTML>