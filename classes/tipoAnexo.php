<?php

class tipoAnexo{
    public $tpanId;
    public $tpanProj;
    public $tpanCntr;
    public $tpanGlob;
    public $tpanSigl;
    public $areaId;
    public $tpanDesc;
    public $tpanUnic;
    public $tpanAprv;
    public $tparId;
    public $tpanGen;
    public $tpanForm;
    public $tpanDescTela;
    public $tpanRevNum;
    public $tpanOrdem;
    public $tpanVisivel;

    

    public function recupere($tpanId = ''){
        if ($tpanId != ''){
            $this->tpanId = $tpanId;
        }
        $sql = "select * from TipoAnexo where tpanId = " . $this->tpanId;
        $result = mssql_query($sql);
        if ( $row = mssql_fetch_array($result, MSSQL_BOTH) ){
            $this->tpanId       = $row['tpanId'];
            $this->tpanProj     = $row['tpanProj'];
            $this->tpanCntr     = $row['tpanCntr'];
            $this->tpanGlob     = $row['tpanGlob'];
            $this->tpanSigl     = $row['tpanSigl'];
            $this->areaId       = $row['areaId'];
            $this->tpanDesc     = iconv('CP850', 'UTF-8', $row['tpanDesc']);
            $this->tpanUnic     = $row['tpanUnic'];
            $this->tpanAprv     = $row['tpanAprv'];
            $this->tparId       = $row['tparId'];
            $this->tpanGen      = $row['tpanGen'];
            $this->tpanGlob     = $row['tpanGlob'];
            $this->tpanForm     = $row['tpanForm'];
            $this->tpanDescTela = iconv('CP850', 'UTF-8//TRANSLIT', $row['tpanDescTela']);
            $this->tpanRevNum   = $row['tpanRevNum'];
            $this->tpanOrdem    = $row['tpanOrdem'];
            $this->tpanVisivel  = $row['tpanVisivel'];
        }

    }

}
