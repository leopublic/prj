<% 
if offline="" then %>
		<table class="menu">
			<tr>
				<td width="13" style="height:5px;font-size:1px">&nbsp;</td>
				<td style="height:5px;font-size:1px">&nbsp;</td>
			</tr>
			<tr>
				<td class="opcNor">&nbsp;</td>
				<td class="opcMenu"><a href="prjListarAtivos.asp" class="opcMenu">Projetos</a></td>
			</tr>
			<tr>
				<td class="opcSep">&nbsp;</td>
				<td class="opcSep">&nbsp;</td>
			</tr>
			<tr>
				<td class="opcSep">&nbsp;</td>
				<td class="opcSep">&nbsp;</td>
			</tr>
			<tr>
				<td class="opcSep">&nbsp;</td>
				<td class="opcSep">&nbsp;</td>
			</tr>
	<% 
if session("perfId") <> "7" then
	if TemAcesso("PRJSIT") = True then %>
			<tr>
				<td class="<% = SeletorMenu("PrjResumo") %>">&nbsp;</td>
				<td class="opcMenu"><a href="prjResumo.asp?projId=<%=projId%>" class="opcMenu">Situa��o do projeto</a></td>
			</tr>
			<tr>
				<td class="opcSep">&nbsp;</td>
				<td class="opcSep">&nbsp;</td>
			</tr>
	<% end if%>
	<% if TemAcesso("PRJRELADC") = True then %>
			<tr>
				<td class="<% = SeletorMenu("PrjRelatorios") %>">&nbsp;</td>
				<td class="opcMenu"><a href="prjRelpResumo.asp?projId=<%=projId%>&relpId=0" class="opcMenu">Relat�rios de progresso</a></td>
			</tr>
			<tr>
				<td class="opcSep">&nbsp;</td>
				<td class="opcSep">&nbsp;</td>
			</tr>
		<% 	if gMenuSelecionado = "PrjRelatorios" then %>			
			<tr>
				<td colspan="2">
					<table width="100%">			
						<tr>
							<td width="15" >&nbsp;</td>
							<td width="10" valign="top"><img src="<% = SeletorSubmenu("PrjRelAtual") %>" class="opc"></td>
							<td><a href="prjRelpResumo.asp?projId=<%=projId%>&relpId=0" class="opcSubMenu">Relat�rio em andamento</a></td>
						</tr>
						<tr>
							<td width="15" >&nbsp;</td>
							<td width="10" valign="top"><img src="<% = SeletorSubmenu("PrjRelHistorico") %>" class="opc"></td>
							<td><a href="prjRelpListar.asp?projId=<%=projId%>" class="opcSubMenu">Relat�rios emitidos</a></td>
						</tr>
					</table>
				</td>
			</tr>
			<% end if %>
	<% end if%>
	<% if TemAcesso("PRJCNT") then %>
			<tr>
				<td class="<% = SeletorMenu("PrjConstrucoes") %>">&nbsp;</td>
				<td class="opcMenu"><a href="prjConstrucoes.asp?projId=<%=projId%>" class="opcMenu">Planejamento de Constru��es</a></td>
			</tr>
			<tr>
				<td class="opcSep">&nbsp;</td>
				<td class="opcSep">&nbsp;</td>
			</tr>
	<% end if%>
			<tr>
				<td class="<% = SeletorMenu("PrjQualidade") %>">&nbsp;</td>
				<td class="opcMenu"><a href="prjQualidade.asp?projId=<%=projId%>" class="opcMenu">Qualidade</a></td>
			</tr>
			<tr>
				<td class="opcSep">&nbsp;</td>
				<td class="opcSep">&nbsp;</td>
			</tr>
			<tr>
				<td class="<% = SeletorMenu("PrjSeguranca") %>">&nbsp;</td>
				<td class="opcMenu"><a href="prjSeguranca.asp?projId=<%=projId%>" class="opcMenu">Seguran�a</a></td>
			</tr>
			<tr>
				<td class="opcSep">&nbsp;</td>
				<td class="opcSep">&nbsp;</td>
			</tr>
			<tr>
<%	for i = 1 to TotalServ
		' S� apresenta os servi�os alocados ao projeto.
		Mostra = False
		if Session("perfId") = "3" then
		    if cStr(usuaIdContArray(i)) = cStr(Session("usuaId")) then
				Mostra = true
			end if
	    else
			Mostra = true
		end if
		if Mostra then
					%>
			<tr>
				<td class="<% = SeletorMenu("Contrato"&i) %>">&nbsp;</td>
				<td class="opcMenu"><a href="prjContrato.asp?projId=<%=projId%>&servInd=<%= i%>&area=QLD" class="opcMenu"><% = cntrNomeServArray(i) %></a></td>
			</tr>
			<% 	if i = cint(servInd) then %>			
			<tr>
				<td colspan="2">
					<table width="100%">			
						<tr>
							<td width="15" >&nbsp;</td>
							<td width="10" valign="top"><img src="<% = SeletorSubmenu("Qualidade") %>" class="opc"></td>
							<td><a href="prjContrato.asp?projId=<%=projId%>&servInd=<%= i%>&area=QLD" class="opcSubMenu">Qualidade</a></td>
						</tr>
						<tr>
							<td width="15" >&nbsp;</td>
							<td width="10" valign="top"><img src="<% = SeletorSubmenu("Seguran�a") %>" class="opc"></td>
							<td><a href="prjContrato.asp?projId=<%=projId%>&servInd=<%= i%>&area=SEG" class="opcSubMenu">Seguran�a</a></td>
						</tr>
						<tr>
							<td width="15" >&nbsp;</td>
							<td width="10" valign="top"><img src="<% = SeletorSubmenu("Planejamento") %>" class="opc"></td>
							<td><a href="prjContrato.asp?projId=<%=projId%>&servInd=<%= i%>&area=PLN" class="opcSubMenu">Planejamento</a></td>
						</tr>
						
						<%
						if TemAcesso("CNTWBS") then
							set lRS_1 = gConexao.execute("select isnull(max(w.wbscId),0) as wbscId from WbsContrato w where w.cntrId = " & cntrIdArray(i))
							lwbscId = ""
							if (not lRS_1.EOF) and (not lRS_1.BOF) then
								if lRS_1("wbscId") > 0 then
									lwbscId = "&wbscId=" & lRS_1("wbscId")
								end if
							end if
							lRS_1.Close
							set lRS_1 = nothing
										
							ln = ln + 1
									
							llink_wbs = "<a href=""wbscontrato.asp?projId=" & projId & "&cntrId=" & cntrIdArray(i) & lwbscId & "&servInd=" & i & """ class=""opcSubMenu""/>Lista WBS</a>"
							%>
							
							<tr>
								<td width="15" >&nbsp;</td>
								<td width="10" valign="top"><img src="../images/nav_pt.gif" class="opc"></td>
								<td><%=llink_wbs%></td>
							</tr>
						<% end if	%>
						
					</table>
				</td>
			</tr>
			<% end if %>
			<tr>
				<td class="opcSep">&nbsp;</td>
				<td class="opcSep">&nbsp;</td>
			</tr>
<%		end if
	next 
%>
			<tr>
				<td class="opcSep">&nbsp;</td>
				<td class="opcSep">&nbsp;</td>
			</tr>
			<tr>
				<td class="opcSep">&nbsp;</td>
				<td class="opcSep">&nbsp;</td>
			</tr>
<%

end if
if TemAcesso("LISTAS") then
%>

			<tr>
				<td class="<% = SeletorMenu("prjListas") %>">&nbsp;</td>
				<td class="opcMenu"><a href="prjListListar.asp?projId=<% =projId %>" class="opcMenu"/>Listas</a></td>
			</tr>
			<tr>
				<td class="opcSep">&nbsp;</td>
				<td class="opcSep">&nbsp;</td>
			</tr>
			<tr>
				<td class="<% = SeletorMenu("prjNaoConformidade") %>">&nbsp;</td>
				<td class="opcMenu"><a href="prjNconListar.asp?projId=<% =projId %>" class="opcMenu"/>N�o-Conformidades</a></td>
			</tr>
			<tr>
				<td class="opcSep">&nbsp;</td>
				<td class="opcSep">&nbsp;</td>
			</tr>
			<tr>
				<td class="<% = SeletorMenu("prjRequListar") %>">&nbsp;</td>
				<td class="opcMenu"><a href="prjRequListar.asp?projId=<% =projId %>" class="opcMenu"/>Requisi��es de Compra</a></td>
			</tr>
			<tr>
				<td class="opcSep">&nbsp;</td>
				<td class="opcSep">&nbsp;</td>
			</tr>
			<tr>
				<td class="<% = SeletorMenu("prjPedcListar") %>">&nbsp;</td>
				<td class="opcMenu"><a href="prjPedcListar.asp?projId=<% =projId %>" class="opcMenu"/>Pedidos de Compra</a></td>
			</tr>
			<tr>
				<td class="opcSep">&nbsp;</td>
				<td class="opcSep">&nbsp;</td>
			</tr>
			<tr>
				<td class="<% = SeletorMenu("prjReceListar") %>">&nbsp;</td>
				<td class="opcMenu"><a href="prjReceListar.asp?projId=<% =projId %>" class="opcMenu"/>Recebimentos de Nota Fiscal</a></td>
			</tr>
			<tr>
				<td class="opcSep">&nbsp;</td>
				<td class="opcSep">&nbsp;</td>
			</tr>
			<tr>
				<td class="<% = SeletorMenu("prjSaidListar") %>">&nbsp;</td>
				<td class="opcMenu"><a href="prjSaidListar.asp?projId=<% =projId %>" class="opcMenu"/>Sa�da de material</a></td>
			</tr>
			<tr>
				<td class="opcSep">&nbsp;</td>
				<td class="opcSep">&nbsp;</td>
			</tr>
			<tr>
				<td class="opcSep">&nbsp;</td>
				<td class="opcSep">&nbsp;</td>
			</tr>
			<tr>
				<td class="<% = SeletorMenu("prjRelatorioMat") %>">&nbsp;</td>
				<td class="opcMenu"><a href="prjRelPendComp.asp?projId=<%=projId%>&area=PENDC" class="opcMenu"/>Relat�rios</a></td>
			</tr>
			<% 	if gMenuSelecionado = "prjRelatorioMat" then %>			
			<tr>
				<td colspan="2">
					<table width="100%">			
						<tr>
							<td width="15" >&nbsp;</td>
							<td width="10" valign="top"><img src="<% = SeletorSubmenu("PENDC") %>" class="opc"></td>
							<td><a href="prjRelPendComp.asp?projId=<%=projId%>&area=PENDC" class="opcSubMenu">Pend�ncias de Compras</a></td>
						</tr>
						<tr>
							<td width="15" >&nbsp;</td>
							<td width="10" valign="top"><img src="<% = SeletorSubmenu("PENDREC") %>" class="opc"></td>
							<td><a href="prjRelPendRece.asp?projId=<%=projId%>&area=PENDREC" class="opcSubMenu">Pend�ncias de Recebimento</a></td>
						</tr>
						<tr>
							<td width="15" >&nbsp;</td>
							<td width="10" valign="top"><img src="<% = SeletorSubmenu("RMRC") %>" class="opc"></td>
							<td><a href="prjRelRMRC.asp?projId=<%=projId%>&area=RMRC" class="opcSubMenu">RMRC</a></td>
						</tr>
						<tr>
							<td width="15" >&nbsp;</td>
							<td width="10" valign="top"><img src="<% = SeletorSubmenu("REGSOBRA") %>" class="opc"></td>
							<td><a href="prjRelRegSobra.asp?projId=<%=projId%>&area=REGSOBRA" class="opcSubMenu">Registro de Sobra de Material</a></td>
						</tr>
					</table>
				</td>
			</tr>
			<% 	end if %>			
			
		</table>
<% end if%>
<% else 
'*========================================================================*
'*== A PARTIR DAQUI � O MENU PARA A VERS�O OFFLINE !!!                    *
'*========================================================================*
%>
		<table class="menu">
			<tr>
				<td width="13" style="height:5px;font-size:1px">&nbsp;</td>
				<td style="height:5px;font-size:1px">&nbsp;</td>
			</tr>
			<tr>
				<td class="opcSep">&nbsp;</td>
				<td class="opcSep">&nbsp;</td>
			</tr>
	<% if TemAcesso("PRJSIT") = True then %>
			<tr>
				<td class="<% = SeletorMenu("PrjResumo") %>">&nbsp;</td>
				<td class="opcMenu"><a href="prjResumo.html" class="opcMenu">Situa��o do projeto</a></td>
			</tr>
			<tr>
				<td class="opcSep">&nbsp;</td>
				<td class="opcSep">&nbsp;</td>
			</tr>
	<% end if%>
	<% if TemAcesso("PRJRELADC") = True then %>
			<tr>
				<td class="<% = SeletorMenu("PrjRelatorios") %>">&nbsp;</td>
				<td class="opcMenu"><a href="prjRelpResumo0.html" class="opcMenu">Relat�rios de progresso</a></td>
			</tr>
			<tr>
				<td class="opcSep">&nbsp;</td>
				<td class="opcSep">&nbsp;</td>
			</tr>
		<% 	if gMenuSelecionado = "PrjRelatorios" then %>			
			<tr>
				<td colspan="2">
					<table width="100%">			
						<tr>
							<td width="15" >&nbsp;</td>
							<td width="10" valign="top"><img src="<% = SeletorSubmenu("PrjRelAtual") %>" class="opc"></td>
							<td><a href="prjRelpResumo0.html" class="opcSubMenu">Relat�rio em andamento</a></td>
						</tr>
						<tr>
							<td width="15" >&nbsp;</td>
							<td width="10" valign="top"><img src="<% = SeletorSubmenu("PrjRelHistorico") %>" class="opc"></td>
							<td><a href="prjRelpListar.html" class="opcSubMenu">Relat�rios emitidos</a></td>
						</tr>
					</table>
				</td>
			</tr>
			<% end if %>
	<% end if%>
	<% if TemAcesso("PRJCNT") then %>
			<tr>
				<td class="<% = SeletorMenu("PrjConstrucoes") %>">&nbsp;</td>
				<td class="opcMenu"><a href="prjConstrucoes.html" class="opcMenu">Planejamento de Constru��es</a></td>
			</tr>
			<tr>
				<td class="opcSep">&nbsp;</td>
				<td class="opcSep">&nbsp;</td>
			</tr>
	<% end if%>
			<tr>
				<td class="<% = SeletorMenu("PrjQualidade") %>">&nbsp;</td>
				<td class="opcMenu"><a href="prjQualidade.html" class="opcMenu">Qualidade</a></td>
			</tr>
			<tr>
				<td class="opcSep">&nbsp;</td>
				<td class="opcSep">&nbsp;</td>
			</tr>
			<tr>
				<td class="<% = SeletorMenu("PrjSeguranca") %>">&nbsp;</td>
				<td class="opcMenu"><a href="prjSeguranca.html" class="opcMenu">Seguran�a</a></td>
			</tr>
			<tr>
				<td class="opcSep">&nbsp;</td>
				<td class="opcSep">&nbsp;</td>
			</tr>
			<tr>
<%	for i = 1 to TotalServ
		' S� apresenta os servi�os alocados ao projeto.
		Mostra = False
		if Session("perfId") = "3" then
		    if cStr(usuaIdContArray(i)) = cStr(Session("usuaId")) then
				Mostra = true
			end if
	    else
			Mostra = true
		end if
		if Mostra then
					%>
			<tr>
				<td class="<% = SeletorMenu("Contrato"&i) %>">&nbsp;</td>
				<td class="opcMenu"><a href="prjContratoQLD<%= i%>.html" class="opcMenu"><% = cntrNomeServArray(i) %></a></td>
			</tr>
			<% 	if i = cint(servInd) then %>			
			<tr>
				<td colspan="2">
					<table width="100%">			
						<tr>
							<td width="15" >&nbsp;</td>
							<td width="10" valign="top"><img src="<% = SeletorSubmenu("Qualidade") %>" class="opc"></td>
							<td><a href="prjContratoQLD<%= i%>.html" class="opcSubMenu">Qualidade</a></td>
						</tr>
						<tr>
							<td width="15" >&nbsp;</td>
							<td width="10" valign="top"><img src="<% = SeletorSubmenu("Seguran�a") %>" class="opc"></td>
							<td><a href="prjContratoSEG<%= i%>.html" class="opcSubMenu">Seguran�a</a></td>
						</tr>
						<tr>
							<td width="15" >&nbsp;</td>
							<td width="10" valign="top"><img src="<% = SeletorSubmenu("Planejamento") %>" class="opc"></td>
							<td><a href="prjContratoPLN<%= i%>.html" class="opcSubMenu">Planejamento</a></td>
						</tr>
					</table>
				</td>
			</tr>
			<% end if %>
			<tr>
				<td class="opcSep">&nbsp;</td>
				<td class="opcSep">&nbsp;</td>
			</tr>
<%		end if
	next 
%>
		</table>

<% end if %>