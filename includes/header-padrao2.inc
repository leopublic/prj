<% Response.Expires= -1%>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html> <!--<![endif]-->
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=8" />
    <title>::PRJ:: Controle remoto de projetos</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
    <meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <link rel="stylesheet" type="text/css" href="../Styles.css">
    <link rel="stylesheet" type="text/css" href="../EstiloMenu.css">
    <link rel="stylesheet" href="../font-awesome-4.0.3/css/font-awesome.min.css">
    <script language="JavaScript" type="text/javascript" src="../scripts/AbrirJanela.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts/PreparaJanela.js"></script>
    <!--#include file="../includes/clsBanco.inc"-->
	<!--#include file="../includes/clsProjeto.inc"-->
    <!--#include file="../includes/opcControle.inc"-->
    <!--#include file="../includes/clsControleAcesso.inc"-->
    <!--#include file="../includes/anxRotinas.inc"-->
	<!--#include file="../includes/basFormatador.inc"-->
</head>