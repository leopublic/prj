<?php

class AnexoController {

    public function teste() {
        return 'Foi!';
    }

    public function getUpload() {
        $loader = new Twig_Loader_Filesystem('views');
        $twig = new Twig_Environment($loader);

        $tpanId = $_GET["tpanId"];
        $projId = $_GET["projId"];
        $cntrId = $_GET["cntrId"];
        $anexId = $_GET["anexId"];

        $anexo = new anexo;
        $anexo->recupere($anexId);
        $tipoAnexo = new tipoAnexo;
        $tipoAnexo->recupere($tpanId);
        if ($anexId == "") {
            if ($tipoAnexo->tpanGen == "M") {
                $tituloPagina = "Carregar novo " . $tipoAnexo->tpanDesc;
            } else {
                $tituloPagina = "Carregar nova " . $tipoAnexo->tpanDesc;
            }
        } else {
            $tituloPagina = "Carregar nova versão de " . $anexo->anexNome;
        }
        if ($tipoAnexo->tpanGlob == "1") {
            $Escopo = " - GLOBAL";
            $LabelNumero = "Norma";
        } else {
            $LabelNumero = "Número";
            if ($tipoAnexo->tpanProj == "1") {
                $Escopo = "PROJETO";
            } else {
                $Escopo = "CONTRATO";
            }
        }


        $data = array(
            'tituloPagina' => $tituloPagina
            , 'LabelNumero' => $LabelNumero
            , 'disabled' => $disabled
            , 'anexo' => $anexo
            , 'tpanId' => $tpanId
            , 'anexId' => $anexId
            , 'cntrId' => $cntrId
            , 'projId' => $projId
        );
        $template = $twig->loadTemplate('anexo/upload_get.twig');
        return $template->render($data);
    }

    public function postUpload() {
        $loader = new Twig_Loader_Filesystem('views');
        $twig = new Twig_Environment($loader);
        try {
            $arquivo = $_FILES['txtArquivo'];
            $tpanId = $_POST['tpanId'];
            $projId = $_POST['projId'];
            $cntrId = $_POST['cntrId'];
            $anexDesc = '';
            $usuaId = 1;
            $anexNomeOrig = $arquivo['name'];
            $anexTam = 0;
            $anexNum = $_POST['txtNumero'];
            $anexNome = $_POST['txtNome'];
            $anexRevNum = $_POST['txtRevNum'];
            $anexId = $_POST['anexId'];

            
            $proc = mssql_init("ANEX_CriarNovo");
            mssql_bind($proc, '@ptpanId', $tpanId, SQLVARCHAR);
            mssql_bind($proc, '@pprojId', $projId, SQLVARCHAR);
            mssql_bind($proc, '@pcntrId', $cntrId, SQLVARCHAR);
            mssql_bind($proc, '@panexDesc', $anexDesc, SQLVARCHAR);
            mssql_bind($proc, '@pusuaIdUpl', $usuaId, SQLVARCHAR);
            mssql_bind($proc, '@panexNomeOrig', $anexNomeOrig, SQLVARCHAR);
            mssql_bind($proc, '@panexTam', $anexTam, SQLVARCHAR);
            mssql_bind($proc, '@panexNum', $anexNum, SQLVARCHAR);
            mssql_bind($proc, '@panexNome', $anexNome, SQLVARCHAR);
            mssql_bind($proc, '@panexRevNum', $anexRevNum, SQLVARCHAR);
            mssql_bind($proc, '@panexId', $anexId, SQLVARCHAR);
            $result = mssql_execute($proc);
            $rs = mssql_fetch_row($result);
            $anexId = $rs[0];
            $anexId = substr("00000000" . $anexId, -8);

            $dir = './Anexos/' . $anexId;
            move_uploaded_file($arquivo['tmp_name'], $dir);

            $data = array(
                'TituloMsg' => "Operação bem sucedida"
                , 'Msg' => "Arquivo carregado com sucesso!"
            );
        } catch (Exception $ex) {
            $data = array(
                  'TituloMsg' => "Erro!"
                , 'Msg' => "Não foi possível realizar a operação:".$ex->getMessage().'<br/>'.$ex->getTraceAsString()
            );            
        }
        $template = $twig->loadTemplate('mensagem.twig');
        return $template->render($data);
    }

}
