<?php

function autoloadClasses($className) {
    $filename = "classes/" . $className . ".php";
    if (is_readable($filename)) {
        require $filename;
    }
}

function autoloadControllers($className) {
    $filename = "controllers/" . $className . ".php";
    if (is_readable($filename)) {
        require $filename;
    }
}


function autoloadFramework($className) {
    $filename = "framework/" . $className . ".php";
    if (is_readable($filename)) {
        require $filename;
    }
}

require_once 'scripts/twig/Autoloader.php';

spl_autoload_register("autoloadClasses");
spl_autoload_register("autoloadControllers");
spl_autoload_register("autoloadFramework");
Twig_Autoloader::register();

// Abre conexao com o banco
ambiente::InicializeAmbiente();
