<%
Public Function NuloLivre(pCampo) 
	if isnull(pCampo) then
		NuloLivre = ""
	else
		NuloLivre = pCampo
	end if
end function

Public Function FormatNumberNull(pCampo,pcasas,percent)
	if isnull(pCampo) then
        FormatNumberNull = "-"
    else
	    if percent then
		    FormatNumberNull = FormatNumber(pCampo,pcasas) & "%"
		else
		    FormatNumberNull = FormatNumber(pCampo,pcasas)
		end if
	end if
end Function

Public Function DtFmt(pData)
	if isnull(pData) then
		DtFmt = "-"
	else
		if trim(pData) = "-" then
			DtFmt = pData
		else
			pData = cStr(pData)
			if trim(pData) = "" then
				DtFmt = "-"
			else
				DtFmt = "->" & pData
'				exit function
				xMes = cInt(mid(pData,4,2))
				xNomeMes = NomeMes(xMes)
				DtFmt =  mid(pData,1,2) & "-" & xNomeMes & "-" & mid(pData,7,2) 
			end if
		end if
	end if
end Function

Public Function NumFmt(pNum)
	if isnull(pNum) then
		NumFmt = "-"
	else
		xNumero = FormatNumber(pNum)
		xNumero = cStr(xNumero)
		xNumero = Replace(xNumero, ".", "*")
		xNumero = Replace(xNumero, ",", ".")
		xNumero = Replace(xNumero, "*", ",")
		NumFmt = xNumero
	end if
end Function

Public Function NumFmtBd(pNum)
	if isnull(pNum) then
		NumFmtBd = "null"
	else
		xNumero = Replace(pNum, ".", "")
		xNumero = Replace(xNumero, ",", ".")
		NumFmtBd = xNumero
	end if
end Function

Public Function FmtDataBd(pData)
	if Len(pData) = 10 then
		FmtDataBd = Mid(pData,7,4) & "-" & Mid(pData,4,2) & "-" & Mid(pData,1,2)
	else
		FmtDataBd = "20" & Mid(pData,7,2) & "-" & Mid(pData,4,2) & "-" & Mid(pData,1,2)
	end if
end Function

%>