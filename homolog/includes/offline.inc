<% 
offline=request.QueryString("offline")
if offline<>"" then
	ContentType = "text/asp"
	Response.AddHeader "Content-Disposition", "attachment; filename=" & filename
	'Response.AddHeader "Content-Length", strFileSize
	' In a Perfect World, Your Client would also have UTF-8 as the default
	' In Their Browser
	Response.Charset = "UTF-8"
	Response.ContentType = ContentType
end if

%>
