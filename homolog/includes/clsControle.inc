<%
Public Sub VerificarAcesso()
	if Session("usuaId") = 0 then
       Session("Msg") = "Acesso restrito a usu�rios autorizados."
       Response.redirect("../loginInput.asp")
    end if
	perfId = Session("perfId")
	if instr(lPerfisAutorizados, perfId ) = 0 then
		Session("Msg") = "Voc� n�o tem acesso a essa fun��o do sistema."
		response.redirect("../LoginInput.asp")
	end if
End Sub

Public Function TemAcesso(funcao)
	perfId = Session("perfId")
	acesso = false
	Select case funcao
		Case "PRJRELADC"
			if perfId = "1" or perfId = "5" then
				acesso = True
			end if
		Case "PRJSIT"
			if perfId = "1" or perfId = "5" or perfId = "2" then
				acesso = True
			end if
		Case "PRJCNT"
			if perfId = "1" or perfId = "5"  then
				acesso = True
			end if
		Case "FORADC"
			if perfId = "5" then
				acesso = true
			end if
		Case "FORALT"
			if perfId = "5" then
				acesso = true
			end if
	end select
	TemAcesso = acesso
End Function
%>