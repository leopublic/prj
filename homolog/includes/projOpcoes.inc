		<table width="100%" border="0" bgcolor="#DDE8EE" cellpadding="0px">
			<tr>
				<td width="13" style="height:5px;font-size:1px">&nbsp;</td>
				<td width="160" style="height:5px;font-size:1px">&nbsp;</td>
			</tr>
			<tr>
				<td class="opcSep">&nbsp;</td>
				<td class="opcSep">&nbsp;</td>
			</tr>
			<tr>
				<td class="<% = SeletorMenu("Projetos") %>">&nbsp;</td>
				<td class="opcMenu"><a href="projListarAtivos.asp" class="opcMenu">Projetos</a></td>
			</tr>
	<% if gMenuSelecionado = "Projetos" then %>
			<tr>
				<td colspan="2">
					<table width="100%">
						<tr>
							<td width="15" >&nbsp;</td>
							<td width="10" valign="top"><img src="<% = imageOpcao("ListarProjetos") %>" class="opc"></td>
							<td><a href="projListarAtivos.asp" class="opcSubMenu">Projetos ativos</a></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td valign="top"><img src="<% = imageOpcao("NovoProjeto") %>" class="opc"></td>
							<td><a  href="abrePopUp.asp?projId=0&UrlOriginal=projListarAtivos.asp" class="opcSubMenu">Novo projeto</a></td>
						</tr>
					</table>
				</td>
			</tr>
	<% else %>
			<tr>
				<td colspan="2" class="opcSep">&nbsp;</td>
			</tr>
	<% end if %>
	<% if gMenuSelecionado = "ProjetoEspecifico" then %>
			<tr>
				<td  class="<% = SeletorMenu("ProjetoEspecifico") %>">&nbsp;</td>
				<td class="opcMenu"><a href="projInicial.asp" class="opcMenu"><% = "Projeto " & Session("projNome") %></a></td>
			</tr>
			<tr>
				<td colspan="2">
					<table width="100%">
						<tr>
							<td width="15" >&nbsp;</td>
							<td width="10" valign="top"><img src="<% = imageOpcao("ResumoProjeto") %>" class="opc"></td>
							<td><a href="projResumo.asp" class="opcSubMenu">Resumo</a></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td valign="top"><img src="<% = imageOpcao("ConstrucaoProjeto") %>" class="opc"></td>
							<td><a href="projConstr.asp" class="opcSubMenu">Constru��es</a></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td valign="top"><img src="<% = imageOpcao("QualidadeProjeto") %>" class="opc"></td>
							<td><a href="projQualidade.asp" class="opcSubMenu">Qualidade</a></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td valign="top"><img src="<% = imageOpcao("SegurancaProjeto") %>" class="opc"></td>
							<td><a href="projSeguranca.asp" class="opcSubMenu">Seguran�a</a></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td valign="top"><img src="<% = imageOpcao("ContratosProjeto") %>" class="opc"></td>
							<td><a href="projContrato.asp" class="opcSubMenu">Contratos</a></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td valign="top">&nbsp;</td>
							<td>
								<table width="100%">
									<tr>
										<td width="10" style="<% = corTraco("Serv. Preliminares") %>">-<td>
										<td class="opcSubMenu" ><a href="projContrato.asp" class="opcSubMenu" style="<% = corTraco("Serv. Preliminares") %>">Serv. preliminares</a></td>
									</tr>
									<tr>
										<td style="<% = corTraco("Estaqueamento") %>">-<td>
										<td class="opcSubMenu"><a href="projContrato.asp" class="opcSubMenu">Estaqueamento</a></td>
									</tr>
									<tr>
										<td style="<% = corTraco("Pr�dios") %>">-<td>
										<td class="opcSubMenu"><a href="projContrato.asp" class="opcSubMenu">Pr�dios</a></td>
									</tr>
									<tr>
										<td class="opcSubMenu">-<td>
										<td class="opcSubMenu"><a href="projContrato.asp" class="opcSubMenu">Constru��o civil</a></td>
									</tr>
									<tr>
										<td class="opcSubMenu">-<td>
										<td class="opcSubMenu"><a href="projContrato.asp" class="opcSubMenu">Montagem</a></td>
									</tr>
									<tr>
										<td class="opcSubMenu">-<td>
										<td class="opcSubMenu"><a href="projContrato.asp" class="opcSubMenu">Transp/Arm/Rig</a></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
	<% End If %>
		</table>
