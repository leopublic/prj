<%
Dim xCusto
Dim resultado

Public Function Contratos(projId, relpId)
	xSQL = ""
	xSQL = xSQL & "select Contrato.cntrId"
	xSQL = xSQL & "     , servNome"	
	xSQL = xSQL & "     , relcId"	
	xSQL = xSQL & "     , isnull(cntrVlrEst  , 0) VlrEst"
	xSQL = xSQL & "     , isnull(relcVlrOrig , 0) relcVlrOrig"
	xSQL = xSQL & "     , isnull(relcVlrAtu , 0) relcVlrAtu"
	xSQL = xSQL & "     , isnull(relcVlrFcn  , 0) relcVlrFcn"
	xSQL = xSQL & "     , isnull(relcVlrPlei , 0) relcVlrPlei"
	xSQL = xSQL & "     , isnull(relcVlrAtu + relcVlrFcn + relcVlrPlei, 0) relcVlrTot"
	xSQL = xSQL & "     , isnull(cntrTermEst1, '2099-12-31') cntrTermEstOrdem"
	xSQL = xSQL & "     , isnull(convert(varchar(10), cntrTermEst1, 3) , '-') TermEst1"
	xSQL = xSQL & "     , isnull(convert(varchar(10), cntrTermEst2, 3) , '-') TermEst2"
	xSQL = xSQL & "     , isnull(convert(varchar(10), cntrTermEst3, 3) , '-') TermEst3"
	xSQL = xSQL & "     , isnull(convert(varchar(10), cntrTermEst4, 3) , '-') TermEst4"
	xSQL = xSQL & "     , convert(varchar(10), relcTermReal1, 3) relcTermReal1"
	xSQL = xSQL & "     , convert(varchar(10), relcTermReal2, 3) relcTermReal2"
	xSQL = xSQL & "     , convert(varchar(10), relcTermReal3, 3) relcTermReal3"
	xSQL = xSQL & "     , convert(varchar(10), relcTermReal4, 3) relcTermReal4"
	xSQL = xSQL & "     , convert(varchar(10), relcTermRepl1, 3) relcTermRepl1"
	xSQL = xSQL & "     , convert(varchar(10), relcTermRepl2, 3) relcTermRepl2"
	xSQL = xSQL & "     , convert(varchar(10), relcTermRepl3, 3) relcTermRepl3"
	xSQL = xSQL & "     , convert(varchar(10), relcTermRepl4, 3) relcTermRepl4"
	xSQL = xSQL & "     , isnull(relcPercAvanEst, 0) relcPercAvanEst"
	xSQL = xSQL & "     , isnull(relcPercAvanAtua, 0) relcPercAvanAtua"
	xSQL = xSQL & "     , isnull(relcPercAvanReal, 0) relcPercAvanReal"
	xSQL = xSQL & "     , isnull(Usuario.emprId, 0 ) emprId"
	xSQL = xSQL & "     , isnull(RelatorioContrato.usuaIdCont, 0 ) usuaIdCont"
	xSQL = xSQL & "     , isnull(emprNome, '(n�o definido)') emprNome"
	xSQL = xSQL & "     , isnull(usuaNome, '(n�o definido)') usuaNome"
	xSQL = xSQL & "  from (((Contrato left join RelatorioContrato on RelatorioContrato.cntrId = Contrato.cntrId and RelatorioContrato.relpId = " & relpId & ")"
	xSQL = xSQL & "       left join Usuario on Usuario.usuaId = RelatorioContrato.usuaIdCont)"
	xSQL = xSQL & "       left join Empreiteira on Empreiteira.emprId = Usuario.emprId)"
	xSQL = xSQL & "     , Servico"
	xSQL = xSQL & " where projId         = " & projId 
	xSQL = xSQL & "   and Servico.servId = Contrato.servId"
	xSQL = xSQL & " order by cntrTermEstOrdem, servOrd"
	Set xRs = gConexao.Execute(xSQL)
	Set Contratos = xRs 
End Function

Public Function RelatorioContrato(projId, relpId)
	xSQL = ""
	xSQL = xSQL & "select relpId"
	xSQL = xSQL & "     , relcId"
	xSQL = xSQL & "     , servNome"
	xSQL = xSQL & "     , isnull(relcVlrEst  , 0) VlrEst"
	xSQL = xSQL & "     , isnull(relcVlrOrig , 0) relcVlrOrig"
	xSQL = xSQL & "     , isnull(relcVlrAtu , 0) relcVlrAtu"
	xSQL = xSQL & "     , isnull(relcVlrFcn  , 0) relcVlrFcn"
	xSQL = xSQL & "     , isnull(relcVlrPlei , 0) relcVlrPlei"
	xSQL = xSQL & "     , isnull(relcVlrAtu + relcVlrFcn + relcVlrPlei, 0) relcVlrTot"
	xSQL = xSQL & "     , isnull(relcTermEst1, '2099-12-31') relcTermEstOrdem"
	xSQL = xSQL & "     , isnull(convert(varchar(10), relcTermEst1, 3) , '-') TermEst1"
	xSQL = xSQL & "     , isnull(convert(varchar(10), relcTermEst2, 3) , '-') TermEst2"
	xSQL = xSQL & "     , isnull(convert(varchar(10), relcTermEst3, 3) , '-') TermEst3"
	xSQL = xSQL & "     , isnull(convert(varchar(10), relcTermEst4, 3) , '-') TermEst4 "
	xSQL = xSQL & "     , convert(varchar(10), relcTermReal1, 3) relcTermReal1"
	xSQL = xSQL & "     , convert(varchar(10), relcTermReal2, 3) relcTermReal2"
	xSQL = xSQL & "     , convert(varchar(10), relcTermReal3, 3) relcTermReal3"
	xSQL = xSQL & "     , convert(varchar(10), relcTermReal4, 3) relcTermReal4 "
	xSQL = xSQL & "     , convert(varchar(10), relcTermRepl1, 3) relcTermRepl1"
	xSQL = xSQL & "     , convert(varchar(10), relcTermRepl2, 3) relcTermRepl2"
	xSQL = xSQL & "     , convert(varchar(10), relcTermRepl3, 3) relcTermRepl3"
	xSQL = xSQL & "     , convert(varchar(10), relcTermRepl4, 3) relcTermRepl4"
	xSQL = xSQL & "     , isnull(RelatorioContrato.usuaIdCont, 0 ) usuaIdCont"
	xSQL = xSQL & "     , isnull(usuaNome, '(n�o definido)' ) usuaNome"
	xSQL = xSQL & "     , isnull(Usuario.emprId, 0 ) emprId"
	xSQL = xSQL & "     , isnull(relcPercAvanEst, 0) relcPercAvanEst"
	xSQL = xSQL & "     , isnull(relcPercAvanAtua, 0) relcPercAvanAtua"
	xSQL = xSQL & "     , isnull(relcPercAvanReal, 0) relcPercAvanReal"
	xSQL = xSQL & "     , isnull(emprNome, '(n�o definido)') emprNome"
	xSQL = xSQL & "     , isnull(RelatorioContrato.servId, 0) servId"
	xSQL = xSQL & "  from ((RelatorioContrato left join Usuario on Usuario.usuaId = RelatorioContrato.usuaIdCont)"
	xSQL = xSQL & "        left join Empreiteira on Empreiteira.emprId = Usuario.emprId)"
	xSQL = xSQL & "     , Servico"
	xSQL = xSQL & " where RelatorioContrato.relpId = " & relpId
	xSQL = xSQL & "   and Servico.servId           = RelatorioContrato.servId"
	xSQL = xSQL & " order by relcTermEstOrdem, servOrd"
	Set xRs = gConexao.Execute(xSQL)
	Set RelatorioContrato = xRs 
End Function

Public Function ContratosResumo(projId, relpId)
	xSQL = ""
	xSQL = xSQL & "select isnull(relcTermEst1, '2099-12-31') relcTermEstOrdem"
	xSQL = xSQL & "     , servOrd"
	xSQL = xSQL & "     , relpId"
	xSQL = xSQL & "     , servNome"
	xSQL = xSQL & "     , relcId"
	xSQL = xSQL & "     , isnull(relcVlrEst  , 0) VlrEst"
	xSQL = xSQL & "     , isnull(relcVlrOrig , 0) relcVlrOrig"
	xSQL = xSQL & "     , isnull(relcVlrAtu , 0) relcVlrAtu"
	xSQL = xSQL & "     , isnull(relcVlrFcn  , 0) relcVlrFcn"
	xSQL = xSQL & "     , isnull(relcVlrPlei , 0) relcVlrPlei"
	xSQL = xSQL & "     , isnull(relcVlrAtu + relcVlrFcn + relcVlrPlei, 0) relcVlrTot"
	xSQL = xSQL & "     , isnull(convert(varchar(10), relcTermEst1, 3) , '-') TermEst1"
	xSQL = xSQL & "     , isnull(convert(varchar(10), relcTermEst2, 3) , '-') TermEst2"
	xSQL = xSQL & "     , isnull(convert(varchar(10), relcTermEst3, 3) , '-') TermEst3"
	xSQL = xSQL & "     , isnull(convert(varchar(10), relcTermEst4, 3) , '-') TermEst4 "
	xSQL = xSQL & "     , convert(varchar(10), relcTermReal1, 3) relcTermReal1"
	xSQL = xSQL & "     , convert(varchar(10), relcTermReal2, 3) relcTermReal2"
	xSQL = xSQL & "     , convert(varchar(10), relcTermReal3, 3) relcTermReal3"
	xSQL = xSQL & "     , convert(varchar(10), relcTermReal4, 3) relcTermReal4 "
	xSQL = xSQL & "     , convert(varchar(10), relcTermRepl1, 3) relcTermRepl1"
	xSQL = xSQL & "     , convert(varchar(10), relcTermRepl2, 3) relcTermRepl2"
	xSQL = xSQL & "     , convert(varchar(10), relcTermRepl3, 3) relcTermRepl3"
	xSQL = xSQL & "     , convert(varchar(10), relcTermRepl4, 3) relcTermRepl4"
	xSQL = xSQL & "     , isnull(relcPercAvanEst, 0) relcPercAvanEst"
	xSQL = xSQL & "     , isnull(relcPercAvanAtua, 0) relcPercAvanAtua"
	xSQL = xSQL & "     , isnull(relcPercAvanReal, 0) relcPercAvanReal"
	xSQL = xSQL & "     , isnull(emprNome, '(n�o definido)') emprNome"
	xSQL = xSQL & "     , isnull(usuaNome, '(n�o definido)') usuaNome"
	xSQL = xSQL & "     , isnull(usuaIdCont, 0) usuaIdCont"
	xSQL = xSQL & "  from ((RelatorioContrato left join Usuario on Usuario.usuaId = RelatorioContrato.usuaIdCont)"
	xSQL = xSQL & "       left join Empreiteira on Empreiteira.emprId = Usuario.emprId)"
	xSQL = xSQL & "     , Servico"
	xSQL = xSQL & " where Servico.servId           = RelatorioContrato.servId"
	xSQL = xSQL & "   and RelatorioContrato.relpId = " & relpId
	xSQL = xSQL & " union "
	xSQL = xSQL & "Select isnull(cntrTermEst1, '2099-12-31') relcTermEstOrdem"
	xSQL = xSQL & "     , servOrd"
	xSQL = xSQL & "     , 0 relpId"
	xSQL = xSQL & "     , servNome"
	xSQL = xSQL & "     , 0 relcId"
	xSQL = xSQL & "     , isnull(cntrVlrEst  , 0) VlrEst"
	xSQL = xSQL & "     , 0 relcVlrOrig"
	xSQL = xSQL & "     , 0 relcVlrAtu"
	xSQL = xSQL & "     , 0 relcVlrFcn"
	xSQL = xSQL & "     , 0 relcVlrPlei"
	xSQL = xSQL & "     , 0 relcVlrTot"
	xSQL = xSQL & "     , isnull(convert(varchar(10), cntrTermEst1, 3) , '-') TermEst1"
	xSQL = xSQL & "     , isnull(convert(varchar(10), cntrTermEst2, 3) , '-') TermEst2"
	xSQL = xSQL & "     , isnull(convert(varchar(10), cntrTermEst3, 3) , '-') TermEst3"
	xSQL = xSQL & "     , isnull(convert(varchar(10), cntrTermEst4, 3) , '-') TermEst4 "
	xSQL = xSQL & "     , null relcTermReal1"
	xSQL = xSQL & "     , null relcTermReal2"
	xSQL = xSQL & "     , null relcTermReal3"
	xSQL = xSQL & "     , null relcTermReal4 "
	xSQL = xSQL & "     , null relcTermRepl1"
	xSQL = xSQL & "     , null relcTermRepl2"
	xSQL = xSQL & "     , null relcTermRepl3"
	xSQL = xSQL & "     , null relcTermRepl4"
	xSQL = xSQL & "     , 0 relcPercAvanEst"
	xSQL = xSQL & "     , 0 relcPercAvanAtua"
	xSQL = xSQL & "     , 0 relcPercAvanReal"
	xSQL = xSQL & "     , isnull(emprNome, '(n�o definido)') emprNome"
	xSQL = xSQL & "     , isnull(usuaNome, '(n�o definido)') usuaNome"
	xSQL = xSQL & "     , isnull(usuaIdCont, 0) usuaIdCont"
	xSQL = xSQL & "  from ((Contrato left join Usuario on usuaId = Contrato.usuaIdCont)"
	xSQL = xSQL & "       left join Empreiteira on Empreiteira.emprId = Usuario.emprId)"
	xSQL = xSQL & "     , Servico"
	xSQL = xSQL & " where Servico.servId           = Contrato.servId"
	xSQL = xSQL & "   and Contrato.projId = " & projId
	xSQL = xSQL & "   and Contrato.cntrId not in (select cntrId from RelatorioContrato where relpId = " & relpId & ")"
	xSQL = xSQL & " order by relcTermEstOrdem, servOrd"
	Set xRs = gConexao.Execute(xSQL)
	Set ContratosResumo = xRs 
End Function



Public Function ContratosEst(projId)
	xsql = ""
	xSQL = xsql & "select Contrato.servId, servNome"
	xSQL = xSQL & "     , isnull(emprNome, 'n�o definido') emprNome"
	xsql = xsql & "     , isnull(Contrato.cntrVlrEst, 0) cntrVlrEst"
	xSQL = xSQL & "     , isnull(cntrTermEst1,'2099-12-31') cntrTermEstOrdem"
	xsql = xsql & "     , convert(varchar(10), Contrato.cntrTermEst1, 3) cntrTermEst1"
	xsql = xsql & "     , convert(varchar(10), Contrato.cntrTermEst2, 3) cntrTermEst2"
	xsql = xsql & "     , convert(varchar(10), Contrato.cntrTermEst3, 3) cntrTermEst3"
	xsql = xsql & "     , convert(varchar(10), Contrato.cntrTermEst4, 3) cntrTermEst4"
	xsql = xsql & "     , isnull(Contrato.emprId, 0) emprId"
	xSQL = xsql & "     , Contrato.cntrId"
	xsql = xsql & "  from (Contrato left join Empreiteira on Empreiteira.emprId = Contrato.emprId), Servico"
	xsql = xsql & " where Contrato.projId = " & projId 
	xSQL = xSQL & "   and Servico.servId = Contrato.servId"
	xsql = xsql & " order by cntrTermEstOrdem, servOrd"
	set xRs = gConexao.execute(xsQL)
	set ContratosEst = xRs
End Function

Public Function DivPrazos2(projId, relpId, relpLib, resumo)
	Response.write "<table width=""100%"" class=""grid"">" & vbcrlf
	Response.write vbtab & "<tr>" & vbcrlf
	Response.write vbtab & vbtab & "<td width=""2%""></td>" & vbcrlf
	Response.write vbtab & vbtab & "<td width=""23%""></td>" & vbcrlf
	Response.write vbtab & vbtab & "<td width=""25%""></td>" & vbcrlf
	Response.write vbtab & vbtab & "<td width=""25%""></td>" & vbcrlf
	Response.write vbtab & vbtab & "<td width=""25%""></td>" & vbcrlf
	Response.write vbtab & "</tr>" & vbcrlf
	Response.write vbtab & "<tr>" & vbcrlf
	Response.write vbtab & vbtab & "<td class=""GridCab"" colspan=""2"">&nbsp;</td>" & vbcrlf
	Response.write vbtab & vbtab & "<td class=""GridCab"" align=""center"">Previsto</td>" & vbcrlf
	Response.write vbtab & vbtab & "<td class=""GridCab"" align=""center"">Atualizado</td>" & vbcrlf
	Response.write vbtab & vbtab & "<td class=""GridCab"" align=""center"">Real</td>" & vbcrlf
	Response.write vbtab & "</tr>" & vbcrlf

	if relpLib then
		if resumo then			
			set xRs = gConexao.execute("exec PROJ_ListarContratosAtual @pprojId = " & projId & " , @pLiberado = 1")
		else
			set xRs = RelatorioContrato(projId, relpId)
		end if
	else
		set xRs = gConexao.execute("exec PROJ_ListarContratosAtual @pprojId = " & projId & " , @pLiberado = 0")
	end if
	xCusto = ""
	estilo = "GridLinhaPar"
	TotalEst = 0 
	TotalOrig = 0
	TotalFcn = 0
	TotalPlei = 0
	TOtalAtu = 0
	while not xRs.eof 
		estiloVari = ""
			
		if xRs("VlrEst") = 0 then
			cntrVari = "(n/a)"
		else
			if xRs("relcVlrTot") = 0 then
				cntrVari = "-"
			else			
				cntrVari = ((xRs("VlrEst") - xRs("relcVlrTot") ) / xRs("VlrEst") ) * 100
				if cntrVari < 0 then
					estiloVari = "style=""color:red"""
				else
					estiloVari = ""
				end if
				cntrVari = FormatNumber(cntrVari,2,-1,-1)
				cntrVari = cntrVari & "%"
			end if
		end if
		
		
		Response.write vbtab & "<tr>" & vbcrlf
		if xRs("usuaIdCont") = "0" then
			xEmpr = " (fornecedor n�o definido)"
		else
			xEmpr = " (" & xRs("emprNome") & ")"
		end if
		Response.write vbtab & vbtab & "<td colspan=""5"" class=""tipo"" height=""20"">" & xRs("servNome") & xEmpr & "</td>" & vbcrlf
		Response.write vbtab & "</tr>" & vbcrlf
		Response.write vbtab & "<tr>" & vbcrlf
		Response.write vbtab & vbtab & "<td ></td>" & vbcrlf
		Response.write vbtab & vbtab & "<td class=""" & estilo & """ style=""font-weight:bold"">Emiss�o MD</td>" & vbcrlf
		Response.write vbtab & vbtab & "<td class=""" & estilo & """ align=""center"">" & DtFmt(xRs("TermEst1")) & "</td>" & vbcrlf
		Response.write vbtab & vbtab & "<td class=""" & estilo & """ align=""center"">" & DtFmt(xRs("relcTermRepl1")) & "</td>" & vbcrlf
		Response.write vbtab & vbtab & "<td class=""" & estilo & """ align=""center"">" & DtFmt(xRs("relcTermReal1")) & "</td>" & vbcrlf
		Response.write vbtab & "</tr>" & vbcrlf

		Response.write vbtab & "<tr>" & vbcrlf
		Response.write vbtab & vbtab & "<td ></td>" & vbcrlf
		Response.write vbtab & vbtab & "<td class=""" & estilo & """ style=""font-weight:bold"">Contrata��o</td>" & vbcrlf
		Response.write vbtab & vbtab & "<td class=""" & estilo & """ align=""center"">" & DtFmt(xRs("TermEst2")) & "</td>" & vbcrlf
		Response.write vbtab & vbtab & "<td class=""" & estilo & """ align=""center"">" & DtFmt(xRs("relcTermRepl2")) & "</td>" & vbcrlf
		Response.write vbtab & vbtab & "<td class=""" & estilo & """ align=""center"">" & DtFmt(xRs("relcTermReal2")) & "</td>" & vbcrlf
		Response.write vbtab & "</tr>" & vbcrlf

		Response.write vbtab & "<tr>" & vbcrlf
		Response.write vbtab & vbtab & "<td></td>" & vbcrlf
		Response.write vbtab & vbtab & "<td class=""" & estilo & """ style=""font-weight:bold"">Mobiliza��o</td>" & vbcrlf
		Response.write vbtab & vbtab & "<td class=""" & estilo & """ align=""center"">" & DtFmt(xRs("TermEst3")) & "</td>" & vbcrlf
		Response.write vbtab & vbtab & "<td class=""" & estilo & """ align=""center"">" & DtFmt(xRs("relcTermRepl3")) & "</td>" & vbcrlf
		Response.write vbtab & vbtab & "<td class=""" & estilo & """ align=""center"">" & DtFmt(xRs("relcTermReal3")) & "</td>" & vbcrlf
		Response.write vbtab & "</tr>" & vbcrlf

		Response.write vbtab & "<tr>" & vbcrlf
		Response.write vbtab & vbtab & "<td></td>" & vbcrlf
		Response.write vbtab & vbtab & "<td class=""" & estilo & """ style=""font-weight:bold"">Desmobiliza��o</td>" & vbcrlf
		Response.write vbtab & vbtab & "<td class=""" & estilo & """ align=""center"">" & DtFmt(xRs("TermEst4")) & "</td>" & vbcrlf
		Response.write vbtab & vbtab & "<td class=""" & estilo & """ align=""center"">" & DtFmt(xRs("relcTermRepl4")) & "</td>" & vbcrlf
		Response.write vbtab & vbtab & "<td class=""" & estilo & """ align=""center"">" & DtFmt(xRs("relcTermReal4")) & "</td>" & vbcrlf
		Response.write vbtab & "</tr>" & vbcrlf
		
		Response.write vbtab & "<tr>" & vbcrlf
		Response.write vbtab & vbtab & "<td></td>" & vbcrlf
		Response.write vbtab & vbtab & "<td class=""Variacao"" style=""text-align:left"">Avan�o (%)</td>" & vbcrlf
		Response.write vbtab & vbtab & "<td class=""Variacao"">" & FormatNumber(xRs("relcPercAvanEst")) & "%</td>" & vbcrlf
		Response.write vbtab & vbtab & "<td class=""Variacao"">" & FormatNumber(xRs("relcPercAvanAtua")) & "%</td>" & vbcrlf
		Response.write vbtab & vbtab & "<td class=""Variacao"">" & FormatNumber(xRs("relcPercAvanReal")) & "%</td>" & vbcrlf
		Response.write vbtab & "</tr>" & vbcrlf

		Response.write vbtab & "<tr>" & vbcrlf
		Response.write vbtab & vbtab & "<td colspan=""5"">&nbsp;</td>" & vbcrlf
		Response.write vbtab & "</tr>" & vbcrlf

		xCusto = xcusto & "<tr>" & vbcrlf
		xcusto = xcusto & vbtab & "<td class=""GridCabLinha"" height=""33"">" & xRs("servNome") & "</td>" & vbcrlf
		xcusto = xcusto & vbtab & "<td class=""" & estilo & "cond" & """ align=""right"">" & FormatNumber(xRs("VlrEst")) & "</td>" & vbcrlf
		xcusto = xcusto & vbtab & "<td class=""" & estilo & "cond" & """ align=""right"">" & FormatNumber(xRs("relcVlrOrig")) & "</td>" & vbcrlf
		xcusto = xcusto & vbtab & "<td class=""" & estilo & "cond" & """ align=""right"">" & FormatNumber(xRs("relcVlrAtu")) & "</td>" & vbcrlf
		xcusto = xcusto & vbtab & "<td class=""" & estilo & "cond" & """ align=""right"">" & FormatNumber(xRs("relcVlrFcn")) & "</td>" & vbcrlf
		xcusto = xcusto & vbtab & "<td class=""" & estilo & "cond" & """ align=""right"">" & FormatNumber(xRs("relcVlrPlei")) & "</td>" & vbcrlf
		xcusto = xcusto & vbtab & "<td class=""" & estilo & "cond" & """ align=""right"">" & FormatNumber(xRs("relcVlrTot")) & "</td>" & vbcrlf
		xcusto = xcusto & vbtab & "<td class=""" & estilo & "cond" & """ align=""right"" " & estiloVari & ">" & cntrVari & "</td>" & vbcrlf
		xCusto = xcusto & "</tr>"		
		TotalEst = TotalEst + xRs("VlrEst")
		TotalOrig = TotalOrig + xRs("relcVlrOrig")
		TotalAtu = TotalAtu + xRs("relcVlrAtu")
		TotalFcn = TotalFcn + xRs("relcVlrFcn")
		TotalPlei = TotalPlei + xRs("relcVlrPlei")
		xRs.MoveNext
		if estilo = "GridLinhaPar" then
			estilo = "GridLinhaImpar"
		else
			estilo = "GridLinhaPar"
		end if
	wEnd
	TotalGeral = TotalAtu + TotalFCN + TotalPlei
	estiloVari = ""
	if TotalEst = 0 then
		VerGeral = "(n/a)"
	else
		if TotalGeral = 0 then
			VarGeral = "-"
		else
			VarGeral = ((TotalEst - TotalGeral ) / TotalEst) * 100
			if VarGeral < 0 then
				estiloVari = "style=""color:red"""
			else
				estiloVari = ""
			end if
			VarGeral = FormatNumber(VarGeral,2,-1,-1)
			VarGeral = VarGeral & "%"
		end if
	end if
	xCusto = xCusto & "<tr>" & vbcrlf
	xcusto = xcusto & vbtab & "<td class=""GridLinhaTot"" height=""33"">TOTAL</td>" & vbcrlf
	xcusto = xcusto & vbtab & "<td class=""GridLinhaTot"" align=""right"">" & FormatNumber(TotalEst) & "</td>" & vbcrlf
	xcusto = xcusto & vbtab & "<td class=""GridLinhaTot"" align=""right"">" & FormatNumber(TotalOrig) & "</td>" & vbcrlf
	xcusto = xcusto & vbtab & "<td class=""GridLinhaTot"" align=""right"">" & FormatNumber(TotalAtu) & "</td>" & vbcrlf
	xcusto = xcusto & vbtab & "<td class=""GridLinhaTot"" align=""right"">" & FormatNumber(TotalFcn) & "</td>" & vbcrlf
	xcusto = xcusto & vbtab & "<td class=""GridLinhaTot"" align=""right"">" & FormatNumber(TotalPlei) & "</td>" & vbcrlf
	xcusto = xcusto & vbtab & "<td class=""GridLinhaTot"" align=""right"">" & FormatNumber(TotalGeral) & "</td>" & vbcrlf
	xcusto = xcusto & vbtab & "<td class=""GridLinhaTot"" align=""right"" " & estiloVari & ">" & VarGeral & "</td>" & vbcrlf
	xCusto = xCusto & "</tr>" & vbcrlf


end Function

Public Function MontarContratos(projId, relpId, Liberado)
	MontarContratosNova projId, relpId, Liberado
	response.write resultado
end function

Public Function MontarContratosNova(projId, relpId, Liberado)
	resultado = ""
	resultado = resultado &  "<table width=""100%"" class=""grid"">" & vbcrlf
	resultado = resultado &  vbtab & "<tr>" & vbcrlf
	resultado = resultado &  vbtab & vbtab & "<td width=""2%""></td>" & vbcrlf
	resultado = resultado &  vbtab & vbtab & "<td width=""23%""></td>" & vbcrlf
	resultado = resultado &  vbtab & vbtab & "<td width=""25%""></td>" & vbcrlf
	resultado = resultado &  vbtab & vbtab & "<td width=""25%""></td>" & vbcrlf
	resultado = resultado &  vbtab & vbtab & "<td width=""25%""></td>" & vbcrlf
	resultado = resultado &  vbtab & "</tr>" & vbcrlf
	resultado = resultado &  vbtab & "<tr>" & vbcrlf
	resultado = resultado &  vbtab & vbtab & "<td class=""GridCab"" colspan=""2"">&nbsp;</td>" & vbcrlf
	resultado = resultado &  vbtab & vbtab & "<td class=""GridCab"" align=""center"">Previsto</td>" & vbcrlf
	resultado = resultado &  vbtab & vbtab & "<td class=""GridCab"" align=""center"">Atualizado</td>" & vbcrlf
	resultado = resultado &  vbtab & vbtab & "<td class=""GridCab"" align=""center"">Real</td>" & vbcrlf
	resultado = resultado &  vbtab & "</tr>" & vbcrlf

	set xRs = gConexao.execute("exec PROJ_ListarContratos @pprojId = " & projId & ", @pLiberado = " & Liberado & ", @prelpId = " & relpId)
	xCusto = ""
	estilo = "GridLinhaPar"
	TotalEst = 0 
	TotalOrig = 0
	TotalFcn = 0
	TotalPlei = 0
	TOtalAtu = 0
	
	while not xRs.eof 
		If xRs("VlrAtu") = 0 then
			xVlrTot = xRs("VlrTotOrig")
		else
			xVlrTot = xRs("VlrTotAtu")
		end if
		estiloVari = ""
			
		if xRs("VlrOrig") = 0 then
			cntrVari = "(n/a)"
		else
			if xVlrTot = 0 then
				cntrVari = "-"
			else			
				cntrVari = ((xRs("VlrOrig") - xVlrTot ) / xRs("VlrOrig") ) * 100
				if cntrVari < 0 then
					estiloVari = "style=""color:red"""
				else
					estiloVari = ""
				end if
				if isnull(cntrVari) then
				    cntrVari = "-"
				else
					cntrVari = FormatNumber(cntrVari,2,-1,-1)
					'cntrVari = cntrVari & "%"
				end if
			end if
		end if
'		If xRs("VlrAtu") = 0 then
'			xVlrTot = xRs("VlrTotOrig")
'		else
'			xVlrTot = xRs("VlrTotAtu")
'		end if
'		estiloVari = ""
'			
'		if xRs("VlrEst") = 0 then
'			cntrVari = "(n/a)"
'		else
'			if xVlrTot = 0 then
'				cntrVari = "-"
'			else			
'				cntrVari = ((xRs("VlrEst") - xVlrTot ) / xRs("VlrEst") ) * 100
'				if cntrVari < 0 then
'					estiloVari = "style=""color:red"""
'				else
'					estiloVari = ""
'				end if
'				if isnull(cntrVari) then
'				    cntrVari = "-"
'				else
'					cntrVari = FormatNumber(cntrVari,2,-1,-1)
'					'cntrVari = cntrVari & "%"
'				end if
'			end if
'		end if
		
		
		resultado = resultado &  vbtab & "<tr>" & vbcrlf
		if isnull(xRs("usuaIdCont")) then
			xEmpr = " (fornecedor n�o definido)"
		else
			xEmpr = " (" & xRs("emprNome") & ")"
		end if
		resultado = resultado &  vbtab & vbtab & "<td colspan=""5"" class=""tipo"" height=""20"">" & xRs("servNome") & xEmpr & "</td>" & vbcrlf
		resultado = resultado &  vbtab & "</tr>" & vbcrlf
		resultado = resultado &  vbtab & "<tr>" & vbcrlf
		resultado = resultado &  vbtab & vbtab & "<td ></td>" & vbcrlf
		resultado = resultado &  vbtab & vbtab & "<td class=""" & estilo & """ style=""font-weight:bold"">Emiss�o MD</td>" & vbcrlf
		resultado = resultado &  vbtab & vbtab & "<td class=""" & estilo & """ align=""center"">" & DtFmt(xRs("TermEst1")) & "</td>" & vbcrlf
		resultado = resultado &  vbtab & vbtab & "<td class=""" & estilo & """ align=""center"">" & DtFmt(xRs("TermRepl1")) & "</td>" & vbcrlf
		resultado = resultado &  vbtab & vbtab & "<td class=""" & estilo & """ align=""center"">" & DtFmt(xRs("TermReal1")) & "</td>" & vbcrlf
		resultado = resultado &  vbtab & "</tr>" & vbcrlf

		resultado = resultado &  vbtab & "<tr>" & vbcrlf
		resultado = resultado &  vbtab & vbtab & "<td ></td>" & vbcrlf
		resultado = resultado &  vbtab & vbtab & "<td class=""" & estilo & """ style=""font-weight:bold"">Contrata��o</td>" & vbcrlf
		resultado = resultado &  vbtab & vbtab & "<td class=""" & estilo & """ align=""center"">" & DtFmt(xRs("TermEst2")) & "</td>" & vbcrlf
		resultado = resultado &  vbtab & vbtab & "<td class=""" & estilo & """ align=""center"">" & DtFmt(xRs("TermRepl2")) & "</td>" & vbcrlf
		resultado = resultado &  vbtab & vbtab & "<td class=""" & estilo & """ align=""center"">" & DtFmt(xRs("TermReal2")) & "</td>" & vbcrlf
		resultado = resultado &  vbtab & "</tr>" & vbcrlf

		resultado = resultado &  vbtab & "<tr>" & vbcrlf
		resultado = resultado &  vbtab & vbtab & "<td></td>" & vbcrlf
		resultado = resultado &  vbtab & vbtab & "<td class=""" & estilo & """ style=""font-weight:bold"">Mobiliza��o</td>" & vbcrlf
		resultado = resultado &  vbtab & vbtab & "<td class=""" & estilo & """ align=""center"">" & DtFmt(xRs("TermEst3")) & "</td>" & vbcrlf
		resultado = resultado &  vbtab & vbtab & "<td class=""" & estilo & """ align=""center"">" & DtFmt(xRs("TermRepl3")) & "</td>" & vbcrlf
		resultado = resultado &  vbtab & vbtab & "<td class=""" & estilo & """ align=""center"">" & DtFmt(xRs("TermReal3")) & "</td>" & vbcrlf
		resultado = resultado &  vbtab & "</tr>" & vbcrlf

		resultado = resultado &  vbtab & "<tr>" & vbcrlf
		resultado = resultado &  vbtab & vbtab & "<td></td>" & vbcrlf
		resultado = resultado &  vbtab & vbtab & "<td class=""" & estilo & """ style=""font-weight:bold"">Desmobiliza��o</td>" & vbcrlf
		resultado = resultado &  vbtab & vbtab & "<td class=""" & estilo & """ align=""center"">" & DtFmt(xRs("TermEst4")) & "</td>" & vbcrlf
		resultado = resultado &  vbtab & vbtab & "<td class=""" & estilo & """ align=""center"">" & DtFmt(xRs("TermRepl4")) & "</td>" & vbcrlf
		resultado = resultado &  vbtab & vbtab & "<td class=""" & estilo & """ align=""center"">" & DtFmt(xRs("TermReal4")) & "</td>" & vbcrlf
		resultado = resultado &  vbtab & "</tr>" & vbcrlf
		
		resultado = resultado &  vbtab & "<tr>" & vbcrlf
		resultado = resultado &  vbtab & vbtab & "<td></td>" & vbcrlf
		resultado = resultado &  vbtab & vbtab & "<td class=""Variacao"" style=""text-align:left"">Avan�o (%)</td>" & vbcrlf
		resultado = resultado &  vbtab & vbtab & "<td class=""Variacao"">" & FormatNumberNull(xRs("PercAvanEst"),2, true) & "</td>" & vbcrlf
		resultado = resultado &  vbtab & vbtab & "<td class=""Variacao"">" & FormatNumberNull(xRs("PercAvanAtua"),2, true) & "</td>" & vbcrlf
		resultado = resultado &  vbtab & vbtab & "<td class=""Variacao"">" & FormatNumberNull(xRs("PercAvanReal"),2, true) & "</td>" & vbcrlf
		resultado = resultado &  vbtab & "</tr>" & vbcrlf

		resultado = resultado &  vbtab & "<tr>" & vbcrlf
		resultado = resultado &  vbtab & vbtab & "<td colspan=""5"">&nbsp;</td>" & vbcrlf
		resultado = resultado &  vbtab & "</tr>" & vbcrlf

		xCusto = xcusto & "<tr>" & vbcrlf
		xcusto = xcusto & vbtab & "<td colspan=""8"" class=""GridCabLinha"">" & xRs("servNome") & " " & xEmpr & "</td>" & vbcrlf
		xCusto = xcusto & "</tr>" & vbcrlf
		xCusto = xcusto & "<tr>" & vbcrlf
		xcusto = xcusto & vbtab & "<td class=""" & estilo & "cond" & """ align=""right""></td>" & vbcrlf
		xcusto = xcusto & vbtab & "<td class=""" & estilo & "cond" & """ align=""right"">" & FormatNumberNull(xRs("VlrEst"),2,false) & "</td>" & vbcrlf
		xcusto = xcusto & vbtab & "<td class=""" & estilo & "cond" & """ align=""right"">" & FormatNumberNull(xRs("VlrOrig"),2,false) & "</td>" & vbcrlf
		xcusto = xcusto & vbtab & "<td class=""" & estilo & "cond" & """ align=""right"">" & FormatNumberNull(xRs("VlrAtu"),2,false) & "</td>" & vbcrlf
		xcusto = xcusto & vbtab & "<td class=""" & estilo & "cond" & """ align=""right"">" & FormatNumberNull(xRs("VlrFcn"),2,false) & "</td>" & vbcrlf
		xcusto = xcusto & vbtab & "<td class=""" & estilo & "cond" & """ align=""right"">" & FormatNumberNull(xRs("VlrPlei"),2,false) & "</td>" & vbcrlf
		if xRs("VlrAtu") <> 0 then
			xcusto = xcusto & vbtab & "<td class=""" & estilo & "cond" & """ align=""right"">" & FormatNumberNull(xRs("VlrTotAtu"),2,false) & "</td>" & vbcrlf
		else
			xcusto = xcusto & vbtab & "<td class=""" & estilo & "cond" & """ align=""right"">" & FormatNumberNull(xRs("VlrTotOrig"),2,false) & "</td>" & vbcrlf
		end if
		xcusto = xcusto & vbtab & "<td class=""" & estilo & "cond" & """ align=""right"" " & estiloVari & ">" & cntrVari & "</td>" & vbcrlf
		xCusto = xcusto & "</tr>"		
		TotalEst = TotalEst + xRs("VlrEst")
		TotalOrig = TotalOrig + xRs("VlrOrig")
		TotalAtu = TotalAtu + xRs("VlrAtu")
		TotalFcn = TotalFcn + xRs("VlrFcn")
		TotalPlei = TotalPlei + xRs("VlrPlei")
		xRs.MoveNext
		if estilo = "GridLinhaPar" then
			estilo = "GridLinhaPar"
'			estilo = "GridLinhaImpar"
		else
			estilo = "GridLinhaPar"
		end if
	wEnd
	TotalGeral = TotalAtu + TotalFCN + TotalPlei
	estiloVari = ""
	if TotalEst = 0 then
		VerGeral = "(n/a)"
	else
		if TotalGeral = 0 then
			VarGeral = "-"
		else
			VarGeral = ((TotalEst - TotalGeral ) / TotalEst) * 100
			if VarGeral < 0 then
				estiloVari = "style=""color:red"""
			else
				estiloVari = ""
			end if
			If isNull(VarGeral) then
				VarGeral = "-"
			else
				VarGeral = FormatNumber(VarGeral,2,-1,-1)
				'VarGeral = VarGeral & "%"
			end if
		end if
	end if
	xCusto = xCusto & "<tr>" & vbcrlf
	xcusto = xcusto & vbtab & "<td class=""GridLinhaTot"" height=""33"">TOTAL</td>" & vbcrlf
	xcusto = xcusto & vbtab & "<td class=""GridLinhaTot"" align=""right"">" & FormatNumberNull(TotalEst,2,False) & "</td>" & vbcrlf
	xcusto = xcusto & vbtab & "<td class=""GridLinhaTot"" align=""right"">" & FormatNumberNull(TotalOrig,2,False) & "</td>" & vbcrlf
	xcusto = xcusto & vbtab & "<td class=""GridLinhaTot"" align=""right"">" & FormatNumberNull(TotalAtu,2,False) & "</td>" & vbcrlf
	xcusto = xcusto & vbtab & "<td class=""GridLinhaTot"" align=""right"">" & FormatNumberNull(TotalFcn,2,False) & "</td>" & vbcrlf
	xcusto = xcusto & vbtab & "<td class=""GridLinhaTot"" align=""right"">" & FormatNumberNull(TotalPlei,2,False) & "</td>" & vbcrlf
	xcusto = xcusto & vbtab & "<td class=""GridLinhaTot"" align=""right"">" & FormatNumberNull(TotalGeral,2,False) & "</td>" & vbcrlf
	xcusto = xcusto & vbtab & "<td class=""GridLinhaTot"" align=""right"" " & estiloVari & ">" & VarGeral & "</td>" & vbcrlf
	xCusto = xCusto & "</tr>" & vbcrlf

end Function

%>