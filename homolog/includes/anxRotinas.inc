<%
'*==========================================================================================*
'* GridAnexos 4 - Grid mais cabecalhos...
'*==========================================================================================*
Public Function GridAnexos4(tpanId, projId, cntrId, url, ordem, offline)
	Dim NomeMes(12)
	NomeMes(1) = "JAN"
	NomeMes(2) = "FEV"
	NomeMes(3) = "MAR"
	NomeMes(4) = "ABR"
	NomeMes(5) = "MAI"
	NomeMes(6) = "JUN"
	NomeMes(7) = "JUL"
	NomeMes(8) = "AGO"
	NomeMes(9) = "SET"
	NomeMes(10) = "OUT"
	NomeMes(11) = "NOV"
	NomeMes(12) = "DEZ"
	'*========================================================================
	'* Obtem projetos do cliente
	'*========================================================================
	if Session("perfId") = "2" then
		xSQL = "select projId from Projeto where usuaIdGer = " & Session("perfId")
		
	end if
	
	xSQL = "select tpanGlob, tpanUnic, tpanForm, tpanProj, tpanDescTela from TipoAnexo where tpanId = " & tpanId
	set xRs = gConexao.execute(xSQL)
	xtpanUnic = xRs("tpanUnic")
	xtpanForm = xRs("tpanForm")
	xtpanProj = xRs("tpanProj")
	xtpanGlob = xRs("tpanGlob")
	xtpanDescTela = xRs("tpanDescTela")
	if xRs("tpanUnic") = true then
		xAnexAtiv = "1"
	else
		xAnexAtiv = "1, 0"
	end if

	Response.write "<!--**************************************************************-->" & vbcrlf
	Response.write "<!-- Inicio output rotina GridAnexos4                             -->" & vbcrlf
	Response.write "<!--**************************************************************-->" & vbcrlf
	Response.write "<tr>" & vbcrlf
	Response.write "<td colspan=""2"" height=""15""></td>" & vbcrlf
	Response.write "</tr>" & vbcrlf
	Response.write "<tr>" & vbcrlf
	Response.write "    <td class=""docTit"">" & xTpanDescTela & "</td>" & vbcrlf
	'===========================================================
	' Controla permissoes para adi��o de novos documentos
	'===========================================================
	if (Session("perfId") = "5" or Session("perfId") = "1") and offline="" then
		Response.write "	<td align=""right""><a href=""javascript:AbrirJanela('anxUpload1.asp?tpanId=" & tpanId & "&projId=" & projId & "&cntrId=" & cntrId & "',600,250);""><img src=""../images/newitem.gif"" class=""botao"">&nbsp;Carregar novo</a></td>" & vbcrlf
	else
		Response.write "	<td align=""right"">&nbsp;</td>" & vbcrlf
	end if							
	Response.write "</tr>" & vbcrlf
	Response.write "<tr>" & vbcrlf
	Response.write "    <td colspan=""2"" background=""../images/pont_cinza_h.gif"" style=""height:2""></td>" & vbcrlf
	Response.write "</tr>" & vbcrlf
	Response.write "<tr>" & vbcrlf
	Response.write "    <td colspan=""2"" height=""5""></td>" & vbcrlf
	Response.write "</tr>" & vbcrlf
	Response.write "<tr>" & vbcrlf
	Response.write " <td colspan=""2"">" & vbcrlf

	xCampoOrdem = Request.QueryString("CampoOrdem" & tpanId)
	if xCampoOrdem = "" then
		xCampoOrdem = Session("CampoOrdem" & tpanId)
		if xCampoOrdem = "" then
			xCampoOrdem = "anexNum"
		end if			
	end if
	Session("CampoOrdem" & tpanId) = xCampoOrdem
	
	SortNome = ""
	SortRev = ""
	SortNum = ""
	if xCampoOrdem = "anexNome" then SortNome = "<img src=""../images/sort.gif"">"
	if xCampoOrdem = "anexNum" then SortNum = "<img src=""../images/sort.gif"">"
	if xCampoOrdem = "anexRev" then SortRev = "<img src=""../images/sort.gif"">"

'	if xtpanProj = true then
'		if xCampoOrdem = "anexRev" then
'			xCampoOrdem = "anexRevNum"
'		end if
'	end if

	xSQL = ""
	xSQL = xSQL & "select anexNomeOrig"
	xSQL = xSQL & "     , anexTam"
	xSQL = xSQL & "     , anexNome"
	xSQL = xSQL & "     , anexId"
	xSQL = xSQL & "     , anexRevNum"
	xSQL = xSQL & "     , anexRevMes"
	xSQL = xSQL & "     , anexRevAno"
    xSQL = xSQL & "     , ((anexRevAno * 100) + anexRevMes) anexRev"
	xSQL = xSQL & "     , anexNum"
	xSQL = xSQL & " from Anexo"
	xSQL = xSQL & " where projId = " & projId
	xSQL = xSQL & "  and cntrId = " & cntrId
	xSQL = xSQL & "  and tpanId = " & tpanId 
	xSQL = xSQL & "  and anexAtiv in (" & xAnexAtiv & ")"
	xsql = xsql & "  and anexRepr = 0"
	xSQL = xSQL & " order by " & xCampoOrdem
	set xRs = gConexao.Execute(xSQL)
	x = ""

	if xtpanGlob then
		xLabelNumero = "Norma"
	else
		xLabelNumero = "N�mero"
	end if

	Response.write vbcrlf
	Response.write "<table class=""grid"" width=""100%"">" & vbcrlf
	Response.write vbtab & "<tr>" & vbcrlf
	Response.write vbtab & vbtab & "<td width=""5%"" class=""GridCab"">&nbsp;</td>" & vbcrlf
	If offline="" then	
		Response.write vbtab & vbtab & "<td width=""20%"" class=""GridCab""><a href=""" & url & "&CampoOrdem" & tpanId & "=anexNum"  & """ class=""GridCab"" title=""Clique para ordenar por essa coluna"">" & xLabelNumero & "</a>" & SortNum  & "</td>" & vbcrlf
		Response.write vbtab & vbtab & "<td width=""55%"" class=""GridCab""><a href=""" & url & "&CampoOrdem" & tpanId & "=anexNome" & """ class=""GridCab"" title=""Clique para ordenar por essa coluna"">T�tulo</a>"  & SortNome & "</td>" & vbcrlf
		Response.write vbtab & vbtab & "<td width=""10%"" class=""GridCab""><a href=""" & url & "&CampoOrdem" & tpanId & "=anexRevNum"  & """ class=""GridCab"" title=""Clique para ordenar por essa coluna"">Revis�o</a>" & SortRev  & "</td>" & vbcrlf
	else
		Response.write vbtab & vbtab & "<td width=""20%"" class=""GridCab""><a href=""#"" class=""GridCab"">" & xLabelNumero & "</a>" & SortNum  & "</td>" & vbcrlf
		Response.write vbtab & vbtab & "<td width=""55%"" class=""GridCab""><a href=""#"" class=""GridCab"">T�tulo</a>"  & SortNome & "</td>" & vbcrlf
		Response.write vbtab & vbtab & "<td width=""10%"" class=""GridCab""><a href=""#"" class=""GridCab"">Revis�o</a>" & SortRev  & "</td>" & vbcrlf
	end if
	Response.write vbtab & vbtab & "<td width=""10%"" class=""GridCab"">&nbsp;</td>" & vbcrlf
	Response.write vbtab & "</tr>" & vbcrlf
	
	if not xRs.eof then
		estilo = "GridLinhaPar"
		while not xRs.Eof
			Response.write x & "<tr>" & vbcrlf
			xExt = Right(xRs("anexNomeOrig"), 3)
			xExt = lcase(xExt)
			if xExt = "doc" or xExt = "xls" or xExt = "mpp" or xExt = "ppt" then
			else
				xExt = "xxx"
			end if
			if offline="" then
				imagem = "<a href=""download.aspx?anexId=" & xRs("anexId") & """><img src=""../images/ic" & xExt & ".gif""></a>"
			else
				anexExt = right(xRs("anexNomeOrig"), len(xRs("anexNomeOrig")) - InStrRev(xRs("anexNomeOrig"), "."))
				imagem = "<a href=""../Anexos/" & right("00000000" & xRs("anexId"), 8) & "." & anexExt & """><img src=""../images/ic" & xExt & ".gif""></a>"
			end if
			x = "        "
			Response.write x & "<td class=""" & estilo & """>" & imagem & "</td>" & vbcrlf 
			Response.write x & "<td class=""" & estilo & """ valign=""middle"">" & xRs("anexNum") & "</td>" & vbcrlf 
			Response.write x & "<td class=""" & estilo & """ valign=""middle"">" & xRs("anexNome") & "</td>" & vbcrlf 
			Response.write x & "<td class=""" & estilo & """>" & xRs("anexRevNum") & "</td>" & vbcrlf 
			if Session("perfId") = "5" and offline="" then
				Response.write x & "<td class=""" & estilo & """ style=""align:right"">"
				Response.write "<a href=""javascript:AbrirJanela('anxUpload1.asp?tpanId=" & tpanId & "&projId=" & projId & "&cntrId=" & cntrId & "&anexId=" & xRs("anexId") & "&novaVer=1',600,250);"" class=""GridCab"" onmouseover=""javascript:document['revisar" & xRs("anexId")  & "'].src='../images/botaoOkA.jpg';"" onmouseout=""javascript:document['revisar" & xRs("anexId") & "'].src='../images/botaoOk.jpg';""><img name=""revisar" & xRs("anexId") & """ id=""revisar" & xRs("anexId") & """ src=""../images/botaoOk.jpg"" title=""Adicionar nova vers�o""></a>"
				Response.write "<a href=""javascript:AbrirJanela('anxExcluir.asp?anexId=" & xRs("anexId") & "',600,250);"" class=""GridCab"" onmouseover=""javascript:document['excluir" & xRs("anexId")  & "'].src='../images/botaoXA.jpg';"" onmouseout=""javascript:document['excluir" & xRs("anexId") & "'].src='../images/botaoX.jpg';""><img name=""excluir" & xRs("anexId") & """ id=""excluir" & xRs("anexId") & """ src=""../images/botaoX.jpg"" title=""Excluir esse documento""></a></td>" & vbcrlf
			else
				Response.write x & "<td class=""" & estilo & """ style=""align:left"">&nbsp;</td>" & vbcrlf
			end if			
			x = "    "
			Response.write x & "</tr>" & vbcrlf 
			xRs.Movenext
			if estilo = "GridLinhaPar" then
				estilo = "GridLinhaImpar"
			else
				estilo = "GridLinhaPar"
			end if
		wend
	else
		Response.write "<tr><td colspan=""5"" class=""gridlinha"">(nenhum documento cadastrado nessa �rea)</td></tr>"
	end if
	Response.write x & "<tr>" & vbcrlf
	x = "        "
	Response.write x & "<td class=""GridRodape"">&nbsp;</td>" & vbcrlf
	Response.write x & "<td class=""GridRodape"">&nbsp;</td>" & vbcrlf
	Response.write x & "<td class=""GridRodape"">&nbsp;</td>" & vbcrlf
	Response.write x & "<td class=""GridRodape"">&nbsp;</td>" & vbcrlf
	Response.write x & "<td class=""GridRodape"">&nbsp;</td>" & vbcrlf
	x = "    "
	Response.write x & "</tr>" & vbcrlf
	Response.write "</table>" & vbcrlf
	Response.write "</td>" & vbcrlf
	Response.write "</tr>" & vbcrlf 
	Response.write "<!--**************************************************************-->" & vbcrlf
	Response.write "<!-- Fim output rotina GridAnexos4                                -->" & vbcrlf
	Response.write "<!--**************************************************************-->" & vbcrlf

End Function

'*==========================================================================================*
'* GridContratos - Grid para documentos com aprova��o
'*==========================================================================================*
Public Function GridContratos(tpanId, projId, cntrId, url, offline)
	xSQL = "select tpanUnic, tpanForm, tpanDescTela from TipoAnexo where tpanId = " & tpanId
	set xRs = gConexao.execute(xSQL)
	xtpanUnic = xRs("tpanUnic")
	xtpanForm = xRs("tpanForm")
'	xtpanDescTela = xRs("tpanDescTela") & " tpanId=" & tpanId & " projId=" & projId & " cntrId=" & cntrId
	xtpanDescTela = xRs("tpanDescTela") 

	Response.write "<!--**************************************************************-->" & vbcrlf
	Response.write "<!-- Inicio output rotina GridContratos                            -->" & vbcrlf
	Response.write "<!--**************************************************************-->" & vbcrlf
	Response.write "<tr>" & vbcrlf
	Response.write "<td colspan=""2"" height=""5""></td>" & vbcrlf
	Response.write "</tr>" & vbcrlf
	Response.write "<tr>" & vbcrlf
	Response.write "    <td class=""docTit"">" & xTpanDescTela & "</td>" & vbcrlf
	'if (Session("perfId") = "5" or Session("perfId") = "1") and offline="" then
	if (Session("perfId") = "5" or Session("perfId") = "1" or Session("perfId") = "4") and offline="" then	' Solicia��o Juliano 17/3/2009
		Response.write "	<td align=""right""><a href=""javascript:AbrirJanela('anxUpload1.asp?tpanId=" & tpanId & "&projId=" & projId & "&cntrId=" & cntrId & "',600,250);""><img src=""../images/newitem.gif"" class=""botao"">&nbsp;Carregar novo</a></td>" & vbcrlf
	else
		Response.write "	<td align=""right"">&nbsp;</td>" & vbcrlf
	end if
	Response.write "</tr>" & vbcrlf
	Response.write "<tr>" & vbcrlf
	Response.write "    <td colspan=""2"" background=""../images/pont_cinza_h.gif"" style=""height:2""></td>" & vbcrlf
	Response.write "</tr>" & vbcrlf
	Response.write "<tr>" & vbcrlf
	Response.write " <td colspan=""2"">" & vbcrlf

	xCampoOrdem = Request.QueryString("CampoOrdem" & tpanId)
	if xCampoOrdem = "" then
		xCampoOrdem = Session("CampoOrdem" & tpanId)
		if xCampoOrdem = "" then
			xCampoOrdem = "anexNum"
		end if			
	end if
	Session("CampoOrdem" & tpanId) = xCampoOrdem
	
	
	SortNome = ""
	SortRev = ""
	SortNum = ""
	if xCampoOrdem = "anexNome" then SortNome = "<img src=""../images/sort.gif"">"
	if xCampoOrdem = "anexNum" then SortNum = "<img src=""../images/sort.gif"">"
	if xCampoOrdem = "anexRev" then SortRev = "<img src=""../images/sort.gif"">"

	if xCampoOrdem = "anexNum" then
		xCampoOrdem = "anexNum, anexRevNum"
	end if

	xSQL = ""
	xSQL = xSQL & "select anexNomeOrig"			
	xSQL = xSQL & "     , anexTam"
	xSQL = xSQL & "     , anexNome"
	xSQL = xSQL & "     , anexId"
	xSQL = xSQL & "     , anexNum"
	xSQL = xSQL & "     , anexDtUpl"
	xSQL = xSQL & "     , anexAprv"
	xSQL = xSQL & "     , anexRevNum "
	xSQL = xSQL & " from Anexo , Contrato"
	xSQL = xSQL & " where Anexo.cntrId    = Contrato.cntrId"
	xSQL = xSQL & "   and Contrato.cntrId = " & cntrId
	xSQL = xSQL & "   and Contrato.projId = " & projId
	xSQL = xSQL & "   and Anexo.projId    = " & projId
	xSQL = xSQL & "   and tpanId          = " & tpanId 
	xSQL = xSQL & "   and anexAtiv = 1"
	xSQL = xSQL & "   and anexRepr = 0"
	xSQL = xSQL & " order by " & xCampoOrdem
	set xRs = gConexao.Execute(xSQL)
	x = ""
	Response.write vbcrlf
	Response.write "<table class=""grid"" width=""100%"">" & vbcrlf
	Response.write vbtab & "<tr>" & vbcrlf
	Response.write vbtab & vbtab & "<td width=""5%"" class=""GridCab"">&nbsp;</td>" & vbcrlf
	if offline="" then
		Response.write vbtab & vbtab & "<td width=""20%"" class=""GridCab""><a href=""" & url & "&CampoOrdem" & tpanId & "=anexNum" & """ class=""GridCab"">N�mero</a>" & SortNum & "</td>" & vbcrlf
		Response.write vbtab & vbtab & "<td width=""55%"" class=""GridCab""><a href=""" & url & "&CampoOrdem" & tpanId & "=anexNome" & """ class=""GridCab"">T�tulo</a>" & SortNome & "</td>" & vbcrlf
		Response.write vbtab & vbtab & "<td width=""10%"" class=""GridCab""><a href=""" & url & "&CampoOrdem" & tpanId & "=anexRev" & """ class=""GridCab"">Revis�o</a>" & SortRev & "</td>" & vbcrlf
	else
		Response.write vbtab & vbtab & "<td width=""20%"" class=""GridCab""><a href=""#"">N�mero</a>" & SortNum & "</td>" & vbcrlf
		Response.write vbtab & vbtab & "<td width=""55%"" class=""GridCab""><a href=""#"">T�tulo</a>" & SortNome & "</td>" & vbcrlf
		Response.write vbtab & vbtab & "<td width=""10%"" class=""GridCab""><a href=""#"">Revis�o</a>" & SortRev & "</td>" & vbcrlf
	end if
	Response.write vbtab & vbtab & "<td width=""10%"" class=""GridCab"">&nbsp;</td>" & vbcrlf
	Response.write vbtab & "</tr>" & vbcrlf
	Response.write vbcrlf
	
	if not xRs.eof then
		estilo = "GridLinhaPar"
		while not xRs.Eof
			Response.write x & "<tr>" & vbcrlf
			xExt = Right(xRs("anexNomeOrig"), 3)
			xExt = lcase(xExt)
			if xExt = "doc" or xExt = "xls" or xExt = "mpp" or xExt = "ppt" then
			else
				xExt = "xxx"
			end if
			if offline="" then
				imagem = "<a href=""download.aspx?anexId=" & xRs("anexId") & """><img src=""../images/ic" & xExt & ".gif""></a>"
			else
				anexExt = right(xRs("anexNomeOrig"), len(xRs("anexNomeOrig")) - InStrRev(xRs("anexNomeOrig"), "."))
				imagem = "<a href=""../Anexos/" & right("00000000" & xRs("anexId"), 8 ) & "." & anexExt & """><img src=""../images/ic" & xExt & ".gif""></a>"
			end if
			x = "        "
			Response.write x & "<td class=""" & estilo & """>" & imagem & "</td>" & vbcrlf 
			Response.write x & "<td class=""" & estilo & """ valign=""middle"">" & xRs("anexNum") & "</td>" & vbcrlf 
			Response.write x & "<td class=""" & estilo & """ valign=""middle"">" & xRs("anexNome") & "</td>" & vbcrlf 
			Response.write x & "<td class=""" & estilo & """ valign=""middle"">" & xRs("anexRevNum") & "</td>" & vbcrlf 
			'if (Session("perfId") = "5" or Session("perfId") = "1") and offline="" then
			if (Session("perfId") = "5" or Session("perfId") = "1" or Session("perfId") = "4") and offline="" then	' SOlicita��o do Juliano 17/3/2009
				Response.write x & "<td class=""" & estilo & """ valign=""middle"" align=""right""><a href=""javascript:AbrirJanela('anxUpload1.asp?tpanId=" & tpanId & "&projId=" & projId & "&cntrId=" & cntrId & "&anexId=" & xRs("anexId") & "',600,250);"" class=""GridCab"" onmouseover=""javascript:document['revisar" & xRs("anexId")  & "'].src='../images/botaoOkA.jpg';"" onmouseout=""javascript:document['revisar" & xRs("anexId") & "'].src='../images/botaoOk.jpg';""><img name=""revisar" & xRs("anexId") & """ id=""revisar" & xRs("anexId") & """ src=""../images/botaoOk.jpg""></a>" & vbcrlf 
				Response.write "<a href=""javascript:AbrirJanela('anxExcluir.asp?anexId=" & xRs("anexId") & "',600,250);"" class=""GridCab"" onmouseover=""javascript:document['excluir" & xRs("anexId")  & "'].src='../images/botaoXA.jpg';"" onmouseout=""javascript:document['excluir" & xRs("anexId") & "'].src='../images/botaoX.jpg';""><img name=""excluir" & xRs("anexId") & """ id=""excluir" & xRs("anexId") & """ src=""../images/botaoX.jpg""></a></td>" & vbcrlf 
			else
				Response.write x & "<td class=""" & estilo & """ valign=""middle"">&nbsp;</td>" & vbcrlf 
			end if
			Response.write x & "</tr>" & vbcrlf 
			xRs.Movenext
		wend
	else
		Response.write "<tr><td colspan=""7"" class=""gridlinha"">(nenhum documento cadastrado nessa �rea)</td></tr>"
	end if
	Response.write x & "<tr>" & vbcrlf
	x = "        "
	Response.write x & "<td class=""GridRodape"">&nbsp;</td>" & vbcrlf
	Response.write x & "<td class=""GridRodape"">&nbsp;</td>" & vbcrlf
	Response.write x & "<td class=""GridRodape"">&nbsp;</td>" & vbcrlf
	Response.write x & "<td class=""GridRodape"">&nbsp;</td>" & vbcrlf
	Response.write x & "<td class=""GridRodape"">&nbsp;</td>" & vbcrlf
	x = "    "
	Response.write x & "</tr>" & vbcrlf
	Response.write "</table>" & vbcrlf
	Response.write "</td>" & vbcrlf
	Response.write "</tr>" & vbcrlf 
	Response.write "<!--**************************************************************-->" & vbcrlf
	Response.write "<!-- Fim output rotina GridContratos                              -->" & vbcrlf
	Response.write "<!--**************************************************************-->" & vbcrlf

End Function

%>