<% Response.Expires= -1%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Controle remoto de projetos</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<link rel="stylesheet" type="text/css" href="../EstiloMenu.css">
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<script language="JavaScript" type="text/javascript" src="../scripts/AbrirJanela.js"></script>
	</HEAD>
	<body class="cabecalho" >
		<!--#include file="../includes/varControleAcesso.inc"-->
		<!--#include file="../includes/varBanco.inc"-->
<%  lPerfisAutorizados = "15"
	VerificarAcesso()

   gMenuSelecionado = "Usuarios"
  ' gOpcaoSelecionada = "Usuarios"
   gmenuGeral = "Configuracao"
   PastaSelecionada = "Configura��o"
%>
		<!--#include file="../includes/cab.inc"-->
			<tr>
				<td style="padding:0px" valign="top" class="blocoOpcoes">
					<!--#include file="../includes/opcConfig.inc"-->
				</td>
				<td colspan="2" valign="top" style="padding:5px" class="blocoPagina">
				<form>
					<table width="100%">
					<tr>
						<td class="docTit">USU�RIOS CADASTRADOS</td>
					<% if TemAcesso("USRADC") then %>
						<td align="right"><a href="javascript:AbrirJanela('cfgUsuaAlterar1.asp?usuaId=0',440,400);"><img src="../images/newitem.gif" class="botao">&nbsp;Criar novo</a></td>
					<% else %>
						<td align="right">&nbsp;</td>
					<% end if %>
					</tr>
					<tr>
						<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
					</tr>
					<tr>
						<td colspan="2" style="height:5"></td>
					</tr>
					<tr>
						<td colspan="2">
							<table class="grid">
<% 	xCampoOrdem = Request.QueryString("CampoOrdem") 
	if xCampoOrdem = "" then
		xCampoOrdem = "usuaLogi"
	end if
	SortLogi = ""
	SortNome = ""
	SortPerf = ""
	SortEmpr = ""
	if xCampoOrdem = "usuaLogi" then SortLogi = "<img src=""../images/sort.gif"">"
	if xCampoOrdem = "usuaNome" then SortNome = "<img src=""../images/sort.gif"">"
	if xCampoOrdem = "perfNome" then SortPerf = "<img src=""../images/sort.gif"">"
	if xCampoOrdem = "emprNome" then SortEmpr = "<img src=""../images/sort.gif"">"
%>
								<tr>
									<td width="20"  class="GridCab">&nbsp;</td>
									<td width="80"  class="GridCab"><a href="cfgUsuaListar.asp?CampoOrdem=usuaLogi" class="GridCab">Login</a><%=SortLogi%></td>
									<td width="150"   class="GridCab"><a href="cfgUsuaListar.asp?CampoOrdem=usuaNome" class="GridCab">Nome usu�rio</a><%=SortNome%></td>
									<td width="120" class="GridCab"><a href="cfgUsuaListar.asp?CampoOrdem=perfNome" class="GridCab">Perfil</a><%=SortPerf%></td>
									<td class="GridCab">&nbsp;</td>
									<td class="GridCab">&nbsp;</td>
								</tr>
	<%

	if TemAcesso("USRALT") then
		acessoOk = true
	else
		acessoOk = false
	end if

	xSQL = "select usuaID, usuaNome, usuaLogi, perfNome "
	xSQL = xSQL & " from Usuario, PerfilAcesso"
	xSQL = xSQL & " where PerfilAcesso.perfId = Usuario.perfId"
	xSQL = xSQL & "   and Usuario.emprId      = 1"
	xSQL = xSQL & "   and Usuario.usuaAtivo   = 1"
	xSQL = xSQL & " order by " & xCampoOrdem
	AbrirConexao
	Set xRs = gConexao.Execute(xSQL)
	if xRs.EOF then
	    Response.write "<tr>" & vbcrlf
	    Response.write "<td colspan=""3"">Nenhum usu�rio cadastrado</td>" & vbcrlf
	    Response.write "</tr>" & vbcrlf
	else
		estilo = "GridLinhaPar"
		while not xRs.EOF
			Response.write "<tr>" & vbcrlf
			if acessoOk then
				response.write vbtab & "<td class=""" & estilo & """><a href=""javascript:AbrirJanela('cfgUsuaAlterar1.asp?usuaId=" & xRs("usuaId") & "',440,270);""><img src=""../images/folder.gif"" style=""border:0""></a></td>"
			else
				response.write vbtab & "<td class=""" & estilo & """><img src=""../images/folder.gif"" style=""border:0""></td>"
			end if
			response.write vbtab & "<td class=""" & estilo & """>" & xRs("usuaLogi") & "</td>"
			response.write vbtab & "<td class=""" & estilo & """>" & xRs("usuaNome") & "</td>"
			response.write vbtab & "<td class=""" & estilo & """>" & xRs("perfNome") & "</td>"
			response.write vbtab & "<td align=""right"" class=""" & estilo & """><a href=""javascript:AbrirJanela('cfgUsuaExcluir1.asp?usuaId=" & xRs("usuaId") & "',600,250);"" class=""GridCab"" onmouseover=""javascript:document['excluir" & xRs("usuaId")  & "'].src='../images/botaoXA.jpg';"" onmouseout=""javascript:document['excluir" & xRs("usuaId") & "'].src='../images/botaoX.jpg';""><img name=""excluir" & xRs("usuaId") & """ id=""excluir" & xRs("usuaId") & """ src=""../images/botaoX.jpg""></a></td>"
			response.write vbtab & "<td class=""" & estilo & """>&nbsp;</td>"
			Response.write "</tr>" & vbcrlf
			xRs.MoveNext
			if estilo = "GridLinhaPar" then
				estilo = "GridLinhaImpar"
			else
				estilo = "GridLinhaPar"
			end if
		wend
	end if
	%>
								<tr>
									<td colspan="6" class="GridRodape">&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2" class="opcSubMenu">(<b>dica</b>: clique no t�tulo das colunas para mudar a ordena��o da consulta.)</td>
					</tr>
					</table>
				</form>
				</td>
			</tr>
			
		<!--#include file="../includes/rod.inc"-->
	</body>
</HTML>
