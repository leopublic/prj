<!--#include file="../includes/clsBanco.inc"-->
<!--#include file="../includes/funcoes.inc"-->
<%

Dim gConexao
AbrirConexao

function e_niveis(nr_niveis, bo_fim, bo_fim_nivel_acima)
	retorno = ""
	''
    for i = 1 to nr_niveis - 1
    	if bo_fim_nivel_acima then
    		retorno = retorno & "&nbsp;<img src='tree/img/empty.gif' style='width:18px; height:18px;' />"
    	else
    		retorno = retorno & "&nbsp;<img src='tree/img/line.gif' style='width:18px; height:18px;' />"
    	end if
    next
    if bo_fim = true then
    	retorno = retorno & "&nbsp;<img src='tree/img/joinbottom.gif' style='width:18px; height:18px;' />"
    else
	    retorno = retorno & "&nbsp;<img src='tree/img/join.gif' style='width:18px; height:18px;' />"
    end if
    ''
    e_niveis = retorno & "&nbsp;"
end function

lTX_Sql1 = "SELECT distinct d.dwbcId, d.dwbcNome, d.dwbcCodWbs, d.dwbcComplemento, d.wbscId FROM DisciplinaWbsContrato d, AtividadeWbsContrato x, ItemWbsContrato y where d.wbscId = " & request.querystring("wbscId") & " and d.dwbcId = x.dwbcId and x.awbcId = y.awbcId and y.stwbId = " & request.querystring("tipo") & " order by d.dwbcCodWbs "
Set lRS_1 = gConexao.execute(lTX_Sql1)

llinha = 0

If (not lRS_1.BOF) and (not lRS_1.EOF) then

	lbe = " style='border:0px;' "
	lbn = " style='border-right:dotted 1px #999;padding-left:2px;' "
	lbd = " style='border-left:dotted 1px #999;padding-left:2px;' "

	response.write AcertaCaracteresHTML("<table class='gridWBS'>") & vbcrlf
	response.write AcertaCaracteresHTML("<tr>") & vbcrlf
	response.write AcertaCaracteresHTML("<th" & lbn &  "style='width:25px' class='cpInd'>&nbsp;</th>") & vbcrlf
	response.write AcertaCaracteresHTML("<th style='width:100px'>WBS</th>") & vbcrlf
	response.write AcertaCaracteresHTML("<th style='width:300px'>Nome</th>") & vbcrlf
	response.write AcertaCaracteresHTML("<th style='width:70px'>Unidade</th>") & vbcrlf
	response.write AcertaCaracteresHTML("<th style='width:70px'>Tipo</th>") & vbcrlf

	select case request.querystring("tipo")
		case 4 'Boletim de Medi��o(Informa��o de conclus�o pelo fornecedor)
			response.write AcertaCaracteresHTML("<th style='width:70px'>Conclu�da</th>") & vbcrlf
			response.write AcertaCaracteresHTML("<th style='width:70px'>Data T�rmino</th>") & vbcrlf
			lcs = "7"
		case 5 'Boletim de Medi��o(VERIFICA��O PELA WHITE)
			response.write AcertaCaracteresHTML("<th style='width:50px'>Verificada?</th>") & vbcrlf
			response.write AcertaCaracteresHTML("<th style='width:300px'>Verificada Por</th>") & vbcrlf
			response.write AcertaCaracteresHTML("<th style='width:70px'>Verificada Em</th>") & vbcrlf
			lcs = "8"
		case else
			lcs = "5"
	end select
	response.write AcertaCaracteresHTML("</tr>") & vbcrlf

	lnd = 0
	lna = 0
	lni = 0

	While not lRS_1.EOF

		llinha = llinha + 1
		response.write AcertaCaracteresHTML("<tr class='disciplina'><td" & lbn &  "class='cpInd'>" & llinha & "</td><td colspan='" & lcs & "'><input id='i_dwbc_" & lRS_1("dwbcId")& "_id' name='i_dwbc' type='hidden' value='" & lRS_1("dwbcId") & "' />&nbsp;" & lRS_1("dwbcCodWbs") & " - " & lRS_1("dwbcNome") & "(<input id='i_dwbc_" & lRS_1("dwbcId") & "_Complemento' type='text' value='" & lRS_1("dwbcComplemento") & "' />)</td></tr>") & vbcrlf

		lTX_Sql2 = "SELECT distinct a.awbcId, a.dwbcId, a.awbcNome, a.awbcCodWbs, a.awbcComplemento FROM AtividadeWbsContrato a, ItemWbsContrato x where a.dwbcId = " & lRS_1("dwbcId") & " and a.awbcId = x.awbcId and x.stwbId = " & request.querystring("tipo") & " order by a.awbcCodWbs "
		Set lRS_2 = gConexao.execute(lTX_Sql2)
		lNR_TOT_ATIV = lRS_2.RecordCount

		if(lNR_TOT_ATIV <> 0) then
			lna = 0
			while not lRS_2.EOF

				llinha = llinha + 1

				la = ""
				la = la & "<tr class='atividade'><td" & lbn &  " class='cpInd'>" & llinha & "</td><td colspan='" & lcs & "'><input id='i_awbc_" & lRS_2("awbcId") & "_id' name='i_dwbc_" & lRS_1("dwbcId") & "' type='hidden' value='" & lRS_2("awbcId") & "' />$_N_$" & lRS_2("awbcCodWbs") & " - " & lRS_2("awbcNome") & "(<input id='i_awbc_" & lRS_2("awbcId") & "_Complemento' type='text' value='" & lRS_2("awbcComplemento") & "' />)</td></tr>" & vbcrlf

				lTX_Sql3 = "SELECT distinct i.iwbcId, i.tiwbId, i.unimId, i.awbcId, i.iwbcNome, i.iwbcComplemento, i.iwbcCodigo, i.iwbcPercPai, u.unimAbrev, t.tiwbDescricao, i.iwbcQtdEst, i.iwbcValUnit, i.iwbcQtdAtual, i.iwbcQtdHH, i.iwbcDtInicioEst, i.iwbcDtTerminoEst, i.iwbcConcluida, i.iwbcDataTermino, i.iwbcVerificada, i.iwbcVerificadaPor, i.iwbcVerificadaEm FROM AtividadeWbsContrato a, ItemWbsContrato i, UnidadeMedida u, TipoItemWbs t where a.awbcId = i.awbcId and u.unimId = i.unimId and t.tiwbId = i.tiwbId and i.awbcId = " & lRS_2("awbcId") & " and i.stwbId = " & request.querystring("tipo") & " order by i.iwbcCodigo "
				Set lRS_3 = gConexao.execute(lTX_Sql3)
				if(Not lRS_3.BOF) and (Not lRS_3.EOF)then
					li = "" & vbcrlf

					lordem = "impar"
					lni = 0
					While Not lRS_3.EOF

						llinha = llinha + 1

						if lordem = "impar" then
							lordem = "par"
						else
							lordem = "impar"
						end if

						li = li & "<tr class='" & lordem & "'>" & vbcrlf
						li = li & "<td" & lbn & " class='cpInd'>" & llinha & "</td>" & vbcrlf
						li = li & "<td" & lbn & " class='cpLTexto'>$_N_$&nbsp;<input id='i_iwbc_" & lRS_3("iwbcId") & "_id' name='i_awbc_" & lRS_2("awbcId") & "' type='hidden' value='" & lRS_3("iwbcId") & "' />" & lRS_3("iwbcCodigo") & "</td>" & vbcrlf
						li = li & "<td" & lbn & " class='cpLTexto'>" & lRS_3("iwbcNome") & "(<input id='i_iwbc_" & lRS_3("iwbcId") & "_Complemento' name='i_iwbc_" & lRS_3("iwbcId") & "_Complemento' type='text' value='" & lRS_3("iwbcComplemento") & "' maxlength=100 />)</td>" & vbcrlf
						li = li & "<td" & lbn & " class='cpLCod'>" & lRS_3("unimAbrev") & "</td>" & vbcrlf
						li = li & "<td" & lbn & " class='cpLCod'>" & lRS_3("tiwbDescricao") & "</td>" & vbcrlf

						select case request.querystring("tipo")
							case 4 'Boletim de Medi��o(Informa��o de conclus�o pelo fornecedor)
								if lRS_3("iwbcConcluida") = 1 then
									li = li & "<td" & lbn & " class='cpLCheck'><input id='i_iwbc_" & lRS_3("iwbcId") & "_Concluida' name='i_iwbc_" & lRS_3("iwbcId") & "_Concluida' type='checkbox' checked /></td>" & vbcrlf
								else
									li = li & "<td" & lbn & " class='cpLCheck'><input id='i_iwbc_" & lRS_3("iwbcId") & "_Concluida' name='i_iwbc_" & lRS_3("iwbcId") & "_Concluida' type='checkbox' /></td>" & vbcrlf
								end if

								li = li & "<td" & lbd & " class='cpLData'><input id='i_iwbc_" & lRS_3("iwbcId") & "_DataTermino' name='i_iwbc_" & lRS_3("iwbcId") & "_DataTermino' type='text' value='" & lRS_3("iwbcDataTermino") & "'  maxlength=10 onblur=""javascript:_autocheck(this,'i_iwbc_" & lRS_3("iwbcId") & "_Concluida');""/></td>" & vbcrlf
							case 5 'Boletim de Medi��o(VERIFICA��O PELA WHITE)
								if lRS_3("iwbcVerificada") = 1 then
									li = li & "<td" & lbn & " class='cpLCheck'><input id='i_iwbc_" & lRS_3("iwbcId") & "_Verificada' name='i_iwbc_" & lRS_3("iwbcId") & "_Verificada' type='checkbox' checked maxlength=1 /></td>" & vbcrlf
								else
									li = li & "<td" & lbn & " class='cpLCheck'><input id='i_iwbc_" & lRS_3("iwbcId") & "_Verificada' name='i_iwbc_" & lRS_3("iwbcId") & "_Verificada' type='checkbox' maxlength=1 /></td>" & vbcrlf
								end if

								li = li & "<td" & lbn & " class='cpLTexto'><input id='i_iwbc_" & lRS_3("iwbcId") & "_VerificadaPor' name='i_iwbc_" & lRS_3("iwbcId") & "_VerificadaPor' type='text' value='" & lRS_3("iwbcVerificadaPor") & "' maxlength=200 style='width:100%' onblur=""javascript:_autocheck(this,'i_iwbc_" & lRS_3("iwbcId") & "_Verificada');""/></td>" & vbcrlf
								li = li & "<td" & lbd & " class='cpLData'><input id='i_iwbc_" & lRS_3("iwbcId") & "_VerificadaEm' name='i_iwbc_" & lRS_3("iwbcId") & "_VerificadaEm' type='text' value='" & lRS_3("iwbcVerificadaEm") & "' maxlength=10 onblur=""javascript:_autocheck(this,'i_iwbc_" & lRS_3("iwbcId") & "_Verificada');""/></td>" & vbcrlf
						end select

						li = li & "</tr>" & vbcrlf

						lRS_3.MoveNext

						if lRS_3.EOF then
							li = replace(li,"$_N_$","$_F_$")
						else
							li = replace(li,"$_N_$","$_Z_$")
						end if

						lni = lni + 1
					wend

				End If
				lRS_3.Close

				lRS_2.MoveNext

				If lRS_2.EOF then
					la = replace(la,"$_N_$",e_niveis(1,true, true))
					li = replace(li,"$_F_$",e_niveis(2,true, true))
					li = replace(li,"$_Z_$",e_niveis(2,false, true))
				else
					la = replace(la,"$_N_$",e_niveis(1,false, true))
					li = replace(li,"$_F_$",e_niveis(2,true, false))
					li = replace(li,"$_Z_$",e_niveis(2,false, false))
				end if

				response.write AcertaCaracteresHTML(la)
				response.write AcertaCaracteresHTML(li)
				la = ""
				li = ""

				lna = lna + 1
			wend

		End If
		lRS_2.Close

		lNR_I = lNR_II + 1
		lRS_1.MoveNext

		lnd = lnd + 1
	wend
	response.write AcertaCaracteresHTML("</table>") & vbcrlf
end if
lRS_1.close

%>