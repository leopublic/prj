<%
	Session.LCID = 1046 'Pt-Br'
	Response.Expires= -1
%>
<!--#include file="../includes/wbscontrato_init.inc"-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Controle de WBS</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="JavaScript" type="text/javascript" src="tree/dtree.js"></script>
		<link rel="stylesheet" type="text/css" href="tree/dtree.css">

		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->

		<script language="JavaScript" type="text/javascript" src="../scripts/functions.js"></script>

		<script language="JavaScript" type="text/javascript" src="../scripts/AbrirJanela.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/NumberFormat.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/PreparaJanela.js"></script>

		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<link rel="stylesheet" type="text/css" href="../EstiloMenu.css">

		<SCRIPT language="JavaScript" type="text/javascript">

			//
			var xml_erro = '';
			//
/*
			function _sort_erro_preenchimento(colnum){
				var termo='';
				if(colnum==1){
					termo='LINHA';
				}else if(colnum==2){
					termo='DESCRICAO';
				}
				if(termo!=''){
					var xxx = _xsl_ret.replace('<xsl:sort select="LINHA"/>','<xsl:sort select="'+termo+'"/>');
					_carregaxmlxslt(xml_erro, xxx, 'divErros');
				}
			}
*/
			_xsl_ret=_gerarxsltretorno('LISTA/ERRO', 'LINHA', 'LINHA|DESCRICAO', 'Linha|Descri��o', '50|325');

			function teste_tratarxmlretorno(){
				var xxx = '';

				xxx+='	<ERRO>';
				xxx+='		<NUM_ERRO>456</NUM_ERRO>';
				xxx+='		<DESC_ERRO>teste 2</DESC_ERRO>';
				xxx+='		<QUERY>teste 3</QUERY>';
				xxx+='	</ERRO>';
				xxx+='	<ERRO>';
				xxx+='		<NUM_ERRO>789</NUM_ERRO>';
				xxx+='		<DESC_ERRO>teste 3</DESC_ERRO>';
				xxx+='		<QUERY>teste 3</QUERY>';
				xxx+='	</ERRO>';
				xxx+='	<ERRO>';
				xxx+='		<NUM_ERRO>123</NUM_ERRO>';
				xxx+='		<DESC_ERRO>teste 1</DESC_ERRO>';
				xxx+='		<QUERY>teste 3</QUERY>';
				xxx+='	</ERRO>';
				xxx+='	<IWBC>';
				xxx+='		<IWBCID>1852</IWBCID>';
				xxx+='		<DESCIWBC>oi... tudo bem?</DESCIWBC>';
				xxx+='	</IWBC>';

				//xxx='<LISTA><ERRO><LINHA>43</LINHA><DESCRICAO>Quantidade estimada inv�lida!</DESCRICAO></ERRO><ERRO><LINHA>44</LINHA><DESCRICAO>Valor Unit�rio inv�lido!</DESCRICAO></ERRO> </LISTA>';

				_tratarxmlretorno(xxx);
			}

			/*
				_gerarxsltretorno: gera um xslt de erro padr�o
					- _nm_tag_mae: nome da tag de loop
						Ex1: XMLRESP/ERRO
						Ex2: LISTA/LINHA
					- _colsort: nome da tag que ser� usado pro sort
					- _cols: nome das tags da colunas separadas por "|" pipe
						Ex: NUM_ERRO|DESC_ERRO|QUERY
					- _cols_names: nomes que as tags especificadas em "cols" ir�o receber. Tmb separados pro "|" pipe
						Ex: Erro|Descri��o|SQL Utilizada
					- _cols_largs: larguras das colunas separadas pro "|" pipe
						Ex: 50|325|325
			*/
			function _gerarxsltretorno(_nm_tag_mae, _colsort, _cols, _cols_names, _cols_largs){

				arr_cols       = _cols.split("|");
				arr_cols_names = _cols_names.split("|");
				arr_cols_largs = _cols_largs.split("|");

				var xsl_retorno_h = '';
				var xsl_retorno_r = '';

				if(arr_cols.length!=arr_cols_names.length){
					alert('Erro na passagem de par�metros da fun��o "_gerarxsltretorno"!!!');
					return false
				}else{
					ldivisor_h = '		<th style="border-left:dotted 1px #DDD" width="1px"></th>';
					ldivisor_r = '        		<td style="border-left:solid 1px #efe"> </td>';
					for(ind_gxr=0;ind_gxr<arr_cols.length;ind_gxr++){
						xsl_retorno_h += ldivisor_h;
						xsl_retorno_h += '      <th width="'+arr_cols_largs[ind_gxr]+'px">'+arr_cols_names[ind_gxr]+'</th> ';

						xsl_retorno_r += ldivisor_r;
						xsl_retorno_r += '        		<td style="text-align:left;"><xsl:value-of select="'+arr_cols[ind_gxr]+'" /></td> ';
					}

					xsl_retorno_h = xsl_retorno_h.substr(ldivisor_h.length);
					xsl_retorno_r = xsl_retorno_r.substr(ldivisor_r.length);
				}

				if(xsl_retorno_r!='' && xsl_retorno_r!=''){

					xsl_retorno  = '';
					xsl_retorno += '<?xml version="1.0" encoding="ISO-8859-1"?>';
					xsl_retorno += '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">';
					xsl_retorno += '';
					xsl_retorno += '<xsl:template match="/">';
					xsl_retorno += '    <h2>Retorno de erros</h2> ';
					xsl_retorno += '    <table class="tbErro">';
					xsl_retorno += '      <tr>';
					xsl_retorno += xsl_retorno_h;
					xsl_retorno += '      </tr>';
					xsl_retorno += '      <xsl:for-each select="'+_nm_tag_mae+'">';
					if(_colsort!=''){
						xsl_retorno += '	  <xsl:sort select="'+_colsort+'"/>';
					}
					xsl_retorno += '		<xsl:choose>';
					xsl_retorno += '          <xsl:when test="(position() mod 2)=0">';
					xsl_retorno += '            <tr style="background-color:#9c9;">';
					xsl_retorno += xsl_retorno_r;
					xsl_retorno += '      		</tr>';
					xsl_retorno += '          </xsl:when>';
					xsl_retorno += '          <xsl:otherwise>';
					xsl_retorno += '            <tr style="background-color:#dfd;">';
					xsl_retorno += xsl_retorno_r;
					xsl_retorno += '      		</tr>';
					xsl_retorno += '          </xsl:otherwise>';
					xsl_retorno += '		</xsl:choose>';
					xsl_retorno += '      </xsl:for-each>';
					xsl_retorno += '  </table>';
					xsl_retorno += '</xsl:template>';
					xsl_retorno += '';
					xsl_retorno += '</xsl:stylesheet>';

					return xsl_retorno
				}else{
					alert('Erro no processamento de par�metros da fun��o "_gerarxsltretorno"!!!');
					return false
				}

			}

			function _tratarxmlretorno(xml_retorno){
				if(xml_retorno!=''){
					xml_retorno='<XMLRESP>'+xml_retorno+'</XMLRESP>';

					xsl_retorno=_gerarxsltretorno('XMLRESP/ERRO', 'NUM_ERRO', 'NUM_ERRO|DESC_ERRO|QUERY', 'Erro|Descri��o|Sql Utilizada', '50|325|325');

					if(xsl_retorno!=false){
						return _carregaxmlxslt(xml_retorno, xsl_retorno, 'divErros');
					}else{
						return xsl_retorno
					}

				}else{
					return true
				}
			}

			function _carregaxmlxslt(p_xml, p_xslt, p_id_div_container){
				var docxml=null;
				var docxsl=null;
				var retorno = '';

				document.getElementById(p_id_div_container).innerHTML = '';

				if (window.ActiveXObject){ // code for IE
				  docxml=new ActiveXObject("Microsoft.XMLDOM");
				  docxml.async="false";
				  docxml.loadXML(p_xml);

				  docxsl=new ActiveXObject("Microsoft.XMLDOM");
				  docxsl.async = "false";
				  docxsl.loadXML(p_xslt);

				  retorno = docxml.transformNode(docxsl);

				}else{// code for Mozilla, Firefox, Opera, etc.
					var parser=new DOMParser();
					docxsl=parser.parseFromString(p_xslt,"text/xml");

					docxml=parser.parseFromString(p_xml,"text/xml");

					var xsltProc  = new XSLTProcessor();
					xsltProc.importStylesheet(docxsl);
					var ser = new XMLSerializer();
					var htmlDoc = xsltProc.transformToDocument(docxml);
					retorno = ser.serializeToString(htmlDoc);
				}

				document.getElementById(p_id_div_container).innerHTML = retorno;
				document.getElementById(p_id_div_container).style.display='block';
			}

			function _limparwbs(ptipo){
				//
				var lind_d = 0;
				var lind_a = 0;
				var lind_i = 0;
				//
				var limite_d=document.getElementsByName("i_dwbc").length;
				//
				for(lind_d=0;lind_d<limite_d;lind_d++){
					//pegando id da disciplina atual
					var i_d = document.getElementsByName("i_dwbc")[lind_d].value;
					//limpando dados da disciplina
					document.getElementById("i_dwbc_"+i_d+"_Complemento").value='';
					//pegando n� de atividades vinculadas a disciplina atual
					var limite_a=document.getElementsByName("i_dwbc_"+i_d).length;
					//

					for(lind_a=0;lind_a<limite_a;lind_a++){
						//pegando id da atividade atual
						var i_a = document.getElementsByName("i_dwbc_"+i_d)[lind_a].value;
						//limpando os campos da atividade atual
						document.getElementById("i_awbc_"+i_a+"_Complemento").value='';
						//pegando n� de itens vinculadas a atividade atual
						var limite_i=document.getElementsByName("i_awbc_"+i_a).length;
						//

						for(lind_i=0;lind_i<limite_i;lind_i++){
							//pegando id do item atual
							var i_i = document.getElementsByName("i_awbc_"+i_a)[lind_i].value;
							//limpando os campos da itemWbs atual
							document.getElementById("i_iwbc_"+i_i+"_Complemento").value='';

							if(ptipo==4){
								document.getElementById("i_iwbc_"+i_i+"_Concluida").checked='';
								document.getElementById("i_iwbc_"+i_i+"_DataTermino").value='';
							}else if(ptipo==5){
								document.getElementById("i_iwbc_"+i_i+"_Verificada").checked='';
								document.getElementById("i_iwbc_"+i_i+"_VerificadaPor").value='';
								document.getElementById("i_iwbc_"+i_i+"_VerificadaEm").value='';
							}
						}
					}
				}
			}

			function _salvarwbs(ptipo){
				//
				document.getElementById("divErros").innerHTML = '';
				var msg_erro = ' Ocorreram erros durante o processamento. \n Por favor, verifique as mensagens de erro!';
				//
				if(_validarwbs(ptipo)){
					//
					var lind_d = 0;
					var lind_a = 0;
					var lind_i = 0;
					//
					var xml_ret = '';
					//
					var limite_d=document.getElementsByName("i_dwbc").length;
					//
					for(lind_d=0;lind_d<limite_d;lind_d++){
						//pegando id da disciplina atual
						var i_d = document.getElementsByName("i_dwbc")[lind_d].value;
						//pegando dados da disciplina
						var ldwbcId = document.getElementById("i_dwbc_"+i_d+"_id").value;
						var ldwbcComplemento = document.getElementById("i_dwbc_"+i_d+"_Complemento").value;
						//pegando n� de atividades vinculadas a disciplina atual
						var limite_a=document.getElementsByName("i_dwbc_"+i_d).length;
						//
						//Atualizando disciplina
						var pagina_d = "wbscontrato_atu.asp?tipo=D&dwbcId="+ldwbcId+"&dwbcComplemento="+ldwbcComplemento;

						var xmlhttp_d = null;
						if(window.XMLHttpRequest){ // code for Mozilla, etc.
							xmlhttp_d=new XMLHttpRequest();
						}else{
							if(window.ActiveXObject){ // code for IE
								xmlhttp_d=new ActiveXObject("Microsoft.XMLHTTP");
							}
						}

						xmlhttp_d.open("GET",pagina_d,false);
						xmlhttp_d.send(null);
						xml_ret +=xmlhttp_d.responseText;

						//
						for(lind_a=0;lind_a<limite_a;lind_a++){
							//pegando id da atividade atual
							var i_a = document.getElementsByName("i_dwbc_"+i_d)[lind_a].value;
							//pegando os campos da atividade atual
							var lawbcId = document.getElementById("i_awbc_"+i_a+"_id").value;
							var lawbcComplemento = document.getElementById("i_awbc_"+i_a+"_Complemento").value;
							//pegando n� de itens vinculadas a atividade atual
							var limite_i=document.getElementsByName("i_awbc_"+i_a).length;
							//
							//Atualizando atividade
							var pagina_a = "wbscontrato_atu.asp?tipo=A&awbcId="+lawbcId+"&awbcComplemento="+lawbcComplemento;

							var xmlhttp_a = null;
							if(window.XMLHttpRequest){ // code for Mozilla, etc.
								xmlhttp_a=new XMLHttpRequest();
							}else{
								if(window.ActiveXObject){ // code for IE
									xmlhttp_a=new ActiveXObject("Microsoft.XMLHTTP");
								}
							}

							xmlhttp_a.open("GET",pagina_a,false);
							xmlhttp_a.send(null);
							xml_ret += xmlhttp_a.responseText;
							//
							for(lind_i=0;lind_i<limite_i;lind_i++){
								//pegando id do item atual
								var i_i = document.getElementsByName("i_awbc_"+i_a)[lind_i].value;
								//pegando os campos da itemWbs atual
								var liwbcId = document.getElementById("i_iwbc_"+i_i+"_id").value;
								var liwbcComplemento = document.getElementById("i_iwbc_"+i_i+"_Complemento").value;

								var liwbcQtdEst = '';
								var liwbcValUnit = '';

								var liwbcQtdAtual = '';
								var liwbcQtdHH = '';
								var liwbcDtInicioEst = '';
								var liwbcDtTerminoEst = '';


								var liwbcConcluida = '';
								var liwbcDataTermino = '';

								var liwbcVerificada = '';
								var liwbcVerificadaPor = '';
								var liwbcVerificadaEm = '';

								var envia = 1;

								if(ptipo==4){
									liwbcConcluida = document.getElementById("i_iwbc_"+i_i+"_Concluida").checked;
									liwbcDataTermino = document.getElementById("i_iwbc_"+i_i+"_DataTermino").value;

									/* if(liwbcConcluida=='' && liwbcDataTermino==''){envia=0;} */
									if(liwbcConcluida==''){liwbcConcluida='false';}
									if(liwbcDataTermino==''){liwbcDataTermino=='null';}

								}else if(ptipo==5){
									liwbcVerificada = document.getElementById("i_iwbc_"+i_i+"_Verificada").checked;
									liwbcVerificadaPor = document.getElementById("i_iwbc_"+i_i+"_VerificadaPor").value;
									liwbcVerificadaEm = document.getElementById("i_iwbc_"+i_i+"_VerificadaEm").value;

									/* if(liwbcVerificada=='' && liwbcVerificadaPor=='' && liwbcVerificadaEm==''){envia=0;} */
									if(liwbcVerificada==''){liwbcVerificada=='false';}
									if(liwbcVerificadaPor==''){liwbcVerificadaPor=='null';}
									if(liwbcVerificadaEm==''){liwbcVerificadaEm=='null';}
								}else{envia=0;}

/*
									var pagina_i = "wbscontrato_atu.asp?tipo=I"+ptipo+"&iwbcId="+liwbcId+"&iwbcComplemento="+liwbcComplemento;
									pagina_i += "&iwbcQtdEst="+liwbcQtdEst+"&iwbcValUnit="+liwbcValUnit; //tipo 1
									pagina_i += "&iwbcQtdAtual="+liwbcQtdAtual; //tipo 2
									pagina_i += "&iwbcQtdHH="+liwbcQtdHH+"&iwbcDtInicioEst="+liwbcDtInicioEst+"&iwbcDtTerminoEst="+liwbcDtTerminoEst; //tipo 3
									pagina_i += "&iwbcConcluida="+liwbcConcluida+"&iwbcDataTermino="+liwbcDataTermino; //tipo 4
									pagina_i += "&iwbcVerificada="+liwbcVerificada+"&iwbcVerificadaPor="+liwbcVerificadaPor+"&iwbcVerificadaEm="+liwbcVerificadaEm; //tipo 5
envia = 0;

alert(pagina_i);
*/

								if(envia==1){
									//Atualizando item
									var pagina_i = "wbscontrato_atu.asp?tipo=I"+ptipo+"&iwbcId="+liwbcId+"&iwbcComplemento="+liwbcComplemento;
									pagina_i += "&iwbcQtdEst="+liwbcQtdEst+"&iwbcValUnit="+liwbcValUnit; //tipo 1
									pagina_i += "&iwbcQtdAtual="+liwbcQtdAtual; //tipo 2
									pagina_i += "&iwbcQtdHH="+liwbcQtdHH+"&iwbcDtInicioEst="+liwbcDtInicioEst+"&iwbcDtTerminoEst="+liwbcDtTerminoEst; //tipo 3
									pagina_i += "&iwbcConcluida="+liwbcConcluida+"&iwbcDataTermino="+liwbcDataTermino; //tipo 4
									pagina_i += "&iwbcVerificada="+liwbcVerificada+"&iwbcVerificadaPor="+liwbcVerificadaPor+"&iwbcVerificadaEm="+liwbcVerificadaEm; //tipo 5

									var xmlhttp_i = null;
									if(window.XMLHttpRequest){ // code for Mozilla, etc.
										xmlhttp_i=new XMLHttpRequest();
									}else{
										if(window.ActiveXObject){ // code for IE
											xmlhttp_i=new ActiveXObject("Microsoft.XMLHTTP");
										}
									}

									xmlhttp_i.open("GET",pagina_i,false);
									xmlhttp_i.send(null);
									xml_ret += xmlhttp_i.responseText;
								}
							}
						}
					}

					if(_tratarxmlretorno(xml_ret)){
						_carregagrid(<%=lcntrId%>, <%=lwbscId%>, <%=ltipo%>);
						alert('Dados salvos com sucesso!');
					}else{
						alert(msg_erro);
					}
				}
			}

			function _validarwbs(ptipo){
				//
				var lind_d = 0;
				var lind_a = 0;
				var lind_i = 0;
				var linha  = 0;
				//
				xml_erro = '';
				//
				var limite_d=document.getElementsByName("i_dwbc").length;
				//
				for(lind_d=0;lind_d<limite_d;lind_d++){
					linha++;
					//pegando id da disciplina atual
					var i_d = document.getElementsByName("i_dwbc")[lind_d].value;
					//pegando n� de atividades vinculadas a disciplina atual
					var limite_a=document.getElementsByName("i_dwbc_"+i_d).length;
					//

					for(lind_a=0;lind_a<limite_a;lind_a++){
						linha++;
						//pegando id da atividade atual
						var i_a = document.getElementsByName("i_dwbc_"+i_d)[lind_a].value;
						//pegando n� de itens vinculadas a atividade atual
						var limite_i=document.getElementsByName("i_awbc_"+i_a).length;
						//

						for(lind_i=0;lind_i<limite_i;lind_i++){
							linha++;
							//pegando id do item atual
							var i_i = document.getElementsByName("i_awbc_"+i_a)[lind_i].value;

							if(ptipo==4){
								if(document.getElementById("i_iwbc_"+i_i+"_DataTermino").value!=''){
									if(isDateJS2(document.getElementById("i_iwbc_"+i_i+"_DataTermino").value,1)==false){
										xml_erro += '<ERRO><LINHA>'+linha+'</LINHA><DESCRICAO>Data de t�rmino inv�lida!</DESCRICAO></ERRO> ';
									}

									_autocheck(document.getElementById("i_iwbc_"+i_i+"_DataTermino"),'i_iwbc_'+i_i+'_Concluida');
								}
							}else if(ptipo==5){
								if(document.getElementById("i_iwbc_"+i_i+"_VerificadaEm").value!=''){
									if(isDateJS2(document.getElementById("i_iwbc_"+i_i+"_VerificadaEm").value,1)==false){
										xml_erro += '<ERRO><LINHA>'+linha+'</LINHA><DESCRICAO>Data de "Verificada Em" inv�lida!</DESCRICAO></ERRO> ';
									}

									_autocheck(document.getElementById("i_iwbc_"+i_i+"_VerificadaEm"),'i_iwbc_'+i_i+'_Verificada');
								}

								if(document.getElementById("i_iwbc_"+i_i+"_VerificadaPor").value!=''){
									_autocheck(document.getElementById("i_iwbc_"+i_i+"_VerificadaPor"),'i_iwbc_'+i_i+'_Verificada');
								}
							}
						}
					}
				}

				if(xml_erro!=''){
					alert('Houveram erros de preechimentos, verifique abaixo!');
					xml_erro='<LISTA>'+xml_erro+'</LISTA>';
					_carregaxmlxslt(xml_erro, _xsl_ret, 'divErros');
					return false
				}else{
					return true
				}
			}

			function _overbts(pbtn){
				pbtn.style.color='#8a8';
				pbtn.style.cursor='pointer';
			}

			function _outbts(pbtn){
				pbtn.style.color='#efe';
			}

			function _autocheck(campo_veri, nm_check){
				if(document.getElementById(nm_check).checked){
				}else{
					if(campo_veri.value!=''){
						document.getElementById(nm_check).checked='true';
					}
				}
			}

			function _carregagrid(p_cntrId, p_wbscId, p_tipo){
				document.getElementById('divTab').style.display = 'none';
				var pagina_c = 'wbscontrato_ppueap_carrega.asp?cntrId='+p_cntrId+'&wbscId='+p_wbscId+'&tipo='+p_tipo;
				var xmlhttp_c = null;
				if(window.XMLHttpRequest){
					xmlhttp_c=new XMLHttpRequest();
				}else{
					if(window.ActiveXObject){
						xmlhttp_c=new ActiveXObject("Microsoft.XMLHTTP");
					}
				}
				xmlhttp_c.open("GET",pagina_c,false);
				xmlhttp_c.send(null);
				document.getElementById('divTab').innerHTML = '';
				document.getElementById('divTab').innerHTML = xmlhttp_c.responseText;
				document.getElementById('divTab').style.display = 'block';
			}

			function _finalizarBM(pwbscId){

				var pagina_bm = "wbscontrato_atu.asp?tipo=FBM&wbscId="+pwbscId;
				var xmlhttp_bm = null;
				if(window.XMLHttpRequest){
					xmlhttp_bm=new XMLHttpRequest();
				}else{
					if(window.ActiveXObject){
						xmlhttp_bm=new ActiveXObject("Microsoft.XMLHTTP");
					}
				}
				xmlhttp_bm.open("GET",pagina_bm,false);
				xmlhttp_bm.send(null);
				retorno_bm = xmlhttp_bm.responseText;

				if(retorno_bm!=''){
					if(isNumberJS(retorno_bm)){
						if(retorno_bm=='0'){
							//alert('Falha na finaliza��o do boletim de medi��o!!!');
							alert('WBS finalizada!!!');
							retorno_bm='<%=lwbscId%>';
						}else{
							alert('Boletim de medi��o finalizado com sucesso!!!');
						}
						window.location.href='wbscontrato_ppueap_boletim_lista.asp?projId=<%=lprojId%>&cntrId=<%=lcntrId%>&wbscId='+retorno_bm+'&tipo=<%=ltipo%>';
					}else{
						_tratarxmlretorno(xml_ret);
					}
				}

			}

			function _marcartodos(pname){
				for(lind_t=0;lind_t<document.getElementsByName(pname).length;lind_t++){
					if(document.getElementsByName(pname)[lind_t].checked){
						document.getElementsByName(pname)[lind_t].checked='';
					}else{
						document.getElementsByName(pname)[lind_t].checked='true';
					}
				}
			}

		</SCRIPT>
		<style type='text/css'>
			.gridWBS {border-collapse:collapse;width:100%; border-bottom:2px solid #999; border-top:2px solid #999; margin-bottom:2px;}
			.gridWBS tr {height: 16px}
			.gridWBS th{font-family:tahoma;font-size:8pt; text-align:center; font-weight:bold; color:#1D4F68; vertical-align:center}
			.gridWBS td{font-family:tahoma;font-size:8pt; text-align:left; font-weight:normal; vertical-align:center}

			/*.gridWBS td{border-left:dashed 1px #999;border-right:dashed 1px #999;}*/

			.gridWBS input {height:16px;border-top:0px;border-left:0px;border-bottom:groove 2px #999;border-right:groove 2px #999;background-color:transparent;}

			td.cpLCod{text-align:center;}
			td.cpLCod input {width:100%}

			td.cpLCheck{text-align:center;}
			td.cpLCheck input {border:0px;}

			td.cpLTexto{text-align:left;}
			/*
			td.cpLTexto input{width:100%;}
			*/
			td.cpLValor{text-align:right;}
			td.cpLValor input {text-align:right;width:100%}
			td.cpLData{text-align:center;}
			td.cpLData input {text-align:right;width:100%}

			div.divBotoes {width:100%; border-top:2px solid #999; border-bottom:2px solid #999;padding:5px;text-align:center}

			div.divBotoes .cpBt {
				border:solid 1px #016521;
				border-bottom:solid 2px #aaa;
				border-right:solid 2px #aaa;
				text-transform:uppercase;
				/*text-transform:capitalize;*/
				color:#efe;
				background-color:#128753;
				font-size:10pt;
				font-weight:bold;
			}
/*
			div.divBotoes .cpBt:hover {
				color:#8a8;
				cursor:pointer;
			}
*/
			td.cpInd {font-weight:bold; text-align:center; filter:Alpha(opacity=65);-moz-opacity:.65; font-size:6pt;}

			tr.disciplina{background-color:#dde8ee; width:100%}
			tr.atividade{background-color:#dde8ee; width:100%}

			tr.disciplina td{font-weight:bold;border-bottom:2px solid #999;border-top:2px solid #999;}
			tr.atividade td{font-weight:bold;border-bottom:2px solid #999;border-top:2px solid #999;}

			tr.impar{background-color:#F0F0F0;}
			tr.par{background-color:#FFF;}

			.tbErro {border-collapse:collapse;width:100%;font-family:tahoma;font-size:8pt;color:#dfd;}
			.tbErro th {font-weight:bold;border-top:solid 2px #999;border-bottom:solid 2px #999;background-color:#128753;text-align:center;}
			.tbErro td {font-weight:normal;}
		</style>
<!--
<%
	''Se atendemos a condi��o abaixo � pq estamos criando um novo boletim, logo, devemos clonar o �ltimo!!!
	if ltipo = "4" and request.querystring("criarnovo") = "1" then
		if lwbscFinalizada = "1" then ''� pq o boletim atual j� foi finalizado, logo podemos criar um novo!!!
			gConexao.execute("exec dbo.WBSC_Clonar @pwbscId = " & lwbscId)

			set lRS_1 = gConexao.execute("select w.wbscId_Clone as novoId from WbsContrato w where w.wbscId = " & lwbscId)
			if(Not lRS_1.BOF) and (Not lRS_1.EOF) then
				if not isnull(lRS_1("novoId")) then
					response.write "<script language=""JavaScript"" type=""text/javascript"">"
					response.write "	alert('Boletim de medi��o criado com sucesso!');"
					response.write "	window.location.href='wbscontrato_ppueap_boletim.asp?cntrId=" & lcntrId & "&wbscId=" & lRS_1("novoId") & "&servInd=" & lservInd & "&tipo=" & ltipo & "';"
					response.write "</script>"
				end if
			end if
		end if
	end if
%>
-->
	</HEAD>
<body class="full" onload='javascript:_carregagrid(<%=request.querystring("cntrId")%>,<%=request.querystring("wbscId")%>,<%=request.querystring("tipo")%>);'>
<!--#include file="../includes/varControleAcesso.inc"-->
<!--#include file="../includes/cab.inc"-->
	<tr>
		<td style="padding:0px" valign="top" class="blocoOpcoes">
			<!--#include file="../includes/opcWBS.inc"-->
		</td>
		<td colspan="2" valign="top" style="padding:5px" class="blocoPagina">
			<form action="" method="post" name="frmWBS" id="frmWBS">
				<div id="principal">
					<input type="hidden" id="curr_wbscId" name="curr_wbscId" value='<%=lwbscId%>' />
					<input type="hidden" id="curr_cntrId" name="curr_cntrId" value='<%=lcntrId%>' />
					<input type='hidden' id='curr_tipo' value='<%=request.querystring("tipo")%>'/>
					<input type="hidden" id="curr_projNome" name="curr_cntrId" value='<%=projNome%>' />
					<input type="hidden" id="curr_cntrDesc" name="curr_cntrId" value='<%=cntrDesc%>' />

					<div id='divTab'>
					</div>

					<div class='divBotoes'>
						<input class='cpBt' type='button' value='Salvar' onclick='javascript:_salvarwbs(<%=ltipo%>);' onmouseover='javascript:_overbts(this);' onmouseout='javascript:_outbts(this);' <%=ldisabled%> />
						<input class='cpBt' type='button' value='Limpar' onclick='javascript:_limparwbs(<%=ltipo%>);' onmouseover='javascript:_overbts(this);' onmouseout='javascript:_outbts(this);' <%=ldisabled%> />
						<%if(ltipo="5") then%>
							<input class='cpBt' type='button' value='Finalizar B.M.' onclick='javascript:_finalizarBM(<%=lwbscId%>);' onmouseover='javascript:_overbts(this);' onmouseout='javascript:_outbts(this);' <%=ldisabled%> />
						<%end if%>
						<!-- <input class='cpBt' type='button' value='TESTA' onclick='javascript:teste_tratarxmlretorno();' /> -->
					</div>
					<div id='divErros' style='width:100%;height:0px;display:none;margin-top:5px;'>
					</div>
				</div>
			</form>
		</td>
	</tr>
<!--#include file="../includes/rod.inc"-->
</script>
</body>
</HTML>
