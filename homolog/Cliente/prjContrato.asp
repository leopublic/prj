<% Response.Expires= -1%>
<% filename = "prjContrato"&Request.QueryString("area")&Request.QueryString("servInd")&".html" %>
<!--#include file="../includes/offline.inc"-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Controle remoto de projetos</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<link rel="stylesheet" type="text/css" href="../EstiloMenu.css">
		<script language="JavaScript" type="text/javascript" src="../scripts/AbrirJanela.js"></script>
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<!--#include file="../includes/anxRotinas.inc"-->
	</HEAD>
	<body class="cabecalho" >
<!--		<div align="center"><img src="../images/fundo2.jpg" align="absmiddle" ></div>-->
<% if Session("usuaId") = "" then
   		response.redirect("../login.asp")
   end if

   If Session("usuaId") = 0 then
       Session("Msg") = "Acesso restrito a usu�rios autorizados."
       Response.redirect("loginInput.asp")
   end if
	projId = Session("projId")
	if projId = "" then
		Response.Redirect("prjListarAtivos.asp")
	end if

	Dim gmenuGeral
	Dim gMenuSelecionado
	Dim gOpcaoSelecionada
	gMenuSelecionado = "PrjContratos"

	gmenuGeral = "Projetos"
	cntrNomeServArray = Session("cntrNomeServArray")
	usuaIdContArray = Session("usuaIdContArray")
	cntrIdArray = Session("cntrIdArray")
	emprNomeArray = Session("emprNomeArray")
	emprIdArray = Session("emprIdArray")
	TotalServ = Session("QtdServicos")
	projId = Request.QueryString("projId")
	servInd = Request.QueryString("servInd")
	area = Request.QueryString("area")
	Dim tipos(6)
	if area = "QLD" then
		xQtdTipos = 3
		tipos(1) = 11
		tipos(2) = 13
		tipos(3) = 12
		areaNome = "Qualidade"
	elseif area = "SEG" then
		xQtdTipos = 6
		tipos(1) = 14
		tipos(2) = 15
		tipos(3) = 16
		tipos(4) = 17
		tipos(5) = 27
		tipos(6) = 33
		areaNome = "Seguran�a"
	elseif area = "PLN" then
		xQtdTipos = 5
		tipos(1) = 19
		tipos(2) = 20
		tipos(3) = 21
		tipos(4) = 28
		tipos(5) = 22
		areaNome = "Planejamento"
	end if
	gOpcaoSelecionada = areaNome
	cntrId = cntrIdArray(servInd)
	PastaSelecionada = "Projetos"
	cntrNomeServ = cntrNomeServArray(servInd)
	emprNome = emprNomeArray(servInd)
	if emprNome = "" then
		emprNome = "(indefinido)"
	end if
	emprId = emprIdArray(servInd)
	Dim gConexao
	AbrirConexao		
%>
		<!--#include file="../includes/cab.inc"-->
			<tr>
				<td style="padding:0px" valign="top" class="blocoOpcoes">
					<!--#include file="../includes/opcProjX2.inc"-->
				</td>
				<td colspan="2" rowspan="2" valign="top" style="padding:5px" class="blocoPagina">
					<table width="100%">
			<% if TotalServ = 0 then %>
						<tr>
							<td colspan="2" class="docTit"><%=Session("projNome")%> - Contratos</td>
						</tr>
						<tr>
							<td colspan="2" height="15" class="docTit">&nbsp;</td>
						</tr>
						<tr>
							<td colspan="2">Esse projeto n�o tem contratos cadastrados.</td>
						</tr>
			<% else %>
					<form>
						<tr>
							<td colspan="2" class="docTit"><%=Session("projNome")%> - Contrato de <% = UCase(cStr(cntrNomeServ))%></td>
						</tr>
						<tr>
							<td colspan="2" >
								<table class="doc">
									<tr>
										<td class="docLabel">Fornecedor: <% = emprNome %></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="2" height="15" class="docTit">&nbsp;</td>
						</tr>
						<tr>
							<td colspan="2" class="docTit"><%=UCase(areaNome)%></td>
						</tr>
						<% url = "prjContrato.asp?projId=" & projId & "&servInd=" & cStr(servInd) & "&area=" & area %>
						<% for i = 1 to xQtdTipos
							   GridContratos tipos(i), projId, cntrId, url, offline 
						   next %>
					</form>
			<% end if%>
					</table>
				</td>
			</tr>
		<!--#include file="../includes/rod.inc"-->
	</body>
</HTML>
