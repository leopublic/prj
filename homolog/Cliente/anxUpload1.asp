<% Response.Expires= -1%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<% 
	'--- Instantiate the FileUpProgress object.
	Set oFileUpProgress = Server.CreateObject("SoftArtisans.FileUpProgress")
 	intProgressID = oFileUpProgress.NextProgressID
	%>
	<HEAD>
		<title>::PRJ:: Controle remoto de projetos</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../EstiloPopUp.css">
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<script language="JavaScript" type="text/javascript" src="../scripts/PreparaJanela.js"></script>
		<SCRIPT language="JavaScript">
			function submitform(){
			  startupload();
			  document.form.submit();
			}
			function validar(){
			  if (document.form.txtArquivo.value == '')
			  {   alert('Informe a localiza��o do arquivo.')}
			  else
			  {   submitform();}
			}
			function startupload() {
				winstyle="height=150,width=500,status=no,toolbar=no,menubar=no,location=no";
				window.open("progress.asp?progressid=<%=intProgressID%>",null,winstyle);
				document.form.action="anxUpload2.asp?progressid=<%=intProgressID%>";
			}
		</SCRIPT> 
	</HEAD>
	<body class="PopUpEntrada" onLoad="javascript:PreparaJanela(600,300);">
<%
	On error resume next
    lPerfisAutorizados = "12345"
	VerificarAcesso
	'* Abrir conexao
	'*==========================================================
	Dim gConexao 
	AbrirConexao
	'* Obter caracter�sticas do tipo de anexo a ser carregado
	'*==========================================================
	tpanId = Request.Querystring("tpanID")
	projId = Request.Querystring("projID")
	cntrId = Request.Querystring("cntrID")
	anexId = Request.Querystring("anexId")
	if anexId <> "" then
	    xSQL = "select anexNome, anexRevNum, anexNum from Anexo where anexId = " & anexId
		set xRs = gConexao.execute(xSQL)
		anexNome = xRs("anexNome")
		anexNum = xRs("anexNum")
		anexRevNum = xRs("anexRevNum")
		xRs.Close
		xDisabled = "disabled"
		if isNumeric(anexRevNum) then
			anexRevNum = anexRevNum + 1
		end if
	else
		xDisabled = ""
		anexNome = ""
		anexNum = "0"
		anexRevNum = "0"
	end if
	xSQL = "select tpanUnic, tpanAprv, tpanDesc, tpanProj, tpanGlob, tpanCntr, tparExte , tpanGen, tpanForm"
	xSQL = xSQL & "  from TipoAnexo "
	xSQL = xSQL & "     , TipoArquivo"
	xSQL = xSQL & " where tpanId = " & tpanId
	xSQL = xSQL & "   and TipoArquivo.tparId = TipoAnexo.tparId"
	set xRs = gConexao.Execute(xSQL)
	tpanUnic = xRs("tpanUnic")
	tpanForm = xRs("tpanForm")
	tpanAprv = cStr(xRs("tpanAprv"))
	tpanGlob = cStr(xRs("tpanGlob"))
	tpanProj = xRs("tpanProj")
	tpanCntr = xRs("tpanCntr")
	tpanGen  = cStr(xRs("tpanGen"))
	if tpanGlob = "1" then
		xEscopo = " - GLOBAL"
		xLabelNumero = "Norma"
	else
		xLabelNumero = "N�mero"
		if tpanProj = "1" then
			xEscopo = "PROJETO"
		else
			xEscopo = "CONTRATO"
		end if
	end if
	tpanDesc = xRs("tpanDesc")
	xRs.Close
	if anexId = "" then
		if tpanGen = "M" then
			tituloPagina = "Carregar novo " & tpandesc
		else
			tituloPagina = "Carregar nova " & tpandesc
		end if
	else
		tituloPagina = "Carregar nova vers�o de " & anexNome
	end if
	Session("TituloMsg") = tituloPagina
%>
	<form action="anxUpload2.asp" method="post" name="form" encType="multipart/form-data" onSubmit="startupload();" >
		<input type="hidden" name="tpanId" value="<%=tpanId%>" >
		<input type="hidden" name="projId" value="<%=projId%>" >
		<input type="hidden" name="cntrId" value="<%=cntrId%>" >
		<input type="hidden" name="anexId" value="<%=anexId%>" >
		<input type="hidden" name="usuaId" value="<%=Session("usuaId")%>" >
		<div id="cab">
			<p><% = tituloPagina %></p>
		</div>
		<div id="separador" style="background-image:url(../images/pont_cinza_h.gif);">
		</div>
		<div id="form">
			<table>
				<tr>
					<td width="110">Arquivo</td>
					<td width="5">:</td>
					<td class="docCmpLivre"><input type="file" name="txtArquivo"></td>
				</tr>
				<tr>
					<td width="110"><%=xLabelNumero%></td>
					<td width="5">:</td>
					<td class="docCmpLivre"><input type="text" name="txtNumero" value="<%=anexNum%>" <%= xDisabled %>></td>
				</tr>
				<tr>
					<td width="110">T�tulo</td>
					<td width="5">:</td>
					<td class="docCmpLivre"><input type="text" name="txtNome" value="<%=anexNome%>" <%= xDisabled %> maxlength="150"></td>
				</tr>
				<tr>
					<td width="110">Revis�o</td>
					<td width="5">:</td>
					<td class="docCmpLivre"><input type="text" name="txtRevNum" value="<%=anexRevNum%>" maxlength="10"></td>						
				</tr>
			</table>
		</div>
		<div id="comandos">
			<img src="../images/btnSalvar.gif"   onclick="javascript:validar();">
			<img src="../images/btnCancelar.gif" onclick="javascript:window.close();">
		</div>
	</form>
	</body>
</HTML>
