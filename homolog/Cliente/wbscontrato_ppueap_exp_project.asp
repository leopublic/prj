<% 
    Server.ScriptTimeout=60000 
%>
<!--#include file="../includes/clsBanco.inc"-->
<!--#include file="../includes/funcoes.inc"-->
<!--#include file="../includes/wbscontrato_init.inc"-->
<%
Session.LCID=1046 'Pt-BR
response.charset = "iso-8859-1"

function ts(s)
	ts = replace(s,"""","''")
end function

function escreve_item(pid, poutline, piwbccodigo, ptarefa, pblwork, pbaselinework1, pbaselinecost, pcost, pcomplete, pworkcomplete, pactualcost, pduration, pstart, pfinish, pbaseline1start, pbaseline1finish, pstartreplanejado, pfinishreplanejado, pstartreal, pfinishreal, ppredecessor, pidsys)
	ret = ""
	sep = ";"
	ret = pid & sep &  poutline & sep & ts(piwbccodigo) & sep & ts(ptarefa) & sep & pblwork & sep & pbaselinework1 & sep & pbaselinecost & sep & pcost & sep & pcomplete & sep & pworkcomplete & sep & pactualcost & sep & pduration & sep & pstart & sep & pfinish & sep & pbaseline1start & sep & pbaseline1finish & sep & pstartreplanejado & sep & pfinishreplanejado & sep & pstartreal & sep & pfinishreal & sep & ppredecessor & sep & pidsys & vbcrlf
	escreve_item = ret
end function

function escreve_mpp(byval pwbscId, byval piwbcIdPai, byref plinha, byval poutline, byval ppenultimonivel, byval pfcn_Id)
	ret  = ""
	lSQL = ""
    lSQL = lSQL & "select isnull(p.projNome, 'Projeto sem nome') projNome, i.iwbcId, i.iwbcCodigo, replace(i.iwbcNome,'""','��') as iwbcNome, i.iwbcFatorHH, i.iwbcValUnitEst, isnull(i.iwbcFLFilho,0) as iwbcFLFilho, isnull(i.iwbcQtdEst,0) iwbcQtdEst, isnull(i.iwbcQtdReal,i.iwbcQtdEst) as iwbcQtdReal, isnull(i.iwbcQtdAtual,0) as iwbcQtdAtual, isnull(i.iwbcValUnitReal,0) iwbcValUnitReal, (isnull(i.iwbcQtdEst,0) * isnull(i.iwbcFatorHH,1)) as iwbcQtdHH, (isnull(p.projIndHH,1) * isnull(i.iwbcQtdEst,0) * isnull(i.iwbcFatorHH,1)) as iwbcValHH, isnull(convert(varchar(10),i.iwbcDtInicioEst,103),'" & projSemIni & "') as iwbcDtInicioEst, isnull(i.iwbcDuracaoEst,0) iwbcDuracaoEst, (isnull(i.iwbcDtInicioEst,getdate())  + isnull(i.iwbcDuracaoEst,0)) as iwbcDtTerminoEst, i.iwbcDataVerif, (case when iwbcDataVerif is not null then 1 else 0 end) iwbcConcluido, isnull(i.iwbcQtdAtual,0) as iwbcQtdAtual, i.fcn_Id, im.icmpBaselineWork1, im.icmpCost, im.icmpBaseline1Start, im.icmpBaseline1Finish, im.icmpStartReplanejado, im.icmpFinishReplanejado, im.icmpStartReal, im.icmpFinishReal, im.icmpPredecessor " 
    lSQL = lSQL & "  from (((((Projeto p inner join Contrato c on c.projId = p.projId) inner join WbsContrato w on w.cntrId = c.cntrId) inner join ItemWbsContrato i on i.wbscId = w.wbscId) left join TipoItemWbs t on i.tiwbId = t.tiwbId) left join UnidadeMedida u on i.unimId = u.unimId) left join ItemWbsContratoMSProject im on i.icmpId = im.icmpId " 
    lSQL = lSQL & " where i.wbscId = " & pwbscId
    if trim(piwbcIdPai) = "" then
        lSQL = lSQL & " and i.iwbcId_Pai is null " 
    else
        lSQL = lSQL & " and i.iwbcId_Pai = " & piwbcIdPai
    end if
	if trim(pfcn_Id) = "" then
        lSQL = lSQL & " and i.fcn_Id is null " 
    else
        lSQL = lSQL & " and i.fcn_Id = " & pfcn_Id
    end if
    lSQL = lSQL & " order by isnull(i.fcn_Id,0), i.iwbcCodigo " 
	'response.write lSQL & "<br><br>"
    'exit function
	AbrirConexao
	Dim lrs_1
	Set lrs_1 = gConexao.execute(lSQL)
	If (not lrs_1.BOF) and (not lrs_1.EOF) then
		While not lrs_1.EOF
			if ((lRS_1("iwbcFLFilho")=1) xor (lRS_1("iwbcFLFilho")=0 and ppenultimonivel=false)) then
				plinha = plinha + 1
				ret = ret & escreve_item(plinha, poutline, lrs_1("iwbcCodigo"), lrs_1("iwbcNome"), lrs_1("iwbcQtdHH"), lrs_1("icmpBaselineWork1"), lrs_1("iwbcValHH"), lrs_1("icmpCost"), lrs_1("iwbcConcluido")*100, lrs_1("iwbcConcluido")*100, "", lrs_1("iwbcDuracaoEst"), lrs_1("iwbcDtInicioEst"), lrs_1("iwbcDtTerminoEst"), lrs_1("icmpBaseline1Start"), lrs_1("icmpBaseline1Finish"), lrs_1("icmpStartReplanejado"), lrs_1("icmpFinishReplanejado"), lrs_1("icmpStartReal"), lrs_1("icmpFinishReal"), lrs_1("icmpPredecessor"), lrs_1("iwbcId"))										
			end if
            if (lRS_1("iwbcFLFilho")=1) then
				ret = ret & escreve_mpp(pwbscId, lrs_1("iwbcId"), plinha, (poutline+1), ppenultimonivel, pfcn_Id)
			End If
			lrs_1.MoveNext
		Wend
	End If
	lrs_1.close
	
	escreve_mpp = ret
end function

lwbscId         = request.querystring("wbscId")
lfcn_Id         = trim(request.querystring("fcn_Id"))
lpenultimonivel = request.querystring("flpenultimonivel")
if(lpenultimonivel=0) then lpenultimonivel = false
if(lpenultimonivel=1) then lpenultimonivel = true
'response.write pimprimirpenultimonivel
retorno = escreve_mpp(lwbscId, "", 0, 1, limprimirpenultimonivel, lfcn_Id)
if(retorno<>"") then
	response.write escreve_item("ID", "OUTLINE LEVEL", "IWBCCODIGO", "NAME", "BL.WORK", "BASELINE WORK 1", "BASELINE COST", "COST", "% COMPLETE", "% WORK COMPLETE", "ACTUAL COST", "DURATION", "START", "FINISH", "BASELINE 1 START", "BASELINE 1 FINISH", "START REPLANEJADO", "FINISH REPLANEJADO", "START REAL", "FINISH REAL", "PREDECESSORS", "IDSYS")
	response.write retorno
	if(isnumeric(lfcn_Id) and (lfcn_Id<>"0")) then
		Response.AddHeader "Content-Disposition", "attachment; filename=WBS_Cronograma_" & request.querystring("wbscId") & "_FCN_" & lfcn_Id & ".csv"
	else
		Response.AddHeader "Content-Disposition", "attachment; filename=WBS_Cronograma_" & request.querystring("wbscId") & ".csv"
	end if
end  if

%>
