<% Response.Expires= -1%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ::</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<link rel="stylesheet" type="text/css" href="../EstiloMenu.css">
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<script language="JavaScript" type="text/javascript" src="../scripts/AbrirJanela.js"></script>
	</HEAD>
	<body class="cabecalho" >
<%  lPerfisAutorizados = "157"
	VerificarAcesso()
	projId = Session("projId")

   Dim gmenuGeral
   Dim gMenuSelecionado
   Dim gOpcaoSelecionada
   projId = Request.QueryString("projId")
	cntrNomeServArray = Session("cntrNomeServArray")
	cntrIdArray = Session("cntrIdArray")
	emprNomeArray = Session("emprNomeArray")
	emprIdArray = Session("emprIdArray")
	TotalServ = Session("QtdServicos")
	servInd = 0
	area = ""
   gmenuGeral = "Projetos"
   gMenuSelecionado = "prjPedcListar"

 %>
		<!--#include file="../includes/cab.inc"-->
			<tr>
				<td style="padding:0px" valign="top" class="blocoOpcoes">
					<!--#include file="../includes/opcProjx2.inc"-->
				</td>
				<td colspan="2" valign="top" style="padding:5px" class="blocoPagina">
				<form>
					<table width="100%">
					<tr>
						<td class="docTit"><%=Session("projNome")%><br><img src="../images/0066_double_arrow.png" class="botao" style="margin-right:3px">Pedidos de Compra</td>
						<td align="right"><a href="javascript:AbrirJanela('prjPedcAlterar0.asp?pedcId=0',500,300);"><img src="../images/newitem.gif" class="botao">&nbsp;Carregar novo</a></td>
					</tr>
					<tr>
						<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
					</tr>
					<tr>
						<td colspan="2" style="height:5"></td>
					</tr>
<%
	Dim campoOrdem(10)
	Dim seta(10)
	campoOrdem(0) = "pedcNum"
	campoOrdem(1) = "fornNome"
	campoOrdem(2) = "pedcDtEntregaPrev"
	campoOrdem(3) = "pedcNumTag"
	campoOrdem(4) = "pedcNumRequisicao"

	indOrdem = request.QueryString("indOrdem")
	if indOrdem = "" then
		indOrdem = 0
	end if
	seta(indOrdem) = "<img src=""../images/sort.gif"">"
%>

					<tr>
						<td colspan="2">
							<table width="100%" class="grid" >
								<tr>
									<td width="30px"  class="GridCab">&nbsp;</td>
									<td width="55px"  class="GridCab"><a href="prjPedcListar.asp?indOrdem=0" class="GridCab">Pedido<%=seta(0)%></a></td>
									<td width="100px" class="GridCab"><a href="prjPedcListar.asp?indOrdem=4" class="GridCab">Requisi��o<%=seta(4)%></a></td>
									<td width="100px" class="GridCab">Lista</td>
									<td class="GridCab"><a href="prjPedcListar.asp?indOrdem=1" class="GridCab">Fornecedor<%=seta(1)%></a></td>
									<td width="80px" class="GridCab"><a href="prjPedcListar.asp?indOrdem=2" class="GridCab">Data Entrega<br>Prevista<%=seta(2)%></a></td>
									<td width="100px" class="GridCab"><a href="prjPedcListar.asp?indOrdem=3" class="GridCab">N�mero MTS<%=seta(3)%></a></td>
									<td class="GridSLinha">&nbsp;</td>
								</tr>
	<%
	Dim gConexao 
	xSQL =        "select pedcId, pedcNum, convert(varchar(10), pedcDtEntregaPrev, 3) pedcDtEntregaPrevFmt, pedcDtEntregaPrev, pedcTipo, pedcNumTag, pedcNumRequisicao, fornNome, requNum, RQ.requId, reviNumero, listSequencial, tpliPrefixo, tpliCodigo, listNumProjeto, LI.listId "
	xSQL = xSQL & "  from Fornecedor F , ((("
	xSQL = xSQL & "       (PedidoCompra PC left join Requisicao RQ on RQ.requId = PC.requId) "
	xSQL = xSQL & "       left join Revisao R on R.reviId = RQ.reviId)"
	xSQL = xSQL & "       left join Lista   LI on LI.listId = R.listId)"
	xSQL = xSQL & "       left join TipoLista   TL on TL.tpliId = LI.tpliId)"
	xSQL = xSQL & " where F.fornId  = PC.fornId"
	xSQL = xSQL & "   and PC.projId = " & session("projId")
	xSQL = xSQL & " order by " & campoOrdem(indOrdem)
	AbrirConexao
	Set xRs = gConexao.Execute(xSQL)
	if xRs.EOF then
	    Response.write "<tr>" & vbcrlf
	    Response.write "<td colspan=""8"">Nenhum pedido de compra cadastrado</td>" & vbcrlf
	    Response.write "</tr>" & vbcrlf
		xPovoou = false
	else
		estilo = "GridLinhaPar"
		while not xRs.EOF
			if xrs("requId") <> "" then
				xRequisicao = "<a href=""prjRequConsultar.asp?requId=" & xrs("requId") & """ title=""Consultar a requisi��o " & xrs("requNum") & """>" & xrs("requNum") & "</a>"
			else
				xRequisicao = xrs("pedcNumRequisicao") 
			end if
			
			if xrs("listId") <> "" then
				xLista = xrs("tpliPrefixo") & "-" & xrs("listNumProjeto") & "-" & xrs("tpliCodigo") & "-" & right("000" & xrs("listSequencial"), 3) & " (Rev:" & xrs("reviNumero") & ")"
				xlista = "<a href=""http://www.prj.com.br/cliente/prjListResumo.asp?listId=" & xrs("listId") & """>" & xLista & "</a>"
			else
				xLista = "-"
			end if
			xPovoou = true
			Response.write "<tr>" & vbcrlf
			response.write vbtab & "<td class=""" & estilo & """><a href=""prjPedcConsultar.asp?pedcId=" & xRs("pedcId") & """><img src=""../images/folder.gif"" style=""border:0""></a></td>"
			response.write vbtab & "<td class=""" & estilo & """>" & xRs("pedcNum") & "</td>"
			response.write vbtab & "<td class=""" & estilo & """>" & xRequisicao & "</td>"
			response.write vbtab & "<td class=""" & estilo & """>" & xLista & "</td>"
			response.write vbtab & "<td class=""" & estilo & """>" & xRs("fornNome") & "</td>"
			response.write vbtab & "<td class=""" & estilo & """>" & xRs("pedcDtEntregaPrevFmt") & "</td>"
			response.write vbtab & "<td class=""" & estilo & """>" & xRs("pedcNumTag") & "</td>"
			response.write vbtab & "<td class=""GridSLinha"">&nbsp;</td>"
			Response.write "</tr>" & vbcrlf
			xRs.MoveNext
			if estilo = "GridLinhaPar" then
				estilo = "GridLinhaImpar"
			else
				estilo = "GridLinhaPar"
			end if
		wend
	end if
	%>
								<tr>
									<td class="GridRodape">&nbsp;</td>
									<td class="GridRodape">&nbsp;</td>
									<td class="GridRodape">&nbsp;</td>
									<td class="GridRodape">&nbsp;</td>
									<td class="GridRodape">&nbsp;</td>
									<td class="GridRodape">&nbsp;</td>
									<td class="GridRodape">&nbsp;</td>
									<td class="GridSLinha">&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
					</table>
				</form>
				</td>
			</tr>
		<!--#include file="../includes/rod.inc"-->
	</body>
</HTML>
