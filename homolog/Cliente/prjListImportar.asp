<!--#include file="../includes/clsBanco.inc"-->
<!--#include file="../includes/clsControleAcesso.inc"-->
<!--#include file="../includes/varControleAcesso.inc"-->
<!--#include file="../includes/varBanco.inc"-->
<%
Dim debug 
Dim ximpoId
Sub debuga (msg)
	xSQL = "exec PCAD_MensagemImportacao @pimpoId = " & ximpoId & ", @pmsgiTexto = '" & replace(msg, "'", "''") & "'"
	gConexao.execute(xSQL)
	
	if debug="on" then
		response.write "<br>" & msg
	end if
end sub

Dim objConn
Dim objRs
AbrirConexao
'== Obt�m arquivo a importar
debug = request.querystring("debug")
ximpoId = request.querystring("impoId")
xPath = Server.MapPath("..\listas")
xArquivo = xPath & "\" & ximpoId & ".txt"
'== Limpa mensagens da �ltima importacao
xSQL = "delete from MensagemImportacao where impoId =  " & ximpoId
gConexao.execute(xSQL)
'== Obtem dados do arquivo
xSQL = "select tpliPrefixo "
xSQL = xSQL & " from TipoLista TL, Importacao I"
xSQL = xSQL & " where TL.tpliId = I.tpliId "
xSQL = xSQL & "   and I.impoId = " & ximpoId
'response.write xSQL 
set xrs = gConexao.execute(xSQL)
xprefixo = xrs("tpliPrefixo")
xrs.close
set xrs = nothing
'== Inicia importa��o do arquivo
	Const ForReading = 1, ForWriting = 2, ForAppending = 3
	Const TristateUseDefault = -2, TristateTrue = -1, TristateFalse = 0
	' Create a filesystem object
	Dim FSO
	set FSO = server.createObject("Scripting.FileSystemObject")

	if FSO.FileExists(xArquivo) Then
	    ' Get a handle to the file
	    Dim file    
	    set file = FSO.GetFile(xArquivo)

	    ' Open the file
	    Dim TextStream
	    Set TextStream = file.OpenAsTextStream(ForReading, TristateUseDefault)

	    ' Read the file line by line
		regNum = 0
		erro = 0
	    if ( not TextStream.AtEndOfStream) then
	        reg = TextStream.readline
			regNum = regNum + 1
			'==============================================================================================
			'== Procura identificador do arquivo
			'==============================================================================================
			debuga "Procurando header com identifica��o da lista..."
			posIdentificacao = instr(1, reg, xprefixo & "-", 1)
			Do While (posIdentificacao = 0) and (not TextStream.AtEndOfStream)
		        reg = TextStream.readline
				regNum = regNum + 1		
				posIdentificacao = instr(1, reg, xprefixo & "-", 1)
			loop

			if (posIdentificacao = 0) then
				debuga "ERRO! Identifica��o da lista n�o encontrada."
				erro = 1
			else
				nomeLista = mid(reg, posIdentificacao,15)
				debuga "Identifica��o encontrada na linha " & regNum & " -->" & nomeLista
				xSplit = split(nomeLista, "-")
				xlistSequencial = xsplit(3)
			end if
			'==============================================================================================
			'== Procura a revis�o do arquivo
			'==============================================================================================
			debuga "Procurando n�mero da revis�o..."
			pos = instr(1, reg, "Rev: ", 1)
			Do While (pos = 0) and (not TextStream.AtEndOfStream)
		        reg = TextStream.readline
				regNum = regNum + 1		
				posIdentificacao = instr(1, reg, xprefixo & "-", 1)
			loop

			if (pos = 0) then
				debuga "ERRO! N�mero da revis�o da lista n�o encontrado."
				erro = 1
			else
				reviNumero = mid(reg, pos+5,2)
				debuga "N�mero da revis�o encontrada na linha " & regNum & " -->" & reviNumero
			end if
			reviNumero = cInt(reviNumero)
			'==============================================================================================
			'== Cria a revis�o no banco
			'==============================================================================================
			xSQL = "exec LIST_Criar "
			xSQL = xSQL & " @pprojId     = " & session("projId")
			xSQL = xSQL & " , @pimpoId     = " & ximpoId
			xSQL = xSQL & " , @previNumero = " & reviNumero
			xSQL = xSQL & " , @plistSequencial = " & xlistSequencial
			xSQL = xSQL & " , @pusuaId         = " & session("usuaId")
			debuga xSQL 
			set xrs = gConexao.execute(xSQL)
			xreviId = xrs("reviId")
			
			'==============================================================================================
			'== Procura o cabe�alho das colunas
			'==============================================================================================
			debuga "Procurando cabe�alho das colunas..."
			pos = instr(1, reg, "Descri��o Resumida", 1)
			Do While (pos = 0) and (not TextStream.AtEndOfStream)
		        reg = TextStream.readline
				regNum = regNum + 1		
				pos = instr(1, reg, "Descri��o Resumida", 1)
			loop
			if (pos = 0) then
				debuga "ERRO! Cabe�alho das linhas n�o encontrado."
				erro = 1
			else
				revisao = mid(reg, pos+5,2)
				debuga "Cabe�alho das colunas encontrado na linha " & regNum 
		        reg = TextStream.readline  ' Pula uma linha				
		        reg = TextStream.readline  ' Pula uma linha				
		        reg = TextStream.readline  ' Pula uma linha				
			end if
			'== Inicia a importa��o dos itens
			Do While (not TextStream.AtEndOfStream)
				'== Identifica nova linha
				'== Pula linhas em branco
				if not (trim(replace(reg, vbtab, "")) = "") then
					if (mid(reg,1, 3) = vbtab & vbtab & """") then
						xNomeLinha = replace(reg, vbtab, "")
						xNomeLinha = mid(xNomeLinha, 2, len(xNomeLinha)-2 )
						xNomeLinha = replace(xNomeLinha, """""", """")
						linha = split(xnomelinha, "-")
						debuga "Nova linha encontrada (" & regNum & ") : " & xNomeLinha
						'== Cria nova linha
						xSQL = "exec PCAD_Linha "
						xsql = xsql & "   @previId     =  " & xreviId
						xsql = xsql & " , @plinhNome   = '" & xNomeLinha & "'"
						xsql = xsql & " , @plinhBitola = '" & linha(0) & "'"
						xsql = xsql & " , @plinhCod1   = '" & linha(1) & "'"
						xsql = xsql & " , @plinhCod2   = '" & linha(2) & "'"
						debuga xSQL 
						set xrs = gConexao.execute(xSQL)
						xlinhId = xrs("linhId")
						xrs.close
						set xrs = nothing
					else
						'== Registra itens da linha
						coluna = Split(reg, vbtab)
						ordem = coluna(0)
						codJde = coluna(1)
						descricao = coluna(2)
						descricao = mid(descricao, 2, len(descricao)-2)
						unidade = coluna(6)
						qtd = trim(replace(coluna(7), ",", "."))
						partes = split(qtd, ".")
						if instr(1, qtd, ".") then
							qtdNum = cint(partes(0)) + (cdbl(partes(1))/(10 * len(partes(1))))
						else
							qtdNum = qtd
						end if
						revisao = coluna(8)
						qtd2 = trim(replace(coluna(9), ",", "."))
						revisao2 = coluna(10)
						obs = coluna(11)
						
						debuga "<br>Item encontrado: " & ordem
						debuga "codJDE -->" & codJde & "<--"
						debuga "descricao -->" & descricao & "<--"
						debuga "unidade -->" & unidade & "<--"
						debuga "qtd -->" & qtd & "<--"
						debuga "qtdNum -->" & qtdNum & "<--"
						debuga "revisao -->" & revisao & "<--"
						debuga "qtd2 -->" & qtd2 & "<--"
						debuga "revisao2 -->" & revisao2 & "<--"
						debuga "Obs -->" & obs & "<--"
						'if revisao2 = 
						if qtd2 <> "" and qtd2 <> "0" and qtd2 <> "0.00" then
						    if qtd2 <> qtd1 then
								xSQL = " exec PCAD_ItemLista"
								xSQL = xSQL & "   @plinhId       = " & xlinhId
								xSQL = xSQL & "  ,@punidSigla    = '" & unidade & "'"
		   						xSQL = xSQL & "  ,@pitelOrdem    = " & ordem
								xSQL = xSQL & "  ,@pitemCodJde   = " & codJde
								xSQL = xSQL & "  ,@pitelTag      = null"
								xSQL = xSQL & "  ,@previId       = " & xreviId
								xSQL = xSQL & "  ,@pitelQtd      = " & qtd2
								xSQL = xSQL & "  ,@pitemDescricao = '" & descricao & "'"
								on error resume next
								set xrs = gConexao.execute(xSQL)
								If Err.Number <> 0 then
									debuga xSQL
									debuga "ERRO: " & Err.Description
									Error.Clear						
								else
									xitelId = xrs("itelId")
								end if
								xrs.close
								set xrs = nothing
							end if
						end if
					end if
				end if
				'== L� proximo registro
				reg = TextStream.readline
				regNum = regNum + 1		
			loop
	    
	    end if
		TextStream.close
	    Set TextStream = nothing
		Session("msg") = "Lista carregada com sucesso!"
		Set FSO = nothing
		if not debug="on" then
			response.redirect("gerMensagem.asp")
		end if
	Else
		Session("msg") = "Arquivo (" & xArquivo & ") n�o encontrado!"	    
		debuga "Arquivo (" & xArquivo & ") n�o encontrado!"
		Set FSO = nothing
		if not debug="on" then
			response.redirect("gerMensagem.asp")
		end if
	End If
%>