<% Response.Expires= -1%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Controle remoto de projetos</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<script language="JavaScript" type="text/javascript" src="../scripts/PreparaJanela.js"></script>
		<SCRIPT language="JavaScript">
			function submitform(){
			  document.form1.submit();
			}
			function validar(){
			  if (document.form1.txtLogin.value == '')
			  { alert('O preenchimento do login � obrigatorio.')}
			  else
			  {   if (document.form1.txtNome.value == '')
			      { alert('O preenchimento do nome � obrigatorio.')}
				  else
				  {    senha1 = document.form1.txtSenha1.value;
		  			   senha2 = document.form1.txtSenha2.value;
			  		   if (senha1 == senha2) 
					   {  if (document.form1.cmbPerfil.options[document.form1.cmbPerfil.selectedIndex].value == '0')
					      {    alert('Defina o perfil de acesso do usu�rio.') }
						  else
						  {    submitform();}
					  }
					  else
					  {   alert('Senhas diferentes');}
					}
				}			  
			}
			function AbrirJanela(url, win_width, win_height) { //v2.0
				var w = screen.width; // Get the width of the screen
				var h = screen.height; // Get the height of the screen
			
				// Where to place the Window
				var left = (w - win_width)/2;
				var top = (h - win_height)/2;
			
				posicao = "width=" + win_width + ",height=" + win_height ;
				posicao += ",top=" + top + ",left=" + left + ",screenX=" + left + ",screenY=" + top;			
				parametros = posicao + ",directories=no,location=no,menubar=no,scrollbars=no,status=yes,toolbar=no,resizable=no;"; 
				titulo = "UsuarioProjeto" ; 
				window.open(url,titulo,parametros);
			}	

		</SCRIPT> 
	</HEAD>
	<body class="PopUpEntrada" >
<%	usuaId = Request.Querystring("usuaID")
	emprId = Request.QueryString("emprId") %>
	<script language="JavaScript">
	<% if usuaId = "0" then %>
		PreparaJanela(460,350);
	<% else %>
		PreparaJanela(460,600);
	<% end if %>
	</script>
<% 
    lPerfisAutorizados = "23451"
	VerificarAcesso
	Dim gConexao 
	AbrirConexao
	if usuaId = "0" then
	    titulo = "Criar novo usu�rio de contratado"
		Session("TituloMsg") = titulo
	    login = ""
		nome = ""
		email = ""
		senha = ""
		perfil = ""
	else
		xSQL = "select usuaNome, usuaLogi, usuaEmai, usuaSenh, perfId, emprId from Usuario where usuaId = " & usuaId
		set xrs = gConexao.execute(xSQL)
		titulo = "Alterar usu�rio de contratado"
		Session("TituloMsg") = titulo
	    login = xRs("usuaLogi")
		nome = xrs("usuaNome")
		session("usuaNomeAlt") = nome
		email = xRs("usuaEmai")
		senha = xRs("usuaSenh")
		perfId = xRs("perfId")
		emprId = cStr(xRs("emprId"))
		xRs.CLose
		Set xRs = nothing
	end if
%>
	<form action="cfgFornAlterar2.asp" method="post" name="form1">
		<input type="hidden" name="usuaId" value=" <% = usuaId %> " >
		<input type="hidden" name="emprId" value=" <% = emprId %> " >
		<table width="100%">
			<tr>
				<td width="15"></td>
				<td width="250"></td>
				<td></td>
			</tr>
			<tr>
				<td colspan="3" class="docTit"><% = titulo %></td>
			</tr>
			<tr>
				<td colspan="3" background="../images/pont_cinza_h.gif" style="height:2"></td>
			</tr>
			<tr>
				<td colspan="3" height="15"></td>
			</tr>
			<tr>
				<td></td>
				<td colspan="2">
					<table class="doc">
						<tr>
							<td width="110" class="docLabel">Login</td>
							<td width="5" class="docLabel">:</td>
							<td class="docCmpLivre"><input type="text" name="txtLogin" class="docCmpLivre" value="<% = login %>"></td>
						</tr>
						<tr>
							<td class="docLabel">Nome</td>
							<td class="docLabel">:</td>
							<td class="docCmpLivre"><input type="text" name="txtNome" class="docCmpLivre" value="<% = nome %>"></td>
						</tr>
						<tr>
							<td class="docLabel">E-mail de contato</td>
							<td class="docLabel">:</td>
							<td class="docCmpLivre"><input type="text" name="txtEmail" class="docCmpLivre" value="<% = email %>"></td>
						</tr>
						<tr>
							<td class="docLabel">Senha de acesso</td>
							<td class="docLabel">:</td>
							<td class="docCmpLivre"><input type="password" name="txtSenha1" class="docCmpLivre" value="<% = senha %>"></td>
						</tr>
						<tr>
							<td class="docLabel" align="right">(repetir)</td>
							<td class="docLabel"></td>
							<td class="docCmpLivre"><input type="password" name="txtSenha2" class="docCmpLivre" value="<% = senha %>"></td>
						</tr>
						<tr>
							<td class="docLabel">Perfil</td>
							<td class="docLabel"></td>
							<td class="docSBorda">
	<%
	if usuaId = "0" then
		xSQL = "select isnull(perfId, 0) from Empreiteira where emprId = " & emprId 
		Set xRs = gConexao.Execute(xSQL)
		perfil = cStr(xRs(0))
		xRs.close
	end if 
	%>
								<select name="cmbPerfil" class="docCmpLivre">
	<%
	xSQL = "select perfId, perfNome"
	xSQL = xSQL & " from PerfilAcesso "
	if perfil = "6" then
		xSQL = xSQL & " where perfId = 6"
	else
		xSQL = xSQL & " where perfId <> 6"
	end if
	xSQL = xSQL & "order by perfNome"
	Set xRs = gConexao.Execute(xSQL)
	xSelected = ""
	if perfId = "" or perfId = "0" then
		response.write vbtab & "<option value=""0"" class=""campo"" selected>(n�o informado)</option>"			
	end if
	while not xRs.EOF
		xperfId = xRs("perfId")
		response.write perfId & "=" & xRs("perfId") & vbcrlf
		xperfId = cStr(xperfId)
		if xRs("perfId") = perfId then
			xSelected = " selected "
		else
			xSelected = ""
		end if
		response.write vbtab & "<option value=""" & xRs("perfId") & """ class=""campo""" & xSelected & ">" & xRs("perfNome") & "</option>" & vbcrlf
		xRs.MoveNext
	wend
	xrs.close
	set xRs = nothing
	%>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="3" height="20"></td>
			</tr>
	
<% if usuaId <> "0" then %>
			<tr>
				<td colspan="3" class="docTit">Projetos/Contratos</td>
			</tr>
			<tr>
				<td colspan="3" background="../images/pont_cinza_h.gif" style="height:2"></td>
			</tr>
			<tr>
				<td colspan="3" height="7"></td>
			</tr>
							<%
							xSQL = ""
							xSQL = xSQL & "select projNome"
							xSQL = xSQL & "     , servNome"
							xSQL = xSQL & "     , cntrId"
							xSQL = xSQL & "     , isnull(cntrTermEst1, '2099-12-31') cntrOrd"
							xSQL = xSQL & "  from Contrato"
							xSQL = xSQL & "     , Projeto "
							xSQL = xSQL & "     , Servico "
							xSQL = xSQL & " where Contrato.cntrCanc = 0 "
							xSQL = xSQL & "   and Contrato.usuaIdCont = " & usuaId 
							xSQL = xSQL & "   and Servico.servId = Contrato.servId "
							xSQL = xSQL & "   and Projeto.projId = Contrato.projId "
							xSQL = xSQL & " order by projNome, cntrOrd, servOrd"
							set xRs = gConexao.execute(xSQL)
							if not xRs.eof then%>
						<tr>
							<td></td>
							<td colspan="2">
							<table width="100%">
								<tr>
<!--									<td class="gridCab"></td>-->
									<td class="gridCab">Projeto</td>
									<td class="gridCab">Contrato</td>
								</tr>
					<%			while not xRs.eof
									response.write "<tr>"
									'response.write vbtab & "<td class=""gridLinha""><a href=""javascript:AbrirJanela('cfgFornCntrExcl.asp?usuaId="& usuaId & "&cntrId=" & xRs("cntrId") & "',400,250);""><img src=""../images/crossm.gif""></a></td>"
									response.write vbtab & "<td class=""gridLinha"">" & xRs("projNome") & "</td>"
									response.write vbtab & "<td class=""gridLinha"">" & xRs("servNome") & "</td>"
									response.write "</tr>"
									xRs.Movenext
								wend 
								%>
							</table>
							</td>
						</tr>
					<% else %>
						<tr>
							<td colspan="3" class="GridLinha">Usu�rio n�o alocado a nenhum contrato.</td>
							</td>
						</tr>
					<% end if %>
			<% end if %>
			<tr>
				<td colspan="3" height="7"></td>
			</tr>
			<tr>
				<td colspan="3" align="center">
					<table>
						<tr>
							<td height="28" width="80" align="center"><img border="0" src="../images/btnSalvar.gif" class="botao" onClick="javascript:validar();"></td>
							<td width="10"></td>
							<td width="80" align="center"><img border="0" src="../images/btnCancelar.gif" class="botao" onClick="javascript:window.close();"></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
	</body>
</HTML>
