<% Response.Expires= -1%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Projetos</title><%%>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<!--#include file="../includes/basFormatador.inc"-->
		<!--#include file="../includes/basValidacao.inc"-->
		<!--#include file="../includes/clsContrato.inc"-->
		<script language="JavaScript" type="text/javascript" src="../scripts/NumberFormat.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/Mid.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/ValidacaoDatas.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/PreparaJanela.js"></script>
		<SCRIPT language="JavaScript">
			function AbrirJanela(url, win_width, win_height) { //v2.0
				var w = screen.width; // Get the width of the screen
				var h = screen.height; // Get the height of the screen
			
				// Where to place the Window
				var left = (w - win_width)/2;
				var top = (h - win_height)/2;
			
				posicao = "width=" + win_width + ",height=" + win_height ;
				posicao += ",top=" + top + ",left=" + left + ",screenX=" + left + ",screenY=" + top;			
				parametros = posicao + ",directories=no,location=no,menubar=no,scrollbars=no,status=yes,toolbar=no,resizable=no;"; 
				titulo = "JanelaAdicionar" ; 
				window.open(url,titulo,parametros);
			}

			function Validar(){
			    document.form.submit();
			}
		</SCRIPT> 
<%
			Public Function MontaComboEmpr(pServInd, pUsuaId, pServId)
				xSQL = ""
				xSQL = xSQL & " select usuaId"
				xSQL = xSQL & "      , emprNome + ' - ' + usuaNome nome "
				xSQL = xSQL & "   from Usuario"
				xSQL = xSQL & "      , Empreiteira "
				xSQL = xSQL & "  where Empreiteira.emprId = Usuario.emprId "
				xSQL = xSQL & "    and Empreiteira.emprId <> 1 "
				xSQL = xSQL & "    and Empreiteira.emprId in (select emprId from EmpreiteiraServico where servId = " & pServId & ")"
				xSQL = xSQL & "   and Usuario.usuaAtivo = 1"
				xSQL = xSQL & "  order by usuaNome"
				set xRs2 = gConexao.execute(xsQL)
				xxEmpr = "<select name=""cmbUsuaId" & pServInd & """ class=""docCmpLivre"">" & vbcrlf
				xxEmpr = xxEmpr & "<option value=""null"" class=""docCmpLivre"">(n�o definido)</option>" & vbcrlf
				if not xRs2 is nothing then
					while not xRs2.EOF
						xxusuaId = cStr(xRs2("usuaId"))
						if xxusuaId = pUsuaId then
							xChecked = " selected "
						else
							xChecked = ""
						end if
						xxEmpr = xxEmpr & "<option value=""" & cStr(xRs2("usuaId")) & """ " & xChecked & " class=""docCmpLivre"">" & cStr(xRs2("nome")) & "</option>" & vbcrlf
						xRs2.MoveNext
					wend
					xRs2.close
					Set xRs2 = nothing
				end if
				xxEmpr = xxEmpr & "</select>" & vbcrlf
				MontaComboEmpr = xxEmpr
			End Function
%>		
	</HEAD>
	<body class="PopUpEntrada" onLoad="javascript:PreparaJanela(680, 250 + (document.form.QtdServicos.value) * 25);" onUnload="javascript:window.opener.location.reload();">
		<!--#include file="../includes/varControleAcesso.inc"-->
		<!--#include file="../includes/varBanco.inc"-->
<% 
    lPerfisAutorizados = "51"
	VerificarAcesso
	AbrirConexao
	projId = Request.Querystring("projId")

%>

	<form action="prjRelpCntr2.asp" method="post" name="form">
		<input type="hidden" name="usuaId" value="<% = Session("usuaId") %>" >
		<input type="hidden" name="projId" value="<% = projId %>" >
		<table width="100%">
			<tr>
				<td width="15"></td>
				<td></td>
			</tr>
			<tr>
				<td class="docTit"><%= Session("projNome")%> - Alterar contratos</td>
				<td align="right"><a href="javascript:AbrirJanela('prjRelpCntrNovo.asp?projId=<%=projId%>',500,250);"><img src="../images/newitem.gif" class="botao">&nbsp;Adicionar contrato</a></td>
			</tr>
			<tr>
				<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
			</tr>
			<tr>
				<td colspan="2" height="15"></td>
			</tr>
			<tr>
				<td colspan="2" align="left">
							<table width="100%" class="grid">
								<tr>
									<td width="5%" class="GridCab" >&nbsp;</td>
									<td width="50%" class="GridCab" align="center">Servi�o</td>
									<td width="45%" class="GridCab" align="center">Fornecedor</td>
								</tr>
<%
	Set xRs = gConexao.execute("PROJ_ListarContratos @pprojId = " & projId & ", @pLiberado = 0, @prelpId=0")
	i = 0 
	while not xRs.eof 
		if xRs("usuaIdCont") = "0" then
			xEmpr = "fornecedor n�o definido"
		else
			xEmpr = xRs("usuaNome") & " - " & xRs("emprNome") 
		end if
	    i = i + 1
		xusuaId = cStr(NuloLivre(xRs("usuaIdCont")))
		xservId = cStr(xRs("servId"))
		Response.write "<tr>" & vbcrlf
		Response.write vbtab & "<input type=""hidden"" name=""cntrId" & cStr(i) & """ value=""" & xRs("cntrId") & """>" & vbcrlf 
		Response.write vbtab & "<td class=""GridLinha"" height=""33""><a href=""javascript:AbrirJanela('prjRelpCntrExcl.asp?cntrId=" & xRs("cntrId") & "',400,200);""><img src=""../images/crossm.gif""></a></td>" & vbcrlf
		Response.write vbtab & "<td class=""GridLinha"">" & xRs("servNome") &  "</td>" & vbcrlf
		Response.write vbtab & "<td class=""gridLinha"" align=""center"">"  & MontaComboEmpr(cStr(i), xusuaId, xServId)  & "</td>" & vbcrlf
'		Response.write vbtab & "<td class=""gridLinha"">" & xEmpr & "</td>" & vbcrlf
		Response.write "</tr>" & vbcrlf
		xRs.MoveNext
	wEnd
%>
					<input type="hidden" name="QtdServicos" value="<%=i%>">
								<tr>
									<td colspan="5" class="GridRodape">&nbsp;</td>
								</tr>
							</table>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<table>
						<tr>
							<td width="45%" align="center"><a href="javascript:document.form.submit();"><img border="0" src="../images/btnSalvar.gif" class="botao"></a></td>
							<td width="10%" align="center">&nbsp;</td>
							<td width="45%" align="center"><img border="0" src="../images/btnCancelar.gif" class="botao" onclick="javascript:window.close();"></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
	</body>
</HTML>
