<% Response.Expires= -1%>
<%@ Import namespace="System.IO"%> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Controle remoto de projetos</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<script language="JavaScript" type="text/javascript" src="../scripts/PreparaJanela.js"></script>
		<SCRIPT language="JavaScript">
			function submitform(){
			  document.form.submit();
			}
			function validar(){
			  if (document.form.txtArquivo.value == '')
			  {   alert('Informe a localiza��o do arquivo.')}
			  else
			  {   submitform();}
			}
		</SCRIPT> 
		<script language="VB" runat="server">
			Sub UploadFile(source As Object, e As EventArgs)
				Dim savePath As String = "C:\Clientes\WhiteMartins\e-Tracking\Produto\E-Tracking\Anexos\"
				
   			    If Not (txtArquivo.PostedFile Is Nothing) Then
				  Dim postedFile = txtArquivo.PostedFile
				  Dim filename As String = Path.GetFileName(txtArquivo.FileName)
				  Dim contentType As String = txtArquivo.ContentType
				  Dim contentLength As Integer = txtArquivo.ContentLength			
				  txtArquivoFile.SaveAs(savePath & filename)
			    End If
			End Sub 
		</script>
 
	</HEAD>
	<body class="PopUpEntrada" onLoad="javascript:PreparaJanela(600,290);">
	<form enctype="multipart/form-data" runat="server">
	  Select File to Upload: 
	  <input id="uploadedFile" type="file" runat="server">
	  <p>
	  <input type=button id="upload" value="Upload" OnServerClick="Upload_Click" runat="server">
	  <p>
	  <asp:Label id="message" runat="server"/>
	</form>
	<form action="anxUpload2.asp" method="post" name="form" encType="multipart/form-data" onSubmit="startupload();" >
		<input type="hidden" name="tpanId" value="<%=tpanId%>" >
		<input type="hidden" name="projId" value="<%=projId%>" >
		<input type="hidden" name="cntrId" value="<%=cntrId%>" >
		<input type="hidden" name="tparExte" value="<%=tparExte%>" >
		<table width="100%">
			<tr>
				<td width="15"></td>
				<td></td>
			</tr>
			<tr>
				<td colspan="2" class="docTit"><% = tituloPagina %></td>
			</tr>
			<tr>
				<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
			</tr>
			<tr>
				<td colspan="2" height="15"></td>
			</tr>
			<tr>
				<td width="15"></td>
				<td>
					<table class="doc">
						<tr>
							<td width="110" class="docLabel">Arquivo</td>
							<td width="5" class="docLabel">:</td>
							<td class="docCmpLivre"><input type="file" runat="server" id="txtArquivo" name="txtArquivo" class="docCmpLivre"></td>
						</tr>
						<tr>
							<td width="110" class="docLabel">N�mero</td>
							<td width="5" class="docLabel">:</td>
							<td class="docCmpLivre"><input type="text" name="txtNumero" class="docCmpLivre"></td>
						</tr>
						<tr>
							<td width="110" class="docLabel">T�tulo</td>
							<td width="5" class="docLabel">:</td>
							<td class="docCmpLivre"><input type="text" name="txtNome" class="docCmpLivre" maxlength="150"></td>
						</tr>
						<tr>
							<td width="110" class="docLabel">Revis�o</td>
							<td width="5" class="docLabel">:</td>
							<td class="docCmpLivre"><input type="text" name="txtRevNum" class="docCmpLivre" maxlength="10"></td>						
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2" height="20"></td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<table>
						<tr>
							<td height="28" width="80" align="center"><img border="0" src="../images/btnSalvar.gif" class="botao" onServerclick="UploadFile"></td>
							<td width="10"></td>
							<td width="80" align="center"><img border="0" src="../images/btnCancelar.gif" class="botao" onclick="javascript:window.close();"></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
	</body>
</HTML>
