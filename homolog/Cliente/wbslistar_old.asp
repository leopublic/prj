<%
	Response.Expires= 0
    Response.AddHeader "PRAGMA", "NO-CACHE" 
    if Session("usuaId") = "" then
   		response.redirect("../login.asp")
    end if
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Controle de WBS</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="JavaScript" type="text/javascript" src="tree/dtree.js"></script>
		<link rel="stylesheet" type="text/css" href="tree/dtree.css">
		<script language="JavaScript" type="text/javascript" src="../scripts/moving_div_on_screen.js"></script>
        <script language="JavaScript" type="text/javascript" src="../scripts/block_background_objects.js"></script>
        <script language="JavaScript" type="text/javascript" src="../scripts/functions.js"></script>
        <link rel="stylesheet" type="text/css" href="../scripts/block_background_objects.css">
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<!--#include file="../includes/funcoes.inc"-->
		<script language="JavaScript" type="text/javascript" src="../scripts/PreparaJanela.js"></script>

		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<link rel="stylesheet" type="text/css" href="../EstiloMenu.css">
        <link rel="stylesheet" type="text/css" href="../scripts/wbs_form.css">
        
		<style type"text/css">
			
		</style>
		<SCRIPT language="JavaScript">
			function ExcluirItem(itemId){
                //alert ('entrou ExcluirItem '+itemId);
                //return false
                
				if(confirm('Confirma a exclus�o deste item e todos os demais relacionados a ele?')){

					document.body.style.cursor='wait';
					var pagina = "wbslistar_excluir_item.asp?itemId="+itemId;
					//
					var xmlhttp = _criaxmlhttprequest();

					xmlhttp.open("GET",pagina,true);
					xmlhttp.onreadystatechange = function() {
						if(xmlhttp.readyState == 4) {
							if(xmlhttp.status == 200){
								if(_tratarxmlretorno(xmlhttp.responseText)){
									alert('Item removido c/ sucesso!');
									_carregaitens('d','divLista','CADM','WBS Modelo','', '', '', '');
								}else{
									alert('Falha ao remover o item!');
									return false
								}
							}
						}
					}

					xmlhttp.send(null);
					document.body.style.cursor='auto';
				}
			}

			function AbrirDetalheItem(_itemId, _itemId_Pai, _tiwbId, _unimId, _itemCodigo, _itemNome, _itemDetalhamento, _itemPercPai, _itemFatorHH, _itemValUnitEst, _itemFLFilho){
                //alert ('entrou AbrirDetalheItem '+_itemId+', '+_itemId_Pai+', '+_tiwbId+', '+_unimId+', '+_itemCodigo+', '+_itemNome+', '+_itemPercPai+', '+_itemFatorHH+', '+_itemValUnitEst+', '+_itemFLFilho+', '+_itemFLFCN);
                //return false
				_ocdiv('divItem'); //Exibindo div

				//Preenchendo campos do div
				//Chaves
				document.getElementById("_itemId").value = _itemId;
				document.getElementById("_itemId_Pai").value = _itemId_Pai;
				document.getElementById("_tiwbId").value = _tiwbId;
				document.getElementById("_unimId").value = _unimId;
				//Descri��es
				document.getElementById("_itemCodigo").value = _itemCodigo;
				document.getElementById("_itemNome").value = _itemNome;
                document.getElementById("_itemDetalhamento").value = _itemDetalhamento;
				//Valores para WBS
				document.getElementById("_itemPercPai").value = _itemPercPai;
				document.getElementById("_itemFatorHH").value = _itemFatorHH;
				document.getElementById("_itemValUnitEst").value = _itemValUnitEst;
				//Dados de apoio
				document.getElementById("_itemFLFilho").value = _itemFLFilho;
				//
                //alert(_itemFLFilho);
				if(_itemFLFilho=='1'){
				 for(r__i=0;r__i<document.getElementsByName('sumir_linha_item').length;r__i++){
				 	document.getElementsByName('sumir_linha_item')[r__i].style.display='none';
				 }
				}
				//
				//document.getElementById("cmbtipoitem").style.display = "block"
				setavaluecombo('cmbtipoitem',_tiwbId);
				setavaluecombo('cmbunidademedida',_unimId);
			}

			function AddNewItemWbs(_itemId_Pai){
                //alert ('entrou AddNewItemWbs '+_itemId_Pai);
                //return false
				AbrirDetalheItem(0, _itemId_Pai, 0, 0, '', '', '', 0, 1, 0, 0, 0);
			}

			function SalvaItem(){
                //alert ('entrou SalvaItem');
                //return false
				//Chaves
				var _itemId = document.getElementById("_itemId").value;
				var _itemId_Pai = document.getElementById("_itemId_Pai").value;
				var _tiwbId = document.getElementById("_tiwbId").value;
				var _unimId = document.getElementById("_unimId").value;
				//Descri��es
				var _itemCodigo = document.getElementById("_itemCodigo").value;
				var _itemNome = document.getElementById("_itemNome").value;
                var _itemDetalhamento = document.getElementById("_itemDetalhamento").value;
				//Valores para WBS
				var _itemPercPai = document.getElementById("_itemPercPai").value;
				var _itemFatorHH = document.getElementById("_itemFatorHH").value;
				var _itemValUnitEst = document.getElementById("_itemValUnitEst").value;
				//Dados de apoio
				var _itemFLFilho = document.getElementById("_itemFLFilho").value;
				//Validando campos
				//  Tipo Item Wbs
				if(_tiwbId==0 && _itemFLFilho=='0'){
					alert('Tipo de Item WBS inv�lido!');
					return false
				}
				//  Unidade de Medida
				if(_unimId==0 && _itemFLFilho=='0'){
					alert('Unidade de Medida inv�lida!');
					return false
				}
				//	Nome
				if(_itemNome.trim==""){
					alert('Nome n�o informado!');
					return false
				}
				//	C�digo Wbs
				if(_itemCodigo.trim==""){
					alert('C�digo WBS n�o informado!');
					return false
				}

				//	Percentual em rela��o ao pai
				if(isNumberJS(_itemPercPai)==false){
					alert('Peso percentual em rela��o a atividade n�o informada!');
					return false
				}

				//	Fator Homem x Hora
				if(isNumberJS(_itemFatorHH)==false && _itemFLFilho=='0'){
					alert('Fator Homem x Hora n�o informado!');
					return false
				}

				if(isNumberJS(_itemValUnitEst)==false){
					_itemValUnitEst=0;
				}

                _itemNome = _itemNome.replace("&", "|AMP|")
                _ocdiv('divItem'); //Fechando div
                
				//Atualizando item
				document.body.style.cursor='wait';
				var pagina = 'wbslistar_atualiza_item.asp?itemId='+_itemId+'&itemId_Pai='+_itemId_Pai+'&tiwbId='+_tiwbId+'&unimId='+_unimId+'&itemCodigo='+_itemCodigo+'&itemNome='+_itemNome+'&itemPercPai='+_itemPercPai+'&itemFatorHH='+_itemFatorHH+'&itemValUnitEst='+_itemValUnitEst+'&itemFLFilho='+_itemFLFilho+'&itemDetalhamento='+_itemDetalhamento;
                
                //document.getElementById('divErros').innerHTML = 'wbslistar_atualiza_item.asp?itemId='+_itemId+'&itemId_Pai='+_itemId_Pai+'&tiwbId='+_tiwbId+'&unimId='+_unimId+'&itemCodigo='+_itemCodigo+'&itemNome='+_itemNome+'&itemPercPai='+_itemPercPai+'&itemFatorHH='+_itemFatorHH+'&itemValUnitEst='+_itemValUnitEst+'&itemFLFilho='+_itemFLFilho+'&itemFLFCN='+_itemFLFCN;
                //document.getElementById('divErros').style.display='block';
                //return true;
				//alert(pagina);
                    
                var xmlhttp = _criaxmlhttprequest();

				xmlhttp.open("GET",pagina,true);
				xmlhttp.onreadystatechange = function() {
					if(xmlhttp.readyState == 4) {
						if(xmlhttp.status == 200){
                            _ocdiv('over');
							if(_tratarxmlretorno(xmlhttp.responseText)){
                                alert('Item inserido com sucesso!');
                                _carregaitens('d','divLista','CADM','WBS Modelo','', '', '', '');
							}else{
								alert('Falha ao inserir o item!');
								return false
							}
						}
					}
				}
				xmlhttp.send(null);
				document.body.style.cursor='auto';
			}
            
		</SCRIPT>
	</HEAD>
<body id="sandbox" class="full">
<div id="over" style="display:none;"></div>
<!--#include file="../includes/varControleAcesso.inc"-->
<!--#include file="../includes/varBanco.inc"-->
<% 
	lPerfisAutorizados = "15"
	VerificarAcesso()

	gMenuSelecionado = "Wbs"
	' gOpcaoSelecionada = "Usuarios"
	gmenuGeral = "Configuracao"
	PastaSelecionada = "Configura��o"
%>
<!--#include file="../includes/cab.inc"-->
	<tr>
		<td style="padding:0px" valign="top" class="blocoOpcoes">
			<!--#include file="../includes/opcConfig.inc"-->
		</td>
		<td colspan="2" valign="top" style="padding:5px" class="blocoPagina">
			<form action="" method="post" name="frmWBS" id="frmWBS">
				<div id="divItem" name="divItem" style="display:none; position:absolute; z-index:1000;" class="divwindow">
					<div class='div_titulo' id='divItem_TIT' name='divItem_TIT' onmousedown="javascript:dragStart(event, 'divItem');">Cadastro de itens</div>
					<div class='sep'></div>
					<input id='_itemId' type='hidden'>
					<input id='_itemId_Pai' type='hidden'>
					<input id='_tiwbId' type='hidden'>
					<input id='_unimId' type='hidden'>
					<input id='_itemFLFilho' type='hidden'>
					<table class="entradadados">
						<tr>
							<th style="width:100px">C�digo WBS</th>
							<th style="width:1px">:</th>
							<td><input id='_itemCodigo' type='text' class='cpTexto' maxlength='10'></td>
						</tr>
						<tr>
							<th style="width:100px">Nome</th>
							<th>:</th>
							<td><input id='_itemNome' type='text' class='cpTexto' maxlength='250'></td>
						</tr>
                        <tr>
							<th style="width:100px">Detalhamento</th>
							<th>:</th>
							<td><textarea id='_itemDetalhamento' class='cpTexto' maxlength='2000' Rows=3></textarea></td>
						</tr>
						<tr>
							<th style="width:100px">Peso Perc. Pai</th>
							<th>:</th>
							<td><input id='_itemPercPai' type='text' class='cpNum' maxlength='11'></td>
						</tr>
						<tr name='sumir_linha_item'>
							<th style="width:100px">Fator Homem X Hora</th>
							<th>:</th>
							<td><input id='_itemFatorHH' type='text' class='cpNum' maxlength='11'></td>
						</tr>
						<tr name='sumir_linha_item'>
							<th style="width:100px">Valor Unit�rio</th>
							<th>:</th>
							<td><input id='_itemValUnitEst' type='text' class='cpNum' maxlength='11'></td>
						</tr>
						<tr name='sumir_linha_item'>
							<th style="width:100px">Tipo de Item</th>
							<th>:</th>
							<td><select id="cmbtipoitem" style="display:block" onchange="javascript:document.getElementById('_tiwbId').value=document.getElementById('cmbtipoitem').value">
								<%
								lTX_Sql = "select 0 as tiwbId, '[n�o definido]' as tiwbDescricao union "
								lTX_Sql = lTX_Sql & "select t.tiwbId, t.tiwbDescricao from TipoItemWbs t order by 2 "
								AbrirConexao
								Set lRS_1 = gConexao.Execute(lTX_Sql)
								while not lRS_1.EOF
									If lRS_1("tiwbId")="0" then
										lTX_Select = "selected"
									else
										lTX_Select = ""
									end if
									response.write vbtab & "<option value=""" & lRS_1("tiwbId") & """ class=""campo"" " & lTX_Select & ">" & lRS_1("tiwbDescricao") & "</option>" & vbcrlf
									lRS_1.MoveNext
								wend
								lRS_1.close
								set lRS_1 = nothing
								%>
								</select>
							</td>
						</tr>
						<tr name='sumir_linha_item'>
							<th style="width:100px">Unidade de Medida</th>
							<th>:</th>
							<td><select id="cmbunidademedida" style="display:block" onchange="javascript:document.getElementById('_unimId').value=document.getElementById('cmbunidademedida').value">
								<%
								lTX_Sql = "select 0 as unimId, '[n�o definido]' as unimNome union "
								lTX_Sql = lTX_Sql & "select u.unimId, (u.unimAbrev + ' - ' + u.unimDescricao) as unimNome from UnidadeMedida u order by 2 "
								AbrirConexao
								Set lRS_1 = gConexao.Execute(lTX_Sql)
								while not lRS_1.EOF
									If lRS_1("unimId")="0" then
										lTX_Select = "selected"
									else
										lTX_Select = ""
									end if
									response.write vbtab & "<option value=""" & lRS_1("unimId") & """ class=""campo"" " & lTX_Select & ">" & lRS_1("unimNome") & "</option>" & vbcrlf
									lRS_1.MoveNext
								wend
								lRS_1.close
								set lRS_1 = nothing
								%>
								</select>
							</td>
                        </tr>
					</table>

					<div class='barrabotoes'>
						<input type='button' onclick="javascript:SalvaItem();" value="Salvar" title="Salva dados do item em quest�o">
						<input type='button' onclick="javascript:_ocdiv('divItem');" value="Fechar" title="Fecha o cadastro de itens">
					</div>
				</div>
				<input type='hidden' id='counter' name='counter'>
				<div class='divtree' id="divLista" name="divLista">
				</div>
				<script language="JavaScript">
					_carregaitens('d', 'divLista', 'CADM', 'WBS Modelo', '', '', '', '');
				</script>
				<div id='divErros' style='width:100%;height:0px;display:none;margin-top:5px;'></div>
			</form>
		</td>
	</tr>
<!--#include file="../includes/rod.inc"-->

</body>
</HTML>
