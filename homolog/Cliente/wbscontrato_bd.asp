<!--#include file="../includes/clsBanco.inc"-->
<!--#include file="../includes/funcoes.inc"-->
<%
Session.LCID=1046 'Pt-BR
response.charset = "iso-8859-1"

on error resume next

lR = ""

ltipo = ucase(request.querystring("tipo"))

Dim gConexao
AbrirConexao
'
lR = ""
lTX_SQL = ""
'
liwbcId          = "null"
liwbcComplemento = "null"
liwbcQtdEst      = "null"
liwbcValUnitReal = "null"
liwbcDtInicioEst = "null"
liwbcDuracaoEst  = "null"
liwbcFLConcForn  = "null"
liwbcFLConcVerif = "null"
lusuaIdVerif     = "null"
lusuaId          = "null"
'
lusuaId = NumberToBD(session("usuaId"),0)
if lusuaId = "ERRO" then lR = NovoErroXML("I-T" & mid(ltipo,2), "usuaId inv�lido!")

liwbcId = NumberToBD(request.querystring("iwbcId"),0)
if liwbcId = "ERRO" then lR = NovoErroXML("I-T" & mid(ltipo,2), "iwbcId inv�lido!")

liwbcComplemento = StringToBD(request.querystring("iwbcComplemento"))
if liwbcComplemento = "ERRO" then lR = NovoErroXML("I-T" & mid(ltipo,2), "iwbcComplemento inv�lido!")
'
lfcn_Id = NumberToBD(request.querystring("fcn_Id"),0)
'
select case ltipo
	case "I1" 'PPU
		liwbcQtdEst = NumberToBD(request.querystring("iwbcQtdEst"),2)
		if liwbcQtdEst = "ERRO" then lR = NovoErroXML("I-T1-C3", "iwbcQtdEst inv�lido!")

		liwbcValUnitReal = NumberToBD(request.querystring("iwbcValUnitReal"),2)
		if liwbcValUnitReal = "ERRO" then lR = NovoErroXML("I-T1-C4", "iwbcValUnitReal inv�lido!")
	case "I2" 'Montar Cronograma
        liwbcDtInicioEst = DateToBD(request.querystring("iwbcDtInicioEst"))
		if liwbcDtInicioEst = "ERRO" then lR = NovoErroXML("I-T2-C6", "iwbcDtInicioEst inv�lido!")
        
        liwbcDuracaoEst = NumberToBD(request.querystring("iwbcDuracaoEst"),2)
		if liwbcDuracaoEst = "ERRO" then lR = NovoErroXML("I-T2-C7", "iwbcDuracaoEst inv�lido!")
	case "I3" 'EAP (Informando avan�o)
        liwbcFLConcForn = "0"
        if request.querystring("iwbcFLConcForn") <> "" then 
            liwbcFLConcForn = NumberToBD(request.querystring("iwbcFLConcForn"),0)
        end if
	case "I4" 'Validando Avan�o (Fechamento de EAP)
        liwbcFLConcVerif = "0"
        if request.querystring("iwbcFLConcVerif") <> "" then 
            liwbcFLConcVerif = NumberToBD(request.querystring("iwbcFLConcVerif"),0)
        end if
       
        lusuaIdVerif = NumberToBD(request.querystring("usuaIdVerif"),0)
		if lusuaIdVerif = "ERRO" then lR = NovoErroXML("I-T5-C10", "usuaIdVerif inv�lido!")
end select
'
if (ltipo = "I1") or (ltipo = "I2") or (ltipo = "I3") or (ltipo = "I4") then
    lTX_SQL = lTX_SQL & "exec dbo.IWBC_Atualizar_CONTRATO "
    lTX_SQL = lTX_SQL & "  @piwbcId          = " & liwbcId
    lTX_SQL = lTX_SQL & ", @piwbcComplemento = " & liwbcComplemento
    lTX_SQL = lTX_SQL & ", @piwbcQtdEst      = " & liwbcQtdEst
    lTX_SQL = lTX_SQL & ", @piwbcValUnitReal = " & liwbcValUnitReal
    lTX_SQL = lTX_SQL & ", @piwbcDtInicioEst = " & liwbcDtInicioEst
    lTX_SQL = lTX_SQL & ", @piwbcDuracaoEst  = " & liwbcDuracaoEst
    lTX_SQL = lTX_SQL & ", @piwbcFLConcForn  = " & liwbcFLConcForn
    lTX_SQL = lTX_SQL & ", @piwbcFLConcVerif = " & liwbcFLConcVerif
    lTX_SQL = lTX_SQL & ", @pusuaIdVerif     = " & lusuaIdVerif
    lTX_SQL = lTX_SQL & ", @pusuaId          = " & lusuaId
    lTX_SQL = lTX_SQL & ", @ptipo            = " & mid(ltipo,2)
End If
'
select case ltipo    
    case "I6" 'Informar quantidades atualizadas    
        '
        liwbcQtdReal = NumberToBD(request.querystring("iwbcQtdReal"),2)
		if liwbcQtdReal = "ERRO" then lR = NovoErroXML("I-T6-C3", "iwbcQtdReal inv�lido!")
        '
        liwdeDesNum = StringToBD(request.querystring("iwdeDesNum"))
		if liwdeDesNum = "ERRO" then lR = NovoErroXML("I-T6-C4", "iwdeDesNum inv�lido!")
        '
        liwdeDesRev = NumberToBD(request.querystring("iwdeDesRev"),0)
		if liwdeDesRev = "ERRO" then lR = NovoErroXML("I-T6-C5", "iwdeDesRev inv�lido!")
        '
        lusuaIdVerif = NumberToBD(request.querystring("usuaIdVerif"),0)
		if lusuaIdVerif = "ERRO" then lR = NovoErroXML("I-T6-C6", "iwbcId inv�lido!")
        '
        lTX_SQL = lTX_SQL & "exec dbo.IWBC_Atualizar_Qtd "
        lTX_SQL = lTX_SQL & "  @piwbcId          = " & liwbcId
        lTX_SQL = lTX_SQL & ", @piwbcComplemento = " & liwbcComplemento
        lTX_SQL = lTX_SQL & ", @piwbcQtdReal     = " & liwbcQtdReal
        lTX_SQL = lTX_SQL & ", @pNumeroDesenho 	 = " & liwdeDesNum
        lTX_SQL = lTX_SQL & ", @pRevisaoDesenho  = " & liwdeDesRev
        lTX_SQL = lTX_SQL & ", @pusuaId          = " & lusuaIdVerif
        '        
    case "-PPU" 'Fechamento de PPU
		lwbscId = NumberToBD(request.querystring("wbscId"),0)
        lTX_SQL = ""
		lTX_SQL = lTX_SQL & "exec dbo.WBSC_FecharPPU @pwbscId = " & lwbscId & ", @pfcn_Id = " & lfcn_Id & ", @pusuaId = " & lusuaId 
	case "-IEAP" 'In�cio dos Avan�os - EAP
		lwbscId = NumberToBD(request.querystring("wbscId"),0)
		lTX_SQL = lTX_SQL & "exec dbo.WBSC_FecharEAP @pwbscId = " & lwbscId & ", @pfcn_Id = " & lfcn_Id & ", @pusuaId = " & lusuaId
	case "-FEAP" 'Finaliza��o dos avan�os - EAP
		lwbscId = NumberToBD(request.querystring("wbscId"),0)
		lTX_SQL = lTX_SQL & "exec dbo.WBSC_FecharBM @pwbscId = " & lwbscId & ", @pusuaId = " & lusuaId
    case "-DEL" 'Excluindo item (e seus filhos)
        lTX_SQL = lTX_SQL & "exec IWBC_ExcluirItem "
        lTX_SQL = lTX_SQL & "  @piwbcId = " & liwbcId
    case "-INC" 'Incluindo item (e seus filhos)
        '
        lwbscId     = NumberToBD(request.querystring("wbscId"),0)
        if lwbscId = "0" or lwbscId = "null" then lR = lR & NovoErroXML("IWBC_ADD", "wbscId inv�lido!","")
        liwbcId_Pai = NumberToBD(request.querystring("iwbcId_Pai"),0)
        if liwbcId_Pai = "0" then liwbcId_Pai = "null"
        lfcn_Id     = NumberToBD(request.querystring("fcn_Id"),0)
        if lfcn_Id = "0" then lfcn_Id = "null"
        if (not isnumeric(lfcn_Id)) then lfcn_Id = "null"
        '
        lTX_SQL = ""
        lTX_SQL = lTX_SQL & "exec IWBC_IncluirItemModelo "
        lTX_SQL = lTX_SQL & "  @piwbcId     = " & liwbcId
        lTX_SQL = lTX_SQL & ", @pwbscId     = " & lwbscId
        lTX_SQL = lTX_SQL & ", @piwbcId_Pai = " & liwbcId_Pai
        lTX_SQL = lTX_SQL & ", @pfcn_Id     = " & lfcn_Id
        lTX_SQL = lTX_SQL & ", @pusuaId     = " & lusuaId
    case "-FCN" 'Incluindo item (e seus filhos)
        '
        lR = ""
        lwbscId     = NumberToBD(request.querystring("wbscId"),0)
        if lwbscId = "0" or lwbscId = "null" then lR = lR & NovoErroXML("FCN_ADD", "wbscId inv�lido!","")
        
        if lfcn_Id = "null" then lR = lR & NovoErroXML("FCN_ADD", "fcn_Id inv�lido!","")
        
        lfcn_Motivo = StringToBD(replace(request.querystring("fcn_Motivo"),"<br>",vbcrlf))
        if lfcn_Motivo = "ERRO" or lfcn_Motivo = "null" then lR = lR & NovoErroXML("FCN_ADD", "fcn_Motivo inv�lido!","")
        
        'lR = lR & NovoErroXML("FCN_ADD", replace(replace(replace(request.querystring("fcn_Motivo"),"\n","aaaaa"),vbcrlf,"bbbbbb"),"<br>","cccc"),"")
        
        lfcn_FLAprovacao = NumberToBD(request.querystring("fcn_FLAprovacao"),0)
        'if (trim(request.querystring("aprovacao")) = "x")then
            'if lfcn_FLAprovacao = "null" then
            '    lR = lR & NovoErroXML("FCN_ADD", "fcn_FLAprovacao inv�lido!","")
            'end if
        'else    
        '    lfcn_FLAprovacao = "null"
        'end if
        '
        lTX_SQL = ""
        lTX_SQL = lTX_SQL & "exec FCN_Atualizar "
        lTX_SQL = lTX_SQL & "  @pfcn_Id     = " & lfcn_Id
        lTX_SQL = lTX_SQL & ", @pwbscId     = " & lwbscId
        lTX_SQL = lTX_SQL & ", @pfcn_Motivo = " & lfcn_Motivo
        lTX_SQL = lTX_SQL & ", @pAprovar    = " & lfcn_FLAprovacao
        lTX_SQL = lTX_SQL & ", @pusuaId     = " & lusuaId
		'response.write lTX_SQL
end select
'response.write "<br/>" & lTX_SQL
'lR=lTX_SQL
if trim(lTX_SQL) <> "" and trim(lR) = "" then
	gConexao.execute(lTX_SQL)

    if(ltipo = "-FEAP") then
		set lRS_Z = gConexao.execute("select isnull(w.wbscId_Clone,0) as novoId from WbsContrato w where w.wbscId = " & lwbscId)
		If(not lRS_Z.BOF) and (Not lRS_Z.EOF) then
			lR = lRS_Z("novoId")
		end if
		lRS_Z.Close
	end if
End if

set gConexao = nothing

if err.number <> 0 then lR = NovoErroXML(err.number, err.description, lTX_SQL)

response.write lR
%>