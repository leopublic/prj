<% Response.Expires= -1%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Controle remoto de projetos</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<script language="JavaScript" type="text/javascript" src="../scripts/PreparaJanela.js"></script>
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<script language="JavaScript" type="text/javascript" src="../scripts/CheckNumeric.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/NumberFormat.js"></script>
		<script language="JavaScript">
			function submitform(){
			  document.form1.submit();
			}
			function validar(){
				if (document.form1.txtDescricao.value == '')
				{   alert('O preenchimento do nome do item � obrigatorio.');
					document.form1.txtDescricao.select();
					document.form1.txtDescricao.focus(); }
				else
				{
					if (document.form1.txtCodJde.value == '')
					{   alert('O preenchimento do c�digo JDE � obrigatorio.');
						document.form1.txtCodJde.select();
						document.form1.txtCodJde.focus(); }
					else
					{   submitform();}
				}
			}
			function Formatar(campo){
				var inputValue = document.getElementById(campo+'Fmt');
				var inputHidden = document.getElementById(campo);
				inputValue.value = FormatarNumero(inputValue.value, '.', ',', ',');
				inputHidden.value = FormatarNumero(inputValue.value, '', ',', '.');
			}
		</script> 
	</HEAD>
	<body class="PopUpEntrada" onLoad="PreparaJanela(600,300);">
<% 
    lPerfisAutorizados = "51"
	VerificarAcesso
	Dim gConexao 
	AbrirConexao
	itemId = Request.Querystring("itemId")
	if itemId = "0" then
	    titulo = "Cadastrar novo item de material"
		Session("TituloMsg") = titulo
		Session("Msg") = "Novo item inclu�do com sucesso!"
		tpliNome = ""
		tpliPrefixo = ""
		tpliCodigo = ""
	else
		xSQL = "select itemId, itemDescricao, itemCodJde, itemTag, itemFatorConvPeso from item where itemId = " & itemId
		set xrs = gConexao.execute(xSQL)
		titulo = "Alterar item de material"
		Session("TituloMsg") = titulo
		Session("Msg") = "Item alterado com sucesso!"
		itemCodJde = xrs("itemCodJde")
		itemDescricao = xrs("itemDescricao")
		itemTag = xrs("itemTag")
		itemFator = xrs("itemFatorConvPeso")
		xRs.CLose
		Set xRs = nothing
	end if
%>

	<form onSubmit="Validar();" action="cfgItemMatAlterar1.asp" method="post" name="form1">
		<input type="hidden" name="itemId" value=" <% = itemId %> " />
		<input type="hidden" name="txtFator" value=""/>
		<div style="margin-right:10px">
		<table width="100%">
			<tr>
				<td width="15"></td>
				<td></td>
			</tr>
			<tr>				
				<td colspan="2" class="docTit"><% = titulo %></td>
			</tr>
			<tr>
				<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
			</tr>
			<tr>
				<td colspan="2" height="15"></td>
			</tr>
			<tr>
				<td></td>
				<td>
					<table class="doc">
						<tr>
							<td width="50px" class="docLabel">C�digo JDE</td>
							<td width="5px" class="docLabel">:</td>
							<td class="docCmpLivre"><input type="text" name="txtCodJde" class="docCmpLivre" value="<% = itemCodJde %>"/></td>
						</tr>
						<tr>
							<td class="docLabel">Descri��o</td>
							<td class="docLabel">:</td>
							<td class="docCmpLivre"><input type="text" name="txtDescricao" maxlength="100" class="docCmpLivre" value="<% = itemDescricao %>"/></td>
						</tr>
						<tr>
							<td class="docLabel">TAG</td>
							<td class="docLabel">:</td>
							<td class="docCmpLivre"><input type="text" name="txtTag" maxlength="50" class="docCmpLivre" value="<% = itemTag %>"/></td>
						</tr>
						<tr>
							<td class="docLabel">Fator de convers�o</td>
							<td class="docLabel">:</td>
							<td class="docCmpLivre"><input type="text" name="txtFatorFmt" id="txtFatorFmt" maxlength="10" class="docCmpLivre" value="<% = itemFator %>"  onBlur="javascript:Formatar('txtFator');"  style="text-align:right;"/></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2" height="20"></td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<table>
						<tr>
							<td height="28" width="45%" align="center"><a href="javascript:validar();"><img src="../images/btnSalvar.gif" class="botao"></a></td>
							<td width="10%"></td>
							<td width="45%" align="center"><a href="javascript:window.close();"><img src="../images/btnCancelar.gif" class="botao"></a></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		</div>
	</form>
	</body>
</HTML>
