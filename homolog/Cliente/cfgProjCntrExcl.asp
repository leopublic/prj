<% Response.Expires= -1%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Projetos</title><%%>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<script language="JavaScript" type="text/javascript" src="../scripts/PreparaJanela.js"></script>
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<!--#include file="../includes/basFormatador.inc"-->
	</HEAD>
	<body class="PopUpEntrada" onLoad="javascript:PreparaJanela(500,200);">
<% 
    lPerfisAutorizados = "125"
	VerificarAcesso
	Dim gConexao 
	AbrirConexao
	cntrId = Request.Querystring("cntrId")
	xSQL = ""
	xSQL = xSQL & "select servNome, isnull(emprNome, 'n�o definido') emprNome from (Contrato left join Empreiteira on Empreiteira.emprId = Contrato.emprId) , Servico where cntrId =  " & cntrId & " and Servico.servId = Contrato.servId"
	set xrs = gConexao.execute(xSQL)
	titulo = "Exclus�o de contrato"
	cntrNomeServ = xRs("servNome")
	emprNome = xrs("emprNome")
	xRs.CLose
	Session("TituloMsg") = titulo
	Set xRs = nothing
%>

	<form action="cfgProjCntrExcl2.asp" method="post" name="form1">
		<input type="hidden" name="cntrId" value="<% = cntrId %>" >
		<table width="100%">
			<tr>
				<td width="15"></td>
				<td></td>
			</tr>
			<tr>
				<td colspan="2" class="docTit"><% = titulo %></td>
			</tr>
			<tr>
				<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
			</tr>
			<tr>
				<td colspan="2" height="15"></td>
			</tr>
			<tr>
				<td colspan="2" class="docSubTit">Voc� confirma a exclus�o do seguinte contrato:</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td align="left">
					<table class="doc">
						<tr>
							<td width="120" class="docLabel">Servi�o</td>
							<td width="05" class="docLabel">:</td>
							<td class="docCmpLivre"><% = cntrNomeServ %></td>
						</tr>
						<tr>
							<td width="120" class="docLabel">Fornecedor</td>
							<td width="05" class="docLabel">:</td>
							<td class="docCmpLivre"><% = emprNome %></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2" height="15"></td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<table>
						<tr>
							<td height="28" width="80" align="center"><a href="javascript:document.form1.submit();"><img src="../images/btnExcluir.gif" class="botao"></a></td>
							<td width="10"></td>
							<td width="80" align="center"><a href="javascript:window.close();"><img src="../images/btnCancelar.gif" class="botao"></a></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
	</body>
</HTML>
