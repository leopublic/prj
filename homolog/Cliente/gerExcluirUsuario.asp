<% Response.Expires= -1%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Controle remoto de projetos</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<SCRIPT language="JavaScript">
			function submitform(){
			  document.form1.submit();
			}
			function validar(){
			  if (document.form1.txtLogin.value == '')
			  { alert('O preenchimento do login � obrigatorio.')}
			  else
			  {   if (document.form1.txtNome.value == '')
			      { alert('O preenchimento do nome � obrigatorio.')}
				  else
				  {    senha1 = document.form1.txtSenha1.value;
		  			   senha2 = document.form1.txtSenha2.value;
			  		   if (senha1 == senha2) {
						  	submitform();}
					  else
						  {alert('Senhas diferentes');}
					}
				}			  
			}
		</SCRIPT> 
	</HEAD>
	<body class="cabecalho">
<% 
    lPerfisAutorizados = "23451"
	VerificarAcesso
	Dim gConexao 
	AbrirConexao
	usuaId = Request.Querystring("usuaID")
	if usuaId = 0 then
	    titulo = "Criar novo usu�rio"
		Session("TituloMsg") = titulo
	    login = ""
		nome = ""
		email = ""
		senha = ""
		perfil = ""
	else
		xSQL = "select usuaNome, usuaLogi, usuaEmai, usuaSenh, perfId from Usuario where usuaId = " & usuaId
		set xrs = gConexao.execute(xSQL)
		titulo = "Alterar usu�rio"
		Session("TituloMsg") = titulo
	    login = xRs("usuaLogi")
		nome = xrs("usuaNome")
		email = xRs("usuaEmai")
		senha = xRs("usuaSenh")
		perfil = xRs("perfId")
		xRs.CLose
		Set xRs = nothing
	end if
%>

	<form action="cfgUsuaAlterar2.asp" method="post" name="form1">
		<input type="hidden" name="usuaId" value=" <% = usuaId %> " >
		<table width="440">
			<tr>
				<td class="docTit"><% = titulo %></td>
			</tr>
			<tr>
				<td background="../images/pont_cinza_h.gif" style="height:2"></td>
			</tr>
			<tr>
				<td height="15"></td>
			</tr>
			<tr>
				<td>
					<table class="doc">
						<tr>
							<td width="120" class="docLabel">Login</td>
							<td width="05" class="docLabel">:</td>
							<td class="docCmpLivre"><input type="text" name="txtLogin" class="docCmpLivre" value="<% = login %>"></td>
						</tr>
						<tr>
							<td class="docLabel">Nome</td>
							<td class="docLabel">:</td>
							<td class="docCmpLivre"><input type="text" name="txtNome" class="docCmpLivre" value="<% = nome %>"></td>
						</tr>
						<tr>
							<td class="docLabel">E-mail de contato</td>
							<td class="docLabel">:</td>
							<td class="docCmpLivre"><input type="text" name="txtEmail" class="docCmpLivre" value="<% = email %>"></td>
						</tr>
						<tr>
							<td class="docLabel">Senha de acesso</td>
							<td class="docLabel">:</td>
							<td class="docCmpLivre"><input type="password" name="txtSenha1" class="docCmpLivre" value="<% = senha %>"></td>
						</tr>
						<tr>
							<td class="docLabel" align="right">(repetir)</td>
							<td class="docLabel"></td>
							<td class="docCmpLivre"><input type="password" name="txtSenha2" class="docCmpLivre" value="<% = senha %>"></td>
						</tr>
						<tr>
							<td class="docLabel">Perfil</td>
							<td class="docLabel"></td>
							<td class="docCmpLivre" style="border:0"><select name="cmbPerfil" class="docCmpLivre" style="width:300">
	<%
	xSQL = "select perfId, perfNome"
	xSQL = xSQL & " from PerfilAcesso order by perfNome"
	Set xRs = gConexao.Execute(xSQL)
	while not xRs.EOF
		if perfil <> "" then
			if xRs("perfId") = perfil then
				xSelected = " selected "
			else
				xSelecionar = ""
			end if
		end if
		response.write vbtab & "<option value=""" & xRs("perfId") & """ class=""campo"" " & xSelected & ">" & xRs("perfNome") & "</option>"
		xSelected = ""
		xRs.MoveNext
	wend
	%>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td height="20"></td>
			</tr>
			<tr>
				<td align="center">
					<table>
						<tr>
							<td height="28" width="80" class="botao"><a class="botao" onclick="javascript:validar();">Salvar</a></td>
							<td width="10"></td>
							<td width="80" class="botao"><a href="" class="botao" onclick="javascript:window.close();">Cancelar</a></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
	</body>
</HTML>
