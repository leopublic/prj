<% Response.Expires= -1%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Controle remoto de projetos</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<link rel="stylesheet" type="text/css" href="../EstiloMenu.css">
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
	</HEAD>
	<body class="cabecalho" >
		<!--#include file="../includes/varBanco.inc"-->
<%
   if Session("usuaId") = "" then
   		response.redirect("../login.asp")
   end if
	AbrirConexao
	projId = Request.Querystring("projId")
	xSQL = ""
	xSQL = xSQL & "select projNome, projNum, moedSigla"
	xSQL = xSQL & " from Projeto, Moeda "
	xSQL = xSQL & " where Projeto.projId = " & projId
	xSQL = xSQL & " and Moeda.moedId = Projeto.moedId"
	set xrs = gConexao.execute(xSQL)
	projNome = xRs("projNome")
	projNum = xRs("projNum")
	Session("projId") = projId
	Session("projNome") = projNome
	Session("projNum") = projNum
	Session("moedSigla") = xRs("moedSigla")
	Set xRs = nothing


	Dim cntrNomeServArray(28)
	Dim cntrIdArray(28)
	Dim emprIdArray(28)
	Dim usuaIdContArray(28)
	Dim emprNomeArray(28)
	xSQL = ""
	xSQL = xSQL & "select Contrato.cntrId"
	xSQL = xSQL & "     , servNome"
	xSQL = xSQL & "     , isnull(Contrato.emprId, 0) emprId"
	xSQL = xSQL & "     , isnull(emprNome, 'n�o definido') emprNome"
	xSQL = xSQL & "     , isnull(usuaIdCont, 0) usuaIdCont"
	xSQL = xSQL & "  from (Contrato left join Empreiteira on Empreiteira.emprId = Contrato.emprId), Servico"
	xSQL = xSQL & " where projId        = " & projId
	xSQL = xSQL & "   and Servico.servId = Contrato.servId"
	xSQL = xSQL & "   and Contrato.cntrCanc = 0"
	xSQL = xSQL & " order by cntrId"
'	Set xRs = gConexao.Execute(xSQL)
	Set xRs = gConexao.Execute("exec PROJ_ListarContratos @pprojId = " & projId & ", @pLiberado = 1, @prelpId = 0")
	i = 0
	while not xRs.eof
		i = i + 1
		if isnull(xRs("cntrId")) then
			cntrIdArray(i) = 0
		else
			cntrIdArray(i) = xRs("cntrId")
		end if
		if isnull(xRs("servNome")) then
			cntrNomeServArray(i) = ""
		else
			cntrNomeServArray(i) = xRs("servNome")
		end if
		if isnull(xRs("emprId")) then
			emprIdArray(i) = 0
		else
			emprIdArray(i) = xRs("emprId")
		end if
		if isnull(xRs("emprNome")) then
			emprNomeArray(i) = ""
		else
			emprNomeArray(i) = xRs("emprNome")
		end if
		if isnull(xRs("usuaIdCont")) then
			usuaIdContArray(i) = 0
		else
			usuaIdContArray(i) = xRs("usuaIdCont")
		end if
		xRs.MoveNext
	wEnd
	Session("cntrNomeServArray") = cntrNomeServArray
	Session("cntrIdArray") = cntrIdArray
	Session("emprNomeArray") = emprNomeArray
	Session("emprIdArray") = emprIdArray
	Session("usuaIdContArray") = usuaIdContArray
	Session("QtdServicos") = i
	if Request.QueryString("offline")="1" then
		Response.Redirect("prjArquivar.asp?projId=" & projId)
	else
		if Session("perfId") = "" then
			Response.Redirect("../logininput.asp")
		else
			if Session("perfId") = "1" or Session("perfId") = "5" then
				Response.Redirect("prjResumo.asp?projId=" & projId)
			elseif Session("perfId") <> "7" then
				Response.Redirect("prjQualidade.asp")
			else
				Response.Redirect("prjListListar.asp")
			end if
		end if
	end if
%>
	</body>
</HTML>
