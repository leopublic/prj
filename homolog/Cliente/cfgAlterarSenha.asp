<% Response.Expires= -1%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Controle remoto de projetos</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<script language="JavaScript" type="text/javascript" src="../scripts/PreparaJanela.js"></script>
		<SCRIPT language="JavaScript">
			function submitform(){
			  document.form1.submit();
			}
			function validar(){
			   senha1 = document.form1.txtSenha1.value;
			   senha2 = document.form1.txtSenha2.value;
			   if (senha1=='') {
			   		alert('A nova senha deve ser diferente de "branco".');}
			   else {
				   if (senha1 == senha2) {
						submitform();}
				   else
					  {alert('As novas senhas est�o diferentes. Verifique e tente novamente.');}
			   }
			}
		</SCRIPT> 
	</HEAD>
	<body class="PopUpEntrada" onLoad="javascript:PreparaJanela(460,320);">
<% 
    lPerfisAutorizados = "1234578"
	VerificarAcesso
	Dim gConexao 
	AbrirConexao
	if Request.QueryString("Limpo") = "1" then
		Session("Msg") = ""
	end if
	Msg = Session("Msg")
	usuaId = Session("usuaId")
	xSQL = "select usuaNome, usuaLogi from Usuario where usuaId = " & usuaId
	set xrs = gConexao.execute(xSQL)
	titulo = "Alterar senha"
	Session("TituloMsg") = titulo
	login = xRs("usuaLogi")
	nome = xrs("usuaNome")
	if cstr(emprId) = "" then
		emprId = "1"
	end if
	xRs.CLose
	Set xRs = nothing
%>
	<form action="cfgAlterarSenha2.asp" method="post" name="form1">
		<table width="100%">
			<tr>
				<td width="15"></td>
				<td></td>
			</tr>
			<tr>
				<td colspan="2" class="docTit"><% = titulo %></td>
			</tr>
			<tr>
				<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
			</tr>
			<tr>
				<td colspan="2" height="15"></td>
			</tr>
			<tr>
				<td width="15"></td>
				<td>
					<table class="doc">
						<tr>
							<td width="110" class="docLabel">Login</td>
							<td width="5" class="docLabel">:</td>
							<td class="docCmpLivre"><% = login %></td>
						</tr>
						<tr>
							<td class="docLabel">Nome</td>
							<td class="docLabel">:</td>
							<td class="docCmpLivre"><% = nome %></td>
						</tr>
						<tr>
							<td class="docLabel">Senha atual</td>
							<td class="docLabel">:</td>
							<td class="docCmpLivre"><input type="password" name="txtSenhaAtual" class="docCmpLivre" value=""></td>
						</tr>
						<tr>
							<td class="docLabel">Nova senha</td>
							<td class="docLabel">:</td>
							<td class="docCmpLivre"><input type="password" name="txtSenha1" class="docCmpLivre" value=""></td>
						</tr>
						<tr>
							<td class="docLabel" align="right">(repetir)</td>
							<td class="docLabel"></td>
							<td class="docCmpLivre"><input type="password" name="txtSenha2" class="docCmpLivre" value=""></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2" height="20"></td>
			</tr>
			<tr>
				<td colspan="2" align="center" style="color:red"><% = Msg %></td>
			</tr>
			<tr>
				<td colspan="2" height="20"></td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<table>
						<tr>
							<td height="28" width="80" align="center"><img border="0" src="../images/btnSalvar.gif" class="botao" onclick="javascript:validar();"></td>
							<td width="10"></td>
							<td width="80" align="center"><img border="0" src="../images/btnCancelar.gif" class="botao" onclick="javascript:window.close();"></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
	</body>
</HTML>
