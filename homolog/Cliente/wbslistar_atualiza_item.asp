<!--#include file="../includes/clsBanco.inc"-->
<!--#include file="../includes/funcoes.inc"-->
<%
Session.LCID=1046 'Pt-BR
response.charset = "iso-8859-1"

on error resume next

lR = ""

'-- Recep��o de par�metros - INI
litemId=Request.QueryString("itemId")
litemId_Pai=Request.QueryString("itemId_Pai")
ltiwbId=Request.QueryString("tiwbId")
lunimId=Request.QueryString("unimId")
litemCodigo=Request.QueryString("itemCodigo")
litemNome=Request.QueryString("itemNome")
litemDetalhamento=Request.QueryString("itemDetalhamento")
litemPercPai=Request.QueryString("itemPercPai")
litemFatorHH=Request.QueryString("itemFatorHH")
litemValUnitEst=Request.QueryString("itemValUnitEst")
litemFLFilho=Request.QueryString("itemFLFilho")
lusuaId=Session("usuaId")
'-- FIM

'-- Valida��es - INI
litemId = NumberToBD(litemId,0)
if litemId = "null" then litemId = "0"
litemId_Pai = NumberToBD(litemId_Pai,0)
if litemId_Pai = "0" then litemId_Pai = "null"
ltiwbId = NumberToBD(ltiwbId,0)
lunimId = NumberToBD(lunimId,0)

litemCodigo = StringToBD(litemCodigo)
litemNome = StringToBD(litemNome)
litemDetalhamento = StringToBD(litemDetalhamento)

litemPercPai = NumberToBD(litemPercPai,2)
if litemPercPai = "ERRO" then lR = NovoErroXML("WCAD-1", "itemPercPai inv�lido!","")

litemFatorHH = NumberToBD(litemFatorHH,2)
if litemFatorHH = "ERRO" then lR = NovoErroXML("WCAD-2", "itemFatorHH inv�lido!","")
if litemFatorHH = "null" then litemFatorHH = "1"

litemValUnitEst = NumberToBD(litemValUnitEst,2)
if litemValUnitEst = "ERRO" then lR = NovoErroXML("WCAD-3", "itemValUnitEst inv�lido!","")

lusuaId = NumberToBD(lusuaId,0)
if lusuaId = "ERRO" then lR = NovoErroXML("WCAD-12", "Login expirado. Por favor, reconecte ao sistema!","")
'-- FIM

if lR = "" then
    Dim gConexao
    AbrirConexao

    lTX_SQL = ""
'    lTX_SQL = lTX_SQL & "exec IWBC_Atualizar_Modelo "
    lTX_SQL = lTX_SQL & "exec IWBC_Atualizar_Modelo "
    lTX_SQL = lTX_SQL & "   @piwbcId		   = " & litemId
    lTX_SQL = lTX_SQL & " , @piwbcId_Pai	   = " & litemId_Pai
    lTX_SQL = lTX_SQL & " , @ptiwbId		   = " & ltiwbId
    lTX_SQL = lTX_SQL & " , @punimId		   = " & lunimId
    lTX_SQL = lTX_SQL & " , @piwbcCodigo	   = " & litemCodigo
    lTX_SQL = lTX_SQL & " , @piwbcNome		   = " & litemNome
    lTX_SQL = lTX_SQL & " , @piwbcDetalhamento = " & litemDetalhamento
    lTX_SQL = lTX_SQL & " , @piwbcPercPai	   = " & litemPercPai
    lTX_SQL = lTX_SQL & " , @piwbcFatorHH	   = " & litemFatorHH
    lTX_SQL = lTX_SQL & " , @piwbcValUnitEst   = " & litemValUnitEst
    lTX_SQL = lTX_SQL & " , @piwbcFLFilho	   = " & litemFLFilho
    lTX_SQL = lTX_SQL & " , @pusuaId     	   = " & lusuaId
	'response.write lTX_SQL
    gConexao.execute(lTX_SQL)
    set gConexao = nothing

    if err.number <> 0 then lR = lR & NovoErroXML(err.number, err.description,lTX_SQL)
End If

response.write lR

%>