<% Response.Expires= -1%>
<% Server.ScriptTimeout=60000 %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Controle remoto de projetos</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<!--#include file="../includes/basFormatador.inc"-->
	</HEAD>
	<body class="cabecalho" >
		<!--#include file="../includes/varControleAcesso.inc"-->
		<!--#include file="../includes/varBanco.inc"-->
		<% 
		Dim objConn
		Dim objRs
		Dim lngFileID
		Dim xNomeCampo 
		AbrirConexao
		'
		' Atualiza outros campos informados...
		Dim xTermEst(6)
		projId = Request.form("projId")
		usuaId = Request.form("usuaId")
		xQtdServicos = Request.form("QtdServicos")
		xQtdServicos = cInt(xQtdServicos)
		'response.write(xQtdServicos) & "<br>"
		for i = 1 to xQtdServicos
			cntrId = Request.form("cntrId" & cStr(i))
			xVlrOrig = Request.form("VlrOrig" & cStr(i))
			xVlrAtu = Request.form("VlrAtu" & cStr(i))
			xVlrFcn = Request.form("VlrFcne" & cStr(i))
			xVlrPlei = Request.form("VlrPlei" & cStr(i))
			if cntrId <> "" then
				xSQL = ""
				xSQL = xSQL & "exec CNTR_AtualizarCustos "
				xSQL = xSQL & "       @pcntrId = " & cntrId
				xSQL = xSQL & "     , @pVlrOrig = " & NumFmtBd(xVlrOrig)
				xSQL = xSQL & "     , @pVlrAtu  = " & NumFmtBd(xVlrAtu)
				xSQL = xSQL & "     , @pVlrFcn  = " & NumFmtBd(xVlrFcn)
				xSQL = xSQL & "     , @pVlrPlei = " & NumFmtBd(xVlrPlei)
				'response.write xSQL & "<br>"
				gConexao.execute xSQL
			else
				exit for
			end if
		next
		xComeCust = request.form("txtComeCusC")
		xComeCust = replace(xComeCust, "'", "''")
		xSQL = " update Projeto set projComeCusC = '" & xComeCust & "' where projId = " & projId
		'Response.write xSQL
		'response.End()
		gConexao.execute xSQL
		Session("TituloMsg") = "ALTERAR ESTIMATIVAS DOS CONTRATOS"
		Session("Msg") = "Estimativas dos contratos alteradas com sucesso!"
		Response.redirect ("gerMensagem.asp")
		%>
	</body>
</HTML>