<% Response.Expires= -1 %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ::</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<link rel="stylesheet" type="text/css" href="../EstiloMenu.css">
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<script language="JavaScript" type="text/javascript" src="../scripts/AbrirJanela.js"></script>
	</HEAD>
	<body class="cabecalho" >
<%  lPerfisAutorizados = "157"
	VerificarAcesso()

	projId  = session("projId")
	xlistId = request.querystring("listId")
	Dim gmenuGeral
	Dim gMenuSelecionado
	Dim gOpcaoSelecionada
	'gOpcaoSelecionada = "Servicos"
	gmenuGeral = "Projetos"
	gmenuGeral = "Projetos"
    gMenuSelecionado = "prjListas"

   	Dim gConexao 
	AbrirConexao
	
	xSQL = "select count(*) from revisao where listId = " & xlistId
	Set xRs = gConexao.Execute(xSQL)
	if not xRs.EOF then
		xQtdRevisoes = xrs(0)
	end if
	xrs.close
	Set xrs = nothing
	xSQL = ""
	xSQL = xSQL & "select tipoLista.tpliId, tpliPrefixo , projNum , tpliNome , tpliCodigo, reviNumero, requNum, listSequencial"
	xSQL = xSQL & "  from lista , tipolista, projeto , revisao left join requisicao on requisicao.reviId = revisao.reviId"
	xSQL = xSQL & " where lista.tpliId = tipolista.tpliId"
	xSQL = xSQL & "   and projeto.projId = lista.projId"
	xSQL = xSQL & "   and revisao.listId = lista.listId"
	xSQL = xSQL & "   and lista.listId = " & xlistId
	'response.write xSQL
	Set xRs = gConexao.Execute(xSQL)
	if not xRs.EOF then
		xLista =  xrs("tpliPrefixo") & "-" & xRs("projNum") & "-" & xRs("tpliCodigo") & "-" & xRs("listSequencial")
		xtpliPrefixo = xrs("tpliPrefixo") 
		xtpliNome = xrs("tpliNome")
		xtpliId = xrs("tpliId")
	end if
    gOpcaoSelecionada = xrs("tpliPrefixo") 

	xrs.close
   
   
 %>
		<!--#include file="../includes/cab.inc"-->
			<tr>
				<td style="padding:0px" valign="top" class="blocoOpcoes">
					<!--#include file="../includes/opcProjx2.inc"-->
				</td>
				<td colspan="2" valign="top" style="padding:5px" class="blocoPagina">
				<form>
					<table width="100%">
					<tr>
						<td class="docTit"><%=session("projNome")%><br><img src="../images/0066_double_arrow.png" class="botao" style="margin-right:3px"><%=xtpliNome %>: <%=xlista%><br><%=xrequisicao%> </td>
						<td align="right">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
					</tr>
					<tr>
						<td colspan="2" style="height:5"></td>
					</tr>
					<tr>
						<td colspan="2">
							<table width="100%" class="grid" >
							<tr>
<%	if xtpliId = "4" or xtpliId = "6" then %>
								<td rowspan="2" width="50px" class="GridCab" >Ordem</td>
								<td rowspan="2" width="100px" class="GridCab" >Cod JDE</td>
								<td rowspan="2"  class="GridCab" >Descri��o</td>
<%	elseif xtpliId = "5" then %>
								<td rowspan="2" width="120px" class="GridCab" >TAG</td>
								<td rowspan="2" width="100px" class="GridCab" >Cod JDE</td>
								<td rowspan="2"  class="GridCab" >Descri��o</td>
<%	end if	
	
	'===========================================================
	' Obter a quantidade de revis�es que ser�o apresentadas
	'===========================================================
	xSQL = "select count(*) from revisao where listId = " & xlistId
	Set xrs = gConexao.Execute(xSQL)
	xQtdRevisoes = cint(xrs(0))
	xrs.close 
	
	Dim xQtdRev()
	for i = 0 to (xQtdRevisoes-1)
		redim preserve xQtdRev(i+1)
		xQtdRev(i) = 0
	next 
	'Cabecalho
	response.write "<td class=""GridCab"" colspan=""" & xQtdRevisoes & """ style=""text-align:center"">REVIS�ES</td></tr><tr>"
	for i = 0 to xQtdRevisoes - 1
		response.write "<td class=""GridCab"" style=""text-align:right;"">" & i & "</td>"
	next
	response.write "</tr>"
	'
	'Corpo
	private sub escreveitem(ptpliId, pTag, pOrdem, pCodJde, pDescricao, byref pQtdsRev())
		response.write "<tr  onmouseover=""javascript:this.style.background='#F0F0F0';"" onmouseout=""javascript:this.style.background='transparent';"">"
		if xtpliId = "4" or xtpliId = "6"  then
			response.write "<td>" & pOrdem & "</td>"
		elseif xtpliId = "5" then
			response.write "<td>" & pTag & "</td>"
		end if
		response.write "<td>" & pCodJde & "</td>"
		response.write "<td>" & pDescricao & "</td>"
		for i = 0 to (ubound(pQtdsRev) - 1)
			if pQtdsRev(i) = "0" then
				lQtd = "-"
			else
				lQtd = FormatNumber(pQtdsRev(i),1,-1,0)
			end if
			response.write "<td style=""text-align:right;"">" & lQtd & "</td>"
			pQtdsRev(i) = 0
		next
		pQtdsRev(i) = 0
		response.write "</tr>"
	end sub
	'
	
	xSQL = ""
	xSQL = xSQl & "select IT.itemId, IE.linhId, linhNome, linhCod1, linhBitola, itemTag, IE.itedId, iterQtd, itemCodJde, itelOrdem, itedOrdem, IE.itemId, IT.itemId, IE.reviId, itemDescricao, reviNumero, itedQtd "
	xSQL = xSQl & "  from (((((itemLista IL join  ItemRevisao IR on IR.itelId = IL.itelId) "
	xSQL = xSQl & "  join revisao RV on IR.reviId = RV.reviId) "
	xSQL = xSQl & "  join item IT on IR.itemId = IT.itemId and IL.itemId = IT.itemId) "
	xSQL = xSQl & "  left join itemDetalhe IE on IE.itemId = IT.itemId and IE.reviId = RV.reviId) "
	xSQL = xSQl & "  left join linha LI on LI.linhId = IE.linhId ) "
	xSQL = xSQl & " where RV.listId = " & xlistId
	xSQL = xSQl & " order by linhNome, itedOrdem, itelOrdem, it.itemDescricao, reviNumero "
	'xSQL = xSQl & " order by linhNome, itedOrdem, itelOrdem, IE.itemId, reviNumero "
	'response.write xSQL 
	Set xRs = gConexao.Execute(xSQL)
	xItem       = xRs("itemId")
	xLinhaAnt   = ""
	xReviNumero = 0
	while not xrs.eof
		'
		if (xRs("itemId") <> xItem) then
			'
			if(xReviNumero<(xQtdRevisoes-1))then 
				'Este cara n�o estava presente nas outras revis�es, portanto, completamos seu prosseguimento com o valor da �ltima apari��o do mesmo...
				for i = (xReviNumero+1) to (xQtdRevisoes-1)
					xQtdRev(i) = xQtdRev(xReviNumero)
				next
			end if
			'
			'Escrevendo o item...
			call escreveitem(xtpliId, xTag, xOrdem, xCodJde, xDescricao, xQtdRev)
			'
			'Prosseguindo para o pr�ximo item...
			xItem = xRs("itemId")
			'
		end if
		'
		'Escrevendo quebra (caso ocorra)...
		if xRS("linhNome") <> "" then
			if xLinhaAnt <> xRs("linhNome") then
				if xRs("linhId") <> "0" then
					Response.write "<tr>" & vbcrlf
					Response.write vbtab & "<td colspan=""" & 3 + xQtdRevisoes & """ style=""padding-top:10px;font-weight:bold"">" & xRs("linhNome") & "</td>"
					Response.write "</tr>" & vbcrlf
				end if
				xLinhaAnt = xRs("linhNome")
			end if
		end if
		'
		'Computando item...
		xTag        = xRs("itemTag")
		xReviNumero = xrs("reviNumero")
		xQtdRev(xReviNumero) = 0
		if xtpliId = "4" then
			xOrdem = xRs("itedOrdem")
			xQtdRev(xReviNumero) = xrs("itedQtd")
		else
			xOrdem = xRs("itelOrdem")
			xQtdRev(xReviNumero) = xrs("iterQtd") 
		end if
		xCodJde = xRs("itemCodJde")
		xDescricao = xRs("itemDescricao")
		'
		xrs.MoveNext
	wend
	'
	'Escrevendo o item que sobra no final...
	call escreveitem(xtpliId, xTag, xOrdem, xCodJde, xDescricao, xQtdRev)
	'
	%>
								<tr>
									<td class="GridRodape">&nbsp;</td>
									<td class="GridRodape">&nbsp;</td>
									<td class="GridRodape">&nbsp;</td>
<%	for i = 0 to xQtdRevisoes - 1%>
		<td class="GridRodape">&nbsp;</td>
<%	next %>
								</tr>
							</table>
						</td>
					</tr>
					</table>
				</form>
				</td>
			</tr>
		<!--#include file="../includes/rod.inc"-->
	</body>
</HTML>
