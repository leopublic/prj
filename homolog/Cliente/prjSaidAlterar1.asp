<% 
	Server.ScriptTimeout=60000 
    pStr = "private, no-cache, must-revalidate" 
    Response.ExpiresAbsolute = #2000-01-01# 
    Response.AddHeader "pragma", "no-cache" 
    Response.AddHeader "cache-control", pStr 

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Controle remoto de projetos</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
	</HEAD>
	<body class="cabecalho" >
		<!--#include file="../includes/varControleAcesso.inc"-->
		<!--#include file="../includes/varBanco.inc"-->
		<% 
		Dim objUpload
		Dim strFileName
		Dim objConn
		Dim objRs
		Dim lngFileID
		AbrirConexao
		' Instantiate Upload Class
		Set objUpload = Server.CreateObject("SoftArtisans.FileUp")
		objUpload.ProgressID = CInt(Request.QueryString("progressid"))
		servidor = Request.ServerVariables("HTTP_HOST")
		if servidor = "www.prj.com.br" then
			xPath = "e:\home\prj\web\Anexos"
		elseif servidor = "www.m2software.com.br" then
			xPath = "e:\home\m2software\web\whitemartins\prj\Anexos"
		else
			xPath = Server.MapPath("..\listas")
		end if
		xPath = Server.MapPath("..\listas")
		objUpload.Path = xpath
			
		' Grab the file name
		projId = objUpload.form("projId")
		usuaId = objUpload.form("usuaId")
		'reviNumero = objUpload.form("txtRevisao")
		reviNumero = '0'
		reviNumRequisicao = objUpload.form("txtRequisicao")
		'listSequencial = objUpload.form("txtSequencial")
		listSequencial = '0'
		tpliId = objUpload.form("cmbTipoLista")
		strFileName = objUpload.form("txtArquivo").ShortFileName
		if strFileName = "" then
			Session("MsgErro") = "Arquivo n�o informado."
			Session("txtDesc") = anexDesc
			Response.Redirect ("prjListAlterar0.asp?&projId=" & projId)
		end if
		'
		' Cria o anexo	
		xSQL = ""
		xSQL = xSQL & "exec LIST_Criar "
		xSQL = xSQL & "  @pprojId            =  " & projId
		xSQL = xSQL & ", @pusuaIdUpl         =  " & usuaId
		xSQL = xSQL & ", @pimpoNomeArquivo   = '" & strFileName & "'"
		xSQL = xSQL & ", @ptpliId            =  " & tpliId
		xSQL = xSQL & ", @previNumero        =  " & reviNumero
		xSQL = xSQL & ", @previNumRequisicao =  " & reviNumRequisicao
		xSQL = xSQL & ", @plistSequencial    =  " & listSequencial 
		'Response.write(xSQL)
		'Response.End()
		Set xRs = gConexao.execute(xSQL)
		xreviId = xRs("reviId")
		ximpoId = xRs("impoId")
		reviId = Right("00000000" & cStr(xreviId), 8) & ".txt"
		objUpload.form("txtArquivo").SaveAs reviId

		Session("TituloMsg") = "Lista carregada com sucesso! em " & xpath
		Session("Msg") = "Lista carregada com sucesso! em " & xpath
		Response.redirect("prjListImportar.asp?reviId=" & xreviId & "&impoId=" & ximpoId)
		%>
	</body>
</HTML>
