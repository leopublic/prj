<% Response.Expires= -1%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Controle de projetos pela internet</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<link rel="stylesheet" type="text/css" href="../EstiloMenu.css">
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<!--#include file="../includes/basFormatador.inc"-->
		<!--#include file="../includes/clsContrato.inc"-->
		<!--#include file="../includes/clsProjeto.inc"-->
		<style>
			td.GridCab				{font-family:tahoma;font-size:8pt;font-weight:bold;border-top:2px solid #999999;border-bottom:2px solid #999999;border-left:1px solid #CCCCCC;background-color:#dde8ee;padding-left:5px}
			td.GridCabLinha			{font-family:tahoma;font-size:8pt;font-weight:bold;border-bottom:1px solid #CCCCCC;border-left:1px solid #CCCCCC;background-color:#dde8ee;padding-left:5px}
			td.GridLinha 			{font-family:tahoma;font-size:8pt;font-weight:normal;padding:2px;border:1px solid #CCCCCC}
        </style>
		<script language="JavaScript" type="text/javascript" src="../scripts/AbrirJanela.js"></script>
		<script language="JavaScript" type="text/javascript">
			 function AbreFecha(nomeGrupo)
			 {
			  	 var grupo = document.getElementById("grp"+nomeGrupo);
				 var imagem = document.getElementById("img"+nomeGrupo);
				 var sit = grupo.style.display
				 if(sit=="block"){
				 	grupo.style.display = "none"
					imagem.src = "../images/icoMenuMais.gif"
				 }
				 else{
				 	grupo.style.display = "block"
					imagem.src = "../images/icoMenuMenos.gif"
				 }
 
			 }
		</script>
	</HEAD>
	<body class="cabecalho" >
		<!--#include file="../includes/varControleAcesso.inc"-->
		<!--#include file="../includes/varBanco.inc"-->
		<!--#include file="../includes/varDatas.inc"-->

<%  lPerfisAutorizados = "15"
	VerificarAcesso()
	filtro = request.QueryString("filtro")
	if filtro = "1" then
		xProjetos = ""
	else
		xProjetos = request.form("Projetos")
		if xProjetos <> "" then
			xProjs = split(xProjetos, ",")
			xQtdProjs = ubound(xProjs)
			xProjetos = ""
			for i = 0 to xQtdProjs
				xValor = request.form(xProjs(i))
				if xValor = "on" then 
					if xProjetos = "" then
						xProjetos = xProjs(i)
					else
						xProjetos = xProjetos & "," & xProjs(i)
					end if
				end if
			next 				
		end if
	end if
	' Filtros
	usuaIdGer = request.form("cmbGerente")
	usuaIdSup = request.form("cmbSupervisor")
	emprIdSupCmp = request.form("cmbSupCmp")
	tpprId = request.form("cmbTipoProjeto")
	if usuaIdGer = "" then
		usuaIdGer = "-1"
	end if
	if usuaIdSup = "" then
		usuaIdSup = "-1"
	end if
	if emprIdSupCmp = "" then
		emprIdSupCmp = "-1"
	end if
	if tpprId = "" then
		tpprId = "-1"
	end if
	
   gMenuSelecionado = "GerResumo"
   gOpcaoSelecionada = "Resumo"
   gmenuGeral = "Gerenciamento"
   gmenuGeral = "Geral"
   PastaSelecionada = gmenuGeral
   
	AbrirConexao
	xSQL = "select isnull(count(*), 0) from Projeto "
	if xProjetos <> "" then
		xSQL = xSQL & " where projId in (" & xProjetos & ")"
	end if	
	set xRs = gConexao.execute(xSQL)
	xQtdProjSel = xRs(0)
	xRs.close
	xSQL = ""
    xSQL = xSQL & "select RelatorioProjeto.projId"
	xSQL = xSQL & "     , projNum"
	xSQL = xSQL & "     , projNome"
	xSQL = xSQL & "     , isnull(Ger.usuaNome, '-')          gerNome"
	xSQL = xSQL & "     , isnull(Sup.usuaNome, '-')          supNome"
	xSQL = xSQL & "     , isnull(Empreiteira.emprNome, '-')  emprNome"
	xSQL = xSQL & "     , isnull(relpCustGerEst, 0)          relpCustGerEst"
	xSQL = xSQL & "     , isnull(relpCustSupcEst, 0)         relpCustSupcEst"
	xSQL = xSQL & "     , isnull(relpCustGerEcn, 0)          relpCustGerEcn"
	xSQL = xSQL & "     , isnull(relpCustSupcEcn, 0)         relpCustSupcEcn"
	xSQL = xSQL & "     , isnull(relpCustGerEst, 0) + isnull(relpCustGerEcn, 0) GerEstTot"
	xSQL = xSQL & "     , isnull(relpCustSupcEst, 0) + isnull(relpCustSupcEcn, 0) SupcEstTot"
	xSQL = xSQL & "     , isnull(relpCustGerEst, 0) + isnull(relpCustSupcEst, 0) + isnull(relpCustGerEcn, 0) + isnull(relpCustSupcEcn, 0) totalEst"
	xSQL = xSQL & "     , isnull(relpCustGerReal, 0)         relpCustGerReal"
	xSQL = xSQL & "     , isnull(relpCustSupcReal, 0)        relpCustSupcReal"
	xSQL = xSQL & "     , isnull(relpCustEtc, 0)             relpCustEtc"
	xSQL = xSQL & "     , isnull(relpCustGerReal, 0) + isnull(relpCustSupcReal, 0) + isnull(relpCustEtc, 0) totalReal"
	xSQL = xSQL & "     , relpIdAtivo"
	xSQL = xSQL & "  from (((RelatorioProjeto "
	xSQL = xSQL & "       left join Usuario Ger on Ger.usuaId         = RelatorioProjeto.usuaIdGer)"
	xSQL = xSQL & "       left join Usuario Sup on Sup.usuaId         = RelatorioProjeto.usuaIdSup)"
	xSQL = xSQL & "       left join Empreiteira on Empreiteira.emprId = RelatorioProjeto.emprIdSupCmp)"
	xSQL = xSQL & "      , Projeto"
	xSQL = xSQL & "  where Projeto.projId = RelatorioProjeto.projId"
	xSQL = xSQL & "    and Projeto.relpIdAtivo = RelatorioProjeto.relpId"
	xSQL = xSQL & "    and Projeto.projAtivo = 0"
	xWhere = ""
	xAnd = " and "
	if xProjetos <> "" then
		xWhere = xWhere & xAnd & " RelatorioProjeto.projId in (" & xProjetos & ")"
	end if
	if usuaIdGer <> "-1" then
		xWhere = xWhere & xAnd & " RelatorioProjeto.usuaIdGer = " & usuaIdGer
	end if
	if usuaIdSup <> "-1" then
		xWhere = xWhere & xAnd & " RelatorioProjeto.usuaIdSup = " & usuaIdSup
	end if
	if emprIdSupCmp <> "-1" then
		xWhere = xWhere & xAnd & " RelatorioProjeto.emprIdSupCmp = " & emprIdSupCmp
	end if
	if tpprId <> "-1" then
		xWhere = xWhere & xAnd & " RelatorioProjeto.tpprId = " & tpprId
	end if
	xSQL = xSQL & xWhere	
	set xRs = gConexao.execute(xSQL)
	Dim xLinha(40)
	Dim xLinhaCusto(40)
	Dim xLinhaPrazo(100)
	xLinha(0)  = "<td colspan=""3"" class=""gridCab"" width=""40px"" style=""border-bottom:0px none;""><img id=""imgCadastro"" src=""../images/icoMenuMenos.gif"" onclick=""javascript:AbreFecha('Cadastro');"">&nbsp;Projeto</td>"
	xLinha(14) = "<td colspan=""3"" class=""gridCab"" align=""center"" style=""border-top:0px none;""><input type=""submit"" value=""Selecionar"" align=""center""></td>"

	xLinha(1)  = "<td colspan=""2"" >&nbsp;</td><td class=""gridCabLinha"">Gerente</td>"
	xLinha(2)  = "<td colspan=""2"" >&nbsp;</td><td class=""gridCabLinha"">Supervisor</td>"
	xLinha(13) = "<td colspan=""2"">&nbsp;</td><td class=""gridCabLinha"">Supervisor de campo</td>"

	xLinha(6)  = "<td colspan=""3"" class=""gridCab""><img id=""imgCustConstr"" src=""../images/icoMenuMenos.gif"" onclick=""javascript:AbreFecha('CustConstr');"">&nbsp;Custos de constru��es</td>"

	xLinha(3)  = "<td colspan=""2"" >&nbsp;</td><td class=""gridCabLinha"" height=""33"">Hh gerenc.</td>"
	xLinha(4)  = "<td colspan=""2"" >&nbsp;</td><td class=""gridCabLinha"" height=""33"">Hh sup. campo</td>"

	xLinha(7)  = "<td>&nbsp;</td><td class=""gridCabLinha"" height=""33"">Ger. real</td>"
	xLinha(8)  = "<td>&nbsp;</td><td class=""gridCabLinha"" height=""33"">Sup campo real</td>"

	xLinha(11) = "<td colspan=""2"" class=""gridCab"" height=""33"">Varia��o</td>"
	xLinha(12) = "<td>&nbsp;</td><td class=""gridRodape"">&nbsp;</td>"
	i = 0
	Dim xrelpIdAtivo(40)
	Dim xprojId(40)
	Dim xVlrEstTot(40)
	Dim xVlrRealTot(40)
	xrelpIds = ""
	xQtdProjSel = 0
	while not xRs.eof
		if xProjetos = "" then
			xProjetos = xRs("projId")
		else
			xProjetos = xProjetos & "," & xRs("projId")
		end if
		estiloVari =""
		if xRs("totalEst") = 0 then
			Variacao = "(n/a)"
		else
			if xRs("totalReal") = 0 then
				Variacao = "-"
			else
				Variacao = ((xRs("totalEst")- xRs("totalReal")) / xRs("totalEst")) * 100
				if Variacao < 0 then
					estiloVari = ";color:red"
				end if
				Variacao = FormatNumber(Variacao,2,-1,-1) & "%"		
			end if
		end if

		if xRs("GerEstTot") = 0 then
			VarGer = "(n/a)"
		else
			if xRs("relpCustGerReal") = 0 then
				VarGer = "-"
			else
				VarGer = ((xRs("GerEstTot")- xRs("relpCustGerReal")) / xRs("GerEstTot")) * 100
				if VarGer < 0 then
					estiloVarGer = ";color:red"
				end if
				VarGer = FormatNumber(VarGer,2,-1,-1) & "%"		
			end if
		end if
		if xRs("SupcEstTot") = 0 then
			VarSupc = "(n/a)"
		else
			if xRs("relpCustSupcReal") = 0 then
				VarSupc = "-"
			else
				VarSupc = ((xRs("SupcEstTot")- xRs("relpCustSupcReal")) / xRs("SupcEstTot")) * 100
				if VarSupc < 0 then
					estiloVarSupc = ";color:red"
				end if
				VarSupc = FormatNumber(VarSupc,2,-1,-1) & "%"		
			end if
		end if
		xLinha(0) = xLinha(0)   & "<td colspan=""3"" class=""GridCab"" style=""width:120px;vertical-align:top;font-weight:normal;border-bottom:0px none;""><b>" & xRs("projNum") & "</b><br>" & xRs("projNome") & "</td>"
		xLinha(14) = xLinha(14) & "<td colspan=""3"" class=""GridCab"" style=""text-align:center;border-top:0px none;""><input type=""checkBox"" name=""" & xRs("projId") & """ checked></input></td>"
		xLinha(1) = xLinha(1)   & "<td colspan=""3"" class=""GridLinha"">" & xRs("gerNome") & "</td>"
		xLinha(2) = xLinha(2)   & "<td colspan=""3"" class=""GridLinha"">" & xRs("supNome") & "</td>"
		xLinha(13) = xLinha(13) & "<td colspan=""3"" class=""GridLinha"">" & xRs("emprNome") & "</td>"
		
		xLinha(3) = xLinha(3) & "<td class=""GridLinha"" style=""text-align:right"">" & FormatNumber(xRs("GerEstTot")) & "</td>" 
		xLinha(3) = xLinha(3) & "<td class=""GridLinha"" style=""text-align:right"">" & FormatNumber(xRs("relpCustGerReal")) & "</td>" 
		xLinha(3) = xLinha(3) & "<td class=""GridLinha"" style=""text-align:right"">" & VarGer & "</td>"
		
		xLinha(4) = xLinha(4) & "<td class=""GridLinha"" style=""text-align:right"">" & FormatNumber(xRs("SupcEstTot")) & "</td>" 
		xLinha(4) = xLinha(4) & "<td class=""GridLinha"" style=""text-align:right"">" & FormatNumber(xRs("relpCustSupcReal")) & "</td>" 
		xLinha(4) = xLinha(4) & "<td class=""GridLinha"" style=""text-align:right"">" & VarSupc & "</td>"
		
		xLinha(6) = xLinha(6) & "<td class=""GridCab"" style=""text-align:right"">" & FormatNumber(xRs("totalEst")) & "</td>" 
		xLinha(6) = xLinha(6) & "<td class=""GridCab"" style=""text-align:right"">" & FormatNumber(xRs("totalReal")) & "</td>" 
		xLinha(6) = xLinha(6) & "<td class=""GridCab"" style=""text-align:right" & estiloVari & """>" & Variacao & "</td>"

		xLinha(12) = xLinha(12) & "<td colspan=""3"" class=""gridRodape"">&nbsp;</td>"
		i = i + 1
		xrelpIdAtivo(i) = xRs("relpIdAtivo")
		xvirgula = ","
		if xrelpIds = "" then
			xVirgula = ""
		end if
		xrelpIds = xrelpIds & xvirgula & xRs("relpIdAtivo")
		xprojId(i) = xRs("projId")
		xQtdProjSel = xQtdProjSel + 1
		xRs.MoveNext
	Wend
	xRs.close
	'*================================================
	' Loop de carga dos contratos
	xSQL = "select count(distinct servId)"
	xSQL = xSQL & " from Servico "
	xSQL = xSQL & " where Servico.servId in (select servId from RelatorioContrato where relpId in (" & xrelpIds & "))"
	set xRs = gConexao.execute(xSQL)
	xQtdServ = xRs(0)
	xRs.close
'	on error resume next
	'response.write(xQtdProjSel) & "<br>"
	'response.write(xQtdServ) & "<br>"
	xLinhaCusto(1) = "<td colspan=""3"" class=""gridCab""><img id=""imgCntrCust"" src=""../images/icoMenuMenos.gif"" onclick=""javascript:AbreFecha('CntrCust');"">&nbsp;Custo dos contratos</td>"
	xLinhaPrazo(1) = "<td colspan=""3"" class=""gridCab""><img id=""imgCntrPraz"" src=""../images/icoMenuMenos.gif"" onclick=""javascript:AbreFecha('CntrPraz');"">&nbsp;Prazo dos contratos</td>"
	dim xServId(40)	
	for xIndProj = 1 to xQtdProjSel 
		xSQL = ""
		xSQL = xSQL & "select Servico.servId"
		xSQL = xSQL & "     , Servico.servNome"
		xSQL = xSQL & "     , RelatorioContrato.relcId"
		xSQL = xSQL & "     , isnull(relcVlrEst, 0 ) relcVlrEst "
		xSQL = xSQL & "     , isnull(convert(varchar(10), relcTermEst1, 5), '-') relcTermEst1"
		xSQL = xSQL & "     , isnull(convert(varchar(10), relcTermEst2, 5), '-') relcTermEst2"
		xSQL = xSQL & "     , isnull(convert(varchar(10), relcTermEst3, 5), '-') relcTermEst3"
		xSQL = xSQL & "     , isnull(convert(varchar(10), relcTermEst4, 5), '-') relcTermEst4"
		xSQL = xSQL & "     , isnull(convert(varchar(10), relcTermReal1, 5), '-') relcTermReal1"
		xSQL = xSQL & "     , isnull(convert(varchar(10), relcTermReal2, 5), '-') relcTermReal2"
		xSQL = xSQL & "     , isnull(convert(varchar(10), relcTermReal3, 5), '-') relcTermReal3"
		xSQL = xSQL & "     , isnull(convert(varchar(10), relcTermReal4, 5), '-') relcTermReal4"
		xSQL = xSQL & "     , isnull(DATEDIFF ( day , relcTermEst1, relcTermReal1) , 0) Dur1"
		xSQL = xSQL & "     , isnull(DATEDIFF ( day , relcTermEst2, relcTermReal2) , 0) Dur2"
		xSQL = xSQL & "     , isnull(DATEDIFF ( day , relcTermEst3, relcTermReal3) , 0) Dur3"
		xSQL = xSQL & "     , isnull(DATEDIFF ( day , relcTermEst4, relcTermReal4) , 0) Dur4"
		xSQL = xSQL & "     , isnull(DATEDIFF ( day , relcTermEst1, relcTermEst4) , 0) DurEst"
		xSQL = xSQL & "     , isnull(DATEDIFF ( day , relcTermReal1, relcTermReal4) , 0) DurReal"
		xSQL = xSQL & "     , isnull(relcVlrAtu + relcVlrFcn + relcVlrPlei, 0) relcVlrTot "
		xSQL = xSQL & " from (Servico left join RelatorioContrato on RelatorioContrato.servId = Servico.servId "
		xSQL = xSQL & "                                          and RelatorioContrato.relpId = " & xrelpIdAtivo(xIndProj) & ")"
		xSQL = xSQL & " where Servico.servId in (select servId from RelatorioContrato where relpId in (" & xrelpIds & "))"
		xSQL = xSQL & " order by servOrd"
		set xRs = gConexao.execute(xSQL)
		xVlrEstTot(i) = 0
		xVlrRealTot(i) = 0
		xIndServCust = 1
		xIndServPrazo = 1
		xLinhaCusto(1) = xLinhaCusto(1) & "<td class=""gridLinhaTot"" >Estimado</td><td class=""gridLinhaTot"" >Real</td><td class=""gridLinhaTot"" >Var.</td>"
		xLinhaPrazo(1) = xLinhaPrazo(1) & "<td class=""gridLinhaTot"" >Estimado</td><td class=""gridLinhaTot"" >Real</td><td class=""gridLinhaTot"" >Var.</td>"
		while not xRs.eof
			xIndServCust = xIndServCust + 1
			xIndServPrazo = xIndServPrazo + 1
			'
			' Carrega custo
			if xIndProj = 1 then
				xServId(xIndServCust) = xRs("ServId") 
				xLinhaCusto(xIndServCust) = "<td>&nbsp;</td><td colspan=""2"" class=""gridCabLinha"">" & xRs("servNome") & "</td>"
			end if
			if isnull(xRs("relcId")) then
				valor = "-"
			else
				valor = FormatNumber(xRs("relcVlrTot"))
				xVlrEstTot(xIndProj) = xVlrEstTot(xIndProj) + xRs("relcVlrEst")
				xVlrRealTot(xIndProj) = xVlrRealTot(xIndProj) + xRs("relcVlrTot")
			end if
			xLinhaCusto(xIndServCust) = xLinhaCusto(xIndServCust) & "<td class=""gridLinha"" style=""text-align:right"">" & FormatNumber(xRs("relcVlrEst")) & "</td>"
			xLinhaCusto(xIndServCust) = xLinhaCusto(xIndServCust) & "<td class=""gridLinha"" style=""text-align:right"">" & FormatNumber(xRs("relcVlrTot")) & "</td>"
			xLinhaCusto(xIndServCust) = xLinhaCusto(xIndServCust) & "<td class=""gridLinha"" style=""text-align:right"">" & FormatNumber(xRs("relcVlrEst")) & "</td>"
			'
			' Carrega prazo
			if xIndProj = 1 then
				xLinhaPrazo(xIndServPrazo) = "<td>&nbsp;</td><td colspan=""2"" class=""gridCabLinha""><img id=""imgServ" & xRs("servId") & """ src=""../images/icoMenuMenos.gif"" onclick=""javascript:AbreFecha('Serv" & xRs("servId") & "');"">&nbsp;" & xRs("servNome") & "</td>"
				xLinhaPrazo(xIndServPrazo+ 1) = "<td>&nbsp;</td><td>&nbsp;</td><td class=""gridCabLinha"">Emiss�o MD</td>"
				xLinhaPrazo(xIndServPrazo+ 2) = "<td>&nbsp;</td><td>&nbsp;</td><td class=""gridCabLinha"">Contrata��o</td>"
				xLinhaPrazo(xIndServPrazo+ 3) = "<td>&nbsp;</td><td>&nbsp;</td><td class=""gridCabLinha"">Mobiliza��o</td>"
				xLinhaPrazo(xIndServPrazo+ 4) = "<td>&nbsp;</td><td>&nbsp;</td><td class=""gridCabLinha"">Desmobiliza��o</td>"
			end if
			xLinhaPrazo(xIndServPrazo) = xLinhaPrazo(xIndServPrazo) & "<td class=""gridLinha"" style=""text-align:right"">" & xRs("DurEst") & "</td>"
			xLinhaPrazo(xIndServPrazo) = xLinhaPrazo(xIndServPrazo) & "<td class=""gridLinha"" style=""text-align:right"">" & xRs("DurReal") & "</td>"
			xLinhaPrazo(xIndServPrazo) = xLinhaPrazo(xIndServPrazo) & "<td class=""gridLinha"" style=""text-align:right"">" & xRs("DurReal") - xRs("DurEst") & "</td>"

			xIndServPrazo = xIndServPrazo + 1
			xLinhaPrazo(xIndServPrazo) = xLinhaPrazo(xIndServPrazo) & "<td class=""gridLinha"" style=""text-align:right"">" & xRs("relcTermEst1") & "</td>"
			xLinhaPrazo(xIndServPrazo) = xLinhaPrazo(xIndServPrazo) & "<td class=""gridLinha"" style=""text-align:right"">" & xRs("relcTermReal1") & "</td>"
			xLinhaPrazo(xIndServPrazo) = xLinhaPrazo(xIndServPrazo) & "<td class=""gridLinha"" style=""text-align:right"">" & xRs("Dur1") & "</td>"
			xIndServPrazo = xIndServPrazo + 1
			xLinhaPrazo(xIndServPrazo) = xLinhaPrazo(xIndServPrazo) & "<td class=""gridLinha"" style=""text-align:right"">" & xRs("relcTermEst2") & "</td>"
			xLinhaPrazo(xIndServPrazo) = xLinhaPrazo(xIndServPrazo) & "<td class=""gridLinha"" style=""text-align:right"">" & xRs("relcTermReal2") & "</td>"
			xLinhaPrazo(xIndServPrazo) = xLinhaPrazo(xIndServPrazo) & "<td class=""gridLinha"" style=""text-align:right"">" & xRs("Dur2") & "</td>"
			xIndServPrazo = xIndServPrazo + 1
			xLinhaPrazo(xIndServPrazo) = xLinhaPrazo(xIndServPrazo) & "<td class=""gridLinha"" style=""text-align:right"">" & xRs("relcTermEst3") & "</td>"
			xLinhaPrazo(xIndServPrazo) = xLinhaPrazo(xIndServPrazo) & "<td class=""gridLinha"" style=""text-align:right"">" & xRs("relcTermReal3") & "</td>"
			xLinhaPrazo(xIndServPrazo) = xLinhaPrazo(xIndServPrazo) & "<td class=""gridLinha"" style=""text-align:right"">" & xRs("Dur3") & "</td>"
			xIndServPrazo = xIndServPrazo + 1
			xLinhaPrazo(xIndServPrazo) = xLinhaPrazo(xIndServPrazo) & "<td class=""gridLinha"" style=""text-align:right"">" & xRs("relcTermEst4") & "</td>"
			xLinhaPrazo(xIndServPrazo) = xLinhaPrazo(xIndServPrazo) & "<td class=""gridLinha"" style=""text-align:right"">" & xRs("relcTermReal4") & "</td>"
			xLinhaPrazo(xIndServPrazo) = xLinhaPrazo(xIndServPrazo) & "<td class=""gridLinha"" style=""text-align:right"">" & xRs("Dur4") & "</td>"
			xRs.MoveNext
		wend
		xRs.close
	'	xLinhaCusto(1) = xLinhaCusto(1) & "<td class=""gridCab"" style=""text-align:right"">" & FormatNumber(xVlrEstTot(i)) & "</td>"
'		xLinhaCusto(1) = xLinhaCusto(1) & "<td class=""gridCab"" style=""text-align:right"">" & FormatNumber(xVlrRealTot(i)) & "</td>"
'		xLinhaCusto(1) = xLinhaCusto(1) & "<td class=""gridCab"" style=""text-align:right"">" & FormatNumber(xVlrEstTot(i)) & "</td>"
	next
'	response.end()
	
	%>
<table width="100%" height="100%" background="../images/fundoHomolog.gif" style="table-layout:fixed;" >
  <!--#include file="../includes/cab.inc"-->
  <tr> 
    <td style="padding:0px" valign="top" class="blocoOpcoes"> <!--#include file="../includes/opcGeral.inc"--> </td>
    <td colspan="2" valign="top" style="padding:5px" class="blocoPagina"> <p style="color:red;text-align:left;padding:0px;margin:1px;border:0px;padding-left:4pt;font-family:tahoma;font-size:10pt;font-weight:bold;letter-spacing:2px;">Compara��o 
        de projetos - Custo de contratos</p>
      <div width="100%"   style="background:url(../images/pont_cinza_h.gif);background-repeat:repeat-x;border:0px;padding:0px;margin:0px;height:2px;"></div>
      <form name="form1" id="form1" action="gerHistCntr.asp" method="post">
        <p style="color:red;text-align:left;padding:0px;margin:1px;border:0px;padding-left:4pt;font-family:tahoma;font-size:10pt;font-weight:bold;letter-spacing:2px;">Filtros</p>
        <table class="doc">
          <tr> 
            <td width="20%" align="right" class="docLabel">Gerente</td>
            <td width="5%" class="docLabel">:</td>
            <td width="25%" class="docCmpLivre"> <%
						xSQL = ""
						xSQL = xSQL & "select usuaId "
						xSQL = xSQL & "     , usuaNome "
						xSQL = xSQL & "  from Usuario, PerfilAcesso "
						xSQL = xSQL & " where PerfilAcesso.perfId = Usuario.perfId"
						xSQL = xSQL & "   and PerfilAcesso.perfId = 2"
						xSQL = xSQL & " order by usuaNome"
						set xRs = gConexao.execute(xsQL)
						xEmpr = "<select name=""cmbGerente"" class=""docCmpLivre"">" & vbcrlf
						xSelected = ""
						if usuaIdGer = "-1" then
							xSelected = "selected"
						end if
						xEmpr = xEmpr & "<option value=""-1"" " & xSelected & " class=""docCmpLivre"">Todos</option>" & vbcrlf
						xEmpr = xEmpr & "<option value=""0"" class=""docCmpLivre"">(n�o definido)</option>" & vbcrlf
						while not xRs.eof
							xEmprId = cstr(xRs("usuaId"))
							if xEmprId = cStr(usuaIdGer) then
								xChecked = " selected "
							else
								xChecked = ""
							end if
							xEmpr = xEmpr & "<option value=""" & xRs("usuaId") & """ " & xChecked & " class=""docCmpLivre"">" & xRs("usuaNome") & "</option>" & vbcrlf
							xRs.Movenext
						wend
						xRs.Close
						Set xRs = nothing
						xEmpr = xEmpr & "</select>" & vbcrlf
						Response.write xEmpr
					%> </td>
            <td width="20%" align="right" class="docLabel">Supervisor</td>
            <td width="5%" class="docLabel">:</td>
            <td width="25%" class="docCmpLivre"> <%
						xSQL = ""
						xSQL = xSQL & "select usuaId "
						xSQL = xSQL & "     , usuaNome "
						xSQL = xSQL & "  from Usuario, PerfilAcesso"
						xSQL = xSQL & " where PerfilAcesso.perfId = Usuario.perfId"
						xSQL = xSQL & "   and PerfilAcesso.perfId in (1,5)"
						xSQL = xSQL & " order by usuaNome"
						set xRs = gConexao.execute(xsQL)
						xEmpr = "<select name=""cmbSupervisor"" class=""docCmpLivre"">" & vbcrlf
						xSelected = ""
						if usuaIdSup = "-1" then
							xSelected = "selected"
						end if
						xEmpr = xEmpr & "<option value=""-1"" " & xSelected & " class=""docCmpLivre"">Todos</option>" & vbcrlf
						xEmpr = xEmpr & "<option value=""0"" class=""docCmpLivre"">(n�o definido)</option>" & vbcrlf
						while not xRs.eof
							xEmprId = cstr(xRs("usuaId"))
							if xEmprId = cStr(usuaIdSup) then
								xChecked = " selected "
							else
								xChecked = ""
							end if
							xEmpr = xEmpr & "<option value=""" & xRs("usuaId") & """ " & xChecked & " class=""docCmpLivre"">" & xRs("usuaNome") & "</option>" & vbcrlf
							xRs.Movenext
						wend
						xRs.Close
						Set xRs = nothing
						xEmpr = xEmpr & "</select>" & vbcrlf
						Response.write xEmpr
					%> </td>
          </tr>
          <tr> 
            <td align="right" class="docLabel">Supervisor de campo</td>
            <td class="docLabel">:</td>
            <td class="docCmpLivre"> <%
						xSQL = ""
						xSQL = xSQL & "select emprId "
						xSQL = xSQL & "     , emprNome "
						xSQL = xSQL & "  from Empreiteira "
						xSQL = xSQL & " where emprEdit = 1"
						xSQL = xSQL & " order by emprNome"
						set xRs = gConexao.execute(xsQL)
						xEmpr = "<select name=""cmbSupCmp"" class=""docCmpLivre"">" & vbcrlf
						xSelected = ""
						if emprIdSupCmp = "-1" then
							xSelected = "selected"
						end if
						xEmpr = xEmpr & "<option value=""-1"" " & xSelected & " class=""docCmpLivre"">Todos</option>" & vbcrlf
						xEmpr = xEmpr & "<option value=""0"" class=""docCmpLivre"">(n�o definido)</option>" & vbcrlf
						while not xRs.eof
							xEmprId = cstr(xRs("emprId"))
							if xEmprId = cStr(emprIdSupCmp) then
								xChecked = " selected "
							else
								xChecked = ""
							end if
							xEmpr = xEmpr & "<option value=""" & xRs("emprId") & """ " & xChecked & " class=""docCmpLivre"">" & xRs("emprNome") & "</option>" & vbcrlf
							xRs.Movenext
						wend
						xRs.Close
						Set xRs = nothing
						xEmpr = xEmpr & "</select>" & vbcrlf
						Response.write xEmpr
					%> </td>
            <td align="right" class="docLabel">Tipo de projeto</td>
            <td class="docLabel">:</td>
            <td class="docCmpLivre"> <%
						response.write "<select name=""cmbTipoProjeto"" class=""docCmpLivre"">" & vbcrlf
						xSQL = "select tpprId, tpprNome"
						xSQL = xSQL & " from TipoProjeto"
						xSQL = xSQL & "  order by tpprNome"
						Set xRs = gConexao.Execute(xSQL)
						xSelected = ""
						if tpprId = "-1" then
							xSelected = "selected"
						end if
						response.write  "<option value=""-1"" " & xSelected & " class=""docCmpLivre"">Todos</option>" & vbcrlf
						while not xRs.EOF
							if tpprId <> "" then
								if cStr(xRs("tpprId")) = cStr(tpprId) then
									xSelected = " selected "
								else
									xSelected = ""
								end if
							end if
							response.write vbtab & "<option value=""" & xRs("tpprId") & """ class=""campo"" " & xSelected & ">" & xRs("tpprNome") & "</option>"
							xSelected = ""
							xRs.MoveNext
						wend
						response.write "</select>"
						xrs.close
						set xRs = nothing
					%> </td>
          </tr>
        </table>
        <p align="center"> 
          <input type="button" value="Filtrar" onclick="javascript:document.form1.action='gerHistCntr.asp?filtro=1';document.form1.submit()" style="font-weight:bold;">
          <input type="button" value="Trazer todos" onclick="javascript:window.location='gerHistCntr.asp';" style="font-weight:bold;">
        </p>
        <input type="hidden" value="<%= xProjetos %>" id="Projetos" name="Projetos"></input> 
        <table style="border-collapse:collapse" >
          <tr> 
            <% = xLinha(0) %>
          </tr>
          <tr> 
            <% = xLinha(14) %>
          </tr>
          <tbody id="grpCadastro" style="display:block">
            <tr> <%= xLinha(1) %> </tr>
            <tr> <%= xLinha(2) %> </tr>
            <tr> <%= xLinha(13) %> </tr>
          </tbody>
          <tr> <%= xLinha(6) %> </tr>
          <tbody id="grpCustConstr" style="display:block">
            <tr> <%= xLinha(3) %> </tr>
            <tr> <%= xLinha(4) %> </tr>
          </tbody>
          <tr> <%= xLinhaCusto(1) %> </tr>
          <tbody id="grpCntrCust" style="display:block">
            <%
						for i = 2 to xIndServCust %>
            <tr> 
              <% 	Response.write xLinhaCusto(i) %>
            </tr>
            <%      next %>
          </tbody>
          <tr> <%= xLinhaPrazo(1) %> </tr>
          <tbody id="grpCntrPraz" style="display:block">
            <%
						for i = 2 to xIndServPrazo %>
            <tr> 
              <% 	Response.write xLinhaPrazo(i) %>
            </tr>
            <%      next %>
          </tbody>
        </table>
      </form></tr>
</table>
<tr>
  <td>&nbsp;</body>
</html>
