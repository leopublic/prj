<% Response.Expires= -1%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ::</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<link rel="stylesheet" type="text/css" href="../EstiloMenu.css">
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<script language="JavaScript" type="text/javascript" src="../scripts/AbrirJanela.js"></script>
	</HEAD>
	<body class="cabecalho" >
<%  lPerfisAutorizados = "15"
	VerificarAcesso()

	projId = session("projId")
	xnconId = request.querystring("nconId")
	Dim gmenuGeral
	Dim gMenuSelecionado
	Dim gOpcaoSelecionada
	'gOpcaoSelecionada = "Servicos"
	gmenuGeral = "Projetos"
	gmenuGeral = "Projetos"
	gMenuSelecionado = "prjNaoConformidade"

   	Dim gConexao 
	AbrirConexao
	xSQL = ""
	xSQL = xSQL & "select n.nconNumero "
	xSQL = xSQL & "  from NaoConformidade n "
	xSQL = xSQL & " where n.nconId = " & xnconId
	'response.write xSQL
	Set xRs = gConexao.Execute(xSQL)
	if not xRs.EOF then
		xnconNumero = xrs("nconNumero")
	end if
	xrs.close   
%>
		<!--#include file="../includes/cab.inc"-->
			<tr>
				<td style="padding:0px" valign="top" class="blocoOpcoes">
					<!--#include file="../includes/opcProjx2.inc"-->
				</td>
				<td colspan="2" valign="top" style="padding:5px" class="blocoPagina">
				<form>
					<table width="100%">
					<tr>
						<td class="docTit"><%=session("projNome")%><br><img src="../images/0066_double_arrow.png" class="botao" style="margin-right:3px">N�o-Conformidade: <%=xnconNumero%></td>
						<td align="right">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
					</tr>
					<tr>
						<td colspan="2" style="height:5"></td>
					</tr>
					<tr>
						<td colspan="2">
							
<%
	xCampoOrdem = Request.QueryString("CampoOrdem")
	if xCampoOrdem = "" then
		xCampoOrdem = Session("CampoOrdemResumo")
		if xCampoOrdem = "" then
			xCampoOrdem = "itemCodJde"
		end if
	end if
	Session("CampoOrdemResumo") = xCampoOrdem

	if xCampoOrdem = "itemCodJde" then SortCod = "<img src=""../images/sort.gif"">"
	if xCampoOrdem = "itemTag" then SortTag = "<img src=""../images/sort.gif"">"
	if xCampoOrdem = "itemDescricao" then SortDescricao = "<img src=""../images/sort.gif"">"


	cab = ""
	cab = cab & "<table width='100%' class='grid' >"
	cab = cab & 	"<tr>"
	cab = cab & 		"<td width='70px' class='GridCab'><a href='prjNconResumo.asp?nconId=" & xnconId & "&CampoOrdem=i.itemCodJde' class='GridCab' title='Clique para ordenar por essa coluna'>Cod JDE</a>" & SortCod & "</td>"
	cab = cab & 		"<td width='80px' class='GridCab'><a href='prjNconResumo.asp?nconId=" & xnconId & "&CampoOrdem=i.itemTag' class='GridCab' title='Clique para ordenar por essa coluna'>TAG</a>" & SortTag & "</td>"
	cab = cab & 		"<td class='GridCab' ><a href='prjNconResumo.asp?nconId=" & xnconId & "&CampoOrdem=i.itemDescricao' class='GridCab' title='Clique para ordenar por essa coluna'>Descri��o</a>" & SortDescricao & "</td>"
	cab = cab & 		"<td width='80px' class='GridCab' style='text-align:right'>Recebido</td>"
	cab = cab & 		"<td width='80px' class='GridCab' style='text-align:right'>Retirado</td>"
	cab = cab & 		"<td width='80px' class='GridCab' style='text-align:right'>Em estoque</td>"
	cab = cab & 	"</tr>"
	rod = ""
	rod = rod & 	"<tr>"
	rod = rod & 		"<td class='GridRodape'>&nbsp;</td>"
	rod = rod & 		"<td class='GridRodape'>&nbsp;</td>"
	rod = rod & 		"<td class='GridRodape'>&nbsp;</td>"
	rod = rod & 		"<td class='GridRodape'>&nbsp;</td>"
	rod = rod & 		"<td class='GridRodape'>&nbsp;</td>"
	rod = rod & 		"<td class='GridRodape'>&nbsp;</td>"
	rod = rod & 	"</tr>"
	rod = rod & "</table>"
	'
	'Imprimindo itens da n�o-conformidade
	xSQL = ""
	xSQL = xSQL & "select ir.itemId "
	xSQL = xSQL & "     , i.itemCODJDE "
	xSQL = xSQL & "     , i.itemDescricao "
	xSQL = xSQL & "     , i.itemTag "
	xSQL = xSQL & "     , isnull(sum(isnull(ir.itrcQtd,0)),0) Recebido  "
	xSQL = xSQL & "     , isnull(sum(isnull(ia.itesQtd,0)),0) Utilizado "
	xSQL = xSQL & "     , (isnull(sum(isnull(ir.itrcQtd,0)),0) - isnull(sum(isnull(ia.itesQtd,0)),0)) Restante "
	xSQL = xSQL & "  from ItemRecebimento ir "
	xSQL = xSQL & "     , (select x.itemId, x.itesQtd from ItemSaida x where x.itelId is null) ia "
	xSQL = xSQL & "     , Recebimento     r  "
	xSQL = xSQL & "     , Item            i  "
	xSQL = xSQL & " where r.nconId  = " & xnconId
	xSQL = xSQL & "   and ir.receId =  r.receId "
	xSQL = xSQL & "   and ir.itepId is null "
	xSQL = xSQL & "   and i.itemId  =  ir.itemId "
	xSQL = xSQL & "   and ia.itemId =* ir.itemId "
	xSQL = xSQL & " group by ir.itemId, i.itemCODJDE, i.itemDescricao, i.itemTag "
	xSQL = xSQL & " order by " & xCampoOrdem
	'response.write xsql
	Set xrs = gConexao.Execute(xSQL)
	if not xrs.eof then
		response.write cab & vbcrlf
		while not xrs.eof
			'
			response.write "<tr>"
			response.write 		"<td class=""gridlinhapar"">" & xrs("itemCodJde") & "</td>"
			response.write 		"<td class=""gridlinhapar"">" & xrs("itemTag") & "</td>"
			response.write 		"<td class=""gridlinhapar"">" & xrs("itemDescricao") & "</td>"
			response.write 		"<td class=""gridlinhapar"" style=""text-align:right"">" & FormatNumber(xrs("Recebido"),1,-1,0) & "</td>"
			response.write 		"<td class=""gridlinhapar"" style=""text-align:right"">" & FormatNumber(xrs("Utilizado"),1,-1,0) & "</td>"
			response.write 		"<td class=""gridlinhapar"" style=""text-align:right"">" & FormatNumber(xrs("Restante"),1,-1,0)   & "</td>"
			response.write "</tr>"
			xrs.MoveNext
		wend
		response.write rod & vbcrlf
	end if
	
	%>					
						</td>
					</tr>
					</table>
				</form>
				</td>
			</tr>
		<!--#include file="../includes/rod.inc"-->
	</body>
</HTML>
