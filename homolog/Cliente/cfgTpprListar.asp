<% Response.Expires= -1%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ::</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<link rel="stylesheet" type="text/css" href="../EstiloMenu.css">
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<script language="JavaScript" type="text/javascript" src="../scripts/AbrirJanela.js"></script>
	</HEAD>
	<body class="cabecalho" >
<%  lPerfisAutorizados = "15"
	VerificarAcesso()

   Dim gmenuGeral
   Dim gMenuSelecionado
   Dim gOpcaoSelecionada
   gMenuSelecionado = "TipoProjeto"
   'gOpcaoSelecionada = "Servicos"
   gmenuGeral = "Configuracao"
   PastaSelecionada = "Configuração"
%>
		<!--#include file="../includes/cab.inc"-->
			<tr>
				<td style="padding:0px" valign="top" class="blocoOpcoes">
					<!--#include file="../includes/opcConfig.inc"-->
				</td>
				<td colspan="2" valign="top" style="padding:5px" class="blocoPagina">
				<form>
					<table width="100%">
					<tr>
						<td class="docTit">TIPOS DE PROJETO CADASTRADOS</td>
						<td align="right"><a href="javascript:AbrirJanela('cfgTpprAlterar1.asp?tpprId=0',500,300);"><img src="../images/newitem.gif" class="botao">&nbsp;Criar novo</a></td>
					</tr>
					<tr>
						<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
					</tr>
					<tr>
						<td colspan="2" style="height:5"></td>
					</tr>
					<tr>
						<td colspan="2">
							<table width="100%" class="grid" >
								<tr>
									<td width="20"  class="GridCab">&nbsp;</td>
									<td width="360" class="GridCab"><a href="cfgServListar.asp" class="GridCab">Nome<%=SortNome%></a></td>
									<td class="GridCab">&nbsp;</td>
									<td width="10"  class="GridCab">&nbsp;</td>
								</tr>
	<%
	Dim gConexao 
	xSQL = "select tpprId, tpprNome"
	xSQL = xSQL & " from TipoProjeto"
	xSQL = xSQL & " where tpprId <> 0"
	xSQL = xSQL & " order by tpprNome"
	AbrirConexao
	Set xRs = gConexao.Execute(xSQL)
	if xRs.EOF then
	    Response.write "<tr>" & vbcrlf
	    Response.write "<td colspan=""3"">Nenhum tipo de projeto cadastrado</td>" & vbcrlf
	    Response.write "</tr>" & vbcrlf
		xPovoou = false
	else
		estilo = "GridLinhaPar"
		while not xRs.EOF
			xPovoou = true
			Response.write "<tr>" & vbcrlf
			response.write vbtab & "<td class=""" & estilo & """><a href=""javascript:AbrirJanela('cfgTpprAlterar1.asp?tpprId=" & xRs("tpprId") & "',440,180);""><img src=""../images/folder.gif"" style=""border:0""></a></td>"
			response.write vbtab & "<td class=""" & estilo & """>" & xRs("tpprNome") & "</td>"
			response.write vbtab & "<td align=""right"" class=""" & estilo & """><a href=""javascript:AbrirJanela('cfgTpprExcluir1.asp?tpprId=" & xRs("tpprId") & "',600,250);"" class=""GridCab"" onmouseover=""javascript:document['excluir" & xRs("tpprId")  & "'].src='../images/botaoXA.jpg';"" onmouseout=""javascript:document['excluir" & xRs("tpprId") & "'].src='../images/botaoX.jpg';""><img name=""excluir" & xRs("tpprId") & """ id=""excluir" & xRs("tpprId") & """ src=""../images/botaoX.jpg""></a></td>"
			response.write vbtab & "<td class=""" & estilo & """>&nbsp;</td>"
			Response.write "</tr>" & vbcrlf
			xRs.MoveNext
			if estilo = "GridLinhaPar" then
				estilo = "GridLinhaImpar"
			else
				estilo = "GridLinhaPar"
			end if
		wend
	end if
	%>
								<tr>
									<td class="GridRodape">&nbsp;</td>
									<td class="GridRodape">&nbsp;</td>
									<td class="GridRodape">&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
					</table>
				</form>
				</td>
			</tr>
		<!--#include file="../includes/rod.inc"-->
	</body>
</HTML>
