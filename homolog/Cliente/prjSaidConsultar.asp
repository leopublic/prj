<% Response.Expires= -1%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ::</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<link rel="stylesheet" type="text/css" href="../EstiloMenu.css">
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<script language="JavaScript" type="text/javascript" src="../scripts/AbrirJanela.js"></script>
	</HEAD>
	<body class="cabecalho" >
<%  lPerfisAutorizados = "15"
	VerificarAcesso()
	
	Session("projId") = request.querystring("projId")	
	projId = Session("projId")
	saidId = request.querystring("saidId")
	
	Dim gmenuGeral
	Dim gMenuSelecionado
	Dim gOpcaoSelecionada
	'gOpcaoSelecionada = "Servicos"
	gmenuGeral = "Projetos"
	gMenuSelecionado = "prjSaidListar"
	tipo = "0"
	listId = "0"
	nconId = "0"

   	Dim gConexao 
	AbrirConexao
	xSQL =  ""
	xSQL = xSQL & "select s.saidId, isnull(s.saidRecebedor,'--') solicitante, s.saidDtHrCad, isnull(s.listId,0) listId, isnull(l.listNome,0) listNome "
	xSQL = xSQL & "  from saidamaterial   s "
	xSQL = xSQL & "     , (select x.listId, (isnull(z.tpliNome, '--') + ' - seq.: ' + convert(varchar(15),isnull(x.listSequencial,0)) + ' - rev.: ' + convert(varchar(15),isnull(k.reviNumero,0))) listNome from lista x, tipolista z, revisao k where x.tpliId *= z.tpliId and x.reviIdAtual *= k.reviId) l "
	xSQL = xSQL & " where s.saidId = " & saidId
	xSQL = xSQL & "   and l.listId = s.listId "
	xDadosComplementares = ""
    Set xrs = gConexao.Execute(xSQL)
	if not xrs.eof then
		xDadosComplementares = xDadosComplementares & "&nbsp;&nbsp;- SOLICITANTE: " & xrs("solicitante") & "<br>"
		xDadosComplementares = xDadosComplementares & "&nbsp;&nbsp;- LISTA: " & xrs("listNome") & "<br>"
		xDadosComplementares = xDadosComplementares & "&nbsp;&nbsp;- DATA/HORA: " & xrs("saidDtHrCad")
		tipo = "1"
		listId = xrs("listId")
	end if
	xrs.close	
	
	if (xDadosComplementares = "") then
		xSQL =  ""
		xSQL = xSQL & "select s.saidId, isnull(s.saidRecebedor,'--') solicitante, s.saidDtHrCad, isnull(s.nconId,0) nconId, isnull(n.nconNumero,0) nconNumero "
		xSQL = xSQL & "  from saidamaterial   s "
		xSQL = xSQL & "     , naoconformidade n "
		xSQL = xSQL & " where s.saidId = " & saidId
		xSQL = xSQL & "   and n.nconId = s.nconId "
		Set xrs = gConexao.Execute(xSQL)
		if not xrs.eof then
			xDadosComplementares = xDadosComplementares & "&nbsp;&nbsp;- SOLICITANTE: " & xrs("solicitante") & "<br>"
			xDadosComplementares = xDadosComplementares & "&nbsp;&nbsp;- N�O-CONFORMIDADE: " & xrs("nconNumero") & "<br>"
			xDadosComplementares = xDadosComplementares & "&nbsp;&nbsp;- DATA/HORA: " & xrs("saidDtHrCad")
			tipo = "2"
			nconId = xrs("nconId")
		end if
		xrs.close
	end if
	
   
   
 %>
		<!--#include file="../includes/cab.inc"-->
			<tr>
				<td style="padding:0px" valign="top" class="blocoOpcoes">
					<!--#include file="../includes/opcProjx2.inc"-->
				</td>
				<td colspan="2" valign="top" style="padding:5px" class="blocoPagina">
				<form>
					<table width="100%">
					<tr>
						<td class="docTit">
							<table>
								<tr><td>SA�DA</td><td>:</td><td><%=right(("00000000" & saidId),8) %></td></tr>
								<tr><td colspan=3><%=xDadosComplementares%></td></tr>
								
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
					</tr>
					<tr>
						<td colspan="2" style="height:5"></td>
					</tr>
					<tr>
						<td colspan="2">
							<table width="100%" class="grid" >
								<tr>
									<td width="80px" class="GridCab"><a href="prjListListar.asp" class="GridCab">Cod JDE<%=SortNome%></a></td>
									<td class="GridCab"><a href="prjPedcListar.asp" class="GridCab">Descri��o<%=SortNome%></a></td>
									<td width="120px"  class="GridCab" STYLE="text-align:right">Qtd<br>Pedida</td>
									<td width="100px"  class="GridCab" STYLE="text-align:right">Qtd<br>Util. nesta sa�da</td>
									<td width="120px"  class="GridCab" STYLE="text-align:right">Qtd<br>Util. at� esta sa�da</td>
									<td class="GridSLinha">&nbsp;</td>
								</tr>
	<%
	if(tipo = "1")then 'Lista
		xSQL =  "select i.itemCodJde, i.itemDescricao, it.itelQtd itelQtd, sum((case when ia.saidId = " & saidId & " then isnull(ia.itesQtd,0) else 0 end)) itesQtd, sum(isnull(ia.itesQtd,0)) itesQtdT "
		xSQL = xSQL & "  from item      i  "
		xSQL = xSQL & "     , itemlista it "
		xSQL = xSQL & "     , itemsaida ia "
		xSQL = xSQL & " where it.listId  = " & listId
		xSQL = xSQL & "   and ia.saidId <= " & saidId
		xSQL = xSQL & "   and it.itelId  = ia.itelId "
		xSQL = xSQL & "   and i.itemId   = it.itemId "
		xSQL = xSQL & " group by i.itemCodJde, i.itemDescricao, it.itelQtd "
	elseif(tipo = "2")then 'N�o-Conformidade
		xSQL =  "select i.itemCodJde, i.itemDescricao, ir.itrcQtd itelQtd, sum((case when ia.saidId = " & saidId & " then isnull(ia.itesQtd,0) else 0 end)) itesQtd, sum(isnull(ia.itesQtd,0)) itesQtdT "
		xSQL = xSQL & "  from item      i  "
		xSQL = xSQL & "     , (select j.itemId, sum(isnull(j.itrcQtd,0)) itrcQtd from itemrecebimento j, recebimento r where j.receId = r.receId and r.nconId = " & nconId & " group by j.itemId) ir "
		xSQL = xSQL & "     , itemsaida ia "
		xSQL = xSQL & " where ir.itemId  = ia.itemId "
		xSQL = xSQL & "   and ia.saidId <= " & saidId
		xSQL = xSQL & "   and i.itemId   = ir.itemId "
		xSQL = xSQL & " group by i.itemCodJde, i.itemDescricao, ir.itrcQtd "
	end if
    'response.write xsql
	Set xRs = gConexao.Execute(xSQL)
	if xRs.EOF then
	    Response.write "<tr>" & vbcrlf
	    Response.write "<td colspan=""5"">Sa�da sem itens</td>" & vbcrlf
	    Response.write "</tr>" & vbcrlf
		xPovoou = false
	else
		estilo = "GridLinhaPar"
		while not xRs.EOF
			Response.write "<tr>" & vbcrlf
			response.write vbtab & "<td class=""" & estilo & """>" & xrs("itemCodJde")    & "</td>"
			response.write vbtab & "<td class=""" & estilo & """>" & xrs("itemDescricao") & "</td>"
			response.write vbtab & "<td class=""" & estilo & """ style=""text-align:right"">" & formatnumber(xrs("itelQtd"),2) & "</td>"
			response.write vbtab & "<td class=""" & estilo & """ style=""text-align:right"">" & formatnumber(xrs("itesQtd"),2) & "</td>"
			response.write vbtab & "<td class=""" & estilo & """ style=""text-align:right"">" & formatnumber(xrs("itesQtdT"),2) & "</td>"
			response.write vbtab & "<td class=""GridSLinha"">&nbsp;</td>"
			Response.write "</tr>" & vbcrlf
			xRs.MoveNext
			if estilo = "GridLinhaPar" then
				estilo = "GridLinhaImpar"
			else
				estilo = "GridLinhaPar"
			end if
		wend
	end if
	%>
								<tr>
									<td class="GridRodape">&nbsp;</td>
									<td class="GridRodape">&nbsp;</td>
									<td class="GridRodape">&nbsp;</td>
									<td class="GridRodape">&nbsp;</td>
									<td class="GridRodape">&nbsp;</td>
									<td class="GridSLinha">&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
					</table>
				</form>
				</td>
			</tr>
		<!--#include file="../includes/rod.inc"-->
	</body>
</HTML>
