<% Response.Expires= -1%>
<% filename = "prjRelpResumo"&Request.QueryString("relpId")&".html" %>
<!--#include file="../includes/offline.inc"-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title><%=Session("TituloSite")%></title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<link rel="stylesheet" type="text/css" href="../EstiloMenu.css">
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/basFormatador.inc"-->
		<!--#include file="../includes/clsContrato.inc"-->
		<!--#include file="../includes/clsProjeto.inc"-->
		<script language="JavaScript" type="text/javascript" >
		function AbrirJanela(url, win_width, win_height) { //v2.0
			var w = screen.width; // Get the width of the screen200.173.208.22
			var h = screen.height; // Get the height of the screen

			// Where to place the Window
			var left = (w - win_width)/2;
			var top = (h - win_height)/2;

			posicao = "width=" + win_width + ",height=" + win_height ;
			posicao += ",top=" + top + ",left=" + left + ",screenX=" + left + ",screenY=" + top;
			parametros = posicao + ",directories=no,location=no,menubar=no,scrollbars=yes,status=yes,toolbar=no,resizable=yes;";
			titulo = "NovaJanela" ;
			window.open(url,titulo,parametros);
		}
		</script>
		<script language="JavaScript" type="text/javascript" src="../scripts/NumberFormat.js"></script>
		<style type="text/css">
		<!--
		td.titulo1 {background-color:#1D4F68;color:#FFFFFF;text-align:center;padding:4pt;font-family:tahoma;font-size:10pt;font-weight:bold}
		td.titulo {color:red;text-align:left;padding-left:4pt;font-family:tahoma;font-size:10pt;font-weight:bold;letter-spacing:2px}
		td.servicos {text-align:left;font-family:tahoma;font-size:8pt;font-weight:bold;border:1;border-style:solid;padding:2;}
		td.servicosCustos {text-align:left;font-family:tahoma;font-size:8pt;font-weight:bold;border:1;border-style:solid;padding:1;}
		td.servicosTit{text-align:center;font-family:tahoma;font-size:10pt;font-weight:bold;border:1;border-style:solid;padding:2;}
		td.dataCustos{vertical-align:center;text-align:right;font-family:tahoma;font-size:7pt;font-weight:normal;border:1;border-style:solid;letter-spacing:-0.6px}
		td.separador{height:20pt;font-size:1pt}
		-->
		</style>
	</HEAD>
	<body class="cabecalho" >
		<!--#include file="../includes/varBanco.inc"-->
		<!--#include file="../includes/varDatas.inc"-->
<%
   if Session("usuaId") = "" then
   		response.redirect("../login.asp")
   end if
   Dim gmenuGeral
   Dim gMenuSelecionado
   Dim gOpcaoSelecionada
   gmenuGeral = "Projetos"
   gMenuSelecionado = "PrjRelatorios"

   gItemSelecionado = "Resumo"

	AbrirConexao
	relpId = Request.QueryString("relpId")
	if relpId = "" then
		relpId = Session("relpId")
	else
		Session("relpId") = relpId
	end if

	if relpId="0" then
	   gOpcaoSelecionada = "PrjRelAtual"
	else
	   gOpcaoSelecionada = "PrjRelHistorico"
	end if
	projId = Session("projId")
	cntrNomeServArray = Session("cntrNomeServArray")
	cntrIdArray = Session("cntrIdArray")
	emprNomeArray = Session("emprNomeArray")
	emprIdArray = Session("emprIdArray")
	usuaIdContArray = Session("usuaIdContArray")
	TotalServ = Session("QtdServicos")
	servInd = 0
	area = ""
	set xrs = Projetos(projId)
	Set xRs = nothing

	set xRS = gConexao.execute("exec PROJ_ListarProjeto @pprojId = " & projId & ", @pLiberado = 0, @prelpId = " & relpId)
	xSolicitarPeriodo = False
	if relpId = "0" then
		if isnull(xRs("projSemIni")) then
			xSolicitarPeriodo = true
		end if
	end if
	projNome = xRs("projNome")
	projNum = xRs("projNum")
	CustGerEst = xRs("CustGerEst")
	CustSupcEst = xRs("CustSupcEst")
	GerReal = xRs("CustGerReal")
	SupcReal = xRs("CustSupcReal")
	GerEcn = xRs("CustGerEcn")
	SupcEcn = xRs("CustSupcEcn")
	Etc = xRs("CustEtc")
	SemIni = xRs("SemIni")
	SemFim = xRs("SemFim")
	ComeCust = xRs("ComeCust")
	ComePraz = xRs("ComePraz")
	ComeCusc = xRs("ComeCusc")
	ComeCUst = replace(ComeCust, vbcrlf, "<br>")
	ComePraz = replace(ComePraz, vbcrlf, "<br>")
	ComeCUsc = replace(ComeCusc, vbcrlf, "<br>")
	emprNomeSupCmp = xRs("NomeSupCmp")
	projNomePla = xRs("NomePla")
	projNomeSup = xRs("NomeSup")
	projNomeGer = xRs("NomeGer")
	CustGerEst = xRs("CustGerEst")
	CustSupcEst = xRs("CustSupcEst")
	Semana = SemIni & " a " & SemFim
	xRS.close
	Set xRs = nothing

	Dim gConexao2
	AbrirConexao2
	emprNome = SupervisorCampo(projId)

	TotalEst = CustGerEst + CustSupcEst + GerEcn + SupcEcn
	CustGerTot = CustGerEst + GerEcn
	CustSupcTot = CustSupcEst + SupcEcn
	TotalReal = GerReal + SupcReal + Etc
	estiloVari = ""
	if TotalEst = 0 then
		Variacao = "(n/a)"
	else
		if TotalReal = 0 then
			Variacao = "-"
		else
			Variacao = ((TotalEst - TotalReal) / TotalEst) * 100
			if Variacao < 0 then
				estiloVari = "style=""color:red"""
			end if
			Variacao = FormatNumber(Variacao,2,-1,-1) & "%"
		end if
	end if
'	response.write totalreal & " " & totalest
'	response.end()
	PastaSelecionada = "Projetos"
%>
	<% if xSolicitarPeriodo then %>
		<script language="JavaScript">
			AbrirJanela('prjRelpNovo.asp?projId=<%=projId%>',400,300);
		</script>
	<% end if%>
		<!--#include file="../includes/cab.inc"-->
			<tr>
				<td style="padding:0px" valign="top" class="blocoOpcoes">
					<!--#include file="../includes/opcProjx2.inc"-->
				</td>
				<td colspan="2" valign="top" style="padding:5px" class="blocoPagina">
				<form>
              <table width="100%">
			  <% if session("perfId") <> "4" then
					if relpId = "0" and offline = "" then %>
                <td class="docTit"><%=Session("projNome")%> - Relat�rio de progresso: <%=Semana%></td>
						<% if xSolicitarPeriodo then %>
                <td align="right" class="Disable"><img src="../images/newitemP.gif" class="botao">&nbsp;Emitir relat�rio</td>
						<% else %>
                <td align="right"><a href="javascript:AbrirJanela('prjRelpLiberar.asp?projId=<%=projId%>',600,250);"><img src="../images/newitem.gif" class="botao">&nbsp;Emitir relat�rio</a></td>
						<% end if %>
					<% else %>
                <td colspan="2" class="docTit"><%=Session("projNome")%> - Relat�rio
                  de progresso - HIST�RICO: <%=Semana%></td>
					<% end if
				end if %>
                <tr>
                  <td colspan="2" height="15"></td>
                </tr>
                <tr>
                  <td colspan="2" class="docTit">Informa��es b�sicas</td>
                </tr>
                <tr>
                  <td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
                </tr>
                <tr>
                  <td colspan="2" >
				  	<table class="doc" >
						<tr>
							<td width="17%" class="docLabel">Projeto</td>
							<td width="2%" class="docLabel">:</td>
							<td width="31%" class="docCmpLivre" ><% = projNome %></td>
							<td width="17%" class="docLabel" align="right">N�mero</td>
							<td width="2%" class="docLabel">:</td>
							<td width="31%"class="docCmpLivre"><% = projNum %></td>
						</tr>
						<tr>
							<td class="docLabel">Gerente</td>
							<td class="docLabel">:</td>
							<td class="docCmpLivre"><% = projNomeGer %></td>
							<td class="docLabel" align="right" >Sup. constru��es</td>
							<td class="docLabel">:</td>
							<td class="docCmpLivre"><% = projNomeSup %></td>
						</tr>
						<tr>
							<td class="docLabel">Planejador</td>
							<td class="docLabel">:</td>
							<td class="docCmpLivre"><% = projNomePla %></td>
							<td class="docLabel" align="right" >&nbsp;</td>
							<td class="docLabel">&nbsp;</td>
							<td class="docLabel">&nbsp;</td>
						</tr>
						<tr>
							<td class="docLabel">Sup. campo</td>
							<td class="docLabel">:</td>
							<td colspan="4" class="docCmpLivre"><% = emprNomeSupCmp %></td>
						</tr>
                    </table></td>
                </tr>
                <tr>
                  <td colspan="2" class="separador">&nbsp;</td>
                </tr>
                <tr>
                  <td class="docTit">Custos de constru��es</td>
                  <% if relpId="0" and offline = "" then %>
                  <td align="right"><a href="javascript:AbrirJanela('prjRelpCust.asp?projId=<%=projId%>',600,250);"><img src="../images/newitem.gif" class="botao">&nbsp;Alterar</a></td>
                  <% else %>
                  <td align="right" class="Disable"><img src="../images/newitemP.gif">&nbsp;Alterar</td>
                  <% end if %>
                </tr>
                <tr>
                  <td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
                </tr>
                <tr>
						<td colspan="2">
							<table class="doc">
								<tr>
									<td width="33%" align="right" class="docLabel" style="font-weight:normal">Hh gerenciamento estimado:</td>
									<td width="15%"  align="right"  class="docCmpLivre"><% = FormatNumber(CustGerEst) %></td>
									<td width="5%">&nbsp;</td>
									<td width="33%">&nbsp;</td>
									<td width="15%">&nbsp;</td>
								</tr>
								<tr>
									<td align="right" class="docLabel"  style="font-weight:normal">ECN gerenciamento:</td>
									<td align="right" class="docCmpLivre"><% = FormatNumber(GerEcn) %></td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td colspan="2" style="height:2px;border-top-width:1px;border-top-style:solid;font-size:1px">&nbsp;</td>
									<td style="font-size:1px">&nbsp;</td>
									<td style="font-size:1px">&nbsp;</td>
									<td style="font-size:1px">&nbsp;</td>
								</tr>
								<tr>
									<td align="right" class="docLabel">Hh gerenciamento estimado total:</td>
									<td align="right" class="docCmpLivre" style="font-weight:bold"><% = FormatNumber(CustGerTot) %></td>
									<td>&nbsp;</td>
									<td align="right" class="docLabel">Hh gerenciamento comprometido:</td>
									<td align="right" class="docCmpLivre" style="font-weight:bold"><% = FormatNumber(GerReal) %></td>
								</tr>
								<tr>
									<td colspan="5" style="height:10px;font-size:1px">&nbsp;</td>
								</tr>
								<tr>
									<td align="right" class="docLabel"  style="font-weight:normal">Hh sup. campo estimado:</td>
									<td align="right" class="docCmpLivre"><% = FormatNumber(CustSupcEst) %></td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td align="right" class="docLabel"  style="font-weight:normal">ECN sup. campo:</td>
									<td align="right" class="docCmpLivre"><% = FormatNumber(SupcEcn) %></td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td colspan="2" style="height:2px;border-top-width:1px;border-top-style:solid;font-size:1px">&nbsp;</td>
									<td style="font-size:1px">&nbsp;</td>
									<td style="font-size:1px">&nbsp;</td>
									<td style="font-size:1px">&nbsp;</td>
								</tr>
								<tr>
									<td align="right" class="docLabel">Hh sup. campo estimado total:</td>
									<td align="right" class="docCmpLivre" style="font-weight:bold"><% = FormatNumber(CustSupcTot) %></td>
									<td>&nbsp;</td>
									<td align="right" class="docLabel">Hh sup. campo comprometido:</td>
									<td align="right" class="docCmpLivre" style="font-weight:bold"><% = FormatNumber(SupcReal) %></td>
								</tr>
								<tr>
									<td colspan="5" style="height:10px;font-size:1px">&nbsp;</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td align="right" class="docLabel">ETC:</td>
									<td align="right" class="docCmpLivre"><% = FormatNumber(ETC) %></td>
								</tr>
								<tr>
									<td colspan="2" style="height:2px;border-top-width:2px;border-top-style:solid;font-size:1px">&nbsp;</td>
									<td style="font-size:1px">&nbsp;</td>
									<td colspan="2" style="height:2px;border-top-width:2px;border-top-style:solid;font-size:1px">&nbsp;</td>
								</tr>
								<tr>
									<td align="right" class="docLabel">Total hh estimado:</td>
									<td align="right" class="docCmpLivre" style="font-weight:bold"><% = FormatNumber(TotalEst) %></td>
									<td>&nbsp;</td>
									<td align="right" class="docLabel">Total hh previsto:</td>
									<td align="right" class="docCmpLivre" style="font-weight:bold"><% = FormatNumber(TotalReal) %></td>
								</tr>
								<tr>
									<td colspan="4" align="right" class="docLabel" style="align:right">Varia��o(%):</td>
									<td align="right" class="docCmpLivre" <%=estiloVari%>><% = Variacao %></td>
								</tr>
								<tr><td colspan="5">&nbsp;</td></tr>
								<tr><td colspan="5" class="docLabel">Coment�rios sobre as varia��es</td></tr>
								<tr><td colspan="5" class="docCmpLivre"><%=ComeCust%></td></tr>
							</table>
						</td>
                </tr>
                <tr>
                  <td colspan="2"  height="85" class="separador">&nbsp;</td>
                </tr>
                <tr>
                  <td class="docTit">Prazo de execu��o dos contratos</td>
                  <% if relpId="0" and offline="" then %>
                  <td align="right"><a href="javascript:AbrirJanela('prjRelpPraz.asp?projId=<%=projId%>',600,250);"><img src="../images/newitem.gif" class="botao">&nbsp;Alterar prazos</a></td>
                  <% else %>
                  <td align="right" class="Disable"><img src="../images/newitemP.gif">&nbsp;Alterar</td>
                  <% end if %>
                </tr>
                <tr>
                  <td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
                </tr>
                <tr>
                  <td colspan="2">
					<% = MontarContratos(projId, relpId, "0") %>
                      <tr>
                        <td colspan="7" class="GridRodape">&nbsp;</td>
                      </tr>
                      <tr>
                        <td colspan="7">&nbsp;</td>
                      </tr>
                      <tr>
                        <td colspan="7" class="docLabel">Coment�rios sobre os
                          prazos dos contratos:</td>
                      </tr>
                      <tr>
                        <td colspan="7" class="docCmpLivre"><%=ComePraz%></td>
                      </tr>
                    </table></td>
                </tr>
                <tr>
                  <td colspan="2" class="separador">&nbsp;</td>
                </tr>
                <tr>
                  <td class="docTit">Custo dos contratos</td>
                  <% if relpId="0"  and offline=""  then %>
                  <td align="right"><a href="javascript:AbrirJanela('prjRelcCust.asp?projId=<%=projId%>',600,250);"><img src="../images/newitem.gif" class="botao">&nbsp;Alterar custos</a></td>
                  <% else %>
                  <td align="right" class="Disable"><img src="../images/newitemP.gif">&nbsp;Atualizar</td>
                  <% end if %>
                </tr>
                <tr>
                  <td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
                </tr>
                <tr>
                  <td colspan="2"> <table width="100%">
                      <tr>
                        <td></td>
                        <td width="90"></td>
                        <td width="90"></td>
                        <td width="90"></td>
                        <td width="90"></td>
                        <td width="90"></td>
                        <td width="90"></td>
                        <td width="60"></td>
                      </tr>
                      <tr>
                        <td class="GridCab" align="center">&nbsp;</td>
                        <td class="GridCab" align="center">Valor<br>Estimado<br>(<%=Session("moedSigla")%>)</td>
                        <td class="GridCab" align="center">Valor<br>Original<br>(<%=Session("moedSigla")%>)</td>
                        <td class="GridCab" align="center">Valor<br>Atual<br>(<%=Session("moedSigla")%>)</td>
                        <td class="GridCab" align="center">FCN<br>(<%=Session("moedSigla")%>)</td>
                        <td class="GridCab" align="center">Pleitos<br>(<%=Session("moedSigla")%>)</td>
                        <td class="GridCab" align="center">Valor<br>Final<br>(<%=Session("moedSigla")%>)</td>
                        <td class="GridCab" align="center">Varia��o<br>Contratual<br>(%)</td>
                      </tr>
                      <% = xCusto%>
                      <tr>
                        <td colspan="8" class="GridRodape">&nbsp;</td>
                      </tr>
                      <tr>
                        <td colspan="8">&nbsp;</td>
                      </tr>
                      <tr>
                        <td colspan="8" class="docLabel">Coment�rios sobre os
                          custos dos contratos:</td>
                      </tr>
                      <tr>
                        <td colspan="8" class="docCmpLivre"><%=ComeCusc%></td>
                      </tr>
                    </table></td>
              </table>
            </form>
				</td>
			</tr>
		<!--#include file="../includes/rod.inc"-->
	</body>
</HTML>
