<% Response.Expires= -1%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Projetos</title><%%>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<!--#include file="../includes/basFormatador.inc"-->
		<!--#include file="../includes/basValidacao.inc"-->
		<script language="JavaScript" type="text/javascript" src="../scripts/NumberFormat.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/Mid.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/PreparaJanela.js"></script>
		<SCRIPT language="JavaScript">
			function CamposOk(){
				return true
			 }
			function Formatar(campo){
				var inputValue = document.getElementById(campo+'Fmt');
				var inputHidden = document.getElementById(campo);
				inputValue.value = FormatarNumero(inputValue.value, '.', ',', ',');
				inputHidden.value = FormatarNumero(inputValue.value, '', ',', '.');
				var Total = Math.round(parseFloat(document.form.projCustGerEsti.value)*100 )
				Total = Total + Math.round(parseFloat(document.form.projCustSupcEsti.value)*100)
				Total = Total + Math.round(parseFloat(document.form.projCustGerEcn.value)*100)
				Total = Total + Math.round(parseFloat(document.form.projCustSupcEcn.value)*100)
				Total = Total / 100
				Total = Total + '';
				document.form.TotalEst.value = FormatarNumero(Total, '.', '.', ',');
			}
		
			function Validar(){
                if (CamposOk()==true)
			    { document.form.submit();}
			}
		</SCRIPT> 
	</HEAD>
	<body class="PopUpEntrada" onLoad="javascript:PreparaJanela(400, 280);">
		<!--#include file="../includes/varControleAcesso.inc"-->
		<!--#include file="../includes/varBanco.inc"-->
<% 
    lPerfisAutorizados = "51"
	VerificarAcesso
	AbrirConexao
	projId = Request.Querystring("projID")
	xSQL = ""
	xSQL = xSQL & "select projNome, projNum, projNomeSup, projNomeGer"
	xSQL = xSQL & " , isnull(projCustGerEst, 0) projCustGerEst"
	xSQL = xSQL & " , isnull(projCustSupcEst, 0) projCustSupcEst"
	xSQL = xSQL & " from Projeto "
	xSQL = xSQL & " where Projeto.projId = " & projId
	set xrs = gConexao.execute(xSQL)
	projNome = xRs("projNome")
	projNum = xRs("projNum")
	projNomeSup = xRs("projNomeSup")
	projNomeGer = xRs("projNomeGer")
	projCustGerEst = xRs("projCustGerEst")
	projCustSupcEst = xRs("projCustSupcEst")
	Session("projId") = projId
	Session("projNome") = projNome
	xRs.Close
	Set xRs = nothing
	
	xSQL = ""
	xSQL = xSQL & "Select isnull(relpCustGerEcn,0)  relpCustGerEcn "
	xSQL = xSQL & "     , isnull(relpCustSupcEcn,0) relpCustSupcEcn "
	xSQL = xSQL & "  from RelatorioProjeto "
	xSQL = xSQL & "     , Projeto "
	xSQL = xSQL & " where Projeto.projId          = " & projId 
	xSQL = xSQL & "   and RelatorioProjeto.relpId = Projeto.relpIdAtivo"
	set xRs = gConexao.execute(xSQL)
	if xRs.Eof then
		relpCustGerEcn = 0
		relpCustSupcEcn = 0
	else
		relpCustGerEcn = xRs("relpCustGerEcn")
		relpCustSupcEcn = xRs("relpCustSupcEcn")
	end if
	xRs.close
	Set xRs = Nothing
	TotalEst = projCustGerEst + projCustSupcEst + relpCustGerEcn + relpCustSupcEcn
	gConexao.close
	Set gConexao = Nothing
%>

	<form onSubmit="Validar();" action="cfgProjAltCustConstr2.asp" method="post" name="form">
		<input type="hidden" name="projId" value="<% = projId %>" >
		<input type="hidden" name="projCustGerEsti"  value="<% = NumFmtBd(projCustGerEst) & " "%>">
		<input type="hidden" name="projCustSupcEsti" value="<% = NumFmtBd(projCustSupcEst) & " " %>">
		<input type="hidden" name="projCustGerEcn"  value="<% = NumFmtBd(relpCustGerEcn) & " "%>">
		<input type="hidden" name="projCustSupcEcn" value="<% = NumFmtBd(relpCustSupcEcn) & " " %>">
		<table width="100%">
			<tr>
				<td width="15"></td>
				<td></td>
			</tr>
			<tr>
				<td colspan="2" class="docTit">ALTERAR CUSTOS DE CONSTRUÇÕES</td>
			</tr>
			<tr>
				<td colspan="2" class="docSubTit"><%= projNome & "(" & projNum & ")"%></td>
			</tr>
			<tr>
				<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
			</tr>
			<tr>
				<td colspan="2" height="15"></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td align="left">
					<table class="doc" width="100%">
						<tr>
							<td class="docLabel">Hh gerenciamento estimado</td>
							<td width="10" class="docLabel">:</td>
							<td width="100"  class="docCmpLivre"><input type="text" name="projCustGerEstiFmt" id="projCustGerEstiFmt" value="<% = FormatNumber(projCustGerEst) %>" class="docCmpLivre" style="text-align:right" onBlur="javascript:Formatar('projCustGerEsti');"></td>
						</tr>
						<tr>
							<td class="docLabel">ECN gerenciamento</td>
							<td class="docLabel">:</td>
							<td align="right" class="docSBorda"><% = FormatNumber(relpCustGerEcn)%></td>
						</tr>
						<tr>
							<td class="docLabel">Hh sup. campo estimado</td>
							<td class="docLabel">:</td>
							<td align="right" class="docCmpLivre"><input type="text" name="projCustSupcEstiFmt" id="projCustSupcEstiFmt"  value="<% = FormatNumber(projCustSupcEst) %>" class="docCmpLivre" style="text-align:right" onBlur="javascript:Formatar('projCustSupcEsti');"></td>
						</tr>
						<tr>
							<td class="docLabel">ECN sup. campo</td>
							<td class="docLabel">:</td>
							<td align="right" class="docSBorda"><% = FormatNumber(relpCustSupcEcn)%></td>
						</tr>
						<tr>
							<td colspan="3" class="docLabel" height="5" style="font-size:1;border-bottom-style:solid;border-bottom-width:1;border-bottom-color:#94bdd1">&nbsp;</td>
						</tr>
						<tr>
							<td class="docLabel">Total hh estimado:</td>
							<td class="docLabel">:</td>
							<td align="right" class="docSBorda"><input type="text" name="TotalEst"  value="<% = FormatNumber(TotalEst) %>" class="docCmpLivre"  readonly="read-only" style="text-align:right;font-weight:bold"></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2" height="15"></td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<table>
						<tr>
							<td height="28" width="80" align="center"><img border="0" src="../images/btnSalvar.gif" class="botao" onclick="javascript:Validar();"></td>
							<td width="10"></td>
							<td width="80" align="center"><img border="0" src="../images/btnCancelar.gif" class="botao" onclick="javascript:window.close();"></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
	</body>
</HTML>
