<% Response.Expires= -1%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Projetos</title><%%>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<!--#include file="../includes/basFormatador.inc"-->
		<!--#include file="../includes/basValidacao.inc"-->
		<script language="JavaScript" type="text/javascript" src="../scripts/NumberFormat.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/Mid.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/ValidacaoDatas.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/PreparaJanela.js"></script>
		<SCRIPT language="JavaScript">
			function CamposOk(){
			   var Cmp = document.form.SemIni
				if(Cmp.value!='')
				{	if(isDate(Cmp)==false)
					{	Cmp.select();
						Cmp.focus();
						return false;
					}
				}
			   var Cmp = document.form.SemFim
				if(Cmp.value!='')
				{	if(isDate(Cmp)==false)
					{	Cmp.select();
						Cmp.focus();
						return false;
					}
				}
				return true
			}

			function Validar(){
                if (CamposOk()==true)
			    { document.form.submit();}
			}
		</SCRIPT> 
	</HEAD>
	<body class="PopUpEntrada" onLoad="javascript:PreparaJanela(380, 250);">
		<!--#include file="../includes/varControleAcesso.inc"-->
		<!--#include file="../includes/varBanco.inc"-->
<% 
    lPerfisAutorizados = "51"
	VerificarAcesso
	AbrirConexao
	projId = Request.Querystring("projID")
	Session("TituloMsg") = Session("projNome") & " - Informar per�odo do primeiro relat�rio"
%>

	<form onSubmit="Validar();" action="prjRelpNovo2.asp" method="post" name="form">
		<input type="hidden" name="projId" value="<% = projId %>" >
		<table width="100%">
			<tr>
				<td width="15"></td>
				<td></td>
			</tr>
			<tr>
				<td colspan="2" class="docTit"><%=Session("TituloMsg")%></td>
			</tr>
			<tr>
				<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
			</tr>
			<tr>
				<td colspan="2" height="15"></td>
			</tr>
			<tr>
				<td colspan="2" class="GridCab" align="Left">Por favor, informe o per�odo do relat�rio:</td>
			</tr>
			<tr>
				<td colspan="2">
					<table align="center" class="doc" style="width:200">
						<tr>
							<td width="90" class="docLabel" align="left">De:</td>
							<td class="docSBorda"><input type="text" name="SemIni" value="" class="docCmpLivre" style="height:20;border:1;border-style:solid;border-color:#CCCCCC;text-align:center" onblur="javascript:this.value=FormatarData(this);"></td>
						</tr>
						<tr>
							<td class="docLabel" align="left">At�:</td>
							<td class="docSBorda"><input type="text" name="SemFim" value="" class="docCmpLivre" style="height:20;border:1;border-style:solid;border-color:#CCCCCC;text-align:center" onblur="javascript:this.value=FormatarData(this);"></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2" height="15"></td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<table>
						<tr>
							<td height="28" width="80" align="center"><img border="0" src="../images/btnSalvar.gif" class="botao" onclick="javascript:Validar();"></td>
							<td width="10"></td>
							<td width="80" align="center"><img border="0" src="../images/btnCancelar.gif" class="botao" onclick="javascript:window.close();"></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
	</body>
</HTML>
