Imports System.Data.OleDb
Imports System.IO

Public Class upload
    Inherits System.Web.UI.Page
    Protected WithEvents lblNormaNumero As System.Web.UI.WebControls.Label
    Protected WithEvents btnSalvar As System.Web.UI.WebControls.ImageButton
    Protected WithEvents panelTitulo As System.Web.UI.WebControls.Panel
    Protected WithEvents painelRevisaoLivre As System.Web.UI.WebControls.Panel
    Protected WithEvents painelRevisaoData As System.Web.UI.WebControls.Panel
    Protected WithEvents txtArquivo As System.Web.UI.HtmlControls.HtmlInputFile
    Protected WithEvents tpanId As System.Web.UI.WebControls.TextBox
    Protected WithEvents projId As System.Web.UI.WebControls.TextBox
    Protected WithEvents cntrId As System.Web.UI.WebControls.TextBox
    Protected WithEvents cmbRevAno As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblMsg As System.Web.UI.WebControls.Label
    Protected WithEvents lblTituloPagina As System.Web.UI.WebControls.Label

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim gConexao As New OleDbConnection()
        Dim xRs As OleDbDataReader
        Dim xCom As OleDbCommand
        Dim xEscopo As String
        Dim tpanDesc As String
        Dim tpanUnic As Boolean
        Dim tpanForm As Boolean
        Dim tpanAprv As Boolean
        Dim tpanGlob As Boolean
        Dim tpanProj As Boolean
        Dim tpanCntr As Boolean
        Dim tparExte As String
        Dim tpanGen As String
        Dim i As Integer
        gConexao.ConnectionString = "Provider=SQLOLEDB.1;SERVER=sql172.prj.com.br;DATABASE=prj;UID=prj;PWD=sossego0001;"
        gConexao.Open()
        xCom = gConexao.CreateCommand

        '* Obter caracter�sticas do tipo de anexo a ser carregado
        '*==========================================================
        Dim xtpanId As String = Request.QueryString("tpanID")
        Dim xprojId As String = Request.QueryString("projID")
        Dim xcntrId As String = Request.QueryString("cntrID")
        tpanId.Text = xtpanId
        projId.Text = xprojId
        cntrId.Text = xcntrId
        Dim xSQL As String
        xSQL = "select tpanUnic, tpanAprv, tpanDesc, tpanProj, tpanGlob, tpanCntr, tparExte , tpanGen, tpanForm"
        xSQL = xSQL & "  from TipoAnexo "
        xSQL = xSQL & "     , TipoArquivo"
        xSQL = xSQL & " where tpanId = " & xtpanId
        xSQL = xSQL & "   and TipoArquivo.tparId = TipoAnexo.tparId"
        xCom.CommandText = xSQL
        xRs = xCom.ExecuteReader
        If xRs.Read() Then
            tpanUnic = xRs("tpanUnic")
            tpanForm = xRs("tpanForm")
            tpanAprv = CStr(xRs("tpanAprv"))
            tpanGlob = CStr(xRs("tpanGlob"))
            tpanProj = xRs("tpanProj")
            tpanCntr = xRs("tpanCntr")
            tparExte = CStr(xRs("tparExte"))
            tpanGen = CStr(xRs("tpanGen"))
            tpanDesc = xRs("tpanDesc")
        End If
        xRs.Close()
        If tpanForm Then
            lblNormaNumero.Text = "Norma"
        Else
            lblNormaNumero.Text = "N�mero"
        End If
        If tpanGlob Then
            xEscopo = " - GLOBAL"
        Else
            If tpanProj = "1" Then
                xEscopo = "PROJETO"
            Else
                xEscopo = "CONTRATO"
            End If
        End If

        If (tpanUnic = False) Or (tpanForm = True) Or (tpanCntr = True) Then
            panelTitulo.Visible = True
        Else
            panelTitulo.Visible = False
        End If

        painelRevisaoLivre.Visible = False
        painelRevisaoData.Visible = False
        If tpanForm = False Then
            If tpanProj = True Then
                painelRevisaoLivre.Visible = True
            Else
                painelRevisaoLivre.Visible = False
                If tpanCntr = False Then
                    painelRevisaoData.Visible = True
                Else
                    painelRevisaoData.Visible = False
                End If
            End If
        End If


        Dim xItem As ListItem

        For i = Year(Now()) To 1950 Step -1
            xItem = New ListItem()
            xItem.Text = CStr(i)
            xItem.Value = i
            cmbRevAno.Items.Add(xItem)
        Next
        xRs.Close()
        If tpanGen = "M" Then
            lblTituloPagina.Text = "Carregar novo " & tpanDesc
        Else
            lblTituloPagina.Text = "Carregar nova " & tpanDesc
        End If
        Session("TituloPagina") = lblTituloPagina.Text
    End Sub

    Private Sub btnSalvar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSalvar.Click
        CriaAnexo()
    End Sub

    Private Sub CriaAnexo()
        Dim gConexao As New OleDbConnection()
        Dim xRs As OleDbDataReader
        Dim xCom As OleDbCommand
        Dim xSQL As String
        Dim anexRevNum, anexNome, anexNum, anexRevMes, anexRevAno, strFileName As String
        Dim savePath As String
        Dim xId As Integer
        Dim anexId As String
        lblMsg.Text = ""
        If Not (txtArquivo.PostedFile Is Nothing) Then
            lblMsg.Text = "Arquivo n�o encontrado."
            Exit Sub
        End If
        savePath = Server.MapPath("..\Anexos") & "\"

        gConexao.ConnectionString = "Provider=SQLOLEDB.1;SERVER=sql172.prj.com.br;DATABASE=prj;UID=prj;PWD=sossego0001;"
        gConexao.Open()
        xCom = gConexao.CreateCommand
        ' Grab the file name
        anexRevNum = Request.Form("txtRevNum")
        anexNome = Request.Form("txtNome")
        anexNum = Request.Form("txtNumero")
        anexRevMes = Request.Form("cmbRevMes")
        anexRevAno = cmbRevAno.SelectedItem.Value
        If anexNum = "" Then
            anexNum = "0"
        End If
        If anexRevMes = "" Then
            anexRevMes = "0"
        End If
        If anexRevAno = "" Then
            anexRevAno = "0"
        End If
        '
        ' Cria o anexo
        xSQL = ""
        xSQL = xSQL & "exec PCAD_Anexo @ptpanId = " & tpanId.Text
        xSQL = xSQL & ",  @pprojId       =  " & projId.Text
        xSQL = xSQL & ",  @pcntrId       =  " & cntrId.Text
        xSQL = xSQL & ",  @panexDesc     = ''"
        xSQL = xSQL & ",  @pusuaIdUpl    =  " & Session("usuaId")
        xSQL = xSQL & ",  @panexNomeOrig = '" & txtArquivo.PostedFile.FileName & "'"
        xSQL = xSQL & ",  @panexTam      =  " & txtArquivo.PostedFile.ContentLength
        xSQL = xSQL & ",  @panexNum      = '" & anexNum & "'"
        xSQL = xSQL & ",  @panexNome     = '" & anexNome & "'"
        xSQL = xSQL & ",  @panexRevNum   = '" & anexRevNum & "'"
        xSQL = xSQL & ",  @panexRevMes   =  " & anexRevMes
        xSQL = xSQL & ",  @panexRevAno   =  " & anexRevAno
        'Response.write(xSQL)
        'Response.End()
        xCom.CommandText = xSQL
        xRs = xCom.ExecuteReader()
        xId = xRs("anexId")
        anexId = Right("00000000" & CStr(xId), 8)
        txtArquivo.PostedFile.SaveAs(savePath & anexId)

        Session("Msg") = "Arquivo carregado com sucesso!"
        Response.Redirect("gerMensagem.aspx")

    End Sub

End Class
