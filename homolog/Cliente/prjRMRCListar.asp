<% 	Response.Expires= 0
    Response.AddHeader "PRAGMA", "NO-CACHE" 
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ::</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<link rel="stylesheet" type="text/css" href="../EstiloMenu.css">
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<script language="JavaScript" type="text/javascript" src="../scripts/AbrirJanela.js"></script>
		<style>
		table.Relatorio{}
		table.Relatorio tr td.titulo{font-size:14pt;font-weight:bold;text-align:center}
		table.Relatorio tr td.Esq{text-align:left;}
		table.Relatorio tr td.Dir{text-align:right;}
		
		
		</style>
	</HEAD>
	<body class="cabecalho" >
<%  lPerfisAutorizados = "15"
	VerificarAcesso()
	projId = Session("projId")

   Dim gmenuGeral
   Dim gMenuSelecionado
   Dim gOpcaoSelecionada
   projId = Request.QueryString("projId")
	cntrNomeServArray = Session("cntrNomeServArray")
	cntrIdArray = Session("cntrIdArray")
	emprNomeArray = Session("emprNomeArray")
	emprIdArray = Session("emprIdArray")
	TotalServ = Session("QtdServicos")
	servInd = 0
	area = ""
   gmenuGeral = "Projetos"
   gMenuSelecionado = "prjRMRC"
	xData = request.form("calendar1_container") 

 %>
	<table width="100%" class="Relatorio">
	<tr>
		<td colspan="2" class="titulo"><%=Session("projNome")%></td>						
	</tr>
	<tr>
		<td class="titulo Esq">Registro de Material Recebido no Campo</td>
		<td class="titulo Dir"><%=xdata%></td>
		
	</tr>
	<tr>
		<td colspan="2" class="separadorTit">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">
			<table width="100%" class="grid" >
	<%
	Dim gConexao 
	xSQL = ""
	xSQL = xSQL & "select RC.receId, receNotaFiscal, pedcNum, requNum, itemCodJDE, itemDescricao"
    xSQL = xSQL & "     , pedcDtEntregaPrev, unidSigla"
    xSQL = xSQL & "     , itelQtdAtual, itelQtdRecebida"
    xSQL = xSQL & "  from Recebimento     RC"
    xSQL = xSQL & "     , (((((((ItemRecebimento IR left join ItemPedido IP on IP.itepId = IR.itepId)"
    xSQL = xSQL & "       left join ItemRequisicao IQ on IQ.iteqId = IP.iteqId)"
    xSQL = xSQL & "       left join ItemRevisao IV on IV.iterId = IQ.iterId)"
    xSQL = xSQL & "       left join ItemLista IL on IL.itelId = IV.itelId)"
    xSQL = xSQL & "       left join PedidoCompra P on P.pedcId = IP.pedcId)"
    xSQL = xSQL & "       left join Unidade U on U.unidId = IP.unidId)"
    xSQL = xSQL & "       left join Requisicao R on r.requId = IQ.requId)"
    xSQL = xSQL & "       left join Item I on I.itemId = IL.itemId"
    xSQL = xSQL & " where IR.receId = RC.receId "
    xSQL = xSQL & "   and convert(varchar(10), RC.receDt, 103) = '" & xData & "'"
    xSQL = xSQL & " order by receNotaFiscal"
'	response.write xSQL 
	'response.end
	AbrirConexao
	Set xRs = gConexao.Execute(xSQL)
	xreceIdAnt = ""
	if xRs.EOF then
	    Response.write "<tr>" & vbcrlf
	    Response.write "<td colspan=""6"">Nenhum recebimento cadastrado</td>" & vbcrlf
	    Response.write "</tr>" & vbcrlf
	else
		estilo = "GridLinhaPar"
		while not xRs.EOF
			if (xRs("receId")<>xreceId) then
				Response.write "<tr>" & vbcrlf
				response.write vbtab & "<td colspan=""6"">Nota fiscal:" & xRs("receNotaFiscal") 
				response.write vbtab & "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Requisi��o: " & xRs("requNum") 
				response.write vbtab & "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pedido: " & xRs("pedcNum")
				Response.write "</td></tr>" & vbcrlf				
				Response.write "<tr>"
				Response.write "	<td width=""50px"" class=""GridCab"">C�digo JDE</td>"
				Response.write "	<td width=""100px"" class=""GridCab"">Descri��o</td>"
				Response.write "	<td width=""100px"" class=""GridCab"">Dt Prevista</td>"
				Response.write "	<td width=""100px"" class=""GridCab"">Unidade</td>"
				Response.write "	<td width=""100px"" class=""GridCab"">Qtd Atual</td>"
				Response.write "	<td width=""100px"" class=""GridCab"">Qtd Recebida</td>"
				Response.write "</tr>"
			end if
			Response.write "<tr>" & vbcrlf
			response.write vbtab & "<td class=""" & estilo & """>" & xRs("itemCodJde") & "</td>"
			response.write vbtab & "<td class=""" & estilo & """>" & xRs("itemDescricao") & "</td>"
			response.write vbtab & "<td class=""" & estilo & """>" & xRs("pedcDtEntregaPrev") & "</td>"
			response.write vbtab & "<td class=""" & estilo & """>" & xRs("unidSigla") & "</td>"
			response.write vbtab & "<td class=""" & estilo & """>" & xRs("itelQtdAtual") & "</td>"
			response.write vbtab & "<td class=""" & estilo & """>" & xRs("itelQtdRecebida") & "</td>"
			Response.write "</tr>" & vbcrlf
			xRs.MoveNext
			if estilo = "GridLinhaPar" then
				estilo = "GridLinhaImpar"
			else
				estilo = "GridLinhaPar"
			end if
		wend
	end if
	%>
				<tr>
					<td class="GridRodape">&nbsp;</td>
					<td class="GridRodape">&nbsp;</td>
					<td class="GridRodape">&nbsp;</td>
					<td class="GridRodape">&nbsp;</td>
					<td class="GridRodape">&nbsp;</td>
					<td class="GridRodape">&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
	</table>
	</body>
</HTML>
