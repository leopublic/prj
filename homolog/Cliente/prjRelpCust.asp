<% Response.Expires= -1%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Projetos</title><%%>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<!--#include file="../includes/basFormatador.inc"-->
		<!--#include file="../includes/basValidacao.inc"-->
		<script language="JavaScript" type="text/javascript" src="../scripts/NumberFormat.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/Mid.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/PreparaJanela.js"></script>
		<SCRIPT language="JavaScript">
			function CamposOk(){
				return true
			 }
			function Formatar(campo){
				var inputValue = document.getElementById(campo+'Fmt');
				var inputHidden = document.getElementById(campo);
				inputValue.value = FormatarNumero(inputValue.value, '.', ',', ',');
				inputHidden.value = FormatarNumero(inputValue.value, '', ',', '.');
				var TotalR = Math.round(parseFloat(document.form.GerReal.value)*100 )
				TotalR = TotalR + Math.round(parseFloat(document.form.SupcReal.value)*100)
				TotalR = TotalR + Math.round(parseFloat(document.form.Etc.value)*100)
				var TotalE = Math.round(parseFloat(document.form.GerEst.value)*100 )
				TotalE = TotalE + Math.round(parseFloat(document.form.SupcEst.value)*100)
				TotalE = TotalE + Math.round(parseFloat(document.form.GerEcn.value)*100)
				TotalE = TotalE + Math.round(parseFloat(document.form.SupcEcn.value)*100)
				var TotalGer = Math.round(parseFloat(document.form.GerEst.value)*100 )
				TotalGer = TotalGer + Math.round(parseFloat(document.form.GerEcn.value)*100)
				var TotalSupc = Math.round(parseFloat(document.form.SupcEst.value)*100)
				TotalSupc = TotalSupc + Math.round(parseFloat(document.form.SupcEcn.value)*100)
				//alert (TotalE)
				//alert (TotalR)
				//alert(parseFloat(document.form.TotalEst.value))
				TotalE = TotalE / 100
				TotalR = TotalR / 100
				TotalGer = TotalGer / 100
				TotalSupc = TotalSupc / 100
				var variacao = (TotalE - TotalR) / TotalE
				//alert(variacao)
				variacao = Math.round((variacao) * 10000)
				//alert(variacao)
				var valorVar = variacao
				variacao = variacao / 100
				//alert(variacao)
				variacao = variacao + '';
				TotalR = TotalR + '';
				TotalE = TotalE + '';
				TotalGer = TotalGer + '';
				TotalSupc = TotalSupc + '';
				//alert ('TotalE:' + TotalE)
				//alert ('TotalR:' + TotalR)
				//alert ('TotalGer:' + TotalGer)
				//alert ('TotalSupc:' + TotalSupc)
				document.form.TotalReal.value = FormatarNumero(TotalR, '.', '.', ',');
				document.form.TotalEst.value = FormatarNumero(TotalE, '.', '.', ',');
				document.form.TotalGer.value = FormatarNumero(TotalGer, '.', '.', ',');
				document.form.TotalSupc.value = FormatarNumero(TotalSupc, '.', '.', ',');
				document.form.Variacao.value = FormatarNumero(variacao, '.', '.', ',');
				//document.form.Variacao.value = FormatarNumero(variacao, '.', '.', ',')+'%';
				if (valorVar<0) {	
					document.form.Variacao.style.color='red'
				}
				else {	
					document.form.Variacao.style.color='#1D4F68'
				}
				//document.form.Variacao.value = variacao;
			}
		
			function Validar(){
                if (CamposOk()==true)
			    { document.form.submit();}
			}
		</SCRIPT> 
	</HEAD>
	<body class="PopUpEntrada" onLoad="javascript:PreparaJanela(700, 480);">
		<!--#include file="../includes/varControleAcesso.inc"-->
		<!--#include file="../includes/varBanco.inc"-->
<% 
    lPerfisAutorizados = "514"
	VerificarAcesso
	AbrirConexao
	AbrirConexao
	projId = Request.Querystring("projId")
'	relpId = Request.Querystring("relpId")
	'
	' Obt�m valores estimados
	xSQL = ""
	xSQL = xSQL & "select isnull(projCustGerEst, 0) CustGerEst"
	xSQL = xSQL & "     , isnull(projCustSupcEst, 0) CustSupcEst"
	xSQL = xSQL & "     , isnull(projCustGerEcn, 0) CustGerEcn"
	xSQL = xSQL & "     , isnull(projCustSupcEcn, 0) CustSupcEcn"
	xSQL = xSQL & "     , isnull(projCustEtc, 0) CustEtc"
	xSQL = xSQL & "     , isnull(projCustGerReal, 0) CustGerReal"
	xSQL = xSQL & "     , isnull(projCustSupcReal, 0) CustSupcReal"
	xSQL = xSQL & "     , isnull(projComeCust, '&nbsp') ComeCust"
	xSQL = xSQL & "     , projSemIni"
	xSQL = xSQL & "     , projSemFim "
	xSQL = xSQL & "  from Projeto "
	xSQL = xSQL & " where Projeto.projId = " & projId
	set xrs = gConexao.execute(xSQL)
	GerEst = xRs("CustGerEst")
	SupcEst = xRs("CustSupcEst")
'	Ecn = xRs("projCustEcnEst")
'	xRs.Close
	'
	' Obt�m valores reais do relat�rio 
	xSQL = ""
	xSQL = xSQL & " select isnull(relpCustGerReal, 0) relpCustGerReal"
	xSQL = xSQL & "      , isnull(relpCustSupcReal, 0) relpCustSupcReal"
	xSQL = xSQL & "      , isnull(relpCustEtc, 0) relpCustEtc"
	xSQL = xSQL & "      , isnull(relpCustGerEcn, 0) relpCustGerEcn"
	xSQL = xSQL & "      , isnull(relpCustSupcEcn, 0) relpCustSupcEcn"
	xSQL = xSQL & "      , isnull(relpComeCust, '&nbsp') relpComeCust"
	xSQL = xSQL & "      , relpSemIni"
	xSQL = xSQL & "      , relpSemFim "
	xSQL = xSQL & "   from RelatorioProjeto "
	xSQL = xSQL & "  where relpId = " & relpId
'	set xRS = gConexao.execute(xSQL)
	GerReal = xRs("CustGerReal")
	SupcReal = xRs("CustSupcReal")
	Etc = xRs("CustEtc")
	GerEcn = xRs("CustGerEcn")
	SupcEcn = xRs("CustSupcEcn")
	SemIni = xRs("projSemIni")
	SemFim = xRs("projSemFim")
	ComeCust = xRs("ComeCust")
	xRS.close
	Set xRs = nothing
	GerTot = GerEst + GerEcn
	SupcTot = SupcEst+ SupcEcn
	TotalEst = GerEst + SupcEst + GerEcn + SupcEcn
	TotalReal = GerReal + SupcReal+ Etc
	estiloVari = ""
	if  TotalEst = 0 then
		Variacao = "(n/a)"
	else
		if TotalReal <> 0 then
			Variacao = ((TotalEst - TotalReal) / TotalEst) * 100
			if Variacao < 0 then
				estiloVari = ";color:red"
			end if
			if isnull(Variacao ) then
				Variacao = "-"
			else
				Variacao = FormatNumber(Variacao,2,-1,-1) & "%"		
			end if
		else
			Variacao = "-"
		end if
	end if
%>

	<form onSubmit="Validar();" action="prjRelpCust2.asp" method="post" name="form">
		<input type="hidden" name="usuaId"   value="<% = Session("usuaId") %>" >
		<input type="hidden" name="projId"   value="<% = projId %>" >
		<input type="hidden" name="relpId"   value="<% = relpId %>" >
		<input type="hidden" name="GerReal"  value="<% = NumFmtBd(GerReal) & " "%>">
		<input type="hidden" name="SupcReal" value="<% = NumFmtBd(SupcReal) & " " %>">
		<input type="hidden" name="Etc"      value="<% = NumFmtBd(Etc) & " " %>">
		<input type="hidden" name="GerEst"   value="<% = NumFmtBd(GerEst) & " "%>">
		<input type="hidden" name="SupcEst"  value="<% = NumFmtBd(SupcEst) & " " %>">
		<input type="hidden" name="GerEcn"   value="<% = NumFmtBd(GerEcn) & " " %>">
		<input type="hidden" name="SupcEcn"  value="<% = NumFmtBd(SupcEcn) & " " %>">
		<table width="100%">
			<tr>
				<td width="15"></td>
				<td></td>
			</tr>
			<tr>
				<td colspan="2" class="docTit"><%=Session("projNome") %> - Atualizar custo de constru��es</td>
			</tr>
			<tr>
				<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
			</tr>
			<tr>
				<td colspan="2" height="15"></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td align="left">
					<table class="doc" width="100%">
						<tr>
							<td width="200" align="right" class="docLabel">Hh gerenciamento estimado:</td>
							<td width="100"  align="right"  class="docSBorda"><% = FormatNumber(GerEst) %></td>
							<td width="30">&nbsp;</td>
							<td width="200">&nbsp;</td>
							<td width="100">&nbsp;</td>
						</tr>
						<tr>
							<td align="right" class="docLabel">ECN gerenciamento:</td>
							<td align="right" class="docCmpLivre"><input type="text" name="GerEcnFmt" value="<% = FormatNumber(GerEcn) %>" class="docCmpLivre" style="text-align:right" onBlur="javascript:Formatar('GerEcn');" tabindex="0"></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
							<tr>
								<td colspan="2" style="height:2px;border-top-width:1px;border-top-style:solid;font-size:1px">&nbsp;</td>
								<td style="font-size:1px">&nbsp;</td>
								<td style="font-size:1px">&nbsp;</td>
								<td style="font-size:1px">&nbsp;</td>
							</tr>
						<tr>
							<td align="right" class="docLabel">Hh gerenciamento estimado total:</td>
							<td align="right" class="docSBorda"><input type="text" name="TotalGer"  value="<% = FormatNumber(GerTot) %>" class="docCmpLivre"  readonly="read-only" style="text-align:right;font-weight:bold" tabindex="-1"></td>
							<td>&nbsp;</td>
							<td align="right" class="docLabel">Hh gerenciamento comprometido:</td>
							<td align="right" class="docCmpLivre"><input type="text" name="GerRealFmt" value="<% = FormatNumber(GerReal) %>" class="docCmpLivre" style="text-align:right" onBlur="javascript:Formatar('GerReal');" tabindex="2"></td>
						</tr>
						<tr>
							<td colspan="5" style="height:10px;font-size:1px">&nbsp;</td>
						</tr>
						<tr>
							<td align="right" class="docLabel">Hh sup. campo estimado:</td>
							<td align="right"  class="docSBorda"><% = FormatNumber(SupcEst) %></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td align="right" class="docLabel">ECN sup. campo:</td>
							<td align="right" class="docCmpLivre"><input type="text" name="SupcEcnFmt" value="<% = FormatNumber(SupcEcn) %>" class="docCmpLivre" style="text-align:right" onBlur="javascript:Formatar('SupcEcn');" tabindex="0"></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
							<tr>
								<td colspan="2" style="height:2px;border-top-width:1px;border-top-style:solid;font-size:1px">&nbsp;</td>
								<td style="font-size:1px">&nbsp;</td>
								<td style="font-size:1px">&nbsp;</td>
								<td style="font-size:1px">&nbsp;</td>
							</tr>
						<tr>
							<td align="right" class="docLabel">Hh sup. campo estimado total:</td>
							<td align="right" class="docSBorda"><input type="text" name="TotalSupc"  value="<% = FormatNumber(SupcTot) %>" class="docCmpLivre"  readonly="read-only" style="text-align:right;font-weight:bold" tabindex="-1"></td>
							<td>&nbsp;</td>
							<td align="right" class="docLabel">Hh sup. campo comprometido:</td>
							<td align="right" class="docCmpLivre"><input type="text" name="SupcRealFmt" value="<% = FormatNumber(SupcReal) %>" class="docCmpLivre" style="text-align:right" onBlur="javascript:Formatar('SupcReal');" tabindex="2"></td>
						</tr>
						<tr>
							<td colspan="5" style="height:10px;font-size:1px">&nbsp;</td>
						</tr>
						<tr>
							<td colspan="3">&nbsp;</td>
							<td align="right" class="docLabel">ETC:</td>
							<td align="right" class="docCmpLivre"><input type="text" name="EtcFmt" value="<% = FormatNumber(Etc) %>" class="docCmpLivre" style="text-align:right" onBlur="javascript:Formatar('Etc');" tabindex="0"></td>
						</tr>
							<tr>
								<td colspan="2" style="height:2px;border-top-width:2px;border-top-style:solid;font-size:1px">&nbsp;</td>
								<td style="font-size:1px">&nbsp;</td>
								<td colspan="2" style="height:2px;border-top-width:2px;border-top-style:solid;font-size:1px">&nbsp;</td>
							</tr>
						<tr>
							<td align="right" class="docLabel">Total hh estimado:</td>
							<td align="right" class="docLabel"><input type="text" name="TotalEst"  value="<% = FormatNumber(TotalEst) %>" class="docCmpLivre"  readonly="read-only" style="text-align:right;font-weight:bold" tabindex="-1"></td>
							<td>&nbsp;</td>
							<td align="right" class="docLabel">Total hh previsto:</td>
							<td align="right" class="docLabel"><input type="text" name="TotalReal"  value="<% = FormatNumber(TotalReal) %>" class="docCmpLivre"  readonly="read-only" style="text-align:right;font-weight:bold" tabindex="-1"></td>
						</tr>
						<tr>
							<td colspan="4" align="right" class="docLabel" style="align:right">Varia��o(%):</td>
							<td align="right" class="docLabel"><input type="text" name="Variacao"  value="<% = Variacao %>" class="docCmpLivre"  readonly="read-only" style="text-align:right;font-weight:bold<%=EstiloVari%>" tabindex="-1"></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="docLabel">Coment�rios sobre as varia��es:</td>
			</tr>
			<tr>
				<td colspan="2" class="docCmpLivre"><textarea name="txtComeCust" class="docCmpLivre" rows="5"  wrap="soft" tabindex="4"><%=ComeCust%></textarea> </td>
			</tr>
			<tr>
				<td colspan="2" height="15"></td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<table>
						<tr>
							<td height="28" width="80" align="center"><img border="0" src="../images/btnSalvar.gif" class="botao" onclick="javascript:Validar();"></td>
							<td width="10"></td>
							<td width="80" align="center"><img border="0" src="../images/btnCancelar.gif" class="botao" onclick="javascript:window.close();"></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
	</body>
</HTML>
