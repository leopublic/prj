<% Response.Expires= -1%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ::</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<link rel="stylesheet" type="text/css" href="../EstiloMenu.css">
		<!--#include file="../includes/opcControle.inc"-->
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
		<script language="JavaScript" type="text/javascript" src="../scripts/AbrirJanela.js"></script>
	</HEAD>
	<body class="cabecalho" >
<%  lPerfisAutorizados = "157"
	VerificarAcesso()
	projId = Session("projId")

   Dim gmenuGeral
   Dim gMenuSelecionado
   Dim gOpcaoSelecionada
   projId = Request.QueryString("projId")
	cntrNomeServArray = Session("cntrNomeServArray")
	cntrIdArray = Session("cntrIdArray")
	emprNomeArray = Session("emprNomeArray")
	emprIdArray = Session("emprIdArray")
	TotalServ = Session("QtdServicos")
	servInd = 0
	
   gmenuGeral = "Projetos"
   gMenuSelecionado = "prjRequListar"

 %>
		<!--#include file="../includes/cab.inc"-->
			<tr>
				<td style="padding:0px" valign="top" class="blocoOpcoes">
					<!--#include file="../includes/opcProjx2.inc"-->
				</td>
				<td colspan="2" valign="top" style="padding:5px" class="blocoPagina">
				<form>
					<table width="100%">
					<tr>
						<td class="docTit"><%=Session("projNome")%><br><img src="../images/0066_double_arrow.png" class="botao" style="margin-right:3px">Requisições de Compra</td>
						<td align="right"><a href="javascript:AbrirJanela('prjRequAlterar0.asp?pedcId=0',500,300);"><img src="../images/newitem.gif" class="botao">&nbsp;Carregar novo</a></td>
					</tr>
					<tr>
						<td colspan="2" background="../images/pont_cinza_h.gif" style="height:2"></td>
					</tr>
					<tr>
						<td colspan="2" style="height:5"></td>
					</tr>
					<tr>
						<td colspan="2">
							<table width="100%" class="grid" >
								<tr>
									<td width="30px"  class="GridCab">&nbsp;</td>
									<td width="80px" class="GridCab"><a href="prjRequListar.asp" class="GridCab">Requisição<%=SortNome%></a></td>
									<td width="180px" class="GridCab"><a href="prjRequListar.asp" class="GridCab">Lista<%=SortNome%></a></td>
									<td width="100px" class="GridCab" style="text-align:right"><a href="prjRequListar.asp" class="GridCab">Pedido Ok<%=SortNome%></a></td>
									<td class="gridSLinha">&nbsp;</td>
								</tr>
	<%
	Dim gConexao 
	AbrirConexao

	xSQL =        "select RV.reviId, RQ.requId, tpliPrefixo , projNum , tpliNome , tpliCodigo, reviNumero, listSequencial, listNumProjeto, requNum, LI.listId"
	xSQL = xSQL & "   , count(IR.iteqId) qtdItens, (select count(*) from itemPedido IP, PedidoCompra PC where PC.pedcId = IP.pedcId and PC.requId = RQ.requId) qtdPedida"	
	xSQL = xSQL & "  from lista LI , tipolista TL, projeto PR, revisao RV, requisicao RQ, itemRequisicao IR "
	xSQL = xSQL & " where LI.tpliId = TL.tpliId"
	xSQL = xSQL & "   and PR.projId = LI.projId"
	xSQL = xSQL & "   and RV.listId = LI.listId"
	xSQL = xSQL & "   and RV.reviId = RQ.reviId"
	xSQL = xSQL & "   and LI.projId = " & projId
	xSQL = xSQL & "   and IR.requId = RQ.requId"
	xSQL = xSQL & "   group by RV.reviId, RQ.requId, tpliPrefixo , projNum , tpliNome , tpliCodigo, reviNumero, listSequencial, listNumProjeto, requNum, LI.listId"
	xSQL = xSQL & " order by requNum"
	Set xRs = gConexao.Execute(xSQL)
	if xRs.EOF then
	    Response.write "<tr>" & vbcrlf
	    Response.write "<td colspan=""5"">Nenhuma requisição cadastrada</td>" & vbcrlf
	    Response.write "</tr>" & vbcrlf
		xPovoou = false
	else
		while not xRs.EOF
			xPovoou = true
			xLista = xrs("tpliPrefixo") & "-" & xrs("listNumProjeto") & "-" & xrs("tpliCodigo") & "-" & right("000" & xrs("listSequencial"), 3) & " (rev. " & xRs("reviNumero") & ")"  
			xlista = "<a href=""http://www.prj.com.br/cliente/prjListResumo.asp?listId=" & xrs("listId") & """>" & xLista & "</a>"
			Response.write "<tr >" & vbcrlf
			response.write vbtab & "<td ><a href=""prjRequConsultar.asp?requId=" & xRs("requId") & """><img src=""../images/folder.gif"" style=""border:0""></a></td>"
			response.write vbtab & "<td >" & xRs("requNum") & "</td>"
			response.write vbtab & "<td >" & xLista & "</td>"
			response.write vbtab & "<td style=""text-align:right;"">" & xRs("qtdPedida") & "/" & xRs("qtdItens") & "</td>"
			response.write vbtab & "<td class=""gridSLinha"">&nbsp;</td>"
			Response.write "</tr>" & vbcrlf
			xRs.MoveNext
			if estilo = "GridLinhaPar" then
				estilo = "GridLinhaImpar"
			else
				estilo = "GridLinhaPar"
			end if
		wend
	end if
	%>
								<tr>
									<td class="GridRodape">&nbsp;</td>
									<td class="GridRodape">&nbsp;</td>
									<td class="GridRodape">&nbsp;</td>
									<td class="GridRodape">&nbsp;</td>
									<td class="gridSLinha">&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
					</table>
				</form>
				</td>
			</tr>
		<!--#include file="../includes/rod.inc"-->
	</body>
</HTML>
