String.prototype.ltrim = function(){
	return this.replace(/^\s+/,"");
}

String.prototype.rtrim = function(){
	return this.replace(/\s+$/,"");
}

String.prototype.trim = function(){
	return this.replace(/^\s+|\s+$/g,"");
}
// JavaScript Document
function isEmail(str)
{
	return ( /^['_a-z0-9-]+(\.['_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*\.(([a-z]{2,4}))$/.exec( str.toLowerCase() ) ? true : false );
}

function validaCPF(val) {
	cpf = val;	
	erro = "";
	if (cpf.length < 11) 
		erro += "Sao necessarios 11 digitos para verificacao do CPF! \n";
	
	var nonNumbers = /\D/;
	
	if (nonNumbers.test(cpf)) 
		erro += "A verificacao de CPF suporta apenas numeros! \n";
	if (cpf == "00000000000" || cpf == "11111111111" || cpf == "22222222222" || cpf == "33333333333" || cpf == "44444444444" || cpf == "55555555555" || cpf == "66666666666" || cpf == "77777777777" || cpf == "88888888888" || cpf == "99999999999")
	{
		erro += "Numero de CPF invalido! \n"
	}
	
	var a = [];
	var b = new Number;
	var c = 11;
	for (i=0; i<11; i++)
	{
		a[i] = cpf.charAt(i);
		if (i < 9)
			b += (a[i] * --c);
	}
	if ((x = b % 11) < 2) 
	{
			a[9] = 0
	}
	else 
	{
		a[9] = 11-x 
	}
	
	b = 0;
	c = 11;
	
	for (y=0; y<10; y++)
	{
		b += (a[y] * c--);
	}
	
	if ((x = b % 11) < 2)
		a[10] = 0; 
	else 
		a[10] = 11-x;
		
	if ((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10]))
	{
		erro += "Digito verificador com problema! \n";
	}
	
	if (erro.length > 0)
	{
		return false;
	}
	else
	{
		return true;
	}
}

function setStyle(sty,id) 
{
	document.getElementById(id).className = sty;
}

function isDateJS2(dateStr, inibir_msg_retorno){
	if(dateStr.trim==""){
		if(inibir_msg_retorno==0){alert("Data n�o informada!");}
		return false;
	}
	
	var datePat = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
	var matchArray = dateStr.match(datePat); // is the format ok?
	var MonthLong = [];
	MonthLong[1]="Janeiro";
	MonthLong[2]="Fevereiro";
	MonthLong[3]="Mar�o";
	MonthLong[4]="Abril";
	MonthLong[5]="Maio";
	MonthLong[6]="Junho";
	MonthLong[7]="Julho";
	MonthLong[8]="Agosto";
	MonthLong[9]="Setembro";
	MonthLong[10]="Outubro";
	MonthLong[11]="Novembro";
	MonthLong[12]="Dezembro";
	
	if (matchArray == null) {
		if(inibir_msg_retorno==0){alert("Por favor informa a data no formato dd/mm/aaaa.");}
		return false;
	}
	
	day = matchArray[1]; // p@rse date into variables
	month = matchArray[3];
	year = matchArray[5];
	
	if (day < 1 || day > 31) {
		if(inibir_msg_retorno==0){alert("Dia tem de estar entre 1 e 31.");}
		return false;
	}
	
	if (month < 1 || month > 12) { // check month range
		if(inibir_msg_retorno==0){alert("M�s tem de estar entre 1 e 12.");}
		return false;
	}
	
	if ((month==4 || month==6 || month==9 || month==11) && day==31) {
		if(inibir_msg_retorno==0){alert("M�s de "+MonthLong[Number(month)]+" n�o possui 31 dias!")}
		return false;
	}
	
	if (month == 2) { // check for february 29th
		var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0)); //check bisix year
		if (day > 29 || (day==29 && !isleap)) {
			if(inibir_msg_retorno==0){alert("Fevereiro " + year + " n�o possui " + day + " dias!");}
			return false;
		}
	}
	
	return true; // date is valid
}

function isDateJS(dateStr) {
	return isDateJS2(dateStr,0);
}

function isTimeJS(pTime){
	if(pTime.trim==""){
		alert("Hora n�o informada!");
		return false
	}else{
		if(pTime.length!=5 && pTime.length!=8){
			alert("Hora inv�lida!");
			return false
		}else{
			var hora    = pTime.substr(0,2);
			var minuto  = pTime.substr(3,2);
			var segundo = "";
			var horageral = pTime.replace(":","");

			if(isNumberJS(horageral)==false){
				alert("Hora inv�lida!");
				return false;
			}
			if(Number(hora)< 1 || Number(hora)> 23){
				alert("Verifique hora. Hora inv�lida!");
				return false;
			}
			if(Number(minuto)< 0 || Number(minuto)>59){
				alert("Verifique hora. Minuto inv�lido!");
				return false;
			}
		
			if(pTime.length==8){
				segundo = pTime.substr(5,2);
				if(Number(segundo)<0 || Number(segundo)>59){
					alert("Verifique hora. Segundo inv�lido!");
					return false;
				}
			}
		}
	}
	return true
}

function isNumberJS(pNumber,pObrigatorio){
	var IsNumber=true;
	if((pObrigatorio==1) && (pNumber=='')){
		IsNumber=false;
	}else{
		var ValidChars = "0123456789.,";
		var Char;
		var Cont_V =0;
		for (i = 0; i < pNumber.length && IsNumber == true; i++) {
			Char = pNumber.charAt(i); 
			Cont_V = Cont_V+(Char==','?1:0);
			if((Char==',') && (Cont_V>1)){
				IsNumber = false;
				break;
			}
			if (ValidChars.indexOf(Char) == -1){
				IsNumber = false;
			}
		}
	}
	return IsNumber;
}

function rightJS(pTexto, pTamanho){
	if(pTexto.trim==""){
		return "";
	}else{
		if(Number(pTamanho) > pTexto.length){
			return pTexto;
		}else{
			return pTexto.substr((pTexto.length-pTamanho),pTamanho);
		}
	}
}

function leftJS(pTexto, pTamanho){
	if(pTexto.trim==""){
		return "";
	}else{
		if(Number(pTamanho) > pTexto.length){
			return pTexto;
		}else{
			return pTexto.substr(0,pTamanho);
		}
	}
}

function InStr(n, s1, s2){
	// Devuelve la posici�n de la primera ocurrencia de s2 en s1
	// Si se especifica n, se empezar� a comprobar desde esa posici�n
	// Sino se especifica, los dos par�metros ser�n las cadenas
	var numargs=InStr.arguments.length;	
	if(numargs<3)
		return n.indexOf(s1)+1;
	else
		return s1.indexOf(s2, n)+1;
}

function setavaluecombo(pcombo, pvalue){
   var x = document.getElementById(pcombo);
   for(i=0;i<x.length;i++){
   	x.options[i].selected = "";
   	if(x.options[i].value==pvalue){
   	   x.options[i].selected = "selected";
   	}
   }
}

function BackGroundRequest(sURL, pcab_erro){
   // Create an instance of the XML HTTP Request object
   var oXMLHTTP = null;
   
   if(window.XMLHttpRequest){ // code for Mozilla, etc.
     oXMLHTTP=new XMLHttpRequest();
   }else{
     if(window.ActiveXObject){ // code for IE
        oXMLHTTP=new ActiveXObject("Microsoft.XMLHTTP");
     }
   }
   
   if(oXMLHTTP!=null) {
      // Prepare the XMLHTTP object for a HTTP POST to our validation ASP page
      //oXMLHTTP.onreadystatechange=state_Change
      oXMLHTTP.open("GET",sURL,true) 
      
      // Execute the request
      oXMLHTTP.send(null)

      var resultado = oXMLHTTP.responseText.split("|FIMLINHA|");
      var lerro = "";

      if(resultado.length > 1){
         lerro=resultado[1];
      }

      if(lerro==""){
         return resultado[0];
      }else{
      	 alert(pcab_erro+'\n'+lerro);
         return 'ERRO';
      }
   }else{
   	alert('Seu browser n�o suporta este padr�o de formul�rio!');
   	return 'ERRO'
   }
}

//--
//-- FUN��ES INSERIDAS EM 28/06/2007 VISANDO APOIO A WBS - INI
//--

function _ocdiv(pdiv){
	expandCollapse(pdiv,'over');
}

function _criaxmlhttprequest(){
	var xmlhttp = null;
	if(window.XMLHttpRequest){ // code for Mozilla, etc.
		xmlhttp=new XMLHttpRequest();
	}else{
		if(window.ActiveXObject){ // code for IE
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	
	return xmlhttp;
}

/*
	_gerarxsltretorno: gera um xslt de erro padr�o
		- _nm_tag_mae: nome da tag de loop
			Ex1: XMLRESP/ERRO
			Ex2: LISTA/LINHA
		- _colsort: nome da tag que ser� usado pro sort
		- _cols: nome das tags da colunas separadas por "|" pipe
			Ex: NUM_ERRO|DESC_ERRO|QUERY
		- _cols_names: nomes que as tags especificadas em "cols" ir�o receber. Tmb separados pro "|" pipe
			Ex: Erro|Descri��o|SQL Utilizada
		- _cols_largs: larguras das colunas separadas pro "|" pipe
			Ex: 50|325|325
*/
function _gerarxsltretorno(_nm_tag_mae, _colsort, _cols, _cols_names, _cols_largs){

	arr_cols       = _cols.split("|");
	arr_cols_names = _cols_names.split("|");
	arr_cols_largs = _cols_largs.split("|");

	var xsl_retorno_h = '';
	var xsl_retorno_r = '';

	if(arr_cols.length!=arr_cols_names.length){
		alert('Erro na passagem de par�metros da fun��o "_gerarxsltretorno"!!!');
		return false
	}else{
		ldivisor_h = '		<th style="border-left:dotted 1px #DDD" width="1px"></th>';
		ldivisor_r = '        		<td style="border-left:solid 1px #efe"> </td>';
		for(ind_gxr=0;ind_gxr<arr_cols.length;ind_gxr++){
			xsl_retorno_h += ldivisor_h;
			xsl_retorno_h += '      <th width="'+arr_cols_largs[ind_gxr]+'px">'+arr_cols_names[ind_gxr]+'</th> ';

			xsl_retorno_r += ldivisor_r;
			xsl_retorno_r += '        		<td style="text-align:left;"><xsl:value-of select="'+arr_cols[ind_gxr]+'" /></td> ';
		}

		xsl_retorno_h = xsl_retorno_h.substr(ldivisor_h.length);
		xsl_retorno_r = xsl_retorno_r.substr(ldivisor_r.length);
	}

	if(xsl_retorno_r!='' && xsl_retorno_r!=''){

		xsl_retorno  = '';
		xsl_retorno += '<?xml version="1.0" encoding="ISO-8859-1"?>';
		//xsl_retorno += '<?xml version="1.0" encoding="utf8"?>';
		xsl_retorno += '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">';
		xsl_retorno += '';
		xsl_retorno += '<xsl:template match="/">';
		xsl_retorno += '    <h2>Retorno de erros</h2> ';
		xsl_retorno += '    <table class="tbErro">';
		xsl_retorno += '      <tr>';
		xsl_retorno += xsl_retorno_h;
		xsl_retorno += '      </tr>';
		xsl_retorno += '      <xsl:for-each select="'+_nm_tag_mae+'">';
		if(_colsort!=''){
			xsl_retorno += '	  <xsl:sort select="'+_colsort+'"/>';
		}
		xsl_retorno += '		<xsl:choose>';
		xsl_retorno += '          <xsl:when test="(position() mod 2)=0">';
		xsl_retorno += '            <tr style="background-color:#9c9;">';
		xsl_retorno += xsl_retorno_r;
		xsl_retorno += '      		</tr>';
		xsl_retorno += '          </xsl:when>';
		xsl_retorno += '          <xsl:otherwise>';
		xsl_retorno += '            <tr style="background-color:#dfd;">';
		xsl_retorno += xsl_retorno_r;
		xsl_retorno += '      		</tr>';
		xsl_retorno += '          </xsl:otherwise>';
		xsl_retorno += '		</xsl:choose>';
		xsl_retorno += '      </xsl:for-each>';
		xsl_retorno += '  </table>';
		xsl_retorno += '</xsl:template>';
		xsl_retorno += '';
		xsl_retorno += '</xsl:stylesheet>';

		return xsl_retorno
	}else{
		alert('Erro no processamento de par�metros da fun��o "_gerarxsltretorno"!!!');
		return false
	}

}

function _tratarxmlretorno(xml_retorno){
    document.getElementById('divErros').style.display='none';
	if(xml_retorno!=''){
		xml_retorno='<XMLRESP>'+xml_retorno+'</XMLRESP>';

		xsl_retorno=_gerarxsltretorno('XMLRESP/ERRO', 'NUM_ERRO', 'NUM_ERRO|DESC_ERRO|QUERY', 'Erro|Descri��o|Sql Utilizada', '50|325|325');
		if(xsl_retorno!=false){
			return _carregaxmlxslt(xml_retorno, xsl_retorno, 'divErros');
		}else{
			return xsl_retorno
		}

	}else{
		return true
	}
}

function _carregaxmlxslt(p_xml, p_xslt, p_id_div_container){
	var docxml=null;
	var docxsl=null;
	var retorno = '';
    
	document.getElementById(p_id_div_container).innerHTML = '';

	if (window.ActiveXObject){ // code for IE
	  docxml=new ActiveXObject("Microsoft.XMLDOM");
	  docxml.async="false";
	  docxml.loadXML(p_xml);

	  docxsl=new ActiveXObject("Microsoft.XMLDOM");
	  docxsl.async = "false";
	  docxsl.loadXML(p_xslt);

	  retorno = docxml.transformNode(docxsl);

	}else{// code for Mozilla, Firefox, Opera, etc.
		var parser=new DOMParser();
		docxsl=parser.parseFromString(p_xslt,"text/xml");

		docxml=parser.parseFromString(p_xml,"text/xml");

		var xsltProc  = new XSLTProcessor();
		xsltProc.importStylesheet(docxsl);
		var ser = new XMLSerializer();
		var htmlDoc = xsltProc.transformToDocument(docxml);
		retorno = ser.serializeToString(htmlDoc);
	}

	document.getElementById(p_id_div_container).innerHTML = retorno;
	document.getElementById(p_id_div_container).style.display='block';
}

function _carregaitens(_objName, _divDestino, _FLTipo, _RootDesc, _wbscId, _cntrId, _servInd, _tipo, _fcn_Id){
	document.body.style.cursor='wait';
	var pagina = 'wbs_carregatree.asp?divcontainer='+_divDestino+'&objName='+_objName+'&tipoitemwbs='+_FLTipo+'&rootdesc='+_RootDesc+'&wbscId='+_wbscId+'&cntrId='+_cntrId+'&servInd='+_servInd+'&tipo='+_tipo+'&fcn_Id='+_fcn_Id;
	//
	var xmlhttp = _criaxmlhttprequest();
	xmlhttp.open("GET",pagina,true);
	xmlhttp.onreadystatechange = function() {
		if(xmlhttp.readyState == 4) {
			if(xmlhttp.status == 200){
                document.getElementById(_divDestino).innerHTML='';
				eval(xmlhttp.responseText);
				eval('document.createElement('+_objName+');'); //gera um bug... mas � s� pra for�ar a recarga do objeto
			}
		}
	}
	xmlhttp.send(null);
	document.body.style.cursor='auto';
}

function _carregaitensNovo(_objName, _divDestino, _FLTipo, _RootDesc, _wbscId, _cntrId, _servInd, _tipo, _fcn_Id){
	document.body.style.cursor='wait';
	var pagina = 'wbs_carregatreeNovo.asp?divcontainer='+_divDestino+'&objName='+_objName+'&tipoitemwbs='+_FLTipo+'&rootdesc='+_RootDesc+'&wbscId='+_wbscId+'&cntrId='+_cntrId+'&servInd='+_servInd+'&tipo='+_tipo+'&fcn_Id='+_fcn_Id;
	//
	var xmlhttp = _criaxmlhttprequest();
	xmlhttp.open("GET",pagina,true);
	xmlhttp.onreadystatechange = function() {
		if(xmlhttp.readyState == 4) {
			if(xmlhttp.status == 200){
                document.getElementById(_divDestino).innerHTML='';
				eval(xmlhttp.responseText);
				eval('document.createElement('+_objName+');'); //gera um bug... mas � s� pra for�ar a recarga do objeto
			}
		}
	}
	xmlhttp.send(null);
	document.body.style.cursor='auto';
}
function _carregaitensNovissimo(_objName, _divDestino, _FLTipo, _RootDesc, _wbscId, _cntrId, _servInd, _tipo, _fcn_Id){
	document.body.style.cursor='wait';
	var pagina = 'wbs_carregatreeNovissimo.asp?divcontainer='+_divDestino+'&objName='+_objName+'&tipoitemwbs='+_FLTipo+'&rootdesc='+_RootDesc+'&wbscId='+_wbscId+'&cntrId='+_cntrId+'&servInd='+_servInd+'&tipo='+_tipo+'&fcn_Id='+_fcn_Id;
	//
	var xmlhttp = _criaxmlhttprequest();
	xmlhttp.open("GET",pagina,true);
	xmlhttp.onreadystatechange = function() {
		if(xmlhttp.readyState == 4) {
			if(xmlhttp.status == 200){
                document.getElementById(_divDestino).innerHTML='';
				eval(xmlhttp.responseText);
				eval('document.createElement('+_objName+');'); //gera um bug... mas � s� pra for�ar a recarga do objeto
			}
		}
	}
	xmlhttp.send(null);
	document.body.style.cursor='auto';
}

function toDecimalBR(s) {
	var v = s.replace(' ','');
	while (v.indexOf('.') > -1){v = v.replace('.','');}
	v = v.replace(',','.');
	return Number(v)
};

function mesclar_tabela(nome_tabela) {
    var w_class = 'impar';
    var w_obj   = document.getElementById(nome_tabela);
    var w_ite_u = 0;
    if (w_obj) {
        for(w__i=1;w__i<=(w_obj.getElementsByTagName('TR').length-1);w__i++){
            if((w_obj.getElementsByTagName('TR')[w__i].className=='') || (w_obj.getElementsByTagName('TR')[w__i].className=='undefined')){
                w_class = (w_class=='par'?'impar':'par');
                w_obj.getElementsByTagName('TR')[w__i].className=w_class;
                w_ite_u++; 
            }
            w_obj.getElementsByTagName('TR')[w__i].getElementsByTagName('TD')[0].style.borderLeft='1px dashed #999';
            w_obj.getElementsByTagName('TR')[w__i].getElementsByTagName('TD')[0].style.borderRight='1px dashed #999';
            w_obj.getElementsByTagName('TR')[w__i].getElementsByTagName('TD')[0].innerHTML=w__i;
        }
    }
    return w_ite_u;
}

function _zebra_linhas(_tabela, _class_impar, _class_par, _linhas_cab){
	if(document.getElementById(_tabela)){
		_tabela=document.getElementById(_tabela);
		_tr_cl=_class_par;
		for(_i_z_l=_linhas_cab;_i_z_l<_tabela.rows.length;_i_z_l++){
			_tr_cl=(_tr_cl==_class_par?_class_impar:_class_par);
			_tabela.rows[_i_z_l].className=_tr_cl;
		}
	}
}
//--
//-- FIM
//--

function _erro_xml_comum(_linha, _msg){
	return '<ERRO><LINHA>'+_linha+'</LINHA><DESCRICAO>'+_msg+'</DESCRICAO></ERRO> ';
}

function _valor_atual_combo(_combo){
	if(document.getElementById(_combo)){
		return document.getElementById(_combo).options[document.getElementById(_combo).selectedIndex].value;
	}
}