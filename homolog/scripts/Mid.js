function Mid(str, start, len){
	if (start < 0 || len < 0) return "";

	var iEnd, iLen = String(str).length;
	if (start + len > iLen)
			iEnd = iLen;
	else
			iEnd = start + len;

	return String(str).substring(start,iEnd);
}
