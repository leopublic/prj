 function AlertError(MethodName,e)
 {
            if (e.description == null) { alert(MethodName + " Exception: " + e.message); }
            else {  alert(MethodName + " Exception: " + e.description); }
 }
 
 function FormatClean(num)
{    var sVal='';
     var nVal = num.length;
     var sChar='';
     
   try
   {
       for(i=0;i<nVal;i++)
      {
         sChar = num.charAt(i);
         nChar = sChar.charCodeAt(0);
         if ((nChar >=48) && (nChar <=57))  { sVal += num.charAt(i);   }
      }
   }
    catch (exception) { AlertError("Format Clean",e); }
    return sVal;
}

function FormatarNumero(num, Separador, PontoEntrada, PontoSaida)
{       
        var sVal='';
        var par1='';
		var par2='';
        if (num.lastIndexOf("-") == 0) { par1='('; par2=')'; }
		var posPonto = num.lastIndexOf(PontoEntrada)
		var posIdeal = num.length - 3
		//alert('num='+num+' posponto:'+posPonto+' num.lenght-3:'+posIdeal)
		if (posPonto < 0) { num = num + '00'; }
		else
			if (posPonto > posIdeal) { num = num + '0';}
			else
				if (posPonto < posIdeal) { num = num.substring(0,num.lastIndexOf(PontoEntrada) + 2);}
        num = FormatClean(num);
		//alert('num='+num)
		//alert('FormatDollar='+FormatDollar(num,Separador))
		//alert('FormatCents='+FormatCents(num))
        sVal = par1 + FormatDollar(num,Separador) + PontoSaida + FormatCents(num) + par2; 
        return sVal;
}

function FormatCents(amount)
{
     var cents = '';
      try
      {
	  	
           amount = parseInt(amount,10);
           var samount = new String(amount);
           if (samount.length == 0) { return '00'; }
           if (samount.length == 1) { return '0' + samount; }
           if (samount.length == 2) { return samount; }
           cents =  samount.substring(samount.length -2,samount.length);
		   
      }
      catch (exception) { AlertError("Format Cents",e); }
      return cents;
}

function FormatDollar(amount,CommaDelimiter)
{
   try 
   {
        amount = parseInt(amount,10);
        var samount = new String(amount);
        if (samount.length < 3) { return 0; }  
        samount =  samount.substring(0,samount.length -2);
		if (CommaDelimiter != '')
		{
			for (var i = 0; i < Math.floor((samount.length-(1+i))/3); i++)
			{	samount = samount.substring(0,samount.length-(4*i+3)) + CommaDelimiter + samount.substring(samount.length-(4*i+3));}
		}
   }
    catch (exception) { AlertError("Format Comma",e); }
    return samount;
}



