<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Controle remoto de projetos</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="Styles.css">
		<link rel="stylesheet" type="text/css" href="EstiloMenu.css">
	</HEAD>
	<body class="cabecalho">
<%	Session("usuaId") = 0
	Session("usuaNome") = ""
	Session("perfNome") = ""
	Session("perfId") = ""
	Session("emprId") = ""
	if Request.QueryString("PrimeiraVez") = "1" then
		Session("Msg") = ""
	end if
%>
		<table width="100%" align="left" height="100%" cellpadding="0" cellspacing="0" style="border-style:solid;border-width:0;border-color:black">
			<tr>
				<td height="50" style="background-color:#396D60;background-image:url(images/blend3.jpg);background-repeat:repeat-y;background-position:right;">
					<table width="100%">
						<tr>
							<td width="5"><img src="images/logo.gif">
							<td style="font-size:20pt;font-weight:bold;color:#DDE4DC">Portal de colabora��o</td>
							<td width="300" style="text-align:right"></td>
						</tr>
					</table>				
				</td>
			</tr>
			<tr>
				<td>
					<table width="100%" height="100%">
						<tr>
							<td width="180" class="SeparadorPreto"></td>
							<td width="500" class="SeparadorPreto"></td>
							<td class="SeparadorPreto"></td>
						</tr>
						<tr>
							<td height="10"  class="TituloPasta"><h1>Home</h1></td>
							<td  colspan="2" align="right" class="TituloUsuario">&nbsp;</td>
						</tr>
						<tr>
							<td  style="padding:0px" valign="top" class="blocoOpcoes">
								<table class="menu">
									<tr>
										<td width="13" style="height:5px;font-size:1px">&nbsp;</td>
										<td width="160" style="height:5px;font-size:1px">&nbsp;</td>
									<tr/>
									<tr>
										<td class="opcSep">&nbsp;</td>
										<td class="opcSep">&nbsp;</td>
									<tr/>
									<tr>
										<td class="opcSel">&nbsp;</td>
										<td class="opcMenu"><a href="logininput.asp" class="opcMenu">Controle de acesso</a></td>
									<tr/>
									<tr>
										<td colspan="2">
											<table width="100%">
												<tr>
													<td width="15" >&nbsp;</td>
													<td width="10"><img src="images/nav_pt2.gif" class="opc"></td>
													<td><a href="logininput.asp" class="opcSubMenu">Identifica��o do usu�rio</a></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
							<td  colspan="2" valign="top" style="padding:5px" class="blocoPagina">
								<table>
									<tr>
										<td><h2>Bem vindo ao portal de colabora��o da Ger�ncia de Constru��es GSS</h2>
										</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td style="font-weight:normal">Esse site � de acesso restrito � usu�rios autorizados. Para prosseguir, por favor identifique-se.
										</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>
											<form action="loginValidar.asp" method="post">
												<table bgcolor="#1D4F68" width="200" align="center" cellpadding="3" bordercolor="#1D4F68" style="border:12;border-style:solid;">
													<tr>
														<td width="75" style="color:#DDE8EE;padding:3;">Usu�rio</td>
														<td width="3" style="align:center;color:#DDE8EE">:</td>
														<td><input type="text" name="txtUsuario"  style="width:100%;border:0"></td>
													</tr>
													<tr>
														<td style="color:#DDE8EE;padding:3;">Senha</td>
														<td style="align:center;color:#DDE8EE">:</td>
														<td><input type="password" name="txtSenha" style="width:100%;border:0"></td>
													</tr>
												</table>
												<p align="center" style="color:red;"><% = Session("Msg") %></p>
												<p align="center"><input type="submit" value="Entrar" size="90" style="width:80;font-size:8pt;font-weight:bold"></p>
											</form>
												</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>			
				</td>
			</tr>
		</table>
	</body>
</HTML>
