<% 
	Server.ScriptTimeout=60000 
    pStr = "private, no-cache, must-revalidate" 
    Response.ExpiresAbsolute = #2000-01-01# 
    Response.AddHeader "pragma", "no-cache" 
    Response.AddHeader "cache-control", pStr 

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>::PRJ:: Controle remoto de projetos</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
		<!--#include file="../includes/clsBanco.inc"-->
		<!--#include file="../includes/clsControleAcesso.inc"-->
	</HEAD>
	<body class="cabecalho" >
		<!--#include file="../includes/varControleAcesso.inc"-->
		<!--#include file="../includes/varBanco.inc"-->
		<% 
		Dim objUpload
		Dim strFileName
		Dim objConn
		Dim objRs
		Dim lngFileID
		AbrirConexao
		' Instantiate Upload Class
		Set objUpload = Server.CreateObject("SoftArtisans.FileUp")
		objUpload.ProgressID = CInt(Request.QueryString("progressid"))
		servidor = Request.ServerVariables("HTTP_HOST")
		if servidor = "www.prj.com.br" then
			xPath = "e:\home\prj\web\listas"
		elseif servidor = "www.m2software.com.br" then
			xPath = "e:\home\m2software\web\whitemartins\prj\listas"
		end if
		objUpload.Path = xpath
			
		' Grab the file name
		projId = objUpload.form("projId")
		usuaId = objUpload.form("usuaId")
		reviNumero = "0"
		listSequencial = "0"
		tpliId = objUpload.form("cmbTipoLista")
		strFileName = objUpload.form("txtArquivo").ShortFileName
		if strFileName = "" then
			Session("MsgErro") = "Arquivo n�o informado."
			Session("txtDesc") = anexDesc
			Response.Redirect ("prjListAlterar0.asp?&projId=" & projId)
		end if
		'
		' Cria a importacao pendente.
		xSQL = ""
		xSQL = xSQL & "exec PCAD_Importacao "
        xSQL = xSQL & "     @pimpoTipo       = 1"
        xSQL = xSQL & "   , @ptpliId         = " & tpliId
        xSQL = xSQL & "   , @pimpoNomeArquivo = '" & strFileName & "'"
        xSQL = xSQL & "   , @pusuaId = " & session("usuaId")
		xSQL = xSQL & "   , @pprojId = " & session("projId")
		response.write xSQL
		'Set xRs = gConexao.execute(xSQL)
		'ximpoId = xRs("impoId")
		response.end
		xNomeArquivo = ximpoId & ".txt"
		objUpload.form("txtArquivo").SaveAs xNomeArquivo

		Session("TituloMsg") = "Importar nova lista"
		Session("Msg") = "Lista carregada com sucesso! em " & xpath
		Response.redirect("prjListImportar.asp?impoId=" & ximpoId)
		%>
	</body>
</HTML>