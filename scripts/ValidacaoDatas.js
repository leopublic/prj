/**
 * DHTML date validation script for dd/mm/yyyy. Courtesy of SmartWebby.com (http://www.smartwebby.com/dhtml/)
 */
// Declaring valid date character, minimum year and maximum year
var dtCh= "/";
var minYear=1900;
var maxYear=2100;

function isInteger(s){
	var i;
    for (i = 0; i < s.length; i++){   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag){
	var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++){   
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function daysInFebruary (year){
	// February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}
function DaysArray(n) {
	for (var i = 1; i <= n; i++) {
		this[i] = 31
		if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
		if (i==2) {this[i] = 29}
   } 
   return this
}

function isDate(dt){
	var dtStr = dt.value
	var daysInMonth = DaysArray(12)
	var pos1=dtStr.indexOf(dtCh)
	var pos2=dtStr.indexOf(dtCh,pos1+1)
	var strDay=dtStr.substring(0,pos1)
	var strMonth=dtStr.substring(pos1+1,pos2)
	var strYear=dtStr.substring(pos2+1)
	strYr=strYear
	if(dtStr.length>8 || strDay.length>2 || strMonth.length>2 || strYear.length>2){
		alert("Formato de data inv�lido para a data '"+dtStr+"'. A data deve ser informada no formato : dd/mm/aa.")
		return false
	}	
	if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
	if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
//	for (var i = 1; i <= 3; i++) {
//		if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
//	}
	month=parseInt(strMonth,10)
	day=parseInt(strDay,10)
	year=parseInt(strYr,10)
	if (pos1==-1 || pos2==-1){
		alert("Formato de data inv�lido para a data '"+dtStr+"'. A data deve ser informada no formato : dd/mm/aa.")
		return false
	}
	if (strMonth.length<1 || month<1 || month>12){
		alert("M�s inv�lido para a data '"+dtStr+"'.")
		return false
	}
	var CentYear = 0
	if (year > 50) CentYear = 1900 + year
	else CentYear = 2000 + year
	if (strYear.length != 2 ){
		alert("Por favor informe um ano v�lido.")
		return false
	}
	if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(CentYear)) || day > daysInMonth[month]){
		alert("Dia inv�lido para a data '"+dtStr+"'.")
		return false
	}
	if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
		alert("Valor de data inv�lido para a data '"+dtStr+"'. Por favor, informe uma data v�lida.")
		return false
	}
return true
}

function FormatarData(dt)
{
	var dtStr = dt.value
	var pos1=dtStr.indexOf(dtCh)
	var pos2=dtStr.indexOf(dtCh,pos1+1)
	var strDay=dtStr.substring(0,pos1)
	var strMonth=dtStr.substring(pos1+1,pos2)
	var strYear=dtStr.substring(pos2+1)
	strYr=strYear
	if (pos1>0 && pos2>0 && dtStr!=''){
		if (strDay.length<2) strDay="0"+strDay
		if (strMonth.length<2) strMonth="0"+strMonth
		if (strYear.length<2) strYear="0"+strYear
		dtStr = strDay+dtCh+strMonth+dtCh+strYear
		return dtStr
	} else return dt.value
}

function ValidateForm(){
	var dt=document.frmSample.txtDate
	if (isDate(dt.value)==false){
		dt.focus()
		return false
	}
    return true
 }